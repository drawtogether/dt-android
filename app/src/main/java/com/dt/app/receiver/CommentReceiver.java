package com.dt.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 评论广播
 * 
 * @author libin
 * 
 */
public class CommentReceiver extends BroadcastReceiver {
	
	public static final String TYPE_ARTGALLERY="TYPE_ARTGALLERY";

	public static final String SEND_RECEIVER_SUCCES="com.dt.app.receiver.CommentReceiver.SEND";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			if (intent.getAction().equals(SEND_RECEIVER_SUCCES)) {
				if(receverIntf!=null){
					receverIntf.success(intent.getLongExtra("workid", 0), intent.getStringExtra("type"));
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private CommentReceverIntf receverIntf;
	
	public interface CommentReceverIntf{
		public void success(long workid,String type);
	}

	public void setCommentReceverIntf(CommentReceverIntf l){
		this.receverIntf=l;
	}
}
