package com.dt.app.receiver;

import java.util.List;

import android.content.Context;
import android.content.IntentFilter;
import android.widget.BaseAdapter;

import com.dt.app.bean.UserWorks;
import com.dt.app.receiver.CommentReceiver.CommentReceverIntf;

/**
 * 刷新适配器评论数字
 * 
 * @author libin
 * 
 */
public class CommonReceiverUtil {
	
	private boolean isRefreshAdapter = false;
	private CommentReceiver mCommentReceiver;

	public void register(Context context, CommentReceverIntf l) {
		mCommentReceiver = new CommentReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(CommentReceiver.SEND_RECEIVER_SUCCES);
		context.registerReceiver(mCommentReceiver, filter);
		mCommentReceiver.setCommentReceverIntf(l);
	}

	public void unregister(Context context) {
		try {
			context.unregisterReceiver(mCommentReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void success(List<UserWorks.UserWork> mBeans, long workid,
			String type) {
		try {
			if (mBeans!=null && mBeans.size()>0) {
				// if (CommentReceiver.TYPE_ARTGALLERY.equals(type)) {
				int count = mBeans.size();
				for (int i = 0; i < count; i++) {
					UserWorks.UserWork work = mBeans.get(i);
					if (work.getId() == workid) {
						isRefreshAdapter = true;
						mBeans.get(i).setCommentedCount(work.getCommentedCount() + 1);
						break;
					}
				}
				// }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 在所在activity调用onResume，避免评论一次更新一次
	 * @param mAdapter
	 */
	public void onResume(BaseAdapter mAdapter) {
		if (isRefreshAdapter) {
			mAdapter.notifyDataSetChanged();
			isRefreshAdapter = false;
		}
	}
}
