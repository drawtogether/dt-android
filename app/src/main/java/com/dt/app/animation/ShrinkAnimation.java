package com.dt.app.animation;


import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout.LayoutParams;

import com.dt.app.widget.ViewPosition;

public class ShrinkAnimation extends ZoomAnimation {
	private int duration;

	public ShrinkAnimation(int duration,View view){
		super(ZoomAnimation.Direction.HIDE, duration, view);
		this.duration=duration;
	}

	@Override
	protected void addShrinkAnimation(View view) {
		LayoutParams params=(LayoutParams) view.getLayoutParams();
		int toXDelta=0;
		int toYDelta=0;
		switch (Integer.valueOf(view.getTag().toString())) {
		case ViewPosition.POSITION_LEFT_BOTTOM:
			toXDelta=-params.leftMargin;
			toYDelta=params.bottomMargin;
			break;
		case ViewPosition.POSITION_LEFT_TOP:
			toXDelta=-params.leftMargin;
			toYDelta=-params.topMargin;
			break;
		case ViewPosition.POSITION_RIGHT_TOP:
			toXDelta=params.rightMargin;
			toYDelta=-params.topMargin;
			break;
		case ViewPosition.POSITION_RIGHT_BOTTOM:
			toXDelta=params.rightMargin;
			toYDelta=params.bottomMargin;
			break;
		}
		
		addAnimation(new TranslateAnimation(0, toXDelta, 0,toYDelta));//X为正：右移；Y为正：下移
		addAnimation(new AlphaAnimation(1F, 0F));
		setInterpolator(new AnticipateOvershootInterpolator(4f));
		setFillAfter(true);
		setDuration(duration);
//		if(view.getVisibility()!=View.GONE)
			view.setVisibility(View.GONE);
	}

	@Override
	protected void addEnlargeAnimation(View view) {
		// TODO Auto-generated method stub

	}

}
