package com.dt.app.animation;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout.LayoutParams;

import com.dt.app.widget.ViewPosition;

/**
 * 放大动画
 * @author LiChaofei 
 * 2013-4-17 下午2:44:14
 */
public class EnlargeAnimation extends ZoomAnimation {
	private int duration;

	public EnlargeAnimation(int duration,View view){
		super(ZoomAnimation.Direction.SHOW, duration, view);
		this.duration=duration;
	}
	
	@Override
	protected void addShrinkAnimation(View view) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void addEnlargeAnimation(View view) {
		LayoutParams params=(LayoutParams) view.getLayoutParams();
//		int leftMargin=params.leftMargin;
//		int bottomMargin=params.bottomMargin;
		int fromXDelta=0;
		int fromYDelta=0;
		switch (Integer.valueOf(view.getTag().toString())) {
		case ViewPosition.POSITION_LEFT_BOTTOM:
			fromXDelta=-params.leftMargin;
			fromYDelta=params.bottomMargin;
			break;
		case ViewPosition.POSITION_LEFT_TOP:
			fromXDelta=-params.leftMargin;
			fromYDelta=-params.topMargin;
			break;
		case ViewPosition.POSITION_RIGHT_TOP:
			fromXDelta=params.rightMargin;
			fromYDelta=-params.topMargin;
			break;
		case ViewPosition.POSITION_RIGHT_BOTTOM:
			fromXDelta=params.rightMargin;
			fromYDelta=params.bottomMargin;
			break;
		}
		
		addAnimation(new TranslateAnimation(fromXDelta,0 , fromYDelta,0));
		addAnimation(new AlphaAnimation(0F, 1F));
//		setInterpolator(new OvershootInterpolator(2f));
		setInterpolator(new BounceInterpolator());
		setFillAfter(true);
		setDuration(duration);
//		if(view.getVisibility()!=View.VISIBLE)
			view.setVisibility(View.VISIBLE);
	}

}
