package com.dt.app.animation;

import android.view.View;
import android.view.animation.AnimationSet;

public abstract class ZoomAnimation extends AnimationSet {

	public Direction direction;

	public enum Direction {
		HIDE, SHOW;
	}

	public ZoomAnimation(Direction direction, long duration, View view) {
		super(true);
		
		this.direction = direction;
		
		switch (this.direction) {
		case HIDE:
			addShrinkAnimation(view);
			break;
		case SHOW:
			addEnlargeAnimation(view);
			break;
		}
		
		setDuration(duration);
	}

	protected abstract void addShrinkAnimation(View view);
	
	protected abstract void addEnlargeAnimation(View view);

}
