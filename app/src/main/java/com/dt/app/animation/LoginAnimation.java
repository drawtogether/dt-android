package com.dt.app.animation;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;

import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import com.dt.app.R;
import com.dt.app.bean.DTCheckUser;
import com.dt.app.common.DTFactoryApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.login.DTGuideLogin;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.Utils;
import com.lidroid.xutils.util.LogUtils;

/**
 * Created by caiyuhao on 15-9-9.
 */
public class LoginAnimation {
    private DTGuideLogin mguideLogin;
    private String input_str;
    private String emailOrmobile;

    public LoginAnimation(Activity activity) {
        mguideLogin = (DTGuideLogin) activity;
    }

    public String defaultName() {
        String str_mobile = PreferencesUtils.getString(mguideLogin, Constant.PrefrencesPt.DTmobilephone);
        String str_email = PreferencesUtils.getString(mguideLogin, Constant.PrefrencesPt.DTemail);
        if (!TextUtils.isEmpty(str_mobile)) {
            return str_mobile;
        } else if (!TextUtils.isEmpty(str_email)) {
            return str_email;
        }
        return null;
    }

    public void initEditorListener() {
        String name = defaultName();
        if (!TextUtils.isEmpty(name)) {
            mguideLogin.et_guid_username.setText(name.trim()+"");
            mguideLogin.et_guid_username.setSelection(name.length());
        }
        //监听输入变化
        mguideLogin.et_guid_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                input_str = editable.toString();
                if (!TextUtils.isEmpty(input_str)) {
                    LogUtils.e("----------afterTextChanged-------->"+Utils.isEmail(input_str));
                    if (Utils.isEmail(input_str)) {
                        mguideLogin.input_result = mguideLogin.Email;
                    } else if (input_str != null && input_str.length() == 11 && Utils.isMobile(input_str)) {
                        //mobile
                        mguideLogin.input_result = mguideLogin.Mobile;
                    } else {
                        mguideLogin.input_result = -1;
                    }
                }
                LogUtils.e("-----input_result----" + mguideLogin.input_result);
            }
        });

        mguideLogin.et_guid_username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    emailOrmobile = mguideLogin.et_guid_username.getText().toString().trim();

                    //Animation animation = AnimationUtils.loadAnimation(mguideLogin, R.anim.login_editext_animation);
                    if (TextUtils.isEmpty(emailOrmobile) || mguideLogin.input_result == -1) {
                        mguideLogin.et_guid_username.startAnimation(mguideLogin.trantAnimation());
                        mguideLogin.trantAnimation().setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                mguideLogin.et_guid_username.setFocusable(true);
                                mguideLogin.et_guid_username.setFocusableInTouchMode(true);
                                mguideLogin.et_guid_username.requestFocus();
                                mguideLogin.et_guid_username.requestFocusFromTouch();
                                LogUtils.e("---------trantAnimation--------->");
                                if (!TextUtils.isEmpty(emailOrmobile)) {
                                    mguideLogin.et_guid_username.setSelection(emailOrmobile.length());
                                } else {
                                    mguideLogin.et_guid_username.setSelection(0);
                                }

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });

                        return true;
                    } else {
                        if ((mguideLogin.input_result.equals(mguideLogin.Email))
                                || (mguideLogin.input_result.equals(mguideLogin.Mobile))) {
                            DTFactoryApi.checkUserData(mguideLogin, input_str, new ResultLinstener() {
                                @Override
                                public void onSuccess(Object obj) {
                                    DTCheckUser  checkUser = (DTCheckUser) obj;
                                    Boolean isOK = checkUser.isData();
                                    if (!isOK) {
                                        mguideLogin.input_result = -1;
                                        mguideLogin.et_guid_username.startAnimation(mguideLogin.trantAnimation());
                                        mguideLogin.trantAnimation().setAnimationListener(new Animation.AnimationListener() {
                                            @Override
                                            public void onAnimationStart(Animation animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animation animation) {
                                                mguideLogin.et_guid_username.setFocusable(true);
                                                mguideLogin.et_guid_username.setFocusableInTouchMode(true);
                                                mguideLogin.et_guid_username.requestFocus();
                                                mguideLogin.et_guid_username.requestFocusFromTouch();
                                                if (!TextUtils.isEmpty(emailOrmobile)) {
                                                    mguideLogin.et_guid_username.setSelection(emailOrmobile.length());
                                                } else {
                                                    mguideLogin.et_guid_username.setSelection(0);
                                                }
                                                mguideLogin.et_guid_pwd.setFocusable(false);
                                            }

                                            @Override
                                            public void onAnimationRepeat(Animation animation) {

                                            }
                                        });

                                    }else {
                                        mguideLogin.et_guid_pwd.setFocusable(true);
                                        mguideLogin.et_guid_pwd.setFocusableInTouchMode(true);
                                        mguideLogin.et_guid_pwd.requestFocus();
                                        mguideLogin.et_guid_pwd.requestFocusFromTouch();
                                    }
                                }
                                @Override
                                public void onFailure(String obj) {

                                }

                                @Override
                                public void onException(String exception) {

                                }
                            });
                        }
                    }
                }
                return true;
            }
        });

    }

}
