package com.dt.app.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Hacky fix for Issue #4 and
 * http://code.google.com/p/android/issues/detail?id=18990
 * <p/>
 * ScaleGestureDetector seems to mess up the touch events, which means that
 * ViewGroups which make use of onInterceptTouchEvent throw a lot of
 * IllegalArgumentException: pointerIndex out of range.
 * <p/>
 * There's not much I can do in my code for now, but we can mask the result by
 * just catching the problem and ignoring it.
 * 
 * @author Chris Banes
 */
public class HackyViewPager extends ViewPager {

	public HackyViewPager(Context context) {
		super(context);

	}

	public HackyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		try {
			return super.onInterceptTouchEvent(ev);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		}
	}

	private int flag = 0;
	private float mLastMotionX = 0;
	private float mLastMotionY = 0;

//	public boolean dispatchTouchEvent(MotionEvent ev) {
//		final float x = ev.getX();
//		final float y = ev.getY();
//		switch (ev.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			getParent().requestDisallowInterceptTouchEvent(true);
//			flag = 1;
//			mLastMotionX = x;
//			mLastMotionY = y;
//			break;
//		case MotionEvent.ACTION_MOVE:
//			if (flag == 1) {
//				if (Math.abs(x - mLastMotionX) < Math.abs(y - mLastMotionY)) {
//					flag = 0;
//					getParent().requestDisallowInterceptTouchEvent(false);
//				} else {
//					// 当用户按下的时候，我们告诉父组件，不要拦截我的事件（这个时候子组件是可以正常响应事件的），拿起之后就会告诉父组件可以阻止
//					getParent().requestDisallowInterceptTouchEvent(true);
//				}
//
//			}
//			break;
//		case MotionEvent.ACTION_UP:
//		case MotionEvent.ACTION_CANCEL:
//			getParent().requestDisallowInterceptTouchEvent(false);
//			break;
//		}
//		return super.dispatchTouchEvent(ev);
//	}

}
