package com.dt.app.view;

import android.app.Activity;
import android.text.Html;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.utils.DensityUtil;

public class ActionBarView implements OnClickListener {
    //整个title
    private RelativeLayout rl_title_bar;
    //回退按钮
    private RelativeLayout rl_back_home_layout;
    private ImageView iv_back_home_icon;
    private TextView tv_back_home_title;
    private TextView tv_back_home_des;
    //中间标题
    public TextView tv_center_title;
    private ImageView iv_center_title;
    //右边
    private LinearLayout ll_right_layout;

    private Activity activity;
    //没有点击事件
    public static final int FINISH_TYPE_2 = 2;
    //有点击事件，执行finish
    public static final int FINISH_TYPE_4 = 4;
    //有点击事件，不执行
    public static final int FINISH_TYPE_8 = 8;
    public static final String FINISH = "FINISH";
    private boolean isActivity = false;

    public ActionBarView(Activity activity) {
        isActivity = true;
        this.activity = activity;
        rl_title_bar = (RelativeLayout) activity.findViewById(R.id.rl_title_bar);
        rl_back_home_layout = (RelativeLayout) activity.findViewById(R.id.rl_back_home_layout);
        iv_back_home_icon = (ImageView) activity.findViewById(R.id.iv_back_home_icon);
        iv_center_title = (ImageView) activity.findViewById(R.id.iv_center_title);
        tv_back_home_title = (TextView) activity.findViewById(R.id.tv_back_home_title);
        tv_back_home_des = (TextView) activity.findViewById(R.id.tv_back_home_title2);
        tv_center_title = (TextView) activity.findViewById(R.id.tv_center_title);
        ll_right_layout = (LinearLayout) activity.findViewById(R.id.ll_right_layout);
    }

    public ActionBarView(Activity activity, View view) {
        this.activity = activity;
        rl_title_bar = (RelativeLayout) view.findViewById(R.id.rl_title_bar);
        rl_back_home_layout = (RelativeLayout) view.findViewById(R.id.rl_back_home_layout);
        iv_back_home_icon = (ImageView) view.findViewById(R.id.iv_back_home_icon);
        iv_center_title = (ImageView) view.findViewById(R.id.iv_center_title);
        tv_back_home_title = (TextView) view.findViewById(R.id.tv_back_home_title);
        tv_back_home_des = (TextView) view.findViewById(R.id.tv_back_home_title2);
        tv_center_title = (TextView) view.findViewById(R.id.tv_center_title);
        ll_right_layout = (LinearLayout) view.findViewById(R.id.ll_right_layout);
    }


    public void setBackground(int color) {
        rl_title_bar.setBackgroundColor(activity.getResources().getColor(color));
    }

    /**
     * 对activity
     *
     * @param drawable
     * @param homeText
     * @param
     */
    public void setBackHome(int drawable, String homeText, int type) {
        rl_back_home_layout.setVisibility(View.VISIBLE);
        tv_back_home_title.setVisibility(View.VISIBLE);
        iv_back_home_icon.setImageResource(drawable);
        tv_back_home_title.setText(Html.fromHtml(homeText));
        if (type == FINISH_TYPE_2) {

        } else if (type == FINISH_TYPE_4) {
            rl_back_home_layout.setTag(FINISH);
            rl_back_home_layout.setOnClickListener(this);
        } else if (type == FINISH_TYPE_8) {
            rl_back_home_layout.setOnClickListener(this);
        }
    }

    /**
     * 对 首页
     *
     * @param drawable
     * @param homeText
     * @param
     */
    public void setBackHome(int drawable, String homeText, String homeText2, int type) {
        rl_back_home_layout.setVisibility(View.VISIBLE);
        tv_back_home_title.setVisibility(View.VISIBLE);
        tv_back_home_des.setVisibility(View.VISIBLE);
        iv_back_home_icon.setImageResource(drawable);
        tv_back_home_title.setText((homeText));
        tv_back_home_title.setTextColor(activity.getResources().getColor(R.color.white));
        TextPaint textPaint = tv_back_home_title.getPaint();
        textPaint.setFakeBoldText(true);
        tv_back_home_des.setText(homeText2);
        if (type == FINISH_TYPE_2) {

        } else if (type == FINISH_TYPE_4) {
            rl_back_home_layout.setTag(FINISH);
            rl_back_home_layout.setOnClickListener(this);
        } else if (type == FINISH_TYPE_8) {
            rl_back_home_layout.setOnClickListener(this);
        }
    }

    //中间标题
    public void setCenter(String title) {
        tv_center_title.setVisibility(View.VISIBLE);
        tv_center_title.setTextColor(activity.getResources().getColor(R.color.white));
        if (!TextUtils.isEmpty(title)) {
            tv_center_title.setText(title);
        }

    }

    public void setCenterTitle(int drawable, int titleColor, String title, int type) {
        tv_center_title.setTextColor(activity.getResources().getColor(titleColor));
        setCenterTitle(drawable, title, type);
    }

    public void setCenterTitle(int drawable, String title, int type) {
        rl_back_home_layout.setVisibility(View.VISIBLE);
        iv_back_home_icon.setImageResource(drawable);
        tv_back_home_title.setVisibility(View.GONE);
        setCenter(title);
        if (type == FINISH_TYPE_2) {

        } else if (type == FINISH_TYPE_4) {
            rl_back_home_layout.setTag(FINISH);
            rl_back_home_layout.setOnClickListener(this);
        } else if (type == FINISH_TYPE_8) {
            rl_back_home_layout.setOnClickListener(this);
        }
    }

    public void setCenterImageTitle(int backdrawable, int titledrawable, int type) {
        iv_center_title.setVisibility(View.VISIBLE);
        iv_center_title.setImageResource(titledrawable);
        iv_back_home_icon.setImageResource(backdrawable);
        if (type == FINISH_TYPE_2) {

        } else if (type == FINISH_TYPE_4) {
            rl_back_home_layout.setTag(FINISH);
            rl_back_home_layout.setOnClickListener(this);
        } else if (type == FINISH_TYPE_8) {
            rl_back_home_layout.setOnClickListener(this);
        }
    }


    public interface ActionBarLinstener {
        public void onClick(View view);
    }

    private ActionBarLinstener linstener;

    public void setActionBarLinstener(ActionBarLinstener linstener) {
        this.linstener = linstener;
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag().equals(FINISH)) {
            if (activity != null && isActivity) {
                activity.finish();
            }
        } else {
            if (linstener != null) {
                linstener.onClick(v);
            }
        }
    }

    public void addTextViewAction(int id, int drawable, String text) {
        View view = inflateAction(id, drawable, text);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.topMargin = DensityUtil.dip2px(activity, 3);
        view.setLayoutParams(params);
        int index = ll_right_layout.getChildCount();
        ll_right_layout.addView(view, index);
    }

    private View inflateAction(int id, int drawable, String text) {
        LayoutInflater mInflater = LayoutInflater.from(activity);
        View view = mInflater.inflate(R.layout.actionbar_item, ll_right_layout,
                false);
        TextView labelView = (TextView) view.findViewById(R.id.actionbar_item);
        labelView.setId(id);
        if (!TextUtils.isEmpty(text)) {
            labelView.setText(text);
        }

        view.setOnClickListener(this);
        return view;
    }

    public View addImageAction(int id, int drawable, int width) {
        View view = inflateImageAction(id, drawable, width, false);
        int index = ll_right_layout.getChildCount();
        ll_right_layout.addView(view, index);
        return view;
    }

    public View addImageAction(int id, int drawable, int width, boolean circle) {
        View view = inflateImageAction(id, drawable, width, circle);
        int index = ll_right_layout.getChildCount();
        ll_right_layout.addView(view, index);
        return view;
    }

    //
    public View addImageAction(int id, int drawable) {
        return addImageAction(id, drawable, 37);
    }

    public View addImageAction(int id, int drawable, boolean circle) {
        return addImageAction(id, drawable, 25, circle);
    }

    private View inflateImageAction(int id, int drawable, int width, boolean circle) {
        ImageView imageView = null;
        if (circle) {
            imageView = new CircleImageView(activity);
        } else {
            imageView = new ImageView(activity);
        }
        int wid = DensityUtil.dip2px(activity, width);
        int height = DensityUtil.dip2px(activity, width);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(wid, height);
        layoutParams.leftMargin = DensityUtil.dip2px(activity, 4);
        imageView.setLayoutParams(layoutParams);
        imageView.setId(id);
        imageView.setImageResource(drawable);
        int padding = DensityUtil.dip2px(activity, 5);
        imageView.setPadding(padding, padding, padding, padding);
        imageView.setOnClickListener(this);

        return imageView;
    }
}
