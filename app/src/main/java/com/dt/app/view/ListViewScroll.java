package com.dt.app.view;

import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

public class ListViewScroll   implements OnScrollListener{
	//listView中第一项的索引
    private int mListViewFirstItem = 0;
    //listView中第一项的在屏幕中的位置
    private int mScreenY = 0;
	private ListView mListView;
	
	public ListViewScroll(ListView mListView){
		this.mListView = mListView;
	}
	 
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
	}
	
	@Override
	public void onScroll(AbsListView view,
			int firstVisibleItem, int visibleItemCount,
			int totalItemCount) {
		if (mListView.getChildCount() > 0) {
			boolean isScrollToUp = false;
			View childAt = mListView
					.getChildAt(firstVisibleItem);
			int[] location = new int[2];
			childAt.getLocationOnScreen(location);
			Log.d("onScroll", "firstVisibleItem= "
					+ firstVisibleItem + " , y=" + location[1]);
			if (firstVisibleItem != mListViewFirstItem) {
				if (firstVisibleItem > mListViewFirstItem) {
					Log.e("--->", "向上滑动");
					isScrollToUp = true;
				} else {
					Log.e("--->", "向下滑动");
					isScrollToUp = false;
				}
				mListViewFirstItem = firstVisibleItem;
				mScreenY = location[1];
			} else {
				if (mScreenY > location[1]) {
					Log.i("--->", "->向上滑动");
					isScrollToUp = true;
				} else if (mScreenY < location[1]) {
					Log.e("--->", "向下滑动");
					isScrollToUp = false;
				}
				mScreenY = location[1];
			}
			if (listener!=null) {
				listener.onScrollDirectionChanged(isScrollToUp);
			}
		}

	}

	public ListViewScrollListener listener;
	
	public void setListViewScrollListener(ListViewScrollListener listener){
		this.listener = listener;
	}
	
	public interface ListViewScrollListener{
		public void onScrollDirectionChanged(boolean isScrollToUp);
	}
}
