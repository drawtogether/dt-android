package com.dt.app.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ListView;

import com.dt.app.R;
import com.dt.app.adapter.FolderAdapter;

public class ListPopupWindow extends BasePopupWindow {
	private ListView listView;
	
	public ListPopupWindow(Activity context, FolderAdapter folderAdapter,
			int width, int height) {
		super(LayoutInflater.from(context).inflate(R.layout.folder_listview,
				null), width, height);
		
		listView.setAdapter(folderAdapter);
	}

	public ListView getListView() {
		return listView;
	}

	@Override
	public void initStyle() {
		this.setAnimationStyle(R.style.ImageSelectorPop);
	}

	@Override
	public void initViews(Context context) {
		listView = (ListView) findViewById(R.id.lv_folder);
	}

	@Override
	public void initEvents() {

	}

//	public void showPop(View view){
//		
//		 
//	}
	
	
}
