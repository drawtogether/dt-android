package com.dt.app.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.dt.app.R;
import com.dt.app.adapter.CommonAdapter;
import com.dt.app.adapter.ViewHolder;
import com.dt.app.listener.HandleCallback;

public class NumberPopwindow extends BasePopupWindow{
	
	private List<Integer> mDataList;
	
	private ListView mListView;
	
	public NumberPopwindow(Context context,int width,int height){
		super(LayoutInflater.from(context).inflate(R.layout.listview_main, null),width,height);
	}
	
	@Override
	public void initStyle() {
		this.setAnimationStyle(R.style.ImageSelectorPop);
	}

	@Override
	public void initViews(Context context) {
		try {
			mDataList = new ArrayList<Integer>();
			mListView = (ListView) findViewById(R.id.lv_pop);
			for (int i = 1; i < 11; i++) {
				mDataList.add(i);
			}
			mListView.setAdapter(new CommonAdapter<Integer>(context,mDataList,R.layout.simple_list_item) {
				@Override
				public void convert(ViewHolder helper, Integer item) {
					helper.setText(R.id.tv_number, ""+item);
				}
			});
			mListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (callback!=null) {
						callback.onSuccess(mDataList.get(position));
					}
					dismiss();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initEvents() {
		
	}

	private HandleCallback<Integer> callback;
	public void setHandleCallback(HandleCallback<Integer> callback){
		this.callback = callback;
	}
}
