package com.dt.app.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.dt.app.R;

public class Web {

	public Web(Activity activity) {
		super();
		this.activity = activity;
		initWebView();
	}

	private Activity activity;
	private LinearLayout mLoadingLayout;

	private WebView mWebView;

	public WebView getWebView() {
		return mWebView;
	}

	/**
	 * 加载详情
	 */

	private void initWebView() {
		mWebView = (WebView) activity.findViewById(R.id.webView);
		mLoadingLayout = (LinearLayout) activity
				.findViewById(R.id.ll_loading_dialog);
		mWebView.setBackgroundColor(0);
		WebSettings webSettings = mWebView.getSettings();
		mWebView.requestFocus();
		webSettings.setJavaScriptEnabled(true);
		mWebView.setFocusable(true);
		webSettings.setBuiltInZoomControls(false);

		mWebView.setScrollBarStyle(0);

		mWebView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onProgressChanged(WebView view, int newProgress) {

				if (newProgress >= 100) {
					if (method != null) {
						mWebView.loadUrl(method);
					}
					if (clearMethod) {
						method = null;
					}

				}
				super.onProgressChanged(view, newProgress);
			}

			@Override
			public boolean onJsAlert(WebView view, String url, String message,
					final JsResult result) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(
						view.getContext());

				builder.setMessage(message).setPositiveButton("确定",
						new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								result.confirm();
							}
						});
				builder.setOnKeyListener(new OnKeyListener() {
					public boolean onKey(DialogInterface dialog, int keyCode,
							KeyEvent event) {
						return true;
					}
				});
				// 禁止响应按back键的事件
				builder.setCancelable(false);
				AlertDialog dialog = builder.create();
				dialog.show();
				return true;
			}

		});

		mWebView.setWebViewClient(new WebViewClient() {
			// 重写父类方法，让新打开的网页在当前的WebView中显示
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				loadUrl(view, url);
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
			}
		});

	}

	public void loadUrl(String url) {
		loadUrl(mWebView, url);
	}

	private void loadUrl(final WebView webView, final String url) {
		webView.loadUrl(url);
	}

	public void loadContent(String content) {
		if (content != null) {
			DisplayMetrics outMetrics = new DisplayMetrics();
			activity.getWindowManager().getDefaultDisplay()
					.getMetrics(outMetrics);
			// height='200'
			float width = outMetrics.widthPixels;
			float den = outMetrics.density;
			int wi = (int) (width / den);
			content = content.replaceAll("<img", "<img width='" + wi + "'");
		}
		mWebView.loadDataWithBaseURL("about:blank", content, "text/html",
				"utf-8", null);
	}

	/**
	 * 加载方法，网页加载完毕时加载方法，防止加载不到方法
	 * 
	 * @param method
	 */
	public void loadMethod(String method) {

		this.method = method;
	}

	private String method = null;
	private boolean clearMethod;

	public void setMethod(boolean clearMethod) {
		this.clearMethod = clearMethod;
	}

	public boolean canGoBack() {
		if (mWebView != null)
			return mWebView.canGoBack();
		return false;
	}

	public void goBack() {
		if (mWebView != null) {
			mWebView.goBack();
		}
	}

	public void setJavascriptInterface(Object obj, String interfaceName) {
		mWebView.addJavascriptInterface(obj, interfaceName);
	}
}
