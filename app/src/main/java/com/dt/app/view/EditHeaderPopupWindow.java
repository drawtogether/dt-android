package com.dt.app.view;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.dt.app.R;
import com.dt.app.adapter.FolderAdapter;
import com.dt.app.utils.ZTDeviceInfo;

public class EditHeaderPopupWindow extends BasePopupWindow implements
		OnClickListener {

	private ImageView iv_take_picture;
	private ImageView iv_select_picture;

	public EditHeaderPopupWindow(Activity context) {
		super(LayoutInflater.from(context).inflate(
				R.layout.dt_edit_header_main, null), ZTDeviceInfo.getInstance()
				.getWidthDevice(), ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void initStyle() {
		this.setAnimationStyle(R.style.ImageSelectorPop);
	}

	@Override
	public void initViews(Context context) {
		iv_take_picture = (ImageView) findViewById(R.id.iv_take_picture);
		iv_select_picture = (ImageView) findViewById(R.id.iv_select_picture);

		iv_take_picture.setOnClickListener(this);
		iv_select_picture.setOnClickListener(this);
	}

	@Override
	public void initEvents() {

	}

	@Override
	public void onClick(View v) {
		if (listener == null) {
			return;
		}
		switch (v.getId()) {
		case R.id.iv_take_picture:
			listener.onClick(true);
			break;
		case R.id.iv_select_picture:
			listener.onClick(false);
			break;
		}
	}

	private EditHeaderPopListener listener;

	public void setEditHeaderPopListener(EditHeaderPopListener l) {
		listener = l;
	}

	public interface EditHeaderPopListener {
		public void onClick(boolean isTakePic);
	}

}
