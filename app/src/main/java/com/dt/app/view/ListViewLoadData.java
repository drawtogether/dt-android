package com.dt.app.view;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dt.app.R;

/**
 * listview_load_main.xml
 * 
 * @author libin
 * 
 */
public class ListViewLoadData {
	private Activity activity;
	private TextView tv_load_nodata_info;
	private ListView lv_data;
	private LinearLayout ll_loading_dialog;
	private View view;

	public ListViewLoadData(Activity activity) {
		this.activity = activity;
	}

	public ListViewLoadData(View view) {
		this.view = view;
	}

	public ListView initView() {
		if (activity != null) {
			tv_load_nodata_info = (TextView) activity
					.findViewById(R.id.tv_load_nodata_info);
			lv_data = (ListView) activity.findViewById(R.id.lv_data);
			ll_loading_dialog = (LinearLayout) activity
					.findViewById(R.id.ll_loading_dialog);
		}
		if (view != null) {
			tv_load_nodata_info = (TextView) view
					.findViewById(R.id.tv_load_nodata_info);
			lv_data = (ListView) view.findViewById(R.id.lv_data);
			ll_loading_dialog = (LinearLayout) view
					.findViewById(R.id.ll_loading_dialog);
		}
		return lv_data;
	}

	public void hasData() {
		ll_loading_dialog.setVisibility(View.GONE);
		tv_load_nodata_info.setVisibility(View.GONE);
		lv_data.setVisibility(View.VISIBLE);
	}

	public void noData(String msg) {
		ll_loading_dialog.setVisibility(View.GONE);
		tv_load_nodata_info.setVisibility(View.VISIBLE);
		tv_load_nodata_info.setText("" + msg);
		lv_data.setVisibility(View.GONE);
	}

	public void noData() {
		ll_loading_dialog.setVisibility(View.GONE);
		tv_load_nodata_info.setVisibility(View.VISIBLE);
		lv_data.setVisibility(View.GONE);
	}

}
