package com.dt.app.view;

import java.util.ArrayList;
import java.util.List;

import com.lidroid.xutils.util.LogUtils;
import uk.co.senab.photoview.PhotoView;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dt.app.R;
import com.dt.app.adapter.ViewPagerAdsAdapter;
import com.dt.app.bean.Photo;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.utils.ZTDeviceInfo;
import com.lidroid.xutils.BitmapUtils;

public class BannerView implements
        ViewPager.OnPageChangeListener {
    private HackyViewPager mHackViewPager;
    public HackyViewPager getmHackViewPager() {
		return mHackViewPager;
	}

	private ViewPagerAdsAdapter mPrivilegeAdapter;
   

	private List<View> mBanner_Views;
    private ImageView[] mImageViews;
    private ImageView mImageView;
    private int mViewCount;
    private int mCurSel;
    private LinearLayout ll_point;

    private Context context;

    private LayoutInflater mInflater;
    //	private TextView tv_ads_description;
    //广告描述信息
    private String[] ads_description;

    public static final String PHOTO_VIEW = "photo_view";
    public static final String FIRST_ADS = "first_ads";
 

    private String bannerType;
 

    private BitmapUtils mBitmapUtils;
    private  List<Photo> mPhotos;
    
    private int height;
    private int maxDistance=200;
    private boolean isFinishAct=false;
    /**
     * 是否显示小圆点
     */
    private boolean isHasDot=false;    
    public BannerView(Context context, List<Photo> mPhotos, String bannerType) {
    	this(context, mPhotos, bannerType, false);
    }
    public BannerView(Context context, List<Photo> mPhotos, String bannerType,boolean isHasDot) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mPhotos = mPhotos;
        this.bannerType = bannerType;
        this.isHasDot = isHasDot;
        mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
    }

    public View initView() {
        View view = null;
        if (PHOTO_VIEW.equals(bannerType)) {
        	 view = mInflater.inflate(R.layout.wish_photo_viewpager, null);
		}else if (FIRST_ADS.equals(bannerType)) {
			 view = mInflater.inflate(R.layout.dt_banner, null);
		}
        height = ZTDeviceInfo.getInstance().getHeightDevice();
        		
        mHackViewPager = (HackyViewPager) view.findViewById(R.id.group_viewpager);
//		tv_ads_description = (TextView) view.findViewById(R.id.tv_ads_description);
        ll_point = (LinearLayout) view.findViewById(R.id.ll_point);

        initViewPage();

        if (isHasDot) {
        	initPoint();
		}

        return view;
    }

    public ViewPagerAdsAdapter getAdapter() {
        return mPrivilegeAdapter;
    }
    int startx; // 记录下来第一次手指触摸屏幕的位置
	int starty;
	boolean isMove = false;
    /**
     * 初始化页面
     */
    private void initViewPage() {
        mBanner_Views = new ArrayList<View>();
        if (mPhotos==null || mPhotos.size()==0) {
			return;
		}
        if (mPhotos != null && mPhotos.size() > 0) {
            ads_description = new String[mPhotos.size()];
            for (int j = 0; j < mPhotos.size(); j++) {
            	 View view = null;
            	if (PHOTO_VIEW.equals(bannerType)) {
                	 view = mInflater.inflate(R.layout.wish_photo_scan_detail, null);
                	 
        		}else if (FIRST_ADS.equals(bannerType)) {
        			 view = mInflater.inflate(R.layout.dt_banner_item, null);
        		}
               
                mImageView = (ImageView) view.findViewById(R.id.banner_item_image);
                if (mImageView instanceof PhotoView) {
                	final PhotoView mPhotoView = (PhotoView) mImageView;
//                	mImageView.setOnClickListener(new OnClickListener() {
//						@Override
//						public void onClick(View v) {
//							if (mPhotoView.getScale()>1.0f) {
//								((Activity)context).finish();
//							}
//							
//						}
//					});
				}
                mImageView.setImageResource(R.mipmap.me_background_icon);
                if (FIRST_ADS.equals(bannerType) && mPhotos.size()>1){
                	mImageView.setOnTouchListener(new MyOnTouchListener());
                }
                
                final View rl_dt_photo_main = view.findViewById(R.id.rl_dt_photo_main);
                if (rl_dt_photo_main!=null) {
                	rl_dt_photo_main.setBackgroundColor(Color.BLACK);
                	PhotoView photoView = (PhotoView) mImageView;
                	photoView.setDispatchTouch();
				}
                Photo mAds2 = mPhotos.get(j);
                mBitmapUtils.display(mImageView, mAds2.getUrl());
                //ads_description[j] = mAds2.getDesc();
                mBanner_Views.add(view);
            }
            mViewCount = mPhotos.size();
        }

        mPrivilegeAdapter = new ViewPagerAdsAdapter(mBanner_Views, mPhotos);
        mHackViewPager.setAdapter(mPrivilegeAdapter);

        mHackViewPager.setOnPageChangeListener(this);
        
        if (mPhotos.size()>1) {
        	viewPagerTask = new ViewPagerTask();
            if (!PHOTO_VIEW.equals(bannerType)){
                mHandler.postDelayed(viewPagerTask, 4000);
            }
		}
        
        mCurSel = 0;
    }

    /**
     * 初始化点
     */
    private void initPoint() {
    	if (mViewCount <= 1)
            return;
        mImageViews = new ImageView[mViewCount];
        LinearLayout.LayoutParams img_layoutparams = new LinearLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        img_layoutparams.rightMargin = 15;
        ll_point.removeAllViews();
        
        for (int i = 0; i < mViewCount; i++) {
            mImageViews[i] = new ImageView(context);
            mImageViews[i].setLayoutParams(img_layoutparams);
            mImageViews[i].setImageResource(R.drawable.point_bg);
            ll_point.addView(mImageViews[i]);
        }
        
        mImageViews[mCurSel].setEnabled(false);
//		tv_ads_description.setText(ads_description[mCurSel]);
    }

    private Handler mHandler = new Handler(){
    	public void handleMessage(Message msg) {
    		mHackViewPager.setCurrentItem(mCurSel);
            LogUtils.e("--------mhandler------"+mCurSel);
            mHandler.postDelayed(viewPagerTask, 4000);
    	};
    };
    private ViewPagerTask viewPagerTask;

    public void onResume(){
    	 if (mPhotos.size()>1) {
    		 if (viewPagerTask==null) {
    			 viewPagerTask = new ViewPagerTask();
			 }
             mHandler.postDelayed(viewPagerTask, 4000);
 		}
    }
    
    public void onPause(){
    	mHandler.removeCallbacksAndMessages(null);
    }
 
    public class MyOnTouchListener implements OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_MOVE:
				mHandler.removeCallbacks(viewPagerTask);
				mHandler.removeCallbacksAndMessages(null);
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				mHandler.postDelayed(viewPagerTask, 4000);
				break;
			}
			return false;
		}
	}
    
    public class ViewPagerTask implements Runnable {
		@Override
		public void run() {
			mCurSel = (mCurSel + 1)% (mPhotos.size());
			mHandler.obtainMessage().sendToTarget();
		}
	}
    
    @Override
    public void onPageSelected(int position) {

        setCurPoint(position);
    }

    //	/**
//	 * 设置当前的点
//	 * 
//	 * @param index
//	 */
    private void setCurPoint(int index) {
        if (index < 0 || index > mViewCount - 1 || mCurSel == index) {
            return;
        }
//		tv_ads_description.setText(ads_description[index]);
        mCurSel = index;
        if (mImageViews!=null) {
        	 mImageViews[mCurSel].setEnabled(true);
             mImageViews[index].setEnabled(false);
		}
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }
 
}
