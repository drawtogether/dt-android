package com.dt.app.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by caiyuhao on 15-10-23.
 */
public class SawtoothEditor extends EditText {
    public SawtoothEditor(Context context) {
        super(context);
    }

    public SawtoothEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SawtoothEditor(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //去锯齿
        PaintFlagsDrawFilter mSetfil = new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG|Paint.FILTER_BITMAP_FLAG);
        canvas.setDrawFilter(mSetfil);

        this.setCursorVisible(true);
        super.onDraw(canvas);
    }
}
