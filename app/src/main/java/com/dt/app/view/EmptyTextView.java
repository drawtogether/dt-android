package com.dt.app.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

public class EmptyTextView extends TextView {

	public EmptyTextView(Context context) {
		super(context);
	}

	public EmptyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public EmptyTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		if (TextUtils.isEmpty(text)) {
			text = "";
		}
		super.setText(text, type);
	}
}
