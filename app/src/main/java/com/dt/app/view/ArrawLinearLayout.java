package com.dt.app.view;

import com.dt.app.R;
import com.dt.app.listener.AnimationListenerImpl;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ArrawLinearLayout extends LinearLayout {
	private Context context;
	private TextView tv_button_text;
	private ImageView iv_button_img;
	
	private RotateAnimation openAnimation;
	private RotateAnimation closeAnimation;
	private boolean isOpen = false;
	//是否已经打开
	private boolean isOpenFlag = false;
	public ArrawLinearLayout(Context context) {
		super(context);
		this.context = context;
		init();
	}

	public ArrawLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}
	//当前是否可以点击
	private boolean currentClick=true;

	private void init(){
		
		openAnimation = new RotateAnimation(0,90,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
		closeAnimation = new RotateAnimation(90,0,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
		
		openAnimation.setDuration(350);
		openAnimation.setFillAfter(true);
		closeAnimation.setDuration(350);
		closeAnimation.setFillAfter(true);
		
		openAnimation.setAnimationListener(new AnimationListenerImpl(){
			@Override
			public void onAnimationEnd(Animation animation) {
				isOpen = true;
				currentClick=true;
				if (layoutListener!=null) {
					layoutListener.open(false);
				}
			}
		});
		closeAnimation.setAnimationListener(new AnimationListenerImpl(){
			@Override
			public void onAnimationEnd(Animation animation) {
				isOpen = false;
				currentClick=true;
				if (layoutListener!=null) {
					layoutListener.close();
				}
			}
		});
		
		View view = LayoutInflater.from(context).inflate(R.layout.dt_button_rotate, null);
		tv_button_text = (TextView) view.findViewById(R.id.tv_button_text);
		iv_button_img = (ImageView) view.findViewById(R.id.iv_button_img);
 
		addView(view);
	}
	
	public void setOnClickListener(){
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!currentClick) {
					return;
				}
				currentClick = false;
				iv_button_img.clearAnimation();
				if (isOpen) {
					iv_button_img.startAnimation(closeAnimation);
				}else {
					iv_button_img.startAnimation(openAnimation);
				}
			}
		});
	}
	
	private boolean enableClick=true;
	public void enableClick(boolean enable){
		enableClick = enable;
	}
	
	public void close(){
		if (enableClick) {
			iv_button_img.clearAnimation();
			iv_button_img.startAnimation(closeAnimation);
		}
	}
	public void open(){
		if (enableClick) {
			iv_button_img.clearAnimation();
			iv_button_img.startAnimation(openAnimation);
		}
	}
	
	public void setText(String text){
		tv_button_text.setText(text);
	}
	
	ArrawLayoutListener layoutListener;
	
	public void setArrawLayoutListener(ArrawLayoutListener l){
		setOnClickListener();
		this.layoutListener = l;
	}
	
	public interface ArrawLayoutListener{
		public void open(boolean isClick);
		public void close();
	}
}
