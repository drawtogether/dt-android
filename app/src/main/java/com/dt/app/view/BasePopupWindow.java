package com.dt.app.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;
/**
 * super(LayoutInflater.from(context).inflate(
				R.layout.main, null),
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
 * @author libin
 *
 */
public abstract class BasePopupWindow extends PopupWindow {

	protected View mContentView;

	public BasePopupWindow() {
		super();
	}

	public BasePopupWindow(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public BasePopupWindow(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BasePopupWindow(Context context) {
		super(context);
	}

	public BasePopupWindow(int width, int height) {
		super(width, height);
	}

	public BasePopupWindow(View contentView, int width, int height,
			boolean focusable) {
		super(contentView, width, height, focusable);
	}

	public BasePopupWindow(View contentView) {
		super(contentView);
	}

	/**
	 * @param contentView
	 * @param width
	 * @param height
	 */
	public BasePopupWindow(View contentView, int width, int height) {
		super(contentView, width, height, true);
		mContentView = contentView;
		setFocusable(true);
		setOutsideTouchable(true);
		// setAnimationStyle(R.style.Popup_Animation_Alpha);
		initStyle();
		setBackgroundDrawable(new ColorDrawable());
		setTouchable(true);
		initViews(mContentView.getContext());
		initEvents();

	}

	public abstract void initStyle();

	public abstract void initViews(Context context);

	public abstract void initEvents();

	public View findViewById(int id) {
		return mContentView.findViewById(id);
	}
}