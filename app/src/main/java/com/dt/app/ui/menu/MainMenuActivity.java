package com.dt.app.ui.menu;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.QiNiuToken;
import com.dt.app.bean.UnReadMsgData;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.ui.art.ArtistSpaceActivity;
import com.dt.app.ui.set.SetActivity;
import com.dt.app.utils.ActivityUtils;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ImageUtils;
import com.dt.app.utils.LoadHtmlActivity;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * Created by caiyuhao on 15-9-13.
 */
public class MainMenuActivity extends BaseActivity
        implements View.OnClickListener {
    @ViewInject(R.id.dt_main_menu_img)
    private ImageView dt_main_menu_img;
    @ViewInject(R.id.dt_main_menu_add)
    private ImageView dt_main_menu_add;
    @ViewInject(R.id.dt_main_menu_search)
    private ImageView dt_main_menu_search;
    @ViewInject(R.id.dt_main_menu_msg)
    private ImageView dt_main_menu_msg;
    @ViewInject(R.id.dt_main_menu_setting)
    private ImageView dt_main_menu_setting;
    @ViewInject(R.id.main_menu_photo_head)
    private CircleImageView main_menu_photo_head;

    @ViewInject(R.id.main_menu_jifeng_exchange)
    private TextView main_menu_jifeng_exchange;
    @ViewInject(R.id.main_menu_invited_friend)
    private TextView main_menu_invited_friend;
    @ViewInject(R.id.main_menu_jifeng_rule)
    private TextView main_menu_jifeng_rule;
    @ViewInject(R.id.menu_username)
    private TextView menu_username;
    @ViewInject(R.id.main_menu_like_works)
    private  TextView main_menu_like_works;
    @ViewInject(R.id.main_menu_my_space)
    private TextView  main_menu_my_space;
    @ViewInject(R.id.tv_no_read_number)
    private TextView tv_no_read_number;
    private Activity mActivity;

    private BitmapUtils mBitmapUtils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_main_menu);
        ViewUtils.inject(this);
        mActivity = this;

        initListener();
    }

    @Override
	protected void onResume() {
    	initData();
        unreadCount();
		super.onResume();
	}
    
    /**
     * initData
     */
    private void initData() {

        String username = PreferencesUtils.getString(mActivity, Constant.PrefrencesPt.DTnickname);
        String logoUrl = PreferencesUtils.getString(mActivity, Constant.PrefrencesPt.DTlogo);
        menu_username.setText(username + "");
        mBitmapUtils.display(main_menu_photo_head, logoUrl);
    }

    /**
     * initListener
     */
    private void initListener() {
        dt_main_menu_img.setOnClickListener(this);
        dt_main_menu_setting.setOnClickListener(this);
        dt_main_menu_add.setOnClickListener(this);
        dt_main_menu_search.setOnClickListener(this);
        dt_main_menu_msg.setOnClickListener(this);
        main_menu_photo_head.setOnClickListener(this);
        main_menu_jifeng_exchange.setOnClickListener(this);
        main_menu_invited_friend.setOnClickListener(this);
        main_menu_jifeng_rule.setOnClickListener(this);
        main_menu_my_space.setOnClickListener(this);
        main_menu_like_works.setOnClickListener(this);
        mBitmapUtils = new BitmapUtils(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dt_main_menu_img:
                this.finish();
                overridePendingTransition(0, R.anim.setting_out_from_top);
                break;
            case R.id.dt_main_menu_add:
                startActivityByCalss(UploadActivity.class);
                overridePendingTransition(R.anim.setting_in_from_bottom, R.anim.setting_apla_to_apla);
                break;
            case R.id.dt_main_menu_search:
            	Intent mIntent = new Intent(this,SearchActivity.class);
            	mIntent.putExtra("cancelhidden",true);
				startActivity(mIntent);
                overridePendingTransition(R.anim.right_in, R.anim.slide_out_to_left);
                break;
            case R.id.dt_main_menu_msg:
                startActivityByCalss(MsgActivity.class);
                break;
            case R.id.dt_main_menu_setting:
                startActivityByCalss(SetActivity.class);
                break;
            case R.id.main_menu_photo_head:
            case R.id.main_menu_my_space:
                Bundle bundle = new Bundle();
                bundle.putLong("memberId", PreferencesUtils.getLong(mActivity, Constant.PrefrencesPt.DTid));
                startActivityByCalss(mActivity, ArtistSpaceActivity.class, bundle);
                break;
            case R.id.main_menu_like_works:
                startActivityByCalss(MyLoveActivity.class);
                break;
            case R.id.main_menu_jifeng_exchange:
                startActivityByCalss(PointHistoryActivity.class);
                break;
            case R.id.main_menu_invited_friend:

            	Intent mIn = new Intent(MainMenuActivity.this,InvitedActivity.class);
            	startActivity(mIn);
                break;
            case R.id.main_menu_jifeng_rule:
                ActivityUtils.startNewIntent(mActivity);
                break;
            default:
                break;
        }
    }

    /**
     *  no read
     */
    private  void unreadCount(){
        HashMap<String, Object> data = new HashMap<String, Object>();
        try {
            RequestApi.commonRequest(this, Constant.URL.DTUnreadCount, data,
                    new ResultLinstener<UnReadMsgData>() {
                        @Override
                        public void onSuccess(UnReadMsgData obj) {
                            if (obj != null) {
                              UnReadMsgData.UnRead unRead = obj.getData();
                              int count =   unRead.getUnreadCount();
                              if (count>0){
                                  tv_no_read_number.setText(count+"");
                                  tv_no_read_number.setVisibility(View.VISIBLE);
                              }else {
                                  tv_no_read_number.setVisibility(View.INVISIBLE);
                              }
                            }
                        }

                        @Override
                        public void onFailure(String obj) {

                        }

                        @Override
                        public void onException(String exception) {
                        }
                    }, new UnReadMsgData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
