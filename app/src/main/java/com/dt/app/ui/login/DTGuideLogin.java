package com.dt.app.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.dt.app.DTApplication;
import com.dt.app.R;
import com.dt.app.adapter.DTFragmentAdapter;
import com.dt.app.animation.LoginAnimation;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.bean.DTInitData;
import com.dt.app.bean.DTUser;
import com.dt.app.common.DTFactoryApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.common.threelogin.LoginQQHelper;
import com.dt.app.utils.Constant;
import com.dt.app.utils.InputMethodUtil;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.GuideLoginViewPager;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.tencent.tauth.IUiListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;

import java.util.List;

/**
 * Created by caiyuhao on 15-9-2.
 */
public class DTGuideLogin extends BaseFragmentActivity implements ViewPager.OnPageChangeListener {
    private DTFragmentAdapter mAdapter;
    private GuideLoginViewPager mPager;

    private ImageView[] mImageViews;
    private int mCurSel;
    private List<DTInitData.DmData> dmDatasSize;
    private int mViewCount;
    private int mDataSize;
    private Activity context;
    private LinearLayout ll_point;

    @ViewInject(R.id.guid_linear_login)
    private RelativeLayout guid_linear_login;
    @ViewInject(R.id.linear_login_pwd)
    public LinearLayout linear_login_pwd;
    @ViewInject(R.id.drawerlayout)
    public DrawerLayout drawerlayout;
    @ViewInject(R.id.et_guid_pwd)
    public EditText et_guid_pwd;
    @ViewInject(R.id.et_guid_username)
    public EditText et_guid_username;

    @ViewInject(R.id.tv_guid_forget_pwd)
    public TextView tv_guid_forget_pwd;

    @ViewInject(R.id.guid_wechat_img)
    public ImageView guid_wechat_img;
    @ViewInject(R.id.guid_qq_img)
    public ImageView guid_qq_img;
    @ViewInject(R.id.guid_register_img)
    public ImageView guid_register_img;
    @ViewInject(R.id.tv_guid_login)
    public TextView tv_guid_login;

    private LoginQQHelper mLogin3PQQHelper;
    private IUiListener loginListener;
    private LoginAnimation mLoginAnimation;
    public static Integer Mobile = 1;
    public static Integer Email = 2;
    public Integer input_result = -1;


    private int viewCount = 0;
    private boolean hasExit = false;
    private int heightDifference = 0;
    private boolean ishasKeybord = false;
    private View login_view_right;
    private float DownX = 0;
    private float DownY = 0;
    private UMSocialService mController;
    public  static  int platpormTag = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_guidelogin);
        context = this;
        ViewUtils.inject(this);
        mAdapter = new DTFragmentAdapter(getSupportFragmentManager());
        mLoginAnimation = new LoginAnimation(context);
        mLoginAnimation.initEditorListener();

        mController = UMServiceFactory.getUMSocialService("com.umeng.login");
        mLogin3PQQHelper = new LoginQQHelper(context, mController);

        login_view_right = findViewById(R.id.login_view_right);
        mPager = (GuideLoginViewPager) findViewById(R.id.pager);
        ll_point = (LinearLayout) findViewById(R.id.guid_view_linear);
        dmDatasSize = DTApplication.getInstance().getDmDatas();
        if (dmDatasSize != null && dmDatasSize.size() > 0) {
            mViewCount = dmDatasSize.size() + 1;
            mDataSize = dmDatasSize.size();
        } else {
            mViewCount = 1;
            mDataSize = 1;
        }
        initPoint();
        mPager.setAdapter(mAdapter);
        mPager.setOnPageChangeListener(this);
        et_guid_pwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                LogUtils.e("--onEditorAction--id = " + id + ",keyEvent=" + keyEvent);
                if (id == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String emailOrmobile = et_guid_username.getText().toString().trim();
                    String pwd = et_guid_pwd.getText().toString().trim();
                    //Animation animation = AnimationUtils.loadAnimation(context, R.anim.login_editext_animation);

                    String premailOrmobile = mLoginAnimation.defaultName();
                    if (!TextUtils.isEmpty(premailOrmobile)){
                        input_result = 0;
                    }
                    if (TextUtils.isEmpty(emailOrmobile) || input_result == -1) {
                        et_guid_username.startAnimation(trantAnimation());
                        et_guid_username.setFocusable(true);
                        et_guid_username.setFocusableInTouchMode(true);
                        et_guid_username.requestFocus();
                        et_guid_username.requestFocusFromTouch();
                        return false;
                    }
                    if (TextUtils.isEmpty(pwd)) {
                        linear_login_pwd.startAnimation(trantAnimation());
                        return false;
                    }
                    if (!TextUtils.isEmpty(pwd) && pwd.length() < 6) {
                        linear_login_pwd.startAnimation(trantAnimation());
                        return false;
                    }
                    PreferencesUtils.putString(context, Constant.PrefrencesPt.DTpwd,pwd);
                    DTFactoryApi.loginData(context, emailOrmobile, pwd, new ResultLinstener() {
                        @Override
                        public void onSuccess(Object obj) {
                            DTUser user = (DTUser) obj;
                            if (user.getCode() != 1) {
                                et_guid_pwd.startAnimation(trantAnimation());
                                et_guid_pwd.setText("");
                                et_guid_pwd.setFocusable(false);
                            }
                        }

                        @Override
                        public void onFailure(String obj) {

                        }

                        @Override
                        public void onException(String exception) {

                        }
                    });
                }
                return false;
            }
        });
        //点击密码框
        et_guid_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailOrmobile = et_guid_username.getText().toString().trim();
                // Animation animation = AnimationUtils.loadAnimation(context, R.anim.login_editext_animation);
                if (TextUtils.isEmpty(emailOrmobile)) {
                    //et_guid_username.setSelection(0);
                    //et_guid_username.setFocusable(true);
                    et_guid_username.setEnabled(true);
                    et_guid_username.setCursorVisible(true);
                    et_guid_username.startAnimation(trantAnimation());
                    et_guid_pwd.setText("");
                } else {
                    //et_guid_pwd.setSelection(et_guid_pwd.getText().length());

                    et_guid_pwd.setFocusable(true);
                    et_guid_pwd.setFocusableInTouchMode(true);
                    et_guid_pwd.requestFocus();
                    et_guid_pwd.requestFocusFromTouch();
                }
            }
        });

        tv_guid_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (heightDifference > 0) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    Log.e("--setOnTouchListener---", "Size: " + heightDifference);
                }
            }

        });

        //监听键盘的高度
        guid_linear_login.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            //当键盘弹出隐藏的时候会 调用此方法。
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //获取当前界面可视部分
                context.getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                //获取屏幕的高度
                int screenHeight = context.getWindow().getDecorView().getRootView().getHeight();
                //此处就是用来获取键盘的高度的， 在键盘没有弹出的时候 此高度为0 键盘弹出的时候为一个正数
                heightDifference = screenHeight - r.bottom;

                if (heightDifference > 0 && !ishasKeybord) {
                    ishasKeybord = true;
                    LogUtils.e("----------screenHeight= " + screenHeight + ",bottom = " + r.bottom);
                    FrameLayout.LayoutParams btnLp = (FrameLayout.LayoutParams) guid_linear_login.getLayoutParams();
                    btnLp.setMargins(0, 0, 0, 130);
                    guid_linear_login.requestLayout();
                    //et_guid_username.setSelection(et_guid_username.getText().length());
                    et_guid_username.setCursorVisible(true);
                } else {
                    ishasKeybord = false;
                }
            }

        });

        drawerlayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {
                et_guid_username.setAlpha(v);
                linear_login_pwd.setAlpha(v);
                guid_register_img.setAlpha(v);
                guid_qq_img.setAlpha(v);
                guid_wechat_img.setAlpha(v);

            }

            @Override
            public void onDrawerOpened(View view) {
                Window window = context.getWindow();
                WindowManager.LayoutParams lp = window.getAttributes();
                lp.screenBrightness = 1.0f;
                window.setAttributes(lp);
            }

            @Override
            public void onDrawerClosed(View view) {
                ll_point.setVisibility(View.VISIBLE);
                if (heightDifference > 0) {
                    InputMethodUtil.hideSoftInput(context);

                }
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

    }

    public TranslateAnimation trantAnimation() {
        TranslateAnimation animation = new TranslateAnimation(5, -5, 0, 0);
        animation.setInterpolator(new OvershootInterpolator());
        animation.setDuration(20);
        animation.setRepeatCount(4);
        animation.setRepeatMode(Animation.REVERSE);
        return animation;
    }

    /**
     * loadslid
     */
    public void loadSlid() {
        login_view_right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        DownX = event.getX();//float DownX
                        DownY = event.getY();//float DownY

                        break;
                    case MotionEvent.ACTION_MOVE:
                        float moveX = event.getX() - DownX;//X轴距离
                        float moveY = event.getY() - DownY;//y轴距离
                        if (moveX < -5) {
                            drawerlayout.openDrawer(Gravity.RIGHT);
                            ll_point.setVisibility(View.GONE);
                        }

                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return true;
            }
        });
    }

    @OnClick(R.id.tv_guid_forget_pwd)
    public void forgetPwd(View view) {

        startActivity(new Intent(this, ForgetPwdActivity.class));
    }

    @OnClick(R.id.guid_wechat_img)
    public void wechatRegister(View view) {
        mLogin3PQQHelper.updateUserInfo(SHARE_MEDIA.WEIXIN);
        platpormTag = 2;
    }

    @OnClick(R.id.guid_qq_img)
    public void qqRegister(View view) {
        mLogin3PQQHelper.updateUserInfo(SHARE_MEDIA.QQ);
        platpormTag = 1;
    }

    @OnClick(R.id.guid_register_img)
    public void dtRegister(View view) {
        startIntent(RegisterActivity.class);
    }


    @Override
    public void onPageScrolled(int i, float v, int arg2) {
        if (i == (mDataSize) && v == 0.0 && arg2 == 0) {
            viewCount += 1;
        } else {
            viewCount = 0;
        }
        //LogUtils.e("------login---i=" + i +",v="+ v+ ",arg2="+arg2+",viewCount="+ viewCount );
        if (i == (mDataSize) && v == 0.0 && arg2 == 0 && viewCount > 1) {
            //LogUtils.e("------login--onTouch--1-" + (mDataSize));
            ll_point.setVisibility(View.GONE);
        } else {
            //LogUtils.e("------login--onTouch--2-" + (i) + viewCount);
            ll_point.setVisibility(View.VISIBLE);
            hasExit = false;
        }
    }


    @Override
    public void onPageSelected(int i) {
        setCurPoint(i);
        if (i == (mDataSize - 1)) {
            login_view_right.setVisibility(View.VISIBLE);
            loadSlid();
        } else {
            login_view_right.setVisibility(View.GONE);
        }

    }

    @Override
    public void onPageScrollStateChanged(int i) {


    }

    /**
     * 初始化点
     */
    private void initPoint() {
        mImageViews = new ImageView[mViewCount];
        LinearLayout.LayoutParams img_layoutparams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        img_layoutparams.rightMargin = 15;
        img_layoutparams.width = 15;
        img_layoutparams.height = 15;
        ll_point.removeAllViews();

        for (int i = 0; i < mViewCount; i++) {
            mImageViews[i] = new ImageView(context);
            mImageViews[i].setLayoutParams(img_layoutparams);
            mImageViews[i].setImageResource(R.drawable.guidlogin_point_bg);
            ll_point.addView(mImageViews[i]);
        }
        mCurSel = 0;
        mImageViews[mCurSel].setEnabled(false);
    }

    /**
     * @param index
     */
    private void setCurPoint(int index) {
        if (index < 0 || index > mViewCount - 1 || mCurSel == index) {
            return;
        }
        mImageViews[mCurSel].setEnabled(true);
        mImageViews[index].setEnabled(false);
        mCurSel = index;
    }

}
