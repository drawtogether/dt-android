package com.dt.app.ui.menu;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.FragmentAdapter;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.fragment.menu.PointHistoryFragment;
import com.dt.app.fragment.menu.PointStatisticsFragment;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ViewPagerCompat;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 收藏画
 *
 * @author libin
 */
public class PointHistoryActivity extends BaseFragmentActivity implements OnClickListener {

    private ViewPagerCompat mPageVp;

    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private FragmentAdapter mFragmentAdapter;

    /**
     * Fragment
     */
    private PointStatisticsFragment pointStatisticsFragment;
    private PointHistoryFragment pointHistoryFragment;

    /**
     * 屏幕的宽度
     */
    private int screenWidth;

    private ActionBarView mActionBarView;

    @ViewInject(R.id.ll_point_statistics)
    private LinearLayout ll_point_statistics;
    @ViewInject(R.id.tv_point_statistics_title)
    private TextView tv_point_statistics;
    @ViewInject(R.id.ll_point_history)
    private LinearLayout ll_point_history;
    @ViewInject(R.id.tv_point_history_title)
    private TextView tv_point_history;

    private boolean isPointStatistics = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_point_history_main);
        ViewUtils.inject(this);

        mActionBarView = new ActionBarView(this);
        mActionBarView.setBackground(R.color.black);
        mActionBarView.setCenterTitle(R.mipmap.dt_back_w, R.color.white, "积分换礼", ActionBarView.FINISH_TYPE_4);

        findById();
        init();

        changeTextColor();
    }

    private void findById() {
        mPageVp = (ViewPagerCompat) this.findViewById(R.id.id_page_vp);
        mPageVp.setViewTouchMode(true);

        ll_point_history.setOnClickListener(this);
        ll_point_statistics.setOnClickListener(this);
    }

    private void init() {
        pointStatisticsFragment = new PointStatisticsFragment();
        pointHistoryFragment = new PointHistoryFragment();

        mFragmentList.add(pointStatisticsFragment);
        mFragmentList.add(pointHistoryFragment);

        mFragmentAdapter = new FragmentAdapter(
                this.getSupportFragmentManager(), mFragmentList);
        mPageVp.setAdapter(mFragmentAdapter);
        mPageVp.setCurrentItem(0);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_point_statistics:
                isPointStatistics = true;
                changeTextColor();
                mPageVp.setCurrentItem(0);
                break;
            case R.id.ll_point_history:
                isPointStatistics = false;
                changeTextColor();
                mPageVp.setCurrentItem(1);
                break;
        }
    }

    private void changeTextColor() {
        if (isPointStatistics) {
            tv_point_statistics.setTextColor(Color.parseColor("#F96363"));
            tv_point_history.setTextColor(getResources().getColor(R.color.gray));
        } else {
            tv_point_history.setTextColor(Color.parseColor("#F96363"));
            tv_point_statistics.setTextColor(getResources().getColor(R.color.gray));
        }
    }
}
