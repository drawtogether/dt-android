package com.dt.app.ui.works;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.Common2Adapter;
import com.dt.app.bean.EveryAdResponse;

import java.util.ArrayList;

/**
 * Created by Besttone on 2016/1/28.
 */
public class EveryAdAdapter2 extends Common2Adapter<EveryAdResponse.Theme2.OutCell> {

    private Context context;

    private ArrayList<EveryAdResponse.Theme2.OutCell> outCells;

    public final int VIEW_TYPE_COUNT = 3;

    public final int VIEW_TYPE_TITLE = 0;

    public final int VIEW_TYPE_CONTENT = 1;

    public final int VIEW_TYPE_IMAGE = 2;

    private int mWidth;

    public EveryAdAdapter2(Context context, ArrayList<EveryAdResponse.Theme2.OutCell> outCells) {
        super(context, outCells);
        this.context = context;
        this.outCells = outCells;

        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        mWidth = wm.getDefaultDisplay().getWidth();
    }

    @Override
    public int getItemViewType(int position) {
        EveryAdResponse.Theme2.OutCell outCell = outCells.get(position);
        if (outCell.getCells().get(0).getType().equals(EveryAdResponse.Theme2.OutCell.Cell.TITLE)) {
            return VIEW_TYPE_TITLE;
        } else if (outCell.getCells().get(0).getType().equals(EveryAdResponse.Theme2.OutCell.Cell.CONTENT)) {
            return VIEW_TYPE_CONTENT;
        } else if (outCell.getCells().get(0).getType().equals(EveryAdResponse.Theme2.OutCell.Cell.IMAGE)) {
            return VIEW_TYPE_IMAGE;
        } else {
            return VIEW_TYPE_TITLE;
        }
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public int getCount() {
        if (outCells != null) {
            return outCells.size();
        }
        return 0;
    }

    @Override
    public EveryAdResponse.Theme2.OutCell getItem(int i) {
        if (outCells != null) {
            return outCells.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        int type = getItemViewType(i);
        Holder holder;
        if (view == null) {
            holder = new Holder();
            if (type == VIEW_TYPE_TITLE) {
                view = LayoutInflater.from(context).inflate(R.layout.textview_title_item_layout, null);
                holder.textView = (TextView) view;
            } else if (type == VIEW_TYPE_CONTENT) {
                view = LayoutInflater.from(context).inflate(R.layout.textview_content_item_layout, null);
                holder.textView = (TextView) view;
            } else if (type == VIEW_TYPE_IMAGE) {
                view = LayoutInflater.from(context).inflate(R.layout.imageview_item_layout, null);
                holder.imageview = (ImageView) view.findViewById(R.id.imageView);
            }
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        EveryAdResponse.Theme2.OutCell outCell = getItem(i);
        if (outCell.getCellCount() == 1) {
            EveryAdResponse.Theme2.OutCell.Cell cell = outCell.getCells().get(0);
            if (type == VIEW_TYPE_TITLE) {
                TextView textView = holder.textView;
                textView.setText(cell.getText());
                textView.setBackgroundColor(Color.WHITE);
            } else if (type == VIEW_TYPE_CONTENT) {
                TextView textView = holder.textView;
                textView.setText(cell.getText());
                if (!TextUtils.isEmpty(cell.getAlign())) {
                    if (cell.getAlign().equals("center")) {
                        textView.setGravity(Gravity.CENTER);
                    } else if (cell.getAlign().equals("left")) {
                        textView.setGravity(Gravity.LEFT);
                    } else if (cell.getAlign().equals("right")) {
                        textView.setGravity(Gravity.RIGHT);
                    }
                }
                if (!TextUtils.isEmpty(cell.getBgcolor())) {
                    textView.setBackgroundColor(Color.parseColor("#" + cell.getBgcolor()));
                }

                if (!TextUtils.isEmpty(cell.getColor())) {
                    if (cell.getColor().equals("black"))
                        textView.setTextColor(Color.BLACK);
                }
            } else if (type == VIEW_TYPE_IMAGE) {
                ViewGroup.LayoutParams params = holder.imageview.getLayoutParams();
                params.height = (cell.getH() * mWidth) / cell.getW();
                params.width = mWidth;

                holder.imageview.setLayoutParams(params);
                mBitmapUtils.display(holder.imageview, cell.getImageUrl() + "?imageMogr2/thumbnail/" + width);
            }
        }
        return view;
    }

    static class Holder {
        ImageView imageview;

        TextView textView;
    }
}
