package com.dt.app.ui.points;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 兑换成功界面
 * 
 * @author Administrator
 * 
 */
public class ExchangeSuccessActivity extends BaseActivity {

	@ViewInject(R.id.tv_exchange_look)
	private TextView tv_exchange_look;
	@ViewInject(R.id.iv_success_close)
	private ImageView iv_success_close;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exchange_success);
		ViewUtils.inject(this);

		initView();

	}

	private void initView() {
		try {
			tv_exchange_look.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
				}
			});
			iv_success_close.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
