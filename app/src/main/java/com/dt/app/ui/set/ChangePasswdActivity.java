package com.dt.app.ui.set;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTSysSettings;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.Md5Util;
import com.dt.app.utils.Utils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

/**
 * 修改密码页面
 */
public class ChangePasswdActivity extends BaseActivity {
    private Activity context;
    @ViewInject(R.id.tv_back)
    private TextView tv_back;
    //当前密码
    @ViewInject(R.id.et_now_pd)
    public EditText et_now_pd;
    //六位新密码
    @ViewInject(R.id.et_new_pd)
    public EditText et_new_pd;
    //再次输入新密码
    @ViewInject(R.id.et_new_pd_again)
    public EditText et_new_pd_again;
    //页面
    @ViewInject(R.id.ll_main)
    private LinearLayout ll_main;
    //保存
    @ViewInject(R.id.tv_save)
    private TextView tv_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.view_ac_changepswd);
        ViewUtils.inject(this);
        context = this;

        initView();

        ChangPwdHelper changPwdHelper = new ChangPwdHelper(context);
        changPwdHelper.editorListener();
    }

    private void initView() {

    }

    @OnClick(R.id.tv_back)
    public void back(View view) {
        finish();
    }

    @OnClick(R.id.ll_main)
    public void main(View view) {
        Utils.hide(view, context);
    }

    @OnClick(R.id.tv_save)
    public void save(View view) {
        if (TextUtils.isEmpty(Utils.getString(et_now_pd))) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "当前密码不能为空");
        } else if (Utils.getString(et_now_pd).length() < 6) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "当前密码必须为六位密码");
        } else if (TextUtils.isEmpty(Utils.getString(et_new_pd))) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "新密码不能为空");
        } else if (Utils.getString(et_new_pd).length() < 6) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "新密码必须为六位密码");
        } else if (TextUtils.isEmpty(Utils.getString(et_new_pd_again))) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "再次输入的新密码不能为空");
        } else if (Utils.getString(et_new_pd_again).length() < 6) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "再次输入的密码必须为六位密码");
        } else if (Utils.getString(et_now_pd).equals(Utils.getString(et_new_pd))) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "新密码不能与当前密码相同");
        } else if (!Utils.getString(et_new_pd).equals(Utils.getString(et_new_pd_again))) {
            Utils.showWrongDialog(view, ChangePasswdActivity.this, "两次新密码输入不一致");
        } else {
//			Utils.showWrongDialog(view,ChangePasswdActivity.this,"当前密码输入不正确");
            setPassword();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void setPassword() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("oldPassword", Md5Util.strMD5(Utils.getString(et_now_pd)));
        data.put("newPassword", Md5Util.strMD5(Utils.getString(et_new_pd)));
        RequestApi.postCommon(this, Constant.URL.DTSettingChangePd, data,
                new ResultLinstener<DTSysSettings>() {
                    @Override
                    public void onSuccess(DTSysSettings obj) {
                        if (obj != null) {
                            Utils.showWrongDialog(et_now_pd, ChangePasswdActivity.this, "密码设置已成功");
                        }
                    }

                    @Override
                    public void onFailure(String obj) {

                    }

                    @Override
                    public void onException(String exception) {
                    }
                }, new DTSysSettings());
    }
}
