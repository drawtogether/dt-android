package com.dt.app.ui.works;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.ImageTextAdapter;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.Artist;
import com.dt.app.bean.Page;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PullToRefreshListViewUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.PullToRefreshView;
import com.dt.app.view.PullToRefreshView.OnFooterRefreshListener;
import com.dt.app.view.PullToRefreshView.OnHeaderRefreshListener;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 喜欢
 *
 * @author libin
 */
public class WorkLovesActivity extends BaseActivity {

    @ViewInject(R.id.tv_love_count)
    private TextView tv_love_count;
    @ViewInject(R.id.gv_love)
    private GridView gv_love;

    private ImageTextAdapter mAdapter;
    private List<Member> members;

    private Page page;
    private PullToRefreshListViewUtils<GridView> mPullToRefreshListViewUtils;
    private ActionBarView actionbar_base;

    private boolean donation;

    /*@ViewInject(R.id.main_pull_refresh_view)
    private PullToRefreshView main_pull_refresh_view;*/

    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_work_loves_main);
        ViewUtils.inject(this);

        initView();

        loadData(1);

    }

    private void initView() {
        try {
            actionbar_base = new ActionBarView(this);
            actionbar_base.setCenterTitle(R.mipmap.dt_back_w, R.color.white, "喜欢", ActionBarView.FINISH_TYPE_4);
            actionbar_base.setBackground(R.color.black);

            donation = getIntent().getBooleanExtra("donation", false);

            members = new ArrayList<UserWorks.Member>();
            mAdapter = new ImageTextAdapter(this, members);
            gv_love.setAdapter(mAdapter);

//			mPullToRefreshListViewUtils = new PullToRefreshListViewUtils<GridView>(gv_love);
//			mPullToRefreshListViewUtils.init(Mode.BOTH);
//			mPullToRefreshListViewUtils.setPullOnRefreshListener(new PullOnRefreshListener<GridView>() {
//				@Override
//				public void onPullDownToRefresh(
//						PullToRefreshBase<GridView> refreshView, int currentPage) {
//					loadData(currentPage);
//				}
//
//				@Override
//				public void onPullUpToRefresh(
//						PullToRefreshBase<GridView> refreshView, int currentPage) {
//					if (page==null) {
//						return;
//					}
//					if (page.getTotalPage()<currentPage) {
//						gv_love.onRefreshComplete();
//						ToastUtils.showTextToast(WorkLovesActivity.this, "没有数据了");
//					}else {
//						loadData(currentPage);
//					}
//				}
//			});
            //这个方法必须要调用initLayoutParams()
//			main_pull_refresh_view.initLayoutParams();
            //main_pull_refresh_view.setPullRefreshEnable(false);

//            main_pull_refresh_view.setOnHeaderRefreshListener(new OnHeaderRefreshListener() {
//                @Override
//                public void onHeaderRefresh(PullToRefreshView view) {
//                    currentPage = 1;
//                    members.clear();
//                    loadData(currentPage);
//                }
//            });
           /* main_pull_refresh_view.setOnFooterRefreshListener(new OnFooterRefreshListener() {

                @Override
                public void onFooterRefresh(PullToRefreshView view) {
                    LogUtils.e("---TotalPage-->"+page.getTotalPage()+",currentPage="+currentPage);
                    if (page == null) {
                        return;
                    }
                    currentPage++;

                    if (page.getTotalPage() < currentPage) {
                        main_pull_refresh_view.onFooterRefreshComplete();
//                        ToastUtils.showTextToast(WorkLovesActivity.this, "没有数据了");
                    } else {
                        loadData(currentPage);
                    }
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("worksId", getIntent().getLongExtra("worksId", 0));
            String url = null;
            if (donation) {
                url = Constant.URL.DTDonationMembers;
            } else {
                url = Constant.URL.DTLikeMembers;
            }
            RequestApi.postCommon(this, url, data,
                    new ResultLinstener<Artist>() {
                        @Override
                        public void onSuccess(Artist obj) {
                            if (obj != null) {
                                if (page == null) {
                                    page = obj.getPager();
                                    tv_love_count.setText("有" + page.getTotalCount() + "位朋友喜欢本幅作品");
                                }
                                if (currentPage == 1) {
                                    members.clear();
                                }

                                members.addAll(obj.getMembers());
                                mAdapter.notifyDataSetChanged();
                               /* if (currentPage == 1) {
                                    main_pull_refresh_view.onHeaderRefreshComplete();
                                } else {
                                    main_pull_refresh_view.onFooterRefreshComplete();
                                }*/
                            }
                        }

                        @Override
                        public void onFailure(String obj) {
//							gv_love.onRefreshComplete();
                            //main_pull_refresh_view.onFooterRefreshComplete();
                        }

                        @Override
                        public void onException(String exception) {
                            //main_pull_refresh_view.onFooterRefreshComplete();
                        }
                    }, new Artist());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}