package com.dt.app.ui.menu;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.QiNiuToken;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.Web;
import com.lidroid.xutils.ViewUtils;

/**
 * 动态
 * 
 * @author libin
 */
public class WebActivity extends BaseActivity {
	private Web mWeb;
	private JSInterface jsInterface;
	private Activity activity;
	private ActionBarView mActionBarView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web);
		ViewUtils.inject(this);
		activity = this;
		String title = getIntent().getStringExtra("title");
		mActionBarView = new ActionBarView(this);
        mActionBarView.setCenterTitle(R.mipmap.dt_back_w,R.color.white, title, ActionBarView.FINISH_TYPE_4);
        
        mWeb = new Web(activity);
		try {
			// 分享
			if (getIntent().getBooleanExtra("share", false)) {
				jsInterface = new JSInterface();
				mWeb.setJavascriptInterface(jsInterface, "jsinterface");
				jsInterface.setShareListener(new WebClientShare());
				// share = new UmengShare(this);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		//邀请好友
		if (getIntent().getBooleanExtra("invite", false)) {
			invited();
		}else {
			mWeb.loadUrl(getIntent().getStringExtra("url"));
		}
	}

	public class WebClientShare implements ShareListener {
		@Override
		public void jsShare(String content, String url, String exra) {
			/*
			 * if (share!=null) { share.dismissShareDialog();
			 * share.showShareUI(exra, content, url); }
			 */
		}
	}

	public class JSInterface {
		public ShareListener shareListener;

		public void setShareListener(ShareListener shareListener) {
			this.shareListener = shareListener;
		}

		@JavascriptInterface
		public void share(String content, String url, String exra) {
			if (shareListener != null) {
				shareListener.jsShare(content, url, exra);
			}
		}

	}

	public interface ShareListener {
		public void jsShare(String content, String url, String exra);
	}
	
	private void invited(){
    	try {
			Map<String, Object> data = new HashMap<String, Object>();
			RequestApi.postCommon(this, Constant.URL.DTUserInvite, data,
					new ResultLinstener<String>() {
						@Override
						public void onSuccess(String obj) {

						}

						@Override
						public void onFailure(String obj) {
						}

						@Override
						public void onException(String exception) {
							mWeb.loadContent(exception);
						}

      }, new String());
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
}
