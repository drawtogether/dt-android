package com.dt.app.ui.set;

import java.util.HashMap;

import android.app.Activity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.dt.app.bean.DTUser;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.Utils;
import com.lidroid.xutils.util.LogUtils;

/**
 * Created by caiyuhao on 15-12-1.
 */
public class ChangPhoneHelper {
    ChangePhoneActivity activity;

    public ChangPhoneHelper(Activity act) {
        activity = (ChangePhoneActivity) act;
    }

    public void editorListener() {
        activity.et_now_phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                String currentPwd = PreferencesUtils.getString(activity, Constant.PrefrencesPt.DTpwd);
                if (id == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(Utils.getString(activity.et_now_phone))) {
                        Utils.showWrongDialog(activity.et_now_phone, activity, "当前手机号不能为空");
                    } else if (!Utils.isMobile(activity.et_now_phone.getText().toString())) {
                        Utils.showWrongDialog(activity.et_now_phone, activity, "请输入正确手机号");
                    } else {
                        activity.et_new_phone.requestFocus();
                    }
                }
                return true;
            }
        });
        activity.et_new_phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(Utils.getString(activity.et_new_phone))) {
                        Utils.showWrongDialog(activity.et_new_phone, activity, "新手机号不能为空");
                    } else if (!Utils.isMobile(activity.et_now_phone.getText().toString())) {
                        Utils.showWrongDialog(activity.et_new_phone, activity, "请输入正确手机号");
                    } else {
                        activity.et_new_phone_code.requestFocus();
                    }
                }
                return true;
            }
        });
        activity.et_new_phone_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

                    if (TextUtils.isEmpty(Utils.getString(activity.et_new_phone_code))) {
                        Utils.showWrongDialog(activity.et_new_phone_code, activity, "验证码不能为空");
                    } else if (Utils.getString(activity.et_new_phone_code).length() < 6 || Utils.getString(activity.et_new_phone_code).length() > 6) {
                        Utils.showWrongDialog(activity.et_new_phone_code, activity, "验证码为六位密码");
                    } else {
                        //activity.setPassword();
                    	update();
                    }
                }
                return true;
            }
        });
    }

    private void update() {
		try {
			String mobilephone = activity.et_new_phone.getText().toString().trim();
			String code = activity.et_new_phone_code.getText().toString().trim();
//			if (TextUtils.isEmpty(mobilephone)) {
//				ToastUtils.showTextToast(activity, "电话号码不能为空");
//			}
//			if (TextUtils.isEmpty(code)) {
//				ToastUtils.showTextToast(activity, "验证码不能为空");
//			}
			
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put("mobilephone", mobilephone);
			data.put("code", code);
			RequestApi.commonRequest(activity, Constant.URL.DTUserProfileUpdate, data,
					new ResultLinstener<DTUser>() {
						@Override
						public void onSuccess(DTUser obj) {
							if (obj!=null && obj.getCode()==1) {
								MemberBean memberBean = obj.getData().getMember();
								ToastUtils.showTextToast(activity, "修改成功");
								PreferencesUtils.putString(activity, Constant.PrefrencesPt.DTmobilephone, memberBean.getMobilephone());
								activity.finish();
							}else {
								if (obj!=null) {
									ToastUtils.showTextToast(activity, obj.getMessage());
								}
							}
						}

						@Override
						public void onFailure(String obj) {
							activity.finish();
						}
						@Override
						public void onException(String exception) {
							activity.finish();
						}
					}, new DTUser());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
