package com.dt.app.ui.menu;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dt.app.R;
import com.dt.app.adapter.NoticeListAdapter;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTNotice;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.library.swipemenulistview.SwipeMenu;
import com.library.swipemenulistview.SwipeMenuCreator;
import com.library.swipemenulistview.SwipeMenuItem;
import com.library.swipemenulistview.SwipeMenuListView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

/**
 * Created by caiyuhao on 15-9-13.
 */
public class MsgActivity extends BaseActivity {
    @ViewInject(R.id.dt_msg_list)
    private SwipeMenuListView dt_msg_list;
    private NoticeListAdapter mNoticeListAdapter;
    private Activity mActivity;
    @ViewInject(R.id.iv_back_home_icon)
    private ImageView iv_back_home_icon;
    @ViewInject(R.id.tv_no_read_number)
    private TextView tv_no_read_number;
    private static int numberNoRead = 0;
    private  List<DTNotice.NoticeBean> noticeBeanList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_menu_msglist);
        ViewUtils.inject(this);
        mActivity = this;

        loadData();

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                deleteItem.setBackground((R.color.red_light));
                deleteItem.setWidth(dp2px(100));
                deleteItem.setIcon(R.mipmap.dt_cancel_w);
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        dt_msg_list.setMenuCreator(creator);

        // step 2. listener item click event
        dt_msg_list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        // delete
                        DTNotice.NoticeBean  noticeBean = noticeBeanList.get(position);
                        deleteData(noticeBean.getId());
                        break;

                }
                return false;
            }
        });

        // test item long click
        dt_msg_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                //Toast.makeText(getApplicationContext(), position + " setOnItemClickListener click", Toast.LENGTH_SHORT).show();
                DTNotice.NoticeBean noticeBean = noticeBeanList.get(position);
               LogUtils.e("--------getContentType------->" + noticeBean.getContentType());
            }
        });
    }

    private void loadData() {
        try {
            HashMap<String, Object> data = new HashMap<String, Object>();
            RequestApi.commonRequest(this, Constant.URL.DTnoticeList, data, new ResultLinstener<DTNotice>() {
                @Override
                public void onSuccess(DTNotice obj) {
                    noticeBeanList = obj.getData().getNotices();
                    for (DTNotice.NoticeBean noticeBean : noticeBeanList) {
                        if (noticeBean.getIsRead().equals(0)) {
                            numberNoRead++;
                        }
                    }
                    mNoticeListAdapter = new NoticeListAdapter(mActivity, obj.getData().getNotices());
                    dt_msg_list.setAdapter(mNoticeListAdapter);
                    if (numberNoRead > 0) {
                        tv_no_read_number.setText(numberNoRead + "");
                    }
                }

                @Override
                public void onFailure(String obj) {
                }

                @Override
                public void onException(String exception) {

                }
            }, new DTNotice());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * delete
     */
    private void deleteData(final long id) {
        try {
            HashMap<String, Object> data = new HashMap<String, Object>();
            data.put("ids", id);
            RequestApi.commonRequest(this, Constant.URL.DTnoticeDelete, data, new ResultLinstener<DTNotice>() {
                @Override
                public void onSuccess(DTNotice obj) {
                    Iterator<DTNotice.NoticeBean> iterator = noticeBeanList.iterator();
                    while (iterator.hasNext()){

                        DTNotice.NoticeBean noticeBean = iterator.next();

                             if (noticeBean.getId().equals(id)){
                                  iterator.remove();
                              }
                    }
                    mNoticeListAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(String obj) {
                }

                @Override
                public void onException(String exception) {

                }
            }, new DTNotice());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void msgDetail(int id) {
        try {
            HashMap<String, Object> data = new HashMap<String, Object>();
            data.put("id", id);
            RequestApi.commonRequest(this, Constant.URL.DTnoticeInfo, data, new ResultLinstener<DTNotice>() {
                @Override
                public void onSuccess(DTNotice obj) {

                }

                @Override
                public void onFailure(String obj) {
                }

                @Override
                public void onException(String exception) {

                }
            }, new DTNotice());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.iv_back_home_icon)
    public void backImg(View view) {
        finish();
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
}
