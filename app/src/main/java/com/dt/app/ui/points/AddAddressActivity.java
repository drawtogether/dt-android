package com.dt.app.ui.points;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.Address;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ActionBarView.ActionBarLinstener;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 添加收货地址
 * 
 * @author Administrator
 * 
 */
public class AddAddressActivity extends BaseActivity {
	@ViewInject(R.id.et_address_username)
	private EditText et_address_username;
	@ViewInject(R.id.et_address_userphone)
	private EditText et_address_userphone;
	@ViewInject(R.id.et_address)
	private TextView et_address;
	@ViewInject(R.id.et_address_details)
	private EditText et_address_details;
	
	private ActionBarView mActionBarView;

	private String consignee;// 收货人名称
	private String mobilephone;// 收货人手机号
	private String cityName;// 收货人所在城市
	private String address;// 收货人详细地址
	private String postcode;// 邮编可为空
	private Integer isDefault;// 是否默认，0否，1是可为空
	
	private Address editAddress;
	private boolean manageAddrss =false;//true：从地址管理过来的
	private boolean exchangeAddrss =false;//true：从兑换页面过来的
	private static final int ADD_ADDRESS_REQ=10;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_address_main);
		ViewUtils.inject(this);
		editAddress = getIntent().getParcelableExtra("editAddress");
		manageAddrss = getIntent().getBooleanExtra(AddressManageActivity.ADDRESS_MANAGE, false);
		exchangeAddrss = getIntent().getBooleanExtra(PointExchageActivity.POINT_EXCHAGE, false);
		
		initView();

	}

	private void initView() {
		try {
			mActionBarView = new ActionBarView(this);
			mActionBarView.setBackground(R.color.black);
			String title = "添加地址";
			if (editAddress!=null) {
				title = "修改地址";
			}
			mActionBarView.setCenterTitle(R.mipmap.dt_back_w, R.color.white,
					title, ActionBarView.FINISH_TYPE_8);
			mActionBarView.addImageAction(R.id.title_bar_right_img1,
					R.mipmap.right_white);
			mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
				@Override
				public void onClick(View view) {
					switch (view.getId()) {
					case R.id.title_bar_right_img1:
						save();
						break;
					case R.id.rl_back_home_layout:
						if (editAddress!=null) {
							ToastUtils.showDialog(AddAddressActivity.this, "是否放弃修改?", new OnClickListener() {
								@Override
								public void onClick(View v) {
									if (v.getId() == R.id.ll_image_cancel) {
										ToastUtils.dismissDialog();
									}else if (v.getId() == R.id.ll_image_confirm){
										ToastUtils.dismissDialog();
										finish();
									}
								}
							});
						}else {
							finish();
						}
						break;
					}
				}
			});
			if (editAddress!=null) {
				et_address_username.setText(editAddress.getConsignee());
				et_address_userphone.setText(editAddress.getMobilephone());
				et_address.setText(editAddress.getCityName());
				et_address_details.setText(editAddress.getAddressAll());
			}
			et_address.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent mIntent = new Intent(AddAddressActivity.this,DateActivity.class);
					mIntent.putExtra("address", et_address.getText().toString().trim());
					startActivityForResult(mIntent, ADD_ADDRESS_REQ);
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void save() {
		try {
			consignee = validateEdittextEmpty(this, et_address_username,
					"收货人姓名不能为空");
			if (consignee == null) {
				return;
			}
			
			mobilephone = validateEdittextEmpty(this, et_address_userphone,
					"手机号码不能为空");
			if (mobilephone == null) {
				return;
			}
			
			cityName = et_address.getText().toString();
			if (cityName == null) {
				ToastUtils.showTextToast(AddAddressActivity.this, "省/市/行政区不能为空");
				return;
			}
			
			address = validateEdittextEmpty(this, et_address_details,
					"详细地址不能为空");
			if (address == null) {
				return;
			}

			Map<String, Object> data = new HashMap<String, Object>();
			data.put("consignee", consignee);
			data.put("mobilephone", mobilephone);
			data.put("cityName", cityName);
			data.put("address", address);
			data.put("postcode", "2000");
			data.put("isDefault", "0");
			if (editAddress!=null) {
				data.put("id", editAddress.getId());
			}
			
			RequestApi.postCommon_List(this, Constant.URL.DTAddressSave, data,
					new ResultLinstener<List<Address>>() {

						@Override
						public void onSuccess(List<Address> obj) {
							AddressDatas.fillAddress(obj);
							finish();
							if (editAddress!=null) {
								ToastUtils.showTextToast(AddAddressActivity.this, "修改成功");
							}
							if (manageAddrss || exchangeAddrss) {
								ToastUtils.showTextToast(AddAddressActivity.this, "添加成功");
							}
						}

						@Override
						public void onFailure(String obj) {

						}

						@Override
						public void onException(String exception) {

						}

					}, new Address());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String validateEdittextEmpty(Context context, EditText editText,
			String msg) {
		String text = editText.getText().toString().trim();
		if (TextUtils.isEmpty(text)) {
			ToastUtils.showTextToast(context, msg);
			return null;
		}
		return text;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ADD_ADDRESS_REQ && resultCode==RESULT_OK) {
			String addres = data.getStringExtra("address");
			if (addres!=null) {
				et_address.setText(addres);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void finish() {
		super.finish();
		if (manageAddrss || editAddress!=null) {
			overridePendingTransition(R.anim.alpha_500_out, R.anim.right_out);
		}
		
	}
}
