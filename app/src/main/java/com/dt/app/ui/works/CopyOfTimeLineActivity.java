//package com.dt.app.ui.works;
//
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.animation.AnimationUtils;
//import android.view.animation.LayoutAnimationController;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.dt.app.R;
//import com.dt.app.base.BaseActivity;
//import com.dt.app.bean.TimeLine;
//import com.dt.app.bean.TimeLine.TimeLineMonth;
//import com.dt.app.common.RequestApi;
//import com.dt.app.common.ResultLinstener;
//import com.dt.app.utils.Constant;
//import com.lidroid.xutils.ViewUtils;
//import com.lidroid.xutils.view.annotation.ViewInject;
//
///**
// * 时间轴
// * 
// * @author libin
// * 
// */
//public class CopyOfTimeLineActivity extends BaseActivity implements OnClickListener{
//
//	@ViewInject(R.id.iv_back_home_icon)
//	private ImageView iv_back_home_icon;
//
//	// @ViewInject(R.id.expandableListView)
//	// private ExpandableListView expandableListView;
//	//
//
//	// private TimeLineAdapter mTimeLineAdapter;
//	// public List<TimeLineYear> timeLineYears;
//	// public Map<Integer, List<TimeLineMonth>> lineMonthMap;
//
//	@ViewInject(R.id.ll_time_line)
//	private LinearLayout ll_time_line;
//
//	private LayoutInflater mInflater;
//
//	private LinearLayout mLinearLayout;
//	private Map<Integer, Integer> yearViewMap = new HashMap<Integer, Integer>();
//
//	private int currentYear;
//	
//	private LinearLayout lastView;
//	private TextView lastTextView;
//	
//	private int[] timeYears;
//
//	
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.dt_time_line_main);
//		ViewUtils.inject(this);
//		mInflater = LayoutInflater.from(this);
//
//		init();
//		
//		load();
//	}
//
//	
//	private void init() {
//		iv_back_home_icon.setOnClickListener(this);
//		
//		currentYear = Calendar.getInstance().get(Calendar.YEAR);
//	}
//
//	
//	public void getView(final TimeLine timeLine,final int index) {
//		
//		View view = mInflater.inflate(R.layout.dt_time_line_year_item, null);
//		ll_time_line.addView(view);
//		
//		mLinearLayout = new LinearLayout(this);
//		mLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(
//				LinearLayout.LayoutParams.MATCH_PARENT,
//				LinearLayout.LayoutParams.WRAP_CONTENT));
//		mLinearLayout.setOrientation(LinearLayout.VERTICAL);
//		mLinearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
////		List<TimeLineMonth> mLineMonths = timeLine.getMonths();
////		if (mLineMonths!=null && mLineMonths.size()>0) {
////			for (int j = 0; j < mLineMonths.size(); j++) {
////				final TimeLineMonth month = mLineMonths.get(j);
////				View monthView = null;
////				if(j%2==0){
////					monthView = mInflater.inflate(R.layout.dt_time_line_month_item_left, null);
////				}else {
////					monthView =mInflater.inflate(R.layout.dt_time_line_month_item_right, null);
////				}
////				monthView.setOnClickListener(new OnClickListener() {
////					@Override
////					public void onClick(View v) {
////						Intent mIntent = new Intent(CopyOfTimeLineActivity.this,CollectionPaintActivity.class);
////						mIntent.putExtra("year", timeLine.getYear());
////						mIntent.putExtra("month", month.getCollectMonth());
////						startActivity(mIntent);
////					}
////				});
////				TextView textView = (TextView) monthView
////						.findViewById(R.id.tv_time_line_month);
////				textView.setText("" + month.getCollectMonth());
////				mLinearLayout.addView(monthView);
////			}
////		}
//		mLinearLayout.measure(0, 0);
//		 
//		yearViewMap.put(index, mLinearLayout.getMeasuredHeight());
//		mLinearLayout.setVisibility(View.GONE);
//		ll_time_line.addView(mLinearLayout);
//
//		
//		final TextView textView = (TextView) view.findViewById(R.id.tv_time_line_year);
//		textView.setText("" + timeLine.getYear());
// 
//			textView.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					int in = index+1;
//					if (lastTextView!=null) {
//						lastTextView.setBackgroundResource(R.drawable.circle_black);
//					}
//					textView.setBackgroundResource(R.drawable.oval_red);
//			 
//					if (lastTextView == textView) {//是当前view
//						textView.setBackgroundResource(R.drawable.circle_black);
//						LayoutAnimationController lac =AnimationUtils.loadLayoutAnimation(CopyOfTimeLineActivity.this, R.anim.list_anim_layout_down);
//						lac.setOrder(LayoutAnimationController.ORDER_NORMAL);
//					    lac.setDelay(0.1f);
//					    lastView.setLayoutAnimation(lac);
//					    lastView.postDelayed(new Runnable() {
//							@Override
//							public void run() {
//								lastView.setVisibility(View.GONE);
//								lastTextView=null;
//							}
//						}, 500);
//					}
//
//					if (lastTextView != textView) {
//						LinearLayout view = (LinearLayout) ll_time_line.getChildAt(in);
//						view.setVisibility(View.VISIBLE);
//						LayoutAnimationController lac =AnimationUtils.loadLayoutAnimation(CopyOfTimeLineActivity.this, R.anim.list_anim_layout);
//						lac.setOrder(LayoutAnimationController.ORDER_NORMAL);
//					    lac.setDelay(0.1f);
//					    view.setLayoutAnimation(lac);
//						lastView = view;
//						lastTextView = textView;
//					}
//					
//			}
//		});
//		
// 
//
//	}
//	private void load() {
//		try {
//			Map<String, Object> data = new HashMap<String, Object>();
//			RequestApi.postCommon_List(this, Constant.URL.DTTimelineYears, data,
//					new ResultLinstener<List<TimeLine>>() {
//						@Override
//						public void onSuccess(List<TimeLine> obj) {
//							if (obj!=null) {
//								for (int i=0;i<obj.size();i++) {
//									getView(obj.get(i),i);
//								}
//							}
//							 
//						}
//
//						@Override
//						public void onFailure(String obj) {
//							 
//						}
//
//						@Override
//						public void onException(String exception) {
//							 
//						}
//					}, new TimeLine());
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//
//	@Override
//	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.iv_back_home_icon:
//			finish();
//			break;
//
//		default:
//			break;
//		}
//	}
//
//	 
//
//}
