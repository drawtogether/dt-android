package com.dt.app.ui.menu;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;

import com.dt.app.common.threelogin.DTShare;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 邀请好友
 *
 * @author libin
 */
public class InvitedActivity extends BaseActivity implements
        View.OnClickListener {

    @ViewInject(R.id.btn_invited)
    private Button btn_invited;
    @ViewInject(R.id.tv_invited_points)
    private TextView tv_invited_points;
    @ViewInject(R.id.iv_back_home_icon)
    private ImageView iv_back_home_icon;
    DTShare dtShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_invited_main);
        ViewUtils.inject(this);
        initView();
        btn_invited.setEnabled(true);
        String userName = PreferencesUtils.getString(this, Constant.PrefrencesPt.DTnickname);
        tv_invited_points.setText(getString(R.string.share_tips_1) + userName + getString(R.string.share_tips_2));
        //invited();
    }

    private void initView() {
        dtShare = new DTShare(this);
        btn_invited.setEnabled(false);
        btn_invited.setOnClickListener(this);
        iv_back_home_icon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_invited:
//			if (objString!=null) {
                String url = Constant.URL.DTUserInvite;
                Map<String, Object> params = new HashMap<String, Object>();
                String mkeyStr = null;
                try {
                    mkeyStr = URLEncoder.encode(PreferencesUtils.getString(this, Constant.PrefrencesPt.DTmkey), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                params.put("ts", System.currentTimeMillis() / 1000);
                params.put("mkey", mkeyStr);


                //params.put("sign", RequestApi.getSign(temp, url ));
                String sign = RequestApi.getSign(params, url);
                String targetUrl = Constant.URL.DTUserInvite + "?mkey=" + mkeyStr + "&ts=" + (System.currentTimeMillis() / 1000) + "&sign=" + sign;
                //LogUtils.e("---------invited target-->"+targetUrl);
                String nickname = PreferencesUtils.getString(this, Constant.PrefrencesPt.DTnickname);
                if (!TextUtils.isEmpty(nickname)) {
                    dtShare.showShareUI("邀请函", nickname + "邀请您加入DRAW TOGETHER", targetUrl, "", "0");
                } else {
                    dtShare.showShareUI("邀请函", "邀请您加入DRAW TOGETHER", targetUrl, "", "0");
                }
                break;
            case R.id.iv_back_home_icon:
                finish();
                break;
            default:
                break;
        }
    }

    private String objString;

    private void invited() {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            RequestApi.postCommon(this, Constant.URL.DTUserInvite, data,
                    new ResultLinstener<String>() {
                        @Override
                        public void onSuccess(String obj) {
                            btn_invited.setEnabled(true);

                        }

                        @Override
                        public void onFailure(String obj) {
                            btn_invited.setEnabled(true);
                        }

                        @Override
                        public void onException(String exception) {
                            objString = exception;
                            btn_invited.setEnabled(true);
                            LogUtils.e("-------objString---->" + objString);
                        }

                    }, new String());
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}
