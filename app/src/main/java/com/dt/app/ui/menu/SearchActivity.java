package com.dt.app.ui.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;

import com.dt.app.R;
import com.dt.app.adapter.TextAdapter;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.Tag;
import com.dt.app.common.CommonApis;
import com.dt.app.listener.HandleCallbackSimple;
import com.dt.app.utils.InputMethodUtil;
import com.dt.app.utils.ToastUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by caiyuhao on 15-9-13.
 */
public class SearchActivity extends BaseActivity
        implements View.OnClickListener {
    @ViewInject(R.id.img_search)
    private ImageView img_search;
    @ViewInject(R.id.et_search_content)
    private EditText et_search_content;
    @ViewInject(R.id.img_cancel)
    private ImageView img_cancel;
    @ViewInject(R.id.list_search_content)
    private ListView list_search_content;
    private Activity  activity;
    private boolean isCancleHidden = false;
    private  List<Tag>  mTags;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_menu_search);
        ViewUtils.inject(this);
        img_cancel.setOnClickListener(this);

        //et_search_content.setOnClickListener(this);
        loadTags();
        activity = this;
        list_search_content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putString("title", mTags.get(i).getName());
                bundle.putInt("tagId",mTags.get(i).getId());
                startActivityByCalss(SearchActivity.this, SearchResultListActivity.class, bundle);
            }
        });
        et_search_content.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                String et_content = et_search_content.getText().toString();
                if (TextUtils.isEmpty(et_content)){
                    ToastUtils.showTextToast(activity,"搜索内容不能为空");
                }else   if (i == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN){
                    Bundle bundle = new Bundle();
                    bundle.putString("title", et_content);
                    bundle.putInt("tagId",mTags.get(i).getId());
                    startActivityByCalss(SearchActivity.this, SearchResultListActivity.class, bundle);
                }
                return true;
            }
        });
      
        InputMethodUtil.enableSoftInput(this);
       
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_search:
              /*  String search = et_search_content.getText().toString().trim();
                if (!TextUtils.isEmpty(search)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("title", search);
                    startActivityByCalss(SearchActivity.this, SearchResultListActivity.class, bundle);
                }*/
                break;
            case R.id.et_search_content:
            	/*if (isCancleHidden) {
					Intent mIntent = new Intent(this,SearchActivity.class);
					startActivity(mIntent);
				}*/
                break;
            case R.id.img_cancel:
                finish();
                overridePendingTransition(R.anim.slide_in_to_left, R.anim.right_out);
                break;

        }
    }

    /**
     * 获取标签
     */
    private void loadTags() {
        Map<String, Object> data = new HashMap<String, Object>();
        CommonApis.getTags(this, data, new HandleCallbackSimple<Tag>() {
            @Override
            public void onSuccess(List<Tag> t) {
                if (t != null && t.size() > 0) {
                    mTags = t;
                    list_search_content.setAdapter(new TextAdapter(activity, t));
                }
            }
        });
    }
}
