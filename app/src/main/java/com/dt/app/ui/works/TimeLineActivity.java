package com.dt.app.ui.works;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.TimeLine.TimeLineYear;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ZTDeviceInfo;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 时间轴
 *
 * @author libin
 */
public class TimeLineActivity extends BaseActivity implements OnClickListener {

    @ViewInject(R.id.iv_back_home_icon)
    private ImageView iv_back_home_icon;

	@ViewInject(R.id.rl_time_line)
	private RelativeLayout ll_time_line;

	private LayoutInflater mInflater;
	
	private int size = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_time_line_main);
        ViewUtils.inject(this);
        mInflater = LayoutInflater.from(this);

        init();
 
        load();
    }
 

	private void init() {
		iv_back_home_icon.setOnClickListener(this);
//		ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) ll_time_line.getLayoutParams();
//		params.height = (int) (ZTDeviceInfo.getInstance().getHeightDevice()-getResources().getDimension(R.dimen.actionbar_height)
//				+DensityUtil.dip2px(this, 2));
//		params.width = ZTDeviceInfo.getInstance().getWidthDevice();
//		ll_time_line.setLayoutParams(params);
		
	}
 

    public void getView(final TimeLineYear timeLine, final int index) {

        View view = mInflater.inflate(R.layout.dt_time_line_main_item, null);

//		FrameLayout fl_time_line_circle = (FrameLayout) findViewById(R.id.fl_time_line_circle);
        TextView tv_time_line_circle = (TextView) view.findViewById(R.id.tv_time_line_circle);
//		ImageView iv_time_line_dot = (ImageView) view.findViewById(R.id.iv_time_line_dot);
 
		TextView tv_time_line_works = (TextView) view.findViewById(R.id.tv_time_line_works);
		TextView tv_time_line_works_desc = (TextView) view.findViewById(R.id.tv_time_line_works_desc);
		tv_time_line_circle.setText(""+timeLine.getYear());
		tv_time_line_works.setText(""+tv_time_line_works.getText()+timeLine.getCollectionQuantity());
		tv_time_line_works_desc.setText(""+tv_time_line_works_desc.getText()+timeLine.getWorksQuantity());
 
		tv_time_line_circle.setOnClickListener(new TimeLineClick(timeLine));
		tv_time_line_works.setOnClickListener(new TimeLineClick(timeLine));
		tv_time_line_works_desc.setOnClickListener(new TimeLineClick(timeLine));
		
		LinearLayout ll_time_line_bigdot = (LinearLayout) view.findViewById(R.id.ll_time_line_bigdot);
		LinearLayout ll_time_line_bigline = (LinearLayout) view.findViewById(R.id.ll_time_line_bigline);
		if (size-1==index) {
			ll_time_line_bigdot.setVisibility(View.VISIBLE);
			ll_time_line_bigline.setVisibility(View.GONE);
		}
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT, 
				RelativeLayout.LayoutParams.WRAP_CONTENT);
    	view.setId(index+1);
     
    	if (index>0) {
			params.addRule(RelativeLayout.BELOW, index);
		}
    	view.setLayoutParams(params);
		ll_time_line.addView(view);

	}
	private void load() {
		try {
			Map<String, Object> data = new HashMap<String, Object>();
			RequestApi.postCommon_List(this, Constant.URL.DTTimelineYears, data,
					new ResultLinstener<List<TimeLineYear>>() {
						@Override
						public void onSuccess(List<TimeLineYear> obj) {
							if (obj!=null) {
								size = obj.size();
								for (int i=0;i<size;i++) {
									getView(obj.get(i),i);
								}
							}
							 
						}

						@Override
						public void onFailure(String obj) {
							 
						}

						@Override
						public void onException(String exception) {
							 
						}
					}, new TimeLineYear());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_back_home_icon:
			finish();
			break;
		}
	}

	
	private class TimeLineClick implements OnClickListener{
		TimeLineYear timeLine;
		public TimeLineClick(TimeLineYear timeLine){
			this.timeLine = timeLine;
		}
		
		@Override
		public void onClick(View v) {
			Intent mIntent = new Intent(TimeLineActivity.this,CollectionPaintActivity.class);
			mIntent.putExtra("year", timeLine.getYear());
//			mIntent.putExtra("month", month.getCollectMonth());
			startActivity(mIntent);
			LogUtils.e("--------------"+timeLine.getYear());
		}
		
	} 
 
 

}
