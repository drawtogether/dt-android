package com.dt.app.ui.set;

import java.util.HashMap;

import android.app.Activity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.dt.app.bean.DTUser;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.Utils;

/**
 * Created by caiyuhao on 15-12-1.
 */
public class ChangEmailHelper {
    ChangeEmailActivity activity;

    public ChangEmailHelper(Activity act) {
        activity = (ChangeEmailActivity) act;
    }

    public void editorListener() {
        activity.et_now_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                String currentPwd = PreferencesUtils.getString(activity, Constant.PrefrencesPt.DTpwd);
                if (id == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(Utils.getString(activity.et_now_email))) {
                        Utils.showWrongDialog(activity.et_now_email, activity, "当前邮箱不能为空");
                    } else if (!Utils.isEmail(activity.et_now_email.getText().toString())) {
                        Utils.showWrongDialog(activity.et_now_email, activity, "请输入正确邮箱");
                    } else {
                        activity.et_new_email.requestFocus();
                    }
                }
                return true;
            }
        });
        activity.et_new_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(Utils.getString(activity.et_new_email))) {
                        Utils.showWrongDialog(activity.et_new_email, activity, "新邮箱不能为空");
                    } else if (!Utils.isEmail(activity.et_new_email.getText().toString())) {
                        Utils.showWrongDialog(activity.et_new_email, activity, "请输入正确邮箱");
                    } else {
                        activity.et_new_email_code.requestFocus();
                    }
                }
                return true;
            }
        });
        activity.et_new_email_code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {

                    if (TextUtils.isEmpty(Utils.getString(activity.et_new_email_code))) {
                        Utils.showWrongDialog(activity.et_new_email_code, activity, "验证码不能为空");
                    } else if (Utils.getString(activity.et_new_email_code).length() < 6 || Utils.getString(activity.et_new_email_code).length() > 6) {
                        Utils.showWrongDialog(activity.et_new_email_code, activity, "验证码为六位密码");
                    } else {
                    	update();
                    }
                }
                return true;
            }
        });
        
        
    }
    
    private void update() {
 		try {
 			String email = activity.et_new_email.getText().toString().trim();
 			String code = activity.et_new_email_code.getText().toString().trim();
// 			if (TextUtils.isEmpty(email)) {
// 				ToastUtils.showTextToast(activity, "电话号码不能为空");
// 			}
// 			if (TextUtils.isEmpty(code)) {
// 				ToastUtils.showTextToast(activity, "验证码不能为空");
// 			}
 			
 			HashMap<String, Object> data = new HashMap<String, Object>();
 			data.put("email", email);
 			data.put("code", code);
 			RequestApi.commonRequest(activity, Constant.URL.DTUserProfileUpdate, data,
 					new ResultLinstener<DTUser>() {
 						@Override
 						public void onSuccess(DTUser obj) {
 							if (obj!=null && obj.getCode()==1) {
 								MemberBean memberBean = obj.getData().getMember();
 								ToastUtils.showTextToast(activity, "修改成功");
 								PreferencesUtils.putString(activity, Constant.PrefrencesPt.DTemail, memberBean.getEmail());
 								activity.finish();
 							}else {
 								if (obj!=null) {
									ToastUtils.showTextToast(activity, obj.getMessage());
								}
							}
 						}

 						@Override
 						public void onFailure(String obj) {
 							activity.finish();
 						}
 						@Override
 						public void onException(String exception) {
 							activity.finish();
 						}
 					}, new DTUser());

 		} catch (Exception e) {
 			e.printStackTrace();
 		}
 	}
}
