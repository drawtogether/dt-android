package com.dt.app.ui.points;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.JifenDetail;
import com.dt.app.bean.JifenDetail.JifenArtist;
import com.dt.app.bean.JifenDetail.JifenGoods;
import com.dt.app.bean.Photo;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.CommonApis;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ArrawLinearLayout;
import com.dt.app.view.ArrawLinearLayout.ArrawLayoutListener;
import com.dt.app.view.BannerView;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 积分换礼
 *
 * @author libin
 */
public class PointGiftActivity extends BaseActivity implements OnClickListener {


    @ViewInject(R.id.tv_exchange_title)
    private TextView tv_exchange_title;
    @ViewInject(R.id.tv_exchange_point)
    private TextView tv_exchange_point;
    @ViewInject(R.id.tv_exchange_point_desc)
    private TextView tv_exchange_point_desc;
    @ViewInject(R.id.tv_exchange_good_desc)
    private ArrawLinearLayout tv_exchange_good_desc;
    @ViewInject(R.id.tv_exchange_good_hidden_desc)
    private TextView tv_exchange_good_hidden_desc;
    @ViewInject(R.id.tv_exchange_server_desc)
    private ArrawLinearLayout tv_exchange_server_desc;
    @ViewInject(R.id.tv_exchange_server_hidden_desc)
    private TextView tv_exchange_server_hidden_desc;
    @ViewInject(R.id.iv_artist_icon)
    private ImageView iv_artist_icon;
    @ViewInject(R.id.iv_exchange)
    private ImageView iv_exchange;
    @ViewInject(R.id.tv_artist_username)
    private TextView tv_artist_username;
//    @ViewInject(R.id.tv_artist_position)
//    private TextView tv_artist_position;
    @ViewInject(R.id.tv_artist_desc)
    private TextView tv_artist_desc;
    @ViewInject(R.id.tv_available_jifen)
    private TextView tv_available_jifen;

    @ViewInject(R.id.ll_artist_info)
    private LinearLayout ll_artist_info;
    @ViewInject(R.id.ll_top_layer)
    private LinearLayout ll_top_layer;


    @ViewInject(R.id.ll_point_gift_ad)
    private LinearLayout ll_point_gift_ad;
    @ViewInject(R.id.ll_recommend_art)
    private LinearLayout ll_recommend_art;

    private ActionBarView mActionBarView;

    private LayoutInflater inflater;

    private BitmapUtils mBitmapUtils;
    
    private JifenArtist mJifenArtist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_point_gift_main);
        ViewUtils.inject(this);
        inflater = LayoutInflater.from(this);

        initView();

        load();

    }

    private void initView() {
        handlerStatic = new HandlerStatic(this);
        mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
        mActionBarView = new ActionBarView(this);
        mActionBarView.setBackground(R.color.black);

        mActionBarView.setCenterTitle(R.mipmap.dt_back_w, R.color.white, "积分换礼", ActionBarView.FINISH_TYPE_4);

        int width = ZTDeviceInfo.getInstance().getWidthDevice();
        tv_exchange_good_desc.setText("商品详情");//
        tv_exchange_server_desc.setText("服务说明");//
        
        tv_exchange_good_desc.setArrawLayoutListener(new ArrawLayoutListener() {
			@Override
			public void open(boolean isClick) {
				tv_exchange_good_hidden_desc.setVisibility(View.VISIBLE);
			}
			@Override
			public void close() {
				tv_exchange_good_hidden_desc.setVisibility(View.GONE);
			}
		});
        tv_exchange_server_desc.setArrawLayoutListener(new ArrawLayoutListener() {
        	@Override
        	public void open(boolean isClick) {
        		tv_exchange_server_hidden_desc.setVisibility(View.VISIBLE);
        	}
        	@Override
        	public void close() {
        		tv_exchange_server_hidden_desc.setVisibility(View.GONE);
        	}
        });

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (int) (width * 0.72));
        ll_point_gift_ad.setLayoutParams(params);

        iv_artist_icon.setOnClickListener(this);
        iv_exchange.setOnClickListener(this);
        

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.tv_exchange_server_desc:
//                if (tv_exchange_server_hidden_desc.getVisibility() == View.GONE) {
//                    tv_exchange_server_hidden_desc.setVisibility(View.VISIBLE);
//                } else {
//                    tv_exchange_server_hidden_desc.setVisibility(View.GONE);
//                }
//                break;
//            case R.id.tv_exchange_good_desc:
//                if (tv_exchange_good_hidden_desc.getVisibility() == View.GONE) {
//                    tv_exchange_good_hidden_desc.setVisibility(View.VISIBLE);
//                } else {
//                    tv_exchange_good_hidden_desc.setVisibility(View.GONE);
//                }
//                break;
            case R.id.iv_exchange:
//			if (userJifen<availableJifen) {
//				ToastUtils.showTextToast(this, "积分不足");
//				return;
//			 }
                Intent intent = new Intent(PointGiftActivity.this, PointExchageActivity.class);
                intent.putExtra("url", getIntent().getStringExtra("url"));
                intent.putExtra("availableJifen", availableJifen);
                intent.putExtra("goodName", goodName);
                intent.putExtra("goodsId", getIntent().getLongExtra("goodsId", 0));
                startActivityForResult(intent, 10);
                break;
            case R.id.iv_artist_icon:
            	if (mJifenArtist==null) {
					return;
				}
            	CommonAcitivty.startArtistSpaceActivity(PointGiftActivity.this, mJifenArtist.getId());
            	break;
            default:
                break;
        }
    }

    /**
     * 需要积分
     */
    private int availableJifen;
    /**
     * 用户积分
     */
    private int userJifen;
    private String goodName;

    private void load() {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("goodsId", getIntent().getLongExtra("goodsId", 0));
            RequestApi.postCommon(this, Constant.URL.DTGoodsView, data,
                    new ResultLinstener<JifenDetail>() {
                        @Override
                        public void onSuccess(JifenDetail obj) {
                            if (obj != null) {
                                //图片轮播
                                JifenGoods goods = obj.getGoods();
                                goodName = goods.getName();
                                availableJifen = goods.getJifen();
                                List<Picture> pictures = goods.getPictures();
                                List<Photo> mPhotos = new ArrayList<Photo>();
                                if (pictures!=null && pictures.size()>1) {
                                	for (int i = 0; i < pictures.size(); i++) {
                                        Photo photo = new Photo();
                                        photo.setUrl(pictures.get(i).getThumbUrl());
                                        mPhotos.add(photo);
                                    }
								}else{
									ArrayList<String> picArrayList = getIntent().getStringArrayListExtra("pictures");
									if (picArrayList!=null) {
										for (String url : picArrayList) {
											Photo photo = new Photo();
	                                        photo.setUrl(url);
	                                        mPhotos.add(photo);
										}
									}
								}
                                
                                BannerView mBannerView = new BannerView(PointGiftActivity.this, mPhotos, BannerView.FIRST_ADS,true);
                                ll_point_gift_ad.addView(mBannerView.initView());

                                tv_exchange_title.setText(goods.getName());
                                tv_exchange_point.setText("" + goods.getJifen());
                                tv_exchange_point_desc.setText(goods.getBrief());

                                String serverInfo = Html.fromHtml(goods.getServiceInfo()).toString();
                                tv_exchange_server_hidden_desc.setText(Html.fromHtml(serverInfo));


                                tv_exchange_good_hidden_desc.setText(goods.getBrief());

//                                Member member = obj.getMember();
//                                if (member != null) {
//                                    userJifen = member.getJifenAvailable();
//                                    String str_jifen = "可用积分  " + "<big><b>" + userJifen + "</big></b>";
//                                    tv_available_jifen.setText(Html.fromHtml(str_jifen));
//                                    CommonApis.setUserJiFen(PointGiftActivity.this, userJifen, false);
//                                }
                                //合作艺术家
                                JifenArtist memberBean = obj.getGoods().getArtist();
                                if (memberBean != null) {
                                	mJifenArtist = memberBean;
                                    mBitmapUtils.display(iv_artist_icon, memberBean.getLogo());
                                    tv_artist_username.setText(memberBean.getNickname());
//                                    tv_artist_position.setText("" + memberBean.getProvinceId());//待修改
//									tv_artist_desc.setText(""+memberBean.getMobilephone());//待修改
                                    ll_artist_info.setVisibility(View.VISIBLE);
                                } else {
                                    ll_artist_info.setVisibility(View.GONE);
                                }

                                if (goods.getArtist()!=null) {
                                	 Message message = handlerStatic.obtainMessage();
                                     message.obj = goods.getArtist().getWorkses();
                                     message.what = 200;
                                     handlerStatic.sendMessage(message);
								}
                                ll_top_layer.setVisibility(View.GONE);
                                
                            }
                        }

                        @Override
                        public void onFailure(String obj) {
                        }

                        @Override
                        public void onException(String exception) {
                        }
                    }, new JifenDetail());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
    	try {
             tv_available_jifen.setText(""+ CommonApis.getUserJifen(this));
		} catch (Exception e) {
			e.printStackTrace();
		}
    	super.onResume();
    }
    private HandlerStatic handlerStatic;

    private class HandlerStatic extends Handler {
        WeakReference<Activity> mActivityReference;

        public HandlerStatic(Activity activity) {
            mActivityReference = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            System.out.println(" -----------handleMessage-------> " + Thread.currentThread().getId());
            final Activity activity = mActivityReference.get();
            if (activity != null) {
                switch (msg.what) {
                    case 200:
                        List<UserWork> userWorks = (List<UserWork>) msg.obj;
                        initGiftRecommend(userWorks);
                        break;
                }
            }
        }
    }

    private void initGiftRecommend(List<UserWork> userWorks) {
        try {
            if (userWorks == null) {
                return;
            }
            int count = userWorks.size();

            for (int i = 0; i < count; i++) {
                final UserWork userWork = userWorks.get(i);
                View view = inflater.inflate(R.layout.dt_point_gift_item, null);
                ImageView iv_artgallery_icon = (ImageView) view.findViewById(R.id.iv_artgallery_icon);
                TextView tv_artgallery_title = (TextView) view.findViewById(R.id.tv_artgallery_title);
                TextView tv_artgallery_love = (TextView) view.findViewById(R.id.tv_artgallery_love);
                iv_artgallery_icon.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						CommonAcitivty.startArtGalleryDetailActivity(PointGiftActivity.this, userWork.getId(),userWork.getMemberId());
					}
				});
                mBitmapUtils.display(iv_artgallery_icon, userWork.getPictures().get(0).getThumbUrl());
                tv_artgallery_title.setText(userWork.getTitle());
                tv_artgallery_love.setText(userWork.getLikedCount() + "人喜欢");

                int width = DensityUtil.dip2px(this, 130);
                LinearLayout.LayoutParams layoutParams = new LayoutParams(width, LayoutParams.WRAP_CONTENT);
                layoutParams.leftMargin = DensityUtil.dip2px(this, 5);
                view.setLayoutParams(layoutParams);
                ll_recommend_art.addView(view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (requestCode==10 && resultCode==RESULT_OK) {
			finish();
		}
    	super.onActivityResult(requestCode, resultCode, data);
    }
}
