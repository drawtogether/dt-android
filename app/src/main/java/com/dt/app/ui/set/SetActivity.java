package com.dt.app.ui.set;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dt.app.DTApplication;
import com.dt.app.MainActivity;
import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTSysSettings;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.login.DTGuideLogin;
import com.dt.app.utils.*;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.CheckSwitchButton;
import com.dt.app.view.WiperSwitch;
import com.dt.app.view.WiperSwitch.OnSwitchChangedListener;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by caiyuhao on 15-9-5.
 */
public class SetActivity extends BaseActivity {
    public Activity context;
    //缓存显示
    @ViewInject(R.id.tv_clear)
    private TextView tv_clear;
    //清理缓存
    @ViewInject(R.id.rl_clear)
    private RelativeLayout rl_clear;
    //修改密码
    @ViewInject(R.id.rl_change_passwd)
    private RelativeLayout rl_change_passwd;

    @ViewInject(R.id.rl_about)
    private RelativeLayout rl_about;
    @ViewInject(R.id.rl_rules)
    private RelativeLayout rl_rules;
    @ViewInject(R.id.tv_account)
    private TextView tv_account;
    @ViewInject(R.id.wiperSwitch_everyday)
    private CheckSwitchButton wiperSwitch_everyday;

    @ViewInject(R.id.rl_exit)
    private RelativeLayout rl_exit;
    @ViewInject(R.id.rl_change_email)
    private RelativeLayout rl_change_email;
    @ViewInject(R.id.rl_change_telphone)
    private RelativeLayout rl_change_telphone;
    @ViewInject(R.id.rl_title_bar)
    private RelativeLayout rl_title_bar;
    private UMSocialService mController;
    private ActionBarView mActionBarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_ac_set);
        context = this;
        ViewUtils.inject(this);
        mController = UMServiceFactory.getUMSocialService("com.umeng.login");
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();

    }

    private void initView() {
        mActionBarView = new ActionBarView(context);
        mActionBarView.setCenterImageTitle(R.drawable.dt_back_img,
                R.mipmap.icon_setting_select,
                ActionBarView.FINISH_TYPE_4);
        try {
            tv_clear.setText(DataCleanManager.getTotalCacheSize(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_account.setText(PreferencesUtils.getString(this, Constant.PrefrencesPt.DTmobilephone));
        getInternet();
        rl_title_bar.setBackgroundResource(R.color.white);

        wiperSwitch_everyday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    updateRemind(0);
                } else {
                    updateRemind(1);
                }
            }
        });
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_clear:
                try {
                    String cache_num = DataCleanManager.getTotalCacheSize(context);
                    if (cache_num.equals("0M")) {
                        Toast.makeText(context, "现在无缓存", Toast.LENGTH_SHORT).show();
                    } else {
                        DataCleanManager.clearAllCache(context);
                        tv_clear.setText(DataCleanManager.getTotalCacheSize(context));
                        Toast.makeText(context, "缓存已清除", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rl_change_passwd:
                startActivityByCalss(ChangePasswdActivity.class);
                break;

            case R.id.rl_about:
                Bundle bundle_about = new Bundle();
                bundle_about.putString("webview_url", Constant.URL.DTjifeng + "?t=aboutus");
                bundle_about.putString("webview_title", "关于我们");
                startActivityByCalss(SetActivity.this, LoadHtmlActivity.class, bundle_about);
                break;
            case R.id.rl_rules:
                Bundle bundle_rules = new Bundle();
                bundle_rules.putString("webview_url", Constant.URL.DTjifeng + "?t=service");
                bundle_rules.putString("webview_title", "用户协议");
                startActivityByCalss(SetActivity.this, LoadHtmlActivity.class, bundle_rules);
                break;
            case R.id.rl_exit:
                //Utils.showChooseDialog(view, SetActivity.this, "确定要退出当前账号吗");
                ToastUtils.showDialog(this, "确定要退出当前账号吗?", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (v.getId()) {
                            case R.id.ll_image_cancel:
                                ToastUtils.dismissDialog();
                                break;
                            case R.id.ll_image_confirm:
                                ToastUtils.dismissDialog();
                                if (DTGuideLogin.platpormTag > 0) {
                                    if (DTGuideLogin.platpormTag == 1) {
                                        exitAccount(SHARE_MEDIA.QQ);
                                    } else {
                                        exitAccount(SHARE_MEDIA.WEIXIN);
                                    }

                                } else {
                                    PreferencesUtils.remove(context, Constant.PrefrencesPt.DTnonce);
                                    PreferencesUtils.remove(context, Constant.PrefrencesPt.DTmkey);
                                    PreferencesUtils.remove(context, Constant.PrefrencesPt.DTSignature);
                                    Intent intent = new Intent(SetActivity.this, DTGuideLogin.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                                break;
                        }
                    }
                });
                break;
            case R.id.rl_change_email:
                startActivityByCalss(ChangeEmailActivity.class);
                break;
            case R.id.rl_change_telphone:
                startActivityByCalss(ChangePhoneActivity.class);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getInternet() {
        Map<String, Object> data = new HashMap<String, Object>();
//		data.put("mkey", DES.encryptDES(PreferencesUtils.getString(this, Constant.PrefrencesPt.DTmkey), RequestApi.USER_SECRET));

        RequestApi.postCommon(this, Constant.URL.DTSysSetting, data,
                new ResultLinstener<DTSysSettings>() {
                    @Override
                    public void onSuccess(DTSysSettings obj) {
                        if (obj != null) {

                            DTSysSettings.Setting setting = obj.getSetting();

                            if (setting.getIsNotify() == 1) {
                                wiperSwitch_everyday.setChecked(true);
                            } else {

                                wiperSwitch_everyday.setChecked(false);

                            }
                        }
                    }

                    @Override
                    public void onFailure(String obj) {

                    }

                    @Override
                    public void onException(String exception) {
                    }
                }, new DTSysSettings());
    }

    /**
     *
     */
    private void updateRemind(Integer isnotify) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("isNotify", isnotify);
        RequestApi.postCommon(this, Constant.URL.DTSysSettingUpdate, data,
                new ResultLinstener<DTSysSettings>() {
                    @Override
                    public void onSuccess(DTSysSettings obj) {
                        if (obj != null) {
                            //System.out.println(obj);
                        }
                    }

                    @Override
                    public void onFailure(String obj) {

                    }

                    @Override
                    public void onException(String exception) {
                    }
                }, new DTSysSettings());
    }

    /**
     * exit
     */
    public void exitAccount(SHARE_MEDIA share_media) {

        mController.deleteOauth(context, share_media,
                new SocializeListeners.SocializeClientListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onComplete(int status, SocializeEntity entity) {
                        if (status == 200) {
                            Toast.makeText(context, "删除成功.",
                                    Toast.LENGTH_SHORT).show();
                            LogUtils.e("------deleteoauth--exit--->");

                        }
                        LogUtils.e("--------->" + status);
                        PreferencesUtils.remove(context, Constant.PrefrencesPt.DTnonce);
                        PreferencesUtils.remove(context, Constant.PrefrencesPt.DTmkey);
                        Intent intent = new Intent(SetActivity.this, DTGuideLogin.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
//                        MainActivity.activity.finish();
                        context.finish();
                    }
                });
    }

}
