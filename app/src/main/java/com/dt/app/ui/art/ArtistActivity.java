package com.dt.app.ui.art;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.FragmentAdapter;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.fragment.ArtGalleryFragment;
import com.dt.app.fragment.artist.AllArtistFragment;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ActionBarView.ActionBarLinstener;
import com.dt.app.view.CustomViewPager;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 艺术家
 * @author Administrator
 *
 */
public class ArtistActivity extends BaseFragmentActivity implements OnClickListener{
	@ViewInject(R.id.ll_main_footer_layout)
	private LinearLayout ll_main_footer_layout;
	@ViewInject(R.id.tv_artist_count)
	private TextView tv_artist_count;
	@ViewInject(R.id.tv_all_artist)
	private TextView tv_all_artist;
	@ViewInject(R.id.tv_last_in)
	private TextView tv_last_in;
	@ViewInject(R.id.tv_artist_attention)
	private TextView tv_artist_attention;
	@ViewInject(R.id.tv_artist_random)
	private TextView tv_artist_random;
	
	//搜索
	@ViewInject(R.id.ll_artist_search)
	private LinearLayout ll_artist_search;
	@ViewInject(R.id.iv_artist_search)
	private ImageView iv_artist_search;
	@ViewInject(R.id.et_artist_search_text)
	private EditText et_artist_search_text;
	@ViewInject(R.id.iv_artist_close)
	private ImageView iv_artist_close;
	
	private ImageView titleRightView ;
	 
	@ViewInject(R.id.id_page_vp)
	 private CustomViewPager mPageVp;
	
	@ViewInject(R.id.artist_fragment)
	private FrameLayout artist_fragment;
	
	private ActionBarView mActionBarView;

    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private FragmentAdapter mFragmentAdapter;
    
    /**
     * ViewPager的当前选中页
     */
    public int currentIndex=0;
    private int lastIndex=0;
    
    private static final int searchId = 12;
     
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.artist);
		ViewUtils.inject(this);
		mActionBarView = new ActionBarView(this);
		
		init();
		
		switchButton();
	}
	
	private void init() {
		
		mActionBarView.setBackground(R.color.black);
		mActionBarView.setCenterImageTitle(R.mipmap.dt_back_w, R.mipmap.artist_white, ActionBarView.FINISH_TYPE_4);
		titleRightView = (ImageView) mActionBarView.addImageAction(searchId, R.mipmap.search_gray);
		mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
			@Override
			public void onClick(View view) {
				switch (view.getId()) {
				case searchId:
					searchIn();
					break;
				}
			}
		});
		
		for (int i = 0; i < 4; i++) {
			mFragmentList.add(AllArtistFragment.newInstance(i));
		}
		
        mFragmentAdapter = new FragmentAdapter(
                this.getSupportFragmentManager(), mFragmentList);
        mPageVp.setAdapter(mFragmentAdapter);
        mPageVp.setCurrentItem(0);
        mPageVp.setScrollable(true);
        mPageVp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int state) {

            }
            @Override
            public void onPageScrolled(int position, float offset,
                                       int offsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            	lastIndex = currentIndex;
                currentIndex = position;
                switchButton();
                try {
               	  int count = ((AllArtistFragment)mFragmentList.get(position)).page.getTotalCount();
               	  setArtistCount(count);
				} catch (Exception e) {
					e.printStackTrace();
				}
            }
        });
	        
	    iv_artist_close.setOnClickListener(this);
	    iv_artist_search.setOnClickListener(this);
	    
	    allArtistFragment = AllArtistFragment.newInstance(4);
	    getSupportFragmentManager().beginTransaction()
        .add(R.id.artist_fragment, allArtistFragment).commit();
	    
	 }
	 
	 private AllArtistFragment allArtistFragment;
	 
	 public void switchButton(){
		 tv_all_artist.setTextColor(getResources().getColor(R.color.gray));
		 tv_last_in.setTextColor(getResources().getColor(R.color.gray));
		 tv_artist_attention.setTextColor(getResources().getColor(R.color.gray));
		 tv_artist_random.setTextColor(getResources().getColor(R.color.gray));
		 switch (currentIndex) {
			case 0:
				tv_all_artist.setTextColor(getResources().getColor(R.color.black));
				break;
			case 1:
				tv_last_in.setTextColor(getResources().getColor(R.color.black));
				break;
			case 2:
				tv_artist_attention.setTextColor(getResources().getColor(R.color.black));
				break;
			case 3:
				tv_artist_random.setTextColor(getResources().getColor(R.color.black));
				break;
		}
	 }

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.iv_artist_close) {
			searchOut();
			return;
		}else if (v.getId() == R.id.iv_artist_search) {
			String search = et_artist_search_text.getText().toString().trim();
			if (TextUtils.isEmpty(search)) {
				return;
			}
			allArtistFragment.searchKeyword(search);
//			((AllArtistFragment)mFragmentList.get(currentIndex)).searchKeyword(search);
			return;
		}
		
		if (lastIndex == currentIndex) {
			return;
		}
		lastIndex = currentIndex;
		switch (v.getId()) {
		case R.id.tv_all_artist:
			currentIndex = 0;
			break;
		case R.id.tv_last_in:
			currentIndex = 1;
			break;
		case R.id.tv_artist_attention:
			currentIndex = 2;
			break;
		case R.id.tv_artist_random:
			currentIndex = 3;
			break;
		}
		switchButton();
		mPageVp.setCurrentItem(currentIndex);
	}
	
	private void searchIn(){
		Animation animation = AnimationUtils.loadAnimation(ArtistActivity.this, R.anim.right_out);
		animation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				mPageVp.setScrollable(false);
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				titleRightView.setVisibility(View.GONE);
				Animation rightAnimation = AnimationUtils.loadAnimation(ArtistActivity.this, R.anim.right_in);
				rightAnimation.setAnimationListener(new AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						
					}
				});
				ll_artist_search.startAnimation(rightAnimation);
				ll_artist_search.setVisibility(View.VISIBLE);
			}
		});
		titleRightView.startAnimation(animation);
	}
	private void searchOut(){
		Animation animation = AnimationUtils.loadAnimation(ArtistActivity.this, R.anim.right_out);
		animation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				mPageVp.setScrollable(true);
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				ll_artist_search.setVisibility(View.GONE);
				Animation rightAnimation = AnimationUtils.loadAnimation(ArtistActivity.this, R.anim.right_in);
				rightAnimation.setAnimationListener(new AnimationListener() {
					@Override
					public void onAnimationStart(Animation animation) {
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						
					}
				});
				titleRightView.startAnimation(rightAnimation);
				titleRightView.setVisibility(View.VISIBLE);
//				((AllArtistFragment)mFragmentList.get(currentIndex)).clearKeyword();
//				allArtistFragment.clearKeyword();
			}
		});
		ll_artist_search.startAnimation(animation);
	}
	
	public void setArtistCount(int count){
		tv_artist_count.setText("目前"+count+"位画廊艺术家");
	}
	
	public boolean isFirstArtistCount = true;
}
