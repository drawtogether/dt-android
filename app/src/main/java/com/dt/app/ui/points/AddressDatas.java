package com.dt.app.ui.points;

import java.util.ArrayList;
import java.util.List;

import com.dt.app.bean.Address;

public class AddressDatas {

	private static ArrayList<Address> mAddresses = new ArrayList<Address>();

	public synchronized static void fillAddress(List<Address> addresses) {
		mAddresses.clear();
		if (addresses != null && addresses.size()>0){
			mAddresses.addAll(addresses);
		}
	}

	/**
	 * 
	 * @param address
	 * @param append
	 *            true执行添加，false 执行替换
	 */
	public synchronized static void fillAddress(Address address, boolean append) {
		if (append) {
			mAddresses.add(address);
		} else {

			for (int i = 0; i < mAddresses.size(); i++) {
				Address tempAddress = mAddresses.get(i);
				if (tempAddress.getId() == address.getId()) {
					mAddresses.set(i, address);
				}
			}
		}

	}

	public synchronized static ArrayList<Address> getAddress() {
		return mAddresses;
	}

	public synchronized static void clear() {
		mAddresses.clear();
	}

	public synchronized static Address getDefaultAddress() {
		for (Address address : mAddresses) {
			if (address.getIsDefault() == 1) {
				return address;
			}
		}
		return null;
	}
	public synchronized static boolean isDefaultAddress(int pos) {
		if (pos >= 0 && pos < mAddresses.size()) {
			Address address = mAddresses.get(pos);
			if (address.getIsDefault() == 1) {
				return true;
			}
		}
		return false;
	}
	public synchronized static int getSelect() {
		for (int i = 0; i < mAddresses.size(); i++) {
			Address tempAddress = mAddresses.get(i);
			if (tempAddress.getIsDefault() == 1) {
				return i;
			}
		}
		return 0;
	}

	
	
	public synchronized static void setDefaultAddress(int pos) {
		if (pos >= 0 && pos < mAddresses.size()) {
			for (int i = 0; i < mAddresses.size(); i++) {
				if (i == pos) {
					mAddresses.get(i).setIsDefault(1);
				} else {
					mAddresses.get(i).setIsDefault(0);
				}
			}
		}
	}
	
}
