package com.dt.app.ui.works;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.EveryAdResponse;
import com.dt.app.bean.Page;
import com.dt.app.bean.Theme;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PullToRefreshListViewUtils;
import com.dt.app.utils.ZTDeviceInfo;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.BitmapUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Besttone on 2016/1/28.
 */
public class EveryAdDayActivity extends BaseActivity {

    private PullToRefreshListView mPullToRefreshListView;

    public LinearLayout ll_top_layer;

    private PullToRefreshListViewUtils<ListView> mListViewUtils;

    private ProgressBar progress_dt;

    private Page page;

    /* 主题 start */
    private Theme mTheme;

    View themeHeaderView;

    private ImageView iv_theme2_icon;

    private TextView tv_theme2_content;

    private TextView tv_theme2_title;

    private BitmapUtils mBitmapUtils;
    /* 主题  end  */

    private ImageView iv_go_back;

    private ArrayList<EveryAdResponse.Theme2.OutCell> mOutCells = new ArrayList<EveryAdResponse.Theme2.OutCell>();

    private EveryAdAdapter2 mEveryAdAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTheme = getIntent().getParcelableExtra("theme");
        setContentView(R.layout.activity_every_ad_day);

        mPullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
        mEveryAdAdapter = new EveryAdAdapter2(this, mOutCells);
        mPullToRefreshListView.setAdapter(mEveryAdAdapter);

        mListViewUtils = new PullToRefreshListViewUtils<ListView>(mPullToRefreshListView);
        mListViewUtils.init();
        mListViewUtils.setPullOnRefreshListener(new PullToRefreshListViewUtils.PullOnRefreshListener() {
            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView,
                                          int currentPage) {
                load(currentPage);
            }

            @Override
            public void onPullDownToRefresh(PullToRefreshBase refreshView,
                                            int currentPage) {
                mOutCells.clear();
                load(currentPage);
            }
        });

//        View rl_title_bar = findViewById(R.id.rl_main_top_common);
        //每日作品
        iv_go_back = (ImageView) findViewById(R.id.iv_go_back);
        iv_go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.setting_out_from_top);
            }
        });
        ll_top_layer = (LinearLayout) findViewById(R.id.ll_top_layer);
//
//        //主题标题
//        themeHeaderView = LayoutInflater.from(this).inflate(R.layout.dt_theme2_header_view, null);
//        iv_theme2_icon = (ImageView) themeHeaderView.findViewById(R.id.iv_theme2_icon);
//        tv_theme2_content = (TextView) themeHeaderView.findViewById(R.id.tv_theme2_content);
//        tv_theme2_title = (TextView) themeHeaderView.findViewById(R.id.tv_theme2_title);
//        int width = ZTDeviceInfo.getInstance().getWidthDevice();
//        LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(width, (int) (width * 0.7));
//        iv_theme2_icon.setLayoutParams(imageLayoutParams);
//        tv_theme2_title.setText(mTheme.getName());
//        tv_theme2_content.setText(mTheme.getBrief());
//        mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
//        mBitmapUtils.display(iv_theme2_icon, mTheme.getPicture());
//        rl_title_bar.setVisibility(View.GONE);
//        mPullToRefreshListView.getRefreshableView().addHeaderView(themeHeaderView);
//        iv_go_back.setVisibility(View.VISIBLE);
//        ll_top_layer.setVisibility(View.GONE);

        load(1);
    }

    /**
     * @param currentPage
     */
    private void load(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("currentPage", currentPage);
            data.put("pageSize", 10);
            String url = Constant.URL.DTThemeWorksDetail;
            data.put("themeId", mTheme.getId());
            RequestApi.postCommon(this, url, data,
                    new ResultLinstener<EveryAdResponse>() {
                        @Override
                        public void onSuccess(EveryAdResponse obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    mOutCells.clear();
                                }
//                                if (ll_top_layer.getVisibility() == View.VISIBLE) {
//                                    ll_top_layer.setVisibility(View.GONE);
//                                    //ll_top_layer.clearAnimation();
//                                    mPullToRefreshListView.setVisibility(View.VISIBLE);
//
//                                }
                                mOutCells.addAll(doNullObj(obj.getTheme().getDetailJson()));
                                page = obj.getPager();
                                mListViewUtils.setPage(page);
                                mEveryAdAdapter.notifyDataSetChanged();
                            }
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }
                    }, new EveryAdResponse());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param outCells
     * @return
     */
    private ArrayList<EveryAdResponse.Theme2.OutCell> doNullObj(ArrayList<EveryAdResponse.Theme2.OutCell> outCells) {
        if (outCells != null) {
            ArrayList<EveryAdResponse.Theme2.OutCell> newOutCell = new ArrayList<EveryAdResponse.Theme2.OutCell>();
            for (EveryAdResponse.Theme2.OutCell outCell : outCells) {
                if (outCell.getCellCount() != 0) {
                    newOutCell.add(outCell);
                }
            }
            return newOutCell;
        } else {
            return null;
        }
    }
}