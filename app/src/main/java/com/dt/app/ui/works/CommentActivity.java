package com.dt.app.ui.works;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.CommentAdapter;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.CommentList;
import com.dt.app.bean.CommentList.Comment;
import com.dt.app.bean.Page;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PullToRefreshListViewUtils;
import com.dt.app.utils.PullToRefreshListViewUtils.PullOnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 评论
 *
 * @author libin
 */
public class CommentActivity extends BaseActivity {
    @ViewInject(R.id.iv_back_home_icon)
    private ImageView iv_back_home_icon;
    @ViewInject(R.id.iv_right)
    private ImageView iv_right;
    @ViewInject(R.id.tv_center_count)
    private TextView tv_center_count;
    @ViewInject(R.id.pull_refresh_list)
    private PullToRefreshListView pull_refresh_list;
    private CommentAdapter adapter;
    private List<Comment> mBeans;

    private Page page;
    private Long worksId;
    private Long themeId;

    private PullToRefreshListViewUtils<ListView> mListViewUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_comment_main);
        ViewUtils.inject(this);

        initView();

        load(1);
    }

    private void initView() {
        try {
            themeId = getIntent().getLongExtra("themeId", -1);
            iv_back_home_icon.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            iv_right.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent commentIntent = new Intent(CommentActivity.this, CommentAddActivity.class);
                    commentIntent.putExtra("worksId", worksId);
                    int count = 0;
                    if (page != null) {
                        count = page.getTotalCount();
                    }
                    commentIntent.putExtra("count", count);
                    if (themeId > 0) {
                        commentIntent.putExtra("themeId", themeId);
                    }
                    commentIntent.putExtra("memberId", getIntent().getLongExtra("memberId", 0));
                    commentIntent.putExtra("workUserIcon", getIntent().getStringExtra("workUserIcon"));
                    startActivityForResult(commentIntent, 10);
                }
            });

            worksId = getIntent().getLongExtra("worksId", 0);
            mListViewUtils = new PullToRefreshListViewUtils<ListView>(pull_refresh_list);
            mListViewUtils.init();
            mBeans = new ArrayList<Comment>();
            adapter = new CommentAdapter(this, mBeans);
            pull_refresh_list.getRefreshableView().setAdapter(adapter);

            mListViewUtils.setPullOnRefreshListener(new PullOnRefreshListener() {
                @Override
                public void onPullUpToRefresh(PullToRefreshBase refreshView,
                                              int currentPage) {
                    load(currentPage);
                }

                @Override
                public void onPullDownToRefresh(PullToRefreshBase refreshView,
                                                int currentPage) {
                    load(currentPage);
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void load(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("currentPage", currentPage);
            data.put("pageSize", 10);
            data.put("worksId", worksId);
            String url = null;
            if (themeId > 0) {
                url = Constant.URL.DTThemeCommentList;
                data.put("themeId", themeId);
            } else {
                url = Constant.URL.DTCommentList;
            }
            RequestApi.postCommon(this, url, data,
                    new ResultLinstener<CommentList>() {
                        @Override
                        public void onSuccess(CommentList obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    mBeans.clear();
                                    page = obj.getPager();
                                    mListViewUtils.setPage(page);
                                    tv_center_count.setText("" + page.getTotalCount());
                                }

                                mBeans.addAll(obj.getComments());
                                adapter.notifyDataSetChanged();

                                mListViewUtils.onRefreshOrLoadComplete(currentPage);
                            }
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }
                    }, new CommentList());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10 && resultCode == RESULT_OK) {
            load(1);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
