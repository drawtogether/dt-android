package com.dt.app.ui.art;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.ArtDetailRecommendAdapter;
import com.dt.app.adapter.ImageTextAdapter;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.bean.ArtistDetail;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.CommonApis;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.common.threelogin.DTShare;
import com.dt.app.commonlayout.CommonFooterFour;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.receiver.CommentReceiver.CommentReceverIntf;
import com.dt.app.receiver.CommonReceiverUtil;
import com.dt.app.ui.works.WorkLovesActivity;
import com.dt.app.utils.ActivityUtils;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.CircleImageView;
import com.dt.app.view.ExtendGridView;
import com.dt.app.widget.PictureLayout;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 艺术画廊详情页
 *
 * @author libin
 */
public class ArtGalleryDetailActivity extends BaseFragmentActivity implements
        OnClickListener {

    @ViewInject(R.id.iv_artgallery_user_icon)
    private CircleImageView iv_artgallery_user_icon;
    @ViewInject(R.id.tv_artgallery_user_name)
    private TextView tv_artgallery_user_name;
//    @ViewInject(R.id.tv_artgallery_position)
//    private TextView tv_artgallery_position;
    @ViewInject(R.id.pictureLayout)
    private PictureLayout pictureLayout;

    @ViewInject(R.id.tv_artgallery_title)
    private TextView tv_artgallery_title;
    @ViewInject(R.id.tv_artgallery_desc)
    private TextView tv_artgallery_desc;
    //多少人喜欢
    @ViewInject(R.id.ll_reward_person_main)
    private LinearLayout ll_reward_person_main;
    @ViewInject(R.id.ll_reward_score_count)
    private LinearLayout ll_reward_score_count;
    @ViewInject(R.id.tv_reward_score_count)
    private TextView tv_reward_score_count;
    @ViewInject(R.id.gv_reward_person)
    private ExtendGridView gv_reward_person;
    @ViewInject(R.id.dt_art_srcollview)
    private ScrollView dt_art_srcollview;
    private List<Member> mRewardList;
    private ImageTextAdapter mRewardAdapter;
    @ViewInject(R.id.pictureLayout_bg)
    private LinearLayout pictureLayout_bg;
    @ViewInject(R.id.ll_top_layer)
    private LinearLayout ll_top_layer;
    //推荐作品
    @ViewInject(R.id.tv_art_love_work)
    private TextView tv_art_love_work;
    @ViewInject(R.id.gv_love)
    private ExtendGridView gv_love;
    private List<UserWork> mLoveList;
    private ArtDetailRecommendAdapter mLoveAdapter;


    private ActionBarView mActionBarView;
    private CommonFooterFour mCommonFooterFour;
    private BitmapUtils mBitmapUtils;
    private CommonReceiverUtil mCommonReceiverUtil;

    private long workId;
    private int commonCount=0;
    private DTShare dtShare;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = LayoutInflater.from(this).inflate(R.layout.dt_art_gallery_detail_main,null);
        setContentView(view);
        ViewUtils.inject(this);
        mCommonFooterFour = new CommonFooterFour(this, view);
        dt_art_srcollview.scrollTo(0, 0);
        init();
        loadData();
        dtShare = new DTShare(this);
    }

    private void init() {
    	workId = getIntent().getLongExtra("workId", 0);
    	mCommonReceiverUtil = new CommonReceiverUtil();
    	mCommonReceiverUtil.register(this, new CommentReceverIntf() {
			@Override
			public void success(long workid, String type) {
				if(ArtGalleryDetailActivity.this.workId == workid){
					if (ArtGalleryDetailActivity.this.commonCount>0) {
						commonCount++;
						mCommonFooterFour.tv_artgallery_comment_count.setText(commonCount + "");
					}
				}
			}
		});
    	
        mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
        mActionBarView = new ActionBarView(this);

        mActionBarView.setCenterTitle(R.mipmap.dt_back_w, "艺术画廊",
                ActionBarView.FINISH_TYPE_4);

        mRewardList = new ArrayList<Member>();
        mRewardAdapter = new ImageTextAdapter(this, mRewardList, R.drawable.dt_black_selector);
        gv_reward_person.setAdapter(mRewardAdapter);


        mLoveList = new ArrayList<UserWork>();
        mLoveAdapter = new ArtDetailRecommendAdapter(this, mLoveList);
        gv_love.setAdapter(mLoveAdapter);

        iv_artgallery_user_icon.setOnClickListener(this);
        ll_reward_score_count.setOnClickListener(this);
        ll_top_layer.setLayoutParams(com.dt.app.utils.ViewUtils.getLayoutParams(ll_top_layer,
        		ZTDeviceInfo.getInstance().getWidthDevice(), ZTDeviceInfo.getInstance().getHeightDevice()));
//        if (getIntent().getBooleanExtra("isLoading", false)) {
//	       	 ll_top_layer.setVisibility(View.GONE);
//		}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_reward_score_count:
                Intent mIntent = new Intent(ArtGalleryDetailActivity.this, WorkLovesActivity.class);
                mIntent.putExtra("worksId", getIntent().getLongExtra("workId", 0));
                startActivity(mIntent);
                break;
                 
            case R.id.iv_artgallery_user_icon:
            	CommonAcitivty.startArtistSpaceActivity(ArtGalleryDetailActivity.this, getIntent().getLongExtra("memberId", 0));
            	break;
        }

    }

    private void loadData() {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("worksId", getIntent().getLongExtra("workId", 0));
            RequestApi.postCommon(this, Constant.URL.DTWorksView, data,
                    new ResultLinstener<ArtistDetail>() {
                        @Override
                        public void onSuccess(ArtistDetail obj) {
                            if (obj != null) {
                            	ll_top_layer.setVisibility(View.GONE);
                                if (obj.getWorks() != null) {
                                    fillData(obj.getWorks());
                                }
                                List<Member> likeMembers = obj.getLikeMembers();
                                if (likeMembers != null && likeMembers.size() > 0) {
                                	List<Member> tempMembers = new ArrayList<Member>();
                                	if (likeMembers.size()>8) {
										for (int i = 0; i < 8; i++) {
											tempMembers.add(likeMembers.get(i));
										}
									}else {
										tempMembers.addAll(likeMembers);
									}
                                    mRewardList.addAll(tempMembers);
                                    mRewardAdapter.notifyDataSetChanged();
                                    tv_reward_score_count.setText(likeMembers.size() + "位朋友喜欢");
                                }
                                List<UserWork> recommendList = obj.getRecommendWorkses();
                                if (recommendList != null && recommendList.size() > 0) {
                                    mLoveList.addAll(recommendList);
                                    mLoveAdapter.notifyDataSetChanged();
                                }
//                                dt_art_srcollview.setVisibility(View.VISIBLE);
                            }else {
//                                dt_art_srcollview.setVisibility(View.INVISIBLE);
                            }
                        }

                        @Override
                        public void onFailure(String obj) {
                        	ll_top_layer.setVisibility(View.GONE);
                        }

                        @Override
                        public void onException(String exception) {
                        	ll_top_layer.setVisibility(View.GONE);
                        }
                    }, new ArtistDetail());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void fillData(UserWork userWork) {
        try {
            Member member = userWork.getMember();
            if (member != null) {
                if (!TextUtils.isEmpty(member.getLogo())) {
                    mBitmapUtils.display(iv_artgallery_user_icon, member.getLogo());
                }
                tv_artgallery_user_name.setText(member.getNickname());
            }
            if (!TextUtils.isEmpty(userWork.getTitle())){
                tv_artgallery_title.setText(userWork.getTitle());
                tv_artgallery_title.setVisibility(View.VISIBLE);
            }else {
                tv_artgallery_title.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(userWork.getContent())){
                tv_artgallery_desc.setVisibility(View.VISIBLE);
                tv_artgallery_desc.setText(userWork.getContent());
            }else {
                tv_artgallery_desc.setVisibility(View.GONE);
            }
            if (userWork.getMe() != null) {
                mCommonFooterFour.setMe(userWork.getMe());
            }
            mCommonFooterFour.tv_love_num.setText(userWork.getLikedCount() + "人喜欢");
            mCommonFooterFour.setObject(userWork,dtShare);
            int commentCount = userWork.getCommentedCount();
            if (commentCount > 0) {
            	this.commonCount = commentCount;
                mCommonFooterFour.tv_artgallery_comment_count.setVisibility(View.VISIBLE);
                mCommonFooterFour.tv_artgallery_comment_count.setText(commentCount + "");
            } else {
                mCommonFooterFour.tv_artgallery_comment_count.setVisibility(View.GONE);
            }

            int type = userWork.getType();//1.单幅方图，2.单幅圆图，3.系列作品
            List<Picture> pictures = userWork.getPictures();
            if (pictures != null && pictures.size() > 0) {
                int count = pictures.size();
                String[] urlSt = new String[count];
                for (int i = 0; i < count; i++) {
                    urlSt[i] = pictures.get(i).getThumbUrl();
                }
                mCommonFooterFour.setWorksId(userWork.getId(), member.getLogo(), urlSt,0);
                pictureLayout.removeAllViews();
                pictureLayout.setOnClick(PictureLayout.DAILY, userWork.getId(),userWork.getMemberId(), member.getLogo());
                String colorStr = pictures.get(0).getPreColor();
                colorStr=colorStr==null?"#FFFFFF":"#"+colorStr;
                colorStr = colorStr.toUpperCase();
                Picture picture_item = pictures.get(0);
//                pictureLayout.setCollectionState(picture_item.getIsSigned());
                if (type == 1) {
                    String url = pictures.get(0).getThumbUrl();
                    pictureLayout.initData(false, url, mBitmapUtils, picture_item.getW(), picture_item.getH());
                    pictureLayout_bg.setBackgroundColor(Color.parseColor(colorStr));
                } else if (type == 2) {
                    String url = pictures.get(0).getImageUrl();
                    pictureLayout.initData(true, url, mBitmapUtils, picture_item.getW(), picture_item.getH());
                    pictureLayout_bg.setBackgroundColor(Color.parseColor(colorStr));
                } else if (type == 3) {
                    List<String> urlsList = new ArrayList<String>();
                    for (Picture picture : pictures) {
                        urlsList.add(picture.getThumbUrl());
                    }
                    pictureLayout.initData(false, urlsList, mBitmapUtils, picture_item.getW(), picture_item.getH());
                    pictureLayout_bg.setBackgroundColor(Color.parseColor(colorStr));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void onDestroy() {
    	mCommonReceiverUtil.unregister(this);
    	super.onDestroy();
    }
}
