package com.dt.app.ui.set;

import android.app.Activity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.Utils;
import com.lidroid.xutils.util.LogUtils;

/**
 * Created by caiyuhao on 15-12-1.
 */
public class ChangPwdHelper {
    ChangePasswdActivity activity;

    public ChangPwdHelper(Activity act) {
        activity = (ChangePasswdActivity) act;
    }

    public void editorListener() {
        activity.et_now_pd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                String currentPwd = PreferencesUtils.getString(activity, Constant.PrefrencesPt.DTpwd);
                if (id == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(Utils.getString(activity.et_now_pd))) {
                        Utils.showWrongDialog(activity.et_now_pd, activity, "当前密码不能为空");
                    } else if (Utils.getString(activity.et_now_pd).length() < 6) {
                        Utils.showWrongDialog(activity.et_now_pd, activity, "当前密码必须为六位密码");
                    } else if (!TextUtils.isEmpty(currentPwd) && !activity.et_now_pd.getText().toString().equals(currentPwd)) {
                        Utils.showWrongDialog(activity.et_now_pd, activity, "当前密码有误");
                    } else {
                        activity.et_new_pd.requestFocus();

                    }

                }
                return true;
            }
        });
        activity.et_new_pd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (TextUtils.isEmpty(Utils.getString(activity.et_new_pd))) {
                        Utils.showWrongDialog(activity.et_new_pd, activity, "新密码不能为空");
                    } else if (Utils.getString(activity.et_new_pd).length() < 6) {
                        Utils.showWrongDialog(activity.et_new_pd, activity, "新密码必须为六位密码");
                    } else {
                        activity.et_new_pd_again.requestFocus();
                    }
                }
                return true;
            }
        });
        activity.et_new_pd_again.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String new_pwd_str = activity.et_new_pd.getText().toString();
                    if (TextUtils.isEmpty(Utils.getString(activity.et_new_pd_again))) {
                        Utils.showWrongDialog(activity.et_now_pd, activity, "新密码不能为空");
                    } else if (Utils.getString(activity.et_new_pd_again).length() < 6) {
                        Utils.showWrongDialog(activity.et_now_pd, activity, "新密码必须为六位密码");
                    } else if (!activity.et_new_pd_again.getText().toString().equals(new_pwd_str)) {
                        LogUtils.e("------1------>" + activity.et_new_pd_again.getText().toString());
                        LogUtils.e("-------2----->" + activity.et_new_pd.getText().toString());
                        Utils.showWrongDialog(activity.et_now_pd, activity, "请按要求重新设置");
                    } else {
                        activity.setPassword();
                    }
                }
                return true;
            }
        });
    }
}
