package com.dt.app.ui.login;

import java.util.HashMap;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTVerfiyCode;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ForgetPwdHelper;
import com.dt.app.utils.InputMethodUtil;
import com.dt.app.utils.ToastUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

/**
 * Created by caiyuhao on 15-9-5.
 */
public class ForgetPwdActivity extends BaseActivity {
    @ViewInject(R.id.forget_username_et)
    public EditText forget_username_et;
    @ViewInject(R.id.forget_input_code_et)
    public EditText forget_input_code_et;
    @ViewInject(R.id.forget_input_resetpwd)
    public EditText forget_input_resetpwd;
    @ViewInject(R.id.forget_input_pwd_confirm)
    public EditText forget_input_pwd_confirm;
    @ViewInject(R.id.forget_back_img)
    public ImageView forget_back_img;
    @ViewInject(R.id.forget_username_linear)
    public LinearLayout forget_username_linear;
    @ViewInject(R.id.forget_send_code)
    public Button forget_send_code;
    @ViewInject(R.id.linear_forget_pwd)
    public FrameLayout linear_forget_pwd;
    @ViewInject(R.id.linear_forget)
    public LinearLayout linear_forget;
   /* @ViewInject(R.id.tv_guid_keyborad_hide)
    private TextView tv_guid_keyborad_hide;*/

    private Activity mActivity;
    public static Integer Mobile = 1;
    public static Integer Email = 2;
    public Integer input_result = -1;

    private String str_sendverfycode;
    private int heightDifference = 0;
    private ForgetPwdHelper mForgetHelper;
    private boolean ishasKeybord = false;
    private Handler mhandler = new Handler();
    public SafeCountTimer safeCountTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_forget_pwd);
        ViewUtils.inject(this);
        mActivity = this;
        mForgetHelper = new ForgetPwdHelper(mActivity);
        mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mForgetHelper.initAnimation();
            }
        }, 100);

        mForgetHelper.initListener();
        mForgetHelper.getfocusWidget();
        mForgetHelper.initListenAction();


        //监听键盘的高度
        linear_forget_pwd.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            //当键盘弹出隐藏的时候会 调用此方法。
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //获取当前界面可视部分
                mActivity.getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                //获取屏幕的高度
                int screenHeight = mActivity.getWindow().getDecorView().getRootView().getHeight();
                //此处就是用来获取键盘的高度的， 在键盘没有弹出的时候 此高度为0 键盘弹出的时候为一个正数
                heightDifference = screenHeight - r.bottom;
                if (heightDifference > 0 && !ishasKeybord) {
                    //LogUtils.e("----------visibile= " + forget_input_pwd_confirm.getVisibility());
                    if (forget_username_linear.getVisibility() == View.VISIBLE ||
                            forget_input_resetpwd.getVisibility() == View.VISIBLE ||
                            forget_input_pwd_confirm.getVisibility() == View.VISIBLE) {
                        //LogUtils.e("----------screenHeight= " + screenHeight + ",bottom = " + r.bottom);
                        ishasKeybord = true;
                        moveView();

                    }
                } else {
                    ishasKeybord = false;
                }
            }
        });
        linear_forget_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (heightDifference > 0) {
                    InputMethodUtil.hideSoftInput(mActivity);

                }
            }
        });


    }

    @OnClick(R.id.forget_back_img)
    public void forgetBack(View view) {
        this.finish();
        // overridePendingTransition(0, R.anim.alpha_out);
    }

    @OnClick(R.id.forget_send_code)
    public void sendVerfyCode(View view) {
        String emailOrmobile = forget_username_et.getText().toString().trim();
        if (TextUtils.isEmpty(emailOrmobile)) {
            ToastUtils.showTextToast(mActivity, "不能为空");
            return;
        }
        if (input_result != Email && input_result != Mobile) {
            ToastUtils.showTextToast(mActivity, "输入无效");
            return;
        }
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("loginname", emailOrmobile);
        data.put("usage", "findpwd");

        safeCountTimer = new SafeCountTimer(60 * 1000, 1000);
        safeCountTimer.start();
        try {
            RequestApi.commonRequest(this, Constant.URL.DTSendVerfyCode, data, new ResultLinstener<DTVerfiyCode>() {
                @Override
                public void onSuccess(DTVerfiyCode obj) {
                    if (obj.getCode() == 1) {
                        //successful
                        str_sendverfycode = obj.getData();
                        forget_input_code_et.setVisibility(View.VISIBLE);

                        final Animation animation_verfycode = AnimationUtils.loadAnimation(mActivity, R.anim.forget_pwd_in_from_bottom);
                        forget_input_code_et.startAnimation(animation_verfycode);
                        animation_verfycode.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                forget_input_code_et.setFocusable(true);
                                forget_input_code_et.requestFocus();


                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        //ToastUtils.showTextToast(mActivity, "" + obj.getMessage());

                    } else {
                        //ToastUtils.showTextToast(mActivity, obj.getMessage() + "");
                    }
                }

                @Override
                public void onFailure(String obj) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onException(String exception) {
                    // TODO Auto-generated method stub

                }
            }, new DTVerfiyCode());
        } catch (Exception e) {
            // TODO: handle exception
        }

    }


    /**
     * 定时器
     */
    public class SafeCountTimer extends CountDownTimer {

        public SafeCountTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            forget_send_code.setText("" + l / 1000);
            forget_send_code.setTextColor(Color.WHITE);
            forget_send_code.setBackgroundResource(R.color.background);
            forget_send_code.setEnabled(false);
        }

        @Override
        public void onFinish() {
            forget_send_code.setEnabled(true);
            forget_send_code
                    .setBackgroundResource(R.color.white);
            forget_send_code.setTextColor(getResources().getColor(
                    R.color.background));
            forget_send_code.setText("发送验证码");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            this.finish();
            //overridePendingTransition(0, R.anim.alpha_out);
        }
        return super.onKeyDown(keyCode, event);
    }

    public void moveView() {
        FrameLayout.LayoutParams btnLp = (FrameLayout.LayoutParams) linear_forget.getLayoutParams();
        btnLp.setMargins(0, 0, 0, 200);
        linear_forget.requestLayout();
    }
}
