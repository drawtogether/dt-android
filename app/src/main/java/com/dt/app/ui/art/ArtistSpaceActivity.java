package com.dt.app.ui.art;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.FragmentAdapter;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.bean.ArtistPerson;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.bean.UserWorks.Me;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.fragment.artist.AllArtistFragment;
import com.dt.app.fragment.artist.ArtistAttentionFragment;
import com.dt.app.fragment.artist.ArtistLikesFragment;
import com.dt.app.fragment.artist.ArtistWorkFragment;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.listener.LayoutCallback;
import com.dt.app.receiver.CommentReceiver;
import com.dt.app.receiver.CommentReceiver.CommentReceverIntf;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ActionBarView.ActionBarLinstener;
import com.dt.app.view.ViewPagerCompat;
import com.dt.app.widget.StickyNavLayout2;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 艺术家空间
 * 
 * @author libin
 * 
 */
public class ArtistSpaceActivity extends BaseFragmentActivity implements OnClickListener,
LayoutCallback<ArtistPerson>{

	private ViewPagerCompat mPageVp;

	private List<Fragment> mFragmentList = new ArrayList<Fragment>();
	private FragmentAdapter mFragmentAdapter;
 
	private ActionBarView mActionBarView;
  
	@ViewInject(R.id.ll_artist_main)
	private StickyNavLayout2 navLayout2;
 
	@ViewInject(R.id.ll_artist_header)
	private LinearLayout ll_artist_header;
	@ViewInject(R.id.iv_artist_user_icon)
	private ImageView iv_artist_user_icon;
	@ViewInject(R.id.ll_artist_sign)
	private LinearLayout ll_artist_sign;
	@ViewInject(R.id.iv_artist_nosign)
	private ImageView iv_artist_nosign;
	@ViewInject(R.id.tv_artist_user_name)
	private TextView tv_artist_user_name;
//	@ViewInject(R.id.tv_artist_position)
//	private TextView tv_artist_position;
 
	@ViewInject(R.id.tv_artist_desc)
	private TextView tv_artist_desc;
	//切换菜单
	@ViewInject(R.id.tv_artist_work)
	private TextView tv_artist_work;
	@ViewInject(R.id.tv_artist_attention)
	private TextView tv_artist_attention;
	@ViewInject(R.id.tv_artist_fensi)
	private TextView tv_artist_fensi;
	@ViewInject(R.id.tv_artist_love)
	private TextView tv_artist_love;
	@ViewInject(R.id.tv_artist_user_location)
	private TextView tv_artist_user_location;
	private int currentIndex = 0;
//	private GestureDetector detector;
	private int flingDis;
	private  BitmapUtils mBitmapUtils;
	private ImageView mRightImageView;
	private long memberId;
	private boolean isSelf=false;//是否是自己
	
	private int titleBarHeight;
	private Context   context;
	private CommentReceiver mCommentReceiver;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dt_artist_space_main);
		ViewUtils.inject(this);
		
		mActionBarView = new ActionBarView(this);
		mActionBarView.setBackground(R.color.black);
		mActionBarView.setCenterTitle(R.mipmap.dt_back_w,R.color.white, "", ActionBarView.FINISH_TYPE_4);
		context = this;
		findById();
		init();
		
	 
	}

	private void findById() {
		mCommentReceiver = new CommentReceiver();
    	IntentFilter filter = new IntentFilter();
    	filter.addAction(CommentReceiver.SEND_RECEIVER_SUCCES);
    	registerReceiver(mCommentReceiver, filter);
    	mCommentReceiver.setCommentReceverIntf(new CommentReceverIntf() {
			@Override
			public void success(long workid, String type) {
				try {
					((ArtistWorkFragment)mFragmentList.get(0)).success(workid, type);
					if (!isSelf) {
						((ArtistLikesFragment)mFragmentList.get(3)).success(workid, type);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		});
		
		mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
		mPageVp = (ViewPagerCompat) this.findViewById(R.id.id_page_vp);
//		mPageVp.setScrollable(false);
		mPageVp.setViewTouchMode(true);
		
		tv_artist_work.setOnClickListener(this);
		tv_artist_attention.setOnClickListener(this);
		tv_artist_fensi.setOnClickListener(this);
		tv_artist_love.setOnClickListener(this);
 
//		detector = new GestureDetector(this);
		
		flingDis = ZTDeviceInfo.getInstance().getHeightDevice()/8;
		
		titleBarHeight = (int) getResources().getDimension(R.dimen.actionbar_height);
		
		ll_artist_header.measure(0, 0);
		mHeaderHeight = ll_artist_header.getMeasuredHeight();
		System.out.println("--------frameLayoutParams-----"+mHeaderHeight);
 
	}

	private void init() {
		changeTextColor();
		
		memberId = getIntent().getLongExtra("memberId", 0);
		
		if (memberId == PreferencesUtils.getLong(this, Constant.PrefrencesPt.DTid)) {
			isSelf = true;
//			mActionBarView.addTextViewAction(R.id.title_bar_right_img2, 0, "编辑");
			mActionBarView.addImageAction(R.id.title_bar_right_img2, R.mipmap.edit);
			mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
				@Override
				public void onClick(View view) {
					if (view.getId()==R.id.title_bar_right_img2) {
						Intent mIntent = new Intent(ArtistSpaceActivity.this,ArtistEditActivity.class);
						startActivityForResult(mIntent, EDIT_PERSON_INFO);
					}
				}
			});
			tv_artist_love.setVisibility(View.GONE);
		}
		
		System.out.println("---------memberId--------->>>>>>>>>>> "+memberId);
		mFragmentList.add(new ArtistWorkFragment(memberId));
		mFragmentList.add(ArtistAttentionFragment.newInstance(memberId));
		mFragmentList.add(AllArtistFragment.newInstance(AllArtistFragment.SNS_FANS,memberId));
		if (!isSelf) {
			mFragmentList.add(new ArtistLikesFragment(memberId));
		}
		mFragmentAdapter = new FragmentAdapter(this.getSupportFragmentManager(), mFragmentList);
		mPageVp.setAdapter(mFragmentAdapter);
		
	}

 
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ll_artist_header:

			break;
		case R.id.iv_artist_user_icon:

			break;
		case R.id.tv_artist_user_name:

			break;
		case R.id.tv_artist_desc:

			break;
		case R.id.tv_artist_work:
			currentIndex = 0;
			changeTextColor();
			break;
		case R.id.tv_artist_attention:
			currentIndex = 1;
			changeTextColor();
			break;
		case R.id.tv_artist_fensi:
			currentIndex = 2;
			changeTextColor();
			break;
		case R.id.tv_artist_love:
			currentIndex = 3;
			changeTextColor();
			break;
		default:
			break;
		}
	}

	private void changeTextColor(){
		mPageVp.setCurrentItem(currentIndex);
  
		textHtml(tv_artist_work, "作品", workCount, false);
		textHtml(tv_artist_attention, "关注", guanzhuCount, false);
		textHtml(tv_artist_fensi, "粉丝", fensiCount, false);
		if(!isSelf){
			textHtml(tv_artist_love, "喜欢", loveCount, false);
		}
		switch (currentIndex) {
		case 0:
			textHtml(tv_artist_work, "作品", workCount, true);
			break;
		case 1:
			textHtml(tv_artist_attention, "关注", guanzhuCount, true);
			break;
		case 2:
			textHtml(tv_artist_fensi, "粉丝", fensiCount, true);
			break;
		case 3:
			textHtml(tv_artist_love, "喜欢", loveCount, true);
			break;
		}
	}
	
	private void textHtml(TextView textView,String text,int num,boolean select){
		if (!select) {
			textView.setText(Html.fromHtml("<font size='15'>"+text+"</font><big><b>"+num+"</b></big>"));
		}else {
			textView.setText(Html.fromHtml("<font size='15'>"+text+"</font><big><b><font color='#F96363'>"+num+"</font></b></big>"));
		}
	}

	
	
	private int workCount;
	private int loveCount;
	private int fensiCount;
	private int guanzhuCount;
	private boolean headerIsLoad = false;
	@Override
	public void callback(ArtistPerson t) {
		try {
			final MemberBean member = t.getMember();
			workCount = member.getWorksCount();
			guanzhuCount = member.getFollowingCount();
			fensiCount = member.getFollowersCount();
			loveCount = member.getLikeCount();
			changeTextColor();
			
			if (!headerIsLoad) {
				mBitmapUtils.display(iv_artist_user_icon, member.getLogo());
				headerIsLoad = true;
			}
			
			tv_artist_user_name.setText(member.getNickname()+"");
			if (!isSelf){
				if (TextUtils.isEmpty(member.getSignature())){
					tv_artist_desc.setText(member.getSignature()+"");
					tv_artist_desc.setHint("");
				}
			}
			if (!TextUtils.isEmpty(member.getLocation())){
				tv_artist_user_location.setText(member.getLocation()+"");
				tv_artist_user_location.setHint("");
			}

			mActionBarView.tv_center_title.setText(member.getNickname()+"");
			if (member.getIsSigned()==0) {//是否签约：0 否，1 是
				iv_artist_nosign.setVisibility(View.VISIBLE);
				ll_artist_sign.setVisibility(View.GONE);
			}else {
				iv_artist_nosign.setVisibility(View.GONE);
				ll_artist_sign.setVisibility(View.VISIBLE);
			}
			navLayout2.refreshHeader();
			final Me me = member.getMe();
			if (me != null && !isSelf) {

				if (me.getIsSelf()!= null && me.getIsSelf()==1) {
					
				}else {
					if (me.getIsFollowed() != null && me.getIsFollowed()==1) {
						System.out.println("----------like_h_red--------> 关注");
						mRightImageView = (ImageView) mActionBarView.addImageAction(R.id.title_bar_right_img1, R.mipmap.like_h_red);
					}else {
						System.out.println("----------like_h_white--------> 关注");
						mRightImageView = (ImageView) mActionBarView.addImageAction(R.id.title_bar_right_img1, R.mipmap.like_h_white);
					}
					mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
						@Override
						public void onClick(View view) {
							if (me.getIsFollowed() != null && me.getIsFollowed()==1) {
								//取消关注
								ToastUtils.showDialog(context, "确定要取消关注吗?", new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										switch (v.getId()) {
											case R.id.ll_image_cancel:
												ToastUtils.dismissDialog();
												break;
											case R.id.ll_image_confirm:
												loadData(member.getId(), false);
												me.setIsFollowed(0);
												ToastUtils.dismissDialog();
												break;
										}
									}
								});
							}else {
								//关注
								loadData(member.getId(), true);
								me.setIsFollowed(1);
							}
						}
					});
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @param memberId
	 * @param isFollow true 关注，false 取消关注
	 */
	public void loadData(long memberId,final boolean isFollow){
		try {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("memberId", memberId);
			String urlStr = null;
			if (isFollow) {
				urlStr = Constant.URL.DTSnsFolloweAdd;
			}else {
				urlStr = Constant.URL.DTSnsFolloweCancel;
			}
			RequestApi.postCommon(this, urlStr, data,
					new ResultLinstener<String>() {
				@Override
				public void onSuccess(String obj) {
					if (isFollow) {
						mRightImageView.setImageResource(R.mipmap.like_h_red);
						ToastUtils.showTextToast(ArtistSpaceActivity.this, "关注成功");
					}else {
						mRightImageView.setImageResource(R.mipmap.like_h_white);
						ToastUtils.showTextToast(ArtistSpaceActivity.this, "取消关注成功");
					}
				}
				
				@Override
				public void onFailure(String obj) {
				}
				
				@Override
				public void onException(String exception) {
				}
			}, new String());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onResume() {
		if (isSelf) {
			String nickname = PreferencesUtils.getString(this, Constant.PrefrencesPt.DTnickname);
			tv_artist_user_name.setText(nickname);
			tv_artist_desc.setText(PreferencesUtils.getString(this, Constant.PrefrencesPt.DTSignature));
			String location = PreferencesUtils.getString(this, Constant.PrefrencesPt.DTLocation);
			if (!TextUtils.isEmpty(location)) {
				tv_artist_user_location.setText(""+location.trim());
				tv_artist_user_location.setVisibility(View.VISIBLE);
			}else{
				tv_artist_user_location.setVisibility(View.GONE);
			}
			ll_artist_header.measure(0, 0);
			navLayout2.setTopViewHeight(ll_artist_header.getMeasuredHeight());
			mBitmapUtils.display(iv_artist_user_icon, PreferencesUtils.getString(this, Constant.PrefrencesPt.DTlogo));
		}
		super.onResume();
	}
 
	
	private int mHeaderHeight=0;
	@Override
	public void callPadding(int val) {
//		if (Math.abs(val)<=mHeaderHeight) {\
//		val = val > 0 ? 0 : val;
//		ll_artist_header.setPadding(0, val, 0, 0);
//		}
	}

	@Override
	public int callGetHeaderHeight() {
		return mHeaderHeight;
	}
 
	@Override
	protected void onDestroy() {
		try {
			unregisterReceiver(mCommentReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}
 
	/**
	 * 编辑个人信息
	 */
	public static final int EDIT_PERSON_INFO=100;
	/**
	 * 编辑作品
	 */
	public static final int EDIT_WORK_REQ=25;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == EDIT_WORK_REQ && resultCode==RESULT_OK) {
			if (data!=null) {
				String title = data.getStringExtra("title");
				String content = data.getStringExtra("content");
				String workid = data.getStringExtra("workid");
				int position = data.getIntExtra("position", -1);
				((ArtistWorkFragment)mFragmentList.get(0)).editSuccess(Long.valueOf(workid), title, content, position);
				LogUtils.i("result title : "+title);
			}
		}else if (requestCode == EDIT_PERSON_INFO && resultCode==RESULT_OK) {
//			String nickname = PreferencesUtils.getString(this, Constant.PrefrencesPt.DTnickname);
//			tv_artist_user_name.setText(nickname);
//			tv_artist_desc.setText(PreferencesUtils.getString(this, Constant.PrefrencesPt.DTSignature));
			
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
