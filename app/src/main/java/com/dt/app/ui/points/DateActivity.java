package com.dt.app.ui.points;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.CityModel;
import com.dt.app.bean.DistrictModel;
import com.dt.app.bean.ProvinceModel;
import com.dt.app.view.ActionBarView;
import com.lidroid.xutils.util.LogUtils;

public class DateActivity extends BaseActivity implements OnClickListener, OnWheelChangedListener {
	private WheelView mViewProvince;
	private WheelView mViewCity;
	private WheelView mViewDistrict;
	private Button mBtnConfirm;
	
	private String defaultPri;
	private String defaultCit;
	private String defaultDir;
	private int defaultPosPri=0;
	private int defaultPosCit=0;
	private int defaultPosDir=0;
	private boolean isFirst =false;
	ArrayWheelAdapter<String> provinceAdapter;
	ArrayWheelAdapter<String> districtAdapter;
	ArrayWheelAdapter<String> cityAdapter;
	private ActionBarView actionBarView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whell_main);
		 
		try {
			String addStr =  getIntent().getStringExtra("address");
			if (addStr!=null && addStr.length()>2) {
				String[] arr = addStr.split(" ");
				defaultPri = arr[0];
				defaultCit = arr[1];
				defaultDir = arr[2];
			}
		} catch (Exception e) {
			defaultPri=null;
			defaultCit=null;
			defaultDir=null;
		}
		
		setUpViews();
		setUpListener();
		setUpData();
	}
	 
	private void setUpViews() {
		actionBarView = new ActionBarView(this);
		actionBarView.setBackground(R.color.black);
		actionBarView.setBackHome(R.mipmap.dt_back_w, "", ActionBarView.FINISH_TYPE_4);
		
		mViewProvince = (WheelView) findViewById(R.id.id_province);
		mViewCity = (WheelView) findViewById(R.id.id_city);
		mViewDistrict = (WheelView) findViewById(R.id.id_district);
		mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
	}
	
	private void setUpListener() {
    	// 添加change事件
    	mViewProvince.addChangingListener(this);
    	// 添加change事件
    	mViewCity.addChangingListener(this);
    	// 添加change事件
    	mViewDistrict.addChangingListener(this);
    	// 添加onclick事件
    	mBtnConfirm.setOnClickListener(this);
    }
	
	private void setUpData() {
		initProvinceDatas();
		provinceAdapter = new ArrayWheelAdapter<String>(DateActivity.this, mProvinceDatas);
		mViewProvince.setViewAdapter(provinceAdapter);
		mViewProvince.setVisibleItems(7);
		mViewCity.setVisibleItems(7);
		mViewDistrict.setVisibleItems(7);
		
		System.out.println("----------"+mProvinceDatas.length);
		
		if (isFirst) {
			mViewProvince.setCurrentItem(defaultPosPri);
			isFirst = false;
		}else {
			mViewProvince.setCurrentItem(0);
		}
		// 设置可见条目数量
		
		updateCities();
		updateAreas();
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		if (wheel == mViewProvince) {
			updateCities();
		} else if (wheel == mViewCity) {
			updateAreas();
		} else if (wheel == mViewDistrict) {
			mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[newValue];
			mCurrentZipCode = mZipcodeDatasMap.get(mCurrentDistrictName);
		}
	}

	/**
	 * 根据当前的市，更新区WheelView的信息
	 */
	private void updateAreas() {
		int pCurrent = mViewCity.getCurrentItem();
		mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
		String[] areas = mDistrictDatasMap.get(mCurrentCityName);

		if (areas == null) {
			areas = new String[] { "" };
		}
		districtAdapter = new ArrayWheelAdapter<String>(this, areas);
		mViewDistrict.setViewAdapter(districtAdapter);
		if (isFirst) {
			mViewDistrict.setCurrentItem(defaultPosDir);
			isFirst = false;
		}else {
			mViewDistrict.setCurrentItem(0);
		}
	}

	/**
	 * 根据当前的省，更新市WheelView的信息
	 */
	private void updateCities() {
		int pCurrent = mViewProvince.getCurrentItem();
		mCurrentProviceName = mProvinceDatas[pCurrent];
		String[] cities = mCitisDatasMap.get(mCurrentProviceName);
		if (cities == null) {
			cities = new String[] { "" };
		}
		cityAdapter = new ArrayWheelAdapter<String>(this, cities);
		mViewCity.setViewAdapter(cityAdapter);
		if (isFirst) {
			mViewCity.setCurrentItem(defaultPosCit);
			isFirst = false;
		}else {
			mViewCity.setCurrentItem(0);
		}
		updateAreas();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_confirm:
			showSelectedResult();
			break;
		default:
			break;
		}
	}

	private void showSelectedResult() {
		String provice = provinceAdapter.getItemText(mViewProvince.getCurrentItem()).toString();
		String city = cityAdapter.getItemText(mViewCity.getCurrentItem()).toString();
		String district = districtAdapter.getItemText(mViewDistrict.getCurrentItem()).toString();
		Intent mIntent = new Intent();
		mIntent.putExtra("address", provice+" "+city+" "+" "+district);
		LogUtils.i("地址： "+provice+" "+city+" "+" "+district);
		setResult(RESULT_OK, mIntent);
		finish();
	}
    
    
    /**
	 * 所有省
	 */
	protected String[] mProvinceDatas;
	/**
	 * key - 省 value - 市
	 */
	protected Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
	/**
	 * key - 市 values - 区
	 */
	protected Map<String, String[]> mDistrictDatasMap = new HashMap<String, String[]>();
	
	/**
	 * key - 区 values - 邮编
	 */
	protected Map<String, String> mZipcodeDatasMap = new HashMap<String, String>(); 

	/**
	 * 当前省的名称
	 */
	protected String mCurrentProviceName;
	/**
	 * 当前市的名称
	 */
	protected String mCurrentCityName;
	/**
	 * 当前区的名称
	 */
	protected String mCurrentDistrictName ="";
	
	/**
	 * 当前区的邮政编码
	 */
	protected String mCurrentZipCode ="";
	
	/**
	 * 解析省市区的XML数据
	 */
	
    protected void initProvinceDatas()
	{
		List<ProvinceModel> provinceList = null;
    	AssetManager asset = getAssets();
        try {
            InputStream input = asset.open("province_data.xml");
            // 创建一个解析xml的工厂对象
			SAXParserFactory spf = SAXParserFactory.newInstance();
			// 解析xml
			SAXParser parser = spf.newSAXParser();
			XmlParserHandler handler = new XmlParserHandler();
			parser.parse(input, handler);
			input.close();
			// 获取解析出来的数据
			provinceList = handler.getDataList();
			//*/ 初始化默认选中的省、市、区
			if (provinceList!= null && !provinceList.isEmpty()) {
				mCurrentProviceName = provinceList.get(0).getName();
				List<CityModel> cityList = provinceList.get(0).getCityList();
				if (cityList!= null && !cityList.isEmpty()) {
					mCurrentCityName = cityList.get(0).getName();
					List<DistrictModel> districtList = cityList.get(0).getDistrictList();
					mCurrentDistrictName = districtList.get(0).getName();
					mCurrentZipCode = districtList.get(0).getZipcode();
				}
			}
			//*/
			mProvinceDatas = new String[provinceList.size()];
        	for (int i=0; i< provinceList.size(); i++) {
        		// 遍历所有省的数据
        		mProvinceDatas[i] = provinceList.get(i).getName();
        		if (mProvinceDatas[i].equals(defaultPri)) {
					defaultPosPri = i;
				}
        		List<CityModel> cityList = provinceList.get(i).getCityList();
        		String[] cityNames = new String[cityList.size()];
        		for (int j=0; j< cityList.size(); j++) {
        			// 遍历省下面的所有市的数据
        			cityNames[j] = cityList.get(j).getName();
        			if (cityNames[j].equals(defaultCit)) {
    					defaultPosCit = j;
    				}
        			
        			List<DistrictModel> districtList = cityList.get(j).getDistrictList();
        			String[] distrinctNameArray = new String[districtList.size()];
        			DistrictModel[] distrinctArray = new DistrictModel[districtList.size()];
        			for (int k=0; k<districtList.size(); k++) {
        				// 遍历市下面所有区/县的数据
        				DistrictModel districtModel = new DistrictModel(districtList.get(k).getName(), districtList.get(k).getZipcode());
        				// 区/县对于的邮编，保存到mZipcodeDatasMap
        				if (districtList.get(k).getName().equals(defaultDir)) {
        					defaultPosDir = k;
        				}
        				mZipcodeDatasMap.put(districtList.get(k).getName(), districtList.get(k).getZipcode());
        				distrinctArray[k] = districtModel;
        				distrinctNameArray[k] = districtModel.getName();
        			}
        			// 市-区/县的数据，保存到mDistrictDatasMap
        			mDistrictDatasMap.put(cityNames[j], distrinctNameArray);
        		}
        		// 省-市的数据，保存到mCitisDatasMap
        		mCitisDatasMap.put(provinceList.get(i).getName(), cityNames);
        	}
        } catch (Throwable e) {  
            e.printStackTrace();  
        } finally {
        	
        } 
	}
}
