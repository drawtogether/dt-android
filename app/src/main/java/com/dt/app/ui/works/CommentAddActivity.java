package com.dt.app.ui.works;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.receiver.CommentReceiver;
import com.dt.app.utils.Constant;
import com.dt.app.utils.InputMethodUtil;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 添加评论
 *
 * @author libin
 */
public class CommentAddActivity extends BaseActivity {
    @ViewInject(R.id.iv_back_home_icon)
    private ImageView iv_back_home_icon;
    @ViewInject(R.id.iv_right)
    private ImageView iv_right;
    @ViewInject(R.id.tv_center_count)
    private TextView tv_center_count;

    @ViewInject(R.id.iv_user_icon)
    private CircleImageView iv_user_icon;
    @ViewInject(R.id.et_comment)
    private EditText et_comment;

    private Long worksId;
    private BitmapUtils mBitmapUtils;
    private Long themeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_comment_add_main);
        ViewUtils.inject(this);

        initView();

        
    }

    private void initView() {
        try {
            themeId = getIntent().getLongExtra("themeId", -1);
            mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
            iv_back_home_icon.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            iv_right.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    commentSubmit();
                }
            });
            worksId = getIntent().getLongExtra("worksId", 0);
            tv_center_count.setText("" + getIntent().getIntExtra("count", 0));
            mBitmapUtils.display(iv_user_icon, getIntent().getStringExtra("workUserIcon"));

            InputMethodUtil.enableSoftInput(this);
            
            iv_user_icon.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					CommonAcitivty.startArtistSpaceActivity(CommentAddActivity.this, getIntent().getLongExtra("memberId", 0));
				}
			});

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void commentSubmit() {
        try {
            String content = et_comment.getText().toString().trim();
            if (TextUtils.isEmpty(content)) {
                return;
            }
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("content", content);
            String url = null;
            if (themeId > 0) {
                url = Constant.URL.DTThemeCommentSubmit;
                data.put("themeId", themeId);
            } else {
                data.put("worksId", worksId);
                url = Constant.URL.DTCommentSubmit;
            }
            RequestApi.postCommon(this, url, data,
                    new ResultLinstener<String>() {
                        @Override
                        public void onSuccess(String obj) {
                            ToastUtils.showTextToast(CommentAddActivity.this, "评论成功");
                            Intent intent = new Intent();
                            intent.setAction(CommentReceiver.SEND_RECEIVER_SUCCES);
                            if (themeId > 0) {

                            } else {
                                intent.putExtra("workid", worksId);
                                intent.putExtra("type", CommentReceiver.TYPE_ARTGALLERY);
                            }
                            sendBroadcast(intent);
                            setResult(RESULT_OK);
                            finish();
                        }

                        @Override
                        public void onFailure(String obj) {
                        }

                        @Override
                        public void onException(String exception) {
                        }
                    }, new String());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
