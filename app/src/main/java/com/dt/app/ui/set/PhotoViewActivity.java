package com.dt.app.ui.set;

import java.util.ArrayList;
import java.util.List;

import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.Photo;
import com.dt.app.common.CommonApis;
import com.dt.app.listener.HandleCallbackSimple;
import com.dt.app.ui.works.CommentActivity;
import com.dt.app.ui.works.RewardActivity;
import com.dt.app.view.BannerView;
import com.dt.app.widget.PathMenuLayout;
import com.dt.app.widget.ScrollLinearLayout;
import com.dt.app.widget.ViewPosition;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PhotoViewActivity extends BaseActivity {

    private BannerView mBannerView;

    @ViewInject(R.id.ll_photo)
    private ScrollLinearLayout ll_photo;
    private ArrayList<String> mPhotos;
    private int index;

    private int sum;
    private PathMenuLayout pathMenu;

    private int currentIndex = 0;
//    @ViewInject(R.id.iv_art_artist_heart_img)
//    private ImageView iv_art_artist_heart_img;
    @ViewInject(R.id.fl_photo_view_main)
    private FrameLayout fl_photo_view_main;

    public static int locationX = 0;
    public static int locationY = 0;

    private int like;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wish_photo_scan);
        try {
            ViewUtils.inject(this);
            mPhotos = getIntent().getStringArrayListExtra("photos");
            index = getIntent().getIntExtra("index", 0);
            locationX = getIntent().getIntExtra("x", 0);
            locationY = getIntent().getIntExtra("y", 0);
            List<Photo> mList = new ArrayList<Photo>();
            if (mPhotos != null) {
                for (String url : mPhotos) {
                    mList.add(new Photo(url, 1l));
                }
            }
            sum = mPhotos.size();
//			pub_title_bar.setTitle((index+1)+"/"+sum, true);
//			pub_title_bar.setBack("", true);

            mBannerView = new BannerView(this, mList, BannerView.PHOTO_VIEW);
            ll_photo.removeAllViews();
            ll_photo.addView(mBannerView.initView());
            mBannerView.getmHackViewPager().setPageTransformer(true, new DepthPageTransformer());

            mBannerView.getmHackViewPager().setCurrentItem(index);
            mBannerView.getmHackViewPager().setOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentIndex = position;
//					pub_title_bar.setTitle((position+1)+"/"+sum, true);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int arg0) {

                }
            });

//            findViews();
//            bindEvents();

            like = getIntent().getIntExtra("like", -1);
//            LogUtils.i("-0-----like----------->" + like);
//            if (like == 1) {
//                iv_art_artist_heart_img.setImageResource(R.mipmap.reward_select);
//            } else if (like == 0) {
//                iv_art_artist_heart_img.setImageResource(R.mipmap.reward_normal);
//            }
//            iv_art_artist_heart_img.setEnabled(false);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {

        private static final float MIN_SCALE = 0.75f;

        @Override
        public void transformPage(View view, float position) {
            // TODO Auto-generated method stub
            int pageWidth = view.getWidth();

            if (position < -1) {
                view.setAlpha(0);
            } else if (position <= 0) {
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);
            } else if (position <= 1) {
                view.setAlpha(1 - position);
                view.setTranslationX(pageWidth * -position);
                float scaleFactor = MIN_SCALE + (1 - MIN_SCALE)
                        * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);
            } else {
                view.setAlpha(0);
            }
        }

    }


//    private void bindEvents() {
//        pathMenu.setOnIcomClick(new PathMenuLayout.OnIconClickListener() {
//            @Override
//            public void onIconClick(View v) {
//                switch (v.getId()) {
//                    case R.id.iv_art_artist_reward_img:
//                        Intent intent = new Intent(PhotoViewActivity.this, RewardActivity.class);
//                        intent.putExtra("workId", getIntent().getLongExtra("workId", 0));
//                        intent.putExtra("logo", getIntent().getStringExtra("logo"));
//                        intent.putExtra("rewardUrl", mPhotos.get(currentIndex));
//                        startActivity(new Intent(intent));
//                        break;
//                    case R.id.iv_art_artist_common_img:
//                        Intent commentIntent = new Intent(PhotoViewActivity.this, CommentActivity.class);
//                        commentIntent.putExtra("worksId", getIntent().getLongExtra("workId", 0));
//                        commentIntent.putExtra("workUserIcon", getIntent().getStringExtra("logo"));
//                        startActivity(commentIntent);
//                        break;
//                    default:
//                        break;
//                }
//            }
//        });
//    }
//
//    private void findViews() {
//        pathMenu = (PathMenuLayout) findViewById(R.id.menu_wrapper);
//        pathMenu.setPosition(ViewPosition.POSITION_RIGHT_BOTTOM);
//    }


//    private void likeSubmit(final boolean like) {
//        try {
//            CommonApis.likeSubmit(this, like, getIntent().getLongExtra("workId", 0), new HandleCallbackSimple<String>() {
//                @Override
//                public void onSuccess(String t) {
//                    if (like) {
////						me.setIsLiked(1);
//                        iv_art_artist_heart_img.setImageResource(R.mipmap.reward_select);
//                    } else {
////						me.setIsLiked(0);
//                        iv_art_artist_heart_img.setImageResource(R.mipmap.reward_normal);
//                    }
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void scrollTo(int startY, int dy) {
        ll_photo.scroll(startY, dy);
    }
}