//package com.dt.app.ui.works;
//
//import android.app.Activity;
//import android.net.Uri;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.dt.app.R;
//import com.lidroid.xutils.util.LogUtils;
//
//import io.vov.vitamio.MediaPlayer;
//import io.vov.vitamio.widget.MediaController;
//import io.vov.vitamio.widget.VideoView;
//
///**
// * Created by Besttone on 2016/2/3.
// */
//public class MediaPlayerVideoActivity extends Activity implements MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener {
//    /**
//     * TODO: Set the path variable to a streaming video URL or a local media file
//     * path.
//     */
//    private String path;//= "http://cache.m.iqiyi.com/dc/dt/mobile/20141225/33/81/5490094a95d702d2ede0835df37c5bf8.m3u8?qypid=3045892409_22&qd_src=5be6a2fdfe4f4a1a8c7b08ee46a18887&qd_tm=1427115639000&qd_ip=121.41.119.169&qd_sc=02acb1123af880e7a29314edd4cce074";
//    private Uri uri;
//    private VideoView mVideoView;
//    private ProgressBar pb;
//    private TextView downloadRateView, loadRateView;
//    private Activity mActivity;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_media_player_video);
//        mActivity = this;
//        mVideoView = (VideoView) findViewById(R.id.buffer);
//        pb = (ProgressBar) findViewById(R.id.probar);
//
//        downloadRateView = (TextView) findViewById(R.id.download_rate);
//        loadRateView = (TextView) findViewById(R.id.load_rate);
//
//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            path = bundle.getString("path");
//        }
//        if (path == "") {
//            // Tell the user to provide a media file URL/path.
//            Toast.makeText(
//                    this,
//                    "Please edit VideoBuffer Activity, and set path"
//                            + " variable to your media file URL/path", Toast.LENGTH_LONG).show();
//            return;
//        } else {
//      /*
//       * Alternatively,for streaming media you can use
//       * mVideoView.setVideoURI(Uri.parse(URLstring));
//       */
//            uri = Uri.parse(path);
//            mVideoView.setVideoURI(uri);
//            mVideoView.setMediaController(new MediaController(this));
//            mVideoView.requestFocus();
//            mVideoView.setOnInfoListener(this);
//            mVideoView.setOnBufferingUpdateListener(this);
//            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                @Override
//                public void onPrepared(MediaPlayer mediaPlayer) {
//                    // optional need Vitamio 4.0
//                    mediaPlayer.setPlaybackSpeed(1.0f);
//                }
//            });
//           /* mVideoView.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View view, MotionEvent motionEvent) {
//                    LogUtils.e("-----setOnTouchListener-----");
//                    mIeverPopuWindow = new IEPopupDialog(mActivity);
//                    mIeverPopuWindow.showAtLocation(mVideoView,Gravity.TOP, 0, 0); //设置layout在PopupWindow中显示的位置
//                    return false;
//                }
//            });*/
////http://pl.youku.com/playlist/m3u8?vid=211952939&type=mp4&ts=1427963205&keyframe=0&ep=XgSexcr2o9joQkH8E0i8gTK%2BfyNzio2PZpKNt4i0FG6R%2BzU%2Bt08V4zw4gxlj6duo&sid=842795805253312a6cf10&token=2455&ctype=20&ev=1&oip=2032760745&uc_param_str=xk
//        }
//    }
//
//    @Override
//    public boolean onInfo(MediaPlayer mp, int what, int extra) {
//        switch (what) {
//            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
//                if (mVideoView.isPlaying()) {
//                    mVideoView.pause();
//                    pb.setVisibility(View.VISIBLE);
//                    downloadRateView.setText("");
//                    loadRateView.setText("");
//                    downloadRateView.setVisibility(View.VISIBLE);
//                    loadRateView.setVisibility(View.VISIBLE);
//
//                }
//                break;
//            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
//                mVideoView.start();
//                pb.setVisibility(View.GONE);
//                downloadRateView.setVisibility(View.GONE);
//                loadRateView.setVisibility(View.GONE);
//                break;
//            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
//                downloadRateView.setText("" + extra + "kb/s" + "  ");
//                break;
//        }
//        return true;
//    }
//
//    @Override
//    public void onBufferingUpdate(MediaPlayer mp, int percent) {
//        loadRateView.setText(percent + "%");
//        LogUtils.e(percent + "%");
//    }
//}