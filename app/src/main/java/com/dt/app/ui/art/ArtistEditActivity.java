package com.dt.app.ui.art;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTUser;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.bean.QiNiuToken;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.utils.Constant;
import com.dt.app.utils.FileUtils;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ActionBarView.ActionBarLinstener;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.qiniu.android.storage.UploadManager;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 编辑用户信息
 * 
 * @author libin
 * 
 */
public class ArtistEditActivity extends BaseActivity   implements OnClickListener {
 
	private ActionBarView mActionBarView;
 
	private BitmapUtils mBitmapUtils;
	
	@ViewInject(R.id.ll_artist_header)
	private LinearLayout ll_artist_header;
	@ViewInject(R.id.iv_artist_user_icon)
	private ImageView iv_artist_user_icon;
	@ViewInject(R.id.tv_artist_user_name)
	private EditText tv_artist_user_name;
	@ViewInject(R.id.tv_artist_user_location)
	private EditText tv_artist_user_location;
 
	@ViewInject(R.id.tv_artist_desc)
	private EditText tv_artist_desc;
	@ViewInject(R.id.iv_artist_reward_collection)
	private ImageView iv_artist_reward_collection;
	@ViewInject(R.id.iv_artist_nosign)
	private ImageView iv_artist_nosign;
	
//	private EditHeaderPopupWindow popupWindow;
//	private ImageUtils mImageUtils;
	UploadManager uploadManager;
	private int reqeustHeaderPath = 22;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dt_artist_edit_main);
		ViewUtils.inject(this);
		uploadManager = new UploadManager();
		initView();
		initQiNiuToken();
 
	}
	private void initView() {
//		popupWindow = new EditHeaderPopupWindow(this);
//		mImageUtils = new ImageUtils(this);
		mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
		mActionBarView = new ActionBarView(this);
		mActionBarView.setBackground(R.color.black);
		String nickname = PreferencesUtils.getString(this, Constant.PrefrencesPt.DTnickname);
		mActionBarView.setCenterTitle(R.mipmap.close_white,R.color.white, nickname, ActionBarView.FINISH_TYPE_4);
		mActionBarView.addImageAction(R.id.title_bar_right_img2, R.mipmap.right_white);
		mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
			@Override
			public void onClick(View view) {
				if (view.getId()==R.id.title_bar_right_img2) {
					String nickname = tv_artist_user_name.getText().toString().trim();
					String signature = tv_artist_desc.getText().toString().trim();
					if (nickname.length()==0) {
						ToastUtils.showTextToast(ArtistEditActivity.this, "昵称不能为空");
						return;
					}
					if (headerPath!=null && logo!=null) {
						if (isCustomPic) {
							load(nickname, signature);
						}else {
							updateUserHeader(nickname, signature);
						}
					}else {
						load(nickname, signature);
					}
				}
			}
		});
		if (PreferencesUtils.getInt(this, Constant.PrefrencesPt.DTisSigned)==1) {
			iv_artist_nosign.setVisibility(View.GONE);
			iv_artist_reward_collection.setVisibility(View.VISIBLE);
		}else {
			iv_artist_nosign.setVisibility(View.VISIBLE);
			iv_artist_reward_collection.setVisibility(View.GONE);
		}
		
		String location = PreferencesUtils.getString(this, Constant.PrefrencesPt.DTLocation);
		if (!TextUtils.isEmpty(location)) {
			tv_artist_user_location.setText(""+location);
		}
		
		mBitmapUtils.display(iv_artist_user_icon, PreferencesUtils.getString(this, Constant.PrefrencesPt.DTlogo));
		tv_artist_user_name.setText(nickname);
		tv_artist_desc.setText(PreferencesUtils.getString(this, Constant.PrefrencesPt.DTSignature));

		tv_artist_user_name.setTextColor(getResources().getColor(R.color.title_gray_color));
		tv_artist_user_location.setTextColor(getResources().getColor(R.color.title_gray_color));
		tv_artist_desc.setTextColor(getResources().getColor(R.color.title_gray_color));
		iv_artist_user_icon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ArtistEditActivity.this,ArtistHeaderActivity.class);
				startActivityForResult(intent, reqeustHeaderPath);
				
			}
		});
	}
	
	
 

	private void load(String nickname,String signature) {
		try {
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put("nickname", nickname);
			data.put("signature", signature);
			data.put("location", tv_artist_user_location.getText().toString().trim());
			if (logo!=null) {
				if (isCustomPic) {
					data.put("logo", headerPath);
				}else {
					data.put("logo", logo);
				}
				
			}
			RequestApi.commonRequest(this, Constant.URL.DTUserProfileUpdate, data,
					new ResultLinstener<DTUser>() {
						@Override
						public void onSuccess(DTUser obj) {
							if (obj!=null && obj.getCode()==1) {
								MemberBean memberBean = obj.getData().getMember();
								ToastUtils.showTextToast(ArtistEditActivity.this, "编辑成功");
								if (memberBean!=null) {
									PreferencesUtils.putString(ArtistEditActivity.this, Constant.PrefrencesPt.DTnickname, memberBean.getNickname());
									PreferencesUtils.putString(ArtistEditActivity.this, Constant.PrefrencesPt.DTSignature, memberBean.getSignature());
									PreferencesUtils.putString(ArtistEditActivity.this, Constant.PrefrencesPt.DTLocation, memberBean.getLocation());
								}
								if (headerPath!=null) {
									FileUtils.deleteFile(headerPath);
								}
								if (logo!=null) {
									String oldLogo = PreferencesUtils.getString(ArtistEditActivity.this, Constant.PrefrencesPt.DTlogo);
									mBitmapUtils.clearCache(oldLogo);
									mBitmapUtils.clearMemoryCache(oldLogo);
									mBitmapUtils.clearDiskCache(oldLogo);
									String newLogo = null;
									try {
										newLogo = obj.getData().getMember().getLogo();
									} catch (Exception e) {
									}
									mBitmapUtils.display(iv_artist_user_icon, newLogo);
									PreferencesUtils.putString(ArtistEditActivity.this, Constant.PrefrencesPt.DTlogo,newLogo);
								}
								setResult(RESULT_OK);
								finish();
							}
						}

						@Override
						public void onFailure(String obj) {
							finish();
						}

						@Override
						public void onException(String exception) {
							finish();
						}
					}, new DTUser());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void onClick(View v) {
		
	}
	
	private boolean isCustomPic = false;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == reqeustHeaderPath && resultCode==RESULT_OK) {
			Long userid = PreferencesUtils.getLong(ArtistEditActivity.this, Constant.PrefrencesPt.DTid);
		    logo = "logos/"+userid+"/"+ UUID.randomUUID().toString()+".jpg";
		    headerPath = data.getStringExtra("url");
		    isCustomPic = data.getBooleanExtra("isCustomPic", false);
		    mBitmapUtils.display(iv_artist_user_icon, headerPath);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private String logo;
	private String headerPath;
	
	public void updateUserHeader(final String nickname,final String signature){
		try {
			RequestApi.updatePhoto(headerPath, logo,mQiNiuToken.getToken(), new ResultLinstener<JSONObject>(){
				@Override
				public void onSuccess(JSONObject obj) {
					if (obj!=null) {
						load(nickname, signature);
					}else{
						logo = null;
					}
						
				}

				@Override
				public void onFailure(String obj) {
					
				}

				@Override
				public void onException(String exception) {
					
				}
				
			});
		} catch (Exception e) {
		}
		
	}
	
	
	
	private QiNiuToken mQiNiuToken;
    private void initQiNiuToken(){
    	try {
    		Map<String, Object> data = new HashMap<String, Object>();
    		RequestApi.postCommon(this, Constant.URL.DTUploadToken, data, new ResultLinstener<QiNiuToken>() {
              @Override
              public void onSuccess(QiNiuToken obj) {
                   if (obj!=null) {
                	   mQiNiuToken = obj;
				   }
              }

              @Override
              public void onFailure(String obj) {
              }

              @Override
              public void onException(String exception) {
              }

          }, new QiNiuToken());
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
}
