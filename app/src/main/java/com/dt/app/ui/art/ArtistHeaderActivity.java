package com.dt.app.ui.art;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.dt.app.DTApplication;
import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTInitData;
import com.dt.app.listener.HandleCallbackSimple;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ImageUtils;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.umeng.socialize.net.v;

/**
 * 获取用户图像的路径
 * 
 * @author libin
 * 
 */
public class ArtistHeaderActivity extends BaseActivity   implements OnClickListener {
 
 
	@ViewInject(R.id.iv_back)
	private ImageView iv_back;
	@ViewInject(R.id.ll_user_headers)
	private LinearLayout ll_user_headers;
	@ViewInject(R.id.iv_take_picture)
	private ImageView iv_take_picture;
	@ViewInject(R.id.iv_select_picture)
	private ImageView iv_select_picture;
 
	private ImageUtils mImageUtils;
	private BitmapUtils mBitmapUtils;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dt_artist_header_main);
		ViewUtils.inject(this);
		initView();
		initUserHeadList();
 
	}
	private void initView() {
		mImageUtils = new ImageUtils(this);
		mBitmapUtils = new BitmapUtils(this);
		iv_take_picture.setOnClickListener(this);
		iv_select_picture.setOnClickListener(this);
		iv_back.setOnClickListener(this);
	}
 
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_take_picture:
			mImageUtils.take();
			break;
		case R.id.iv_select_picture:
			mImageUtils.startActivityOne(true);
			break;
		case R.id.iv_back:
			finish();
			break;
		}
	}
	
	//初始化用户列表
	private void initUserHeadList() {
		try {
			List<DTInitData.LogoData> logoDatas = DTApplication.getInstance()
					.getLogoDataList();
			int count = logoDatas.size();
			for (int i = 0; i < count; i++) {
				final DTInitData.LogoData data = logoDatas.get(i);
				CircleImageView circleImageView = new CircleImageView(this);
				int width = DensityUtil.dip2px(this, 85);
				LinearLayout.LayoutParams layoutParams = new LayoutParams(width, width);
				layoutParams.leftMargin = DensityUtil.dip2px(this, 20);
				circleImageView.setLayoutParams(layoutParams);
				circleImageView.setScaleType(ScaleType.CENTER_CROP);
				circleImageView.setImageResource(R.mipmap.girl_default);
				mBitmapUtils.display(circleImageView, data.getImageUrl());
				ll_user_headers.addView(circleImageView);
				circleImageView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						finish(data.getImageUrl(), true);
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		mImageUtils.onActivityResult(requestCode, resultCode, data, this, new HandleCallbackSimple<ArrayList<String>>(){
			@Override
			public void onSuccess(ArrayList<String> t) {
			    try {
				    finish(t.get(0),false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void  finish(String headUrl,boolean isCustomPic){
		if (!TextUtils.isEmpty(headUrl)) {
			Intent intent = new Intent();
			intent.putExtra("url", headUrl);
			intent.putExtra("isCustomPic", isCustomPic);
			setResult(RESULT_OK, intent);
		}
		finish();
	}
	
}
