package com.dt.app.ui.works;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.dt.app.bean.EveryAdResponse;

import java.util.ArrayList;

/**
 * Created by Besttone on 2016/2/23.
 */
public class MyLinearLayout extends LinearLayout {
    private Context context;

    private ArrayList<EveryAdResponse.Theme2.OutCell> outCells;

    public final int VIEW_TYPE_COUNT = 3;

    public final int VIEW_TYPE_TITLE = 0;

    public final int VIEW_TYPE_CONTENT = 1;

    public final int VIEW_TYPE_IMAGE = 2;

    public MyLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setData(ArrayList<EveryAdResponse.Theme2.OutCell> outCells) {
        this.outCells = outCells;
    }

    public void init(){

    }
}
