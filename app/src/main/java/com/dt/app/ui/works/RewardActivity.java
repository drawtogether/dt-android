package com.dt.app.ui.works;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Relation;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.CommonAdapter;
import com.dt.app.adapter.ViewHolder;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.Donation;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.Me;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.utils.AnimUtils;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.ActionBarView;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 打赏
 * 13916361767
 *
 * @author libin
 */
public class RewardActivity extends BaseActivity implements OnClickListener {
    private ActionBarView actionbar_base;

    @ViewInject(R.id.ll_reward_up)
    private LinearLayout ll_reward_up;
    @ViewInject(R.id.tv_reward_score_title)
    private TextView tv_reward_score_title;
    @ViewInject(R.id.tv_reward_score_1)
    private TextView tv_reward_score_1;
    @ViewInject(R.id.tv_reward_score_2)
    private TextView tv_reward_score_2;
    @ViewInject(R.id.tv_reward_score_3)
    private TextView tv_reward_score_3;

    @ViewInject(R.id.iv_reward_icon_bg)
    private ImageView iv_reward_icon_bg;
    @ViewInject(R.id.iv_reward_icon)
    private ImageView iv_reward_icon;
    //下半部分，上层
    @ViewInject(R.id.gv_reward_person)
    private GridView gv_reward_person;
    @ViewInject(R.id.ll_reward_person_main)
    private LinearLayout ll_reward_person_main;
    @ViewInject(R.id.ll_reward_score_count)
    private LinearLayout ll_reward_score_count;
    @ViewInject(R.id.tv_reward_score_count)
    private TextView tv_reward_score_count;

    //下半部分，下层
    @ViewInject(R.id.ll_reward_pic_main)
    private LinearLayout ll_reward_pic_main;
    @ViewInject(R.id.tv_reward_pic_title)
    private TextView tv_reward_pic_title;
    @ViewInject(R.id.iv_reward_pic)
    private ImageView iv_reward_pic;
    @ViewInject(R.id.ll_reward_scroll)
    private LinearLayout ll_reward_scroll;
    @ViewInject(R.id.ll_top_layer)
    private LinearLayout ll_top_layer;

    //--------

    private int lastId;
    private boolean backgroudGrendient = false;

    private BitmapUtils mBitmapUtils;
    private long workId;//作品id
    private String rewardUrl;

    private Donation mDonation;

    private CommonAdapter<Member> mAdapter;
    private List<Member> members;
    private int jifen ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reward_main);
        ViewUtils.inject(this);
        initView();

        loadWork();
    }


    private void initView() {
        int width = ZTDeviceInfo.getInstance().getWidthDevice();
        int height = ZTDeviceInfo.getInstance().getHeightDevice();
        ll_reward_scroll.setLayoutParams(com.dt.app.utils.ViewUtils.getLayoutParams(ll_reward_scroll, width, height));
        actionbar_base = new ActionBarView(this);
        actionbar_base.setCenterTitle(R.mipmap.dt_back_w, R.color.white, "打赏", ActionBarView.FINISH_TYPE_4);
        actionbar_base.setBackground(R.color.transparent);
        actionbar_base.addTextViewAction(1, 0, "To");
        ImageView imageView = (ImageView) actionbar_base.addImageAction(2, R.drawable.dt_app_icon, true);
        mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
        workId = getIntent().getLongExtra("workId", 0);
        rewardUrl = getIntent().getStringExtra("rewardUrl");
        mBitmapUtils.display(imageView, getIntent().getStringExtra("logo"));
        int itemWidth = ZTDeviceInfo.getInstance().getWidthDevice() / 2;
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(itemWidth, itemWidth);
        params.topMargin = DensityUtil.dip2px(this, 10);
        iv_reward_pic.setLayoutParams(params);
        mBitmapUtils.display(iv_reward_pic, rewardUrl);

        tv_reward_score_1.setOnClickListener(this);
        tv_reward_score_2.setOnClickListener(this);
        tv_reward_score_3.setOnClickListener(this);
        iv_reward_icon.setOnClickListener(this);

//        ll_reward_pic_main.setVisibility(View.GONE);
//        ll_reward_person_main.setVisibility(View.GONE);

        members = new ArrayList<UserWorks.Member>();
        mAdapter = new CommonAdapter<Member>(this, members, R.layout.artist_all_fragment_item) {
            @Override
            public void convert(ViewHolder helper, final Member item) {
                helper.setText(R.id.tv_artist_name, item.getNickname());
                mBitmapUtils.display(helper.getView(R.id.iv_artist_all), item.getLogo());
                helper.getView(R.id.iv_artist_all).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        CommonAcitivty.startArtistSpaceActivity(RewardActivity.this, item.getMemberId());
                    }
                });


            }
        };
        gv_reward_person.setAdapter(mAdapter);

    }


    //获取打赏信息
    private void loadWork() {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("worksId", workId);
            RequestApi.postCommon(this, Constant.URL.DTDonationView, data,
                    new ResultLinstener<Donation>() {
                        @Override
                        public void onSuccess(final Donation obj) {
                            if (obj != null) {
                                members.clear();
                                mDonation = obj;
                                List<Member> mList = obj.getDonates();

                                if (mList != null) {
                                    int count = mList.size();
                                    count = count > 8 ? 8 : count;
                                    for (int i = 0; i < count; i++) {
                                        members.add(mList.get(i));
                                        System.out.println("listview ....... " + mList.get(i).getId());
                                    }
                                    mAdapter.notifyDataSetChanged();
                                    int size = mList.size();
                                    tv_reward_score_count.setText("已有" + size + "位爱心打赏");
                                    if (size > 0) {
                                        tv_reward_score_count.setOnClickListener(RewardActivity.this);
                                    }
                                }
                                if (obj.getWorks() != null) {
                                    tv_reward_pic_title.setText("打赏给喜欢的作品《" + obj.getWorks().getTitle() + "》");
                                }

                                ll_top_layer.setVisibility(View.GONE);

                                UserWork userWork = obj.getWorks();
                                if (userWork != null) {
                                    Me me = userWork.getMe();
                                    if (me != null && me.getIsDonated() == Integer.valueOf(1)) {
                                        System.out.println("--->> " + me.getIsDonated());
                                        isDonationSubmit = true;
//										ll_reward_pic_main.setVisibility(View.GONE);
//									    ll_reward_person_main.setVisibility(View.VISIBLE);
                                        tv_reward_score_title.setText("已打赏");
                                        tv_reward_score_1.setVisibility(View.GONE);
                                        tv_reward_score_2.setVisibility(View.GONE);
                                        tv_reward_score_3.setVisibility(View.GONE);
                                        ll_reward_up.setBackgroundResource(R.drawable.reward_bg_gradient);
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(String obj) {
                        }

                        @Override
                        public void onException(String exception) {
                        }
                    }, new Donation());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 打赏成功之后就不能在打赏
     */
    private boolean isDonationSubmit = false;

    //打赏
    private void donationSubmit(int jifen) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("worksId", workId);
            data.put("jifen", jifen);
            LogUtils.e("---jifen-->"+jifen);
            RequestApi.postCommon(this, Constant.URL.DTDonationSubmit, data,
                    new ResultLinstener<String>() {
                        @Override
                        public void onSuccess(String obj) {
                            loadWork();
                            if (rewardScoreIndex > 0) {
                                if (rewardScoreIndex == 1) {
                                    tv_reward_score_2.setVisibility(View.GONE);
                                    tv_reward_score_3.setVisibility(View.GONE);
                                } else if (rewardScoreIndex == 2) {
                                    tv_reward_score_1.setVisibility(View.GONE);
                                    tv_reward_score_3.setVisibility(View.GONE);
                                } else if (rewardScoreIndex == 3) {
                                    tv_reward_score_1.setVisibility(View.GONE);
                                    tv_reward_score_2.setVisibility(View.GONE);
                                }
                            }
                            isDonationSubmit = true;
                            tv_reward_score_title.setText("已打赏");
//                            iv_reward_icon_bg.setBackgroundResource(R.drawable.oval_gray);
                            closeAnimation();
                            ToastUtils.showTextToast(RewardActivity.this, "打赏成功");

                        }

                        @Override
                        public void onFailure(String obj) {
                            isDonationSubmit = false;
                        }

                        @Override
                        public void onException(String exception) {
                            isDonationSubmit = false;
                        }
                    }, new String());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_reward_score_1:
                if (isDonationSubmit)
                    return;
                switchColor(R.id.tv_reward_score_1);
                lastId = R.id.tv_reward_score_1;
                jifen = 10;
                break;
            case R.id.tv_reward_score_2:
                if (isDonationSubmit)
                    return;
                switchColor(R.id.tv_reward_score_2);
                lastId = R.id.tv_reward_score_2;
                jifen = 25;
                break;
            case R.id.tv_reward_score_3:
                if (isDonationSubmit)
                    return;
                switchColor(R.id.tv_reward_score_3);
                lastId = R.id.tv_reward_score_3;
                jifen = 50;
                break;
            case R.id.iv_reward_icon:
                if (backgroudGrendient && !isDonationSubmit) {
                    donationSubmit(jifen);
                }

                break;
            case R.id.tv_reward_score_count:
                Intent mIntent = new Intent(RewardActivity.this, WorkLovesActivity.class);
                mIntent.putExtra("donation", true);
                mIntent.putExtra("worksId", mDonation.getWorks().getId());
                startActivity(mIntent);
                break;

            default:
                break;
        }
    }

    private void switchColor(int id) {
        tv_reward_score_1.setTextColor(getResources().getColor(R.color.white));
        tv_reward_score_2.setTextColor(getResources().getColor(R.color.white));
        tv_reward_score_3.setTextColor(getResources().getColor(R.color.white));
        if (lastId == id && backgroudGrendient) {
//            closeAnimation();
            clearHeartAnimation();
            backgroudGrendient = false;
            tv_reward_score_title.setText("请选择打赏积分");
//            iv_reward_icon_bg.setBackgroundResource(R.drawable.oval_gray);
            iv_reward_icon_bg.setVisibility(View.GONE);
            iv_reward_icon.setImageResource(R.mipmap.reward_heart_gh);
            ll_reward_up.setBackgroundColor(getResources().getColor(R.color.black));
            return;
        }

        iv_reward_icon_bg.setVisibility(View.VISIBLE);
        iv_reward_icon.setImageResource(R.drawable.reward_heart);

        ll_reward_up.setBackgroundResource(R.drawable.reward_bg_gradient);

        if (!backgroudGrendient) {
//            openAnimation();
            heartAnimation();
            ll_reward_up.startAnimation(AnimUtils.aphlaAnimation(true, 500));
        }


        backgroudGrendient = true;
        tv_reward_score_title.setText("点击爱心确认打赏");
        switch (id) {
            case R.id.tv_reward_score_1:
                rewardScoreIndex = 1;
                tv_reward_score_1.setTextColor(getResources().getColor(R.color.reward_points_select));
                break;
            case R.id.tv_reward_score_2:
                rewardScoreIndex = 2;
                tv_reward_score_2.setTextColor(getResources().getColor(R.color.reward_points_select));
                break;
            case R.id.tv_reward_score_3:
                rewardScoreIndex = 3;
                tv_reward_score_3.setTextColor(getResources().getColor(R.color.reward_points_select));
                break;
        }
    }

    private int rewardScoreIndex = -1;

    private void openAnimation() {
//        ll_reward_pic_main.setVisibility(View.VISIBLE);
//        ll_reward_person_main.setVisibility(View.GONE);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.list_bottom_to_top);
        ll_reward_pic_main.startAnimation(animation);
    }

    private void closeAnimation() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_out_to_bottom);
        animation.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                clearHeartAnimation();
//                ll_reward_person_main.setVisibility(View.VISIBLE);
//                ll_reward_pic_main.setVisibility(View.GONE);
                iv_reward_icon_bg.setVisibility(View.GONE);
                iv_reward_icon.setImageResource(R.mipmap.reward_heart_gh);
            }
        });
        ll_reward_pic_main.startAnimation(animation);
    }

    public void heartAnimation() {
        ScaleAnimation scaleAnim = (ScaleAnimation) AnimUtils.scaleAnimation(true, 1.0f, 1.08f, 1.0f, 1.08f, 500);
        scaleAnim.setRepeatMode(Animation.REVERSE);
        scaleAnim.setRepeatCount(Animation.INFINITE);
        ScaleAnimation scaleAnim2 = (ScaleAnimation) AnimUtils.scaleAnimation(true, 0.8f, 1.15f, 0.8f, 1.15f, 500);

        scaleAnim2.setRepeatMode(Animation.REVERSE);
        scaleAnim2.setRepeatCount(Animation.INFINITE);
        iv_reward_icon_bg.startAnimation(scaleAnim);
        iv_reward_icon.startAnimation(scaleAnim2);
    }

    public void clearHeartAnimation() {
        iv_reward_icon_bg.clearAnimation();
        iv_reward_icon.clearAnimation();
    }
}
