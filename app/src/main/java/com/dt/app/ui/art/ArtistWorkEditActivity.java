package com.dt.app.ui.art;

import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTUser;
import com.dt.app.bean.TimeLine;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.FileUtils;
import com.dt.app.utils.InputMethodUtil;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ActionBarView.ActionBarLinstener;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 
 * @author libin
 * 
 */
public class ArtistWorkEditActivity extends BaseActivity {

	@ViewInject(R.id.et_work_title)
	private EditText et_work_title;
	@ViewInject(R.id.et_work_desc)
	private EditText et_work_desc;
	private long workid;
 
	private ActionBarView mActionBarView;
	private int position =-1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dt_art_work_edit_main);
		ViewUtils.inject(this);
		initView();

	}

	private void initView() {
		try {

			et_work_title.setText(getIntent().getStringExtra("title"));
			workid = getIntent().getLongExtra("workid", 0);
			et_work_desc.setText(getIntent().getStringExtra("desc"));
			position = getIntent().getIntExtra("position", -1);
			
			mActionBarView = new ActionBarView(this);
			mActionBarView.setBackground(R.color.white);
			mActionBarView.setBackHome(R.mipmap.dt_back_b, "", ActionBarView.FINISH_TYPE_4);
			mActionBarView.addImageAction(R.id.title_bar_right_img2, R.mipmap.right_black);
			mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
				@Override
				public void onClick(View view) {
					if (view.getId()==R.id.title_bar_right_img2) {
						if (TextUtils.isEmpty(et_work_title.getText().toString())) {
							ToastUtils.showTextToast(ArtistWorkEditActivity.this, "标题不能为空");
						}
						if (TextUtils.isEmpty(et_work_desc.getText().toString())) {
							ToastUtils.showTextToast(ArtistWorkEditActivity.this, "内容不能为空");
						}
						edit(et_work_title.getText().toString(),et_work_desc.getText().toString());
					}
				}
			});
			
			InputMethodUtil.enableSoftInput(this);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void edit(final String title,final String content) {
		try {
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put("workid", workid);
			data.put("title", title);
			data.put("content", content);
			RequestApi.postCommon(this, Constant.URL.DTWorksUpdate, data,
					new ResultLinstener<String>() {
						@Override
						public void onSuccess(String obj) {
								ToastUtils.showTextToast(ArtistWorkEditActivity.this, "编辑成功");
								Intent intent = new Intent();
								 
								intent.putExtra("title", title);
								intent.putExtra("content", content);
								intent.putExtra("workid", workid);
								intent.putExtra("position", position);
								setResult(RESULT_OK,intent);
								finish();
							 
						}

						@Override
						public void onFailure(String obj) {
							finish();
						}

						@Override
						public void onException(String exception) {
							finish();
						}
					}, new String());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
