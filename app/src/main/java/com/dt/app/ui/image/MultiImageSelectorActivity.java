package com.dt.app.ui.image;

import java.io.File;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.TypedValue;
import android.view.View;

import com.dt.app.R;

/**
 * 多图选择
 * Created by Nereo on 2015/4/7.
 */
public class MultiImageSelectorActivity extends FragmentActivity implements MultiImageSelectorFragment.Callback{

    /** 最大图片选择次数，int类型，默认9 */
    public static final String EXTRA_SELECT_COUNT = "max_select_count";
    /** 图片选择模式，默认多选 */
    public static final String EXTRA_SELECT_MODE = "select_count_mode";
    /** 是否显示相机，默认显示 */
    public static final String EXTRA_SHOW_CAMERA = "show_camera";
    /** 选择结果，返回为 ArrayList&lt;String&gt; 图片路径集合  */
    public static final String EXTRA_RESULT = "select_result";
    /** 默认选择集 */
    public static final String EXTRA_DEFAULT_SELECTED_LIST = "default_list";
    /** 是否裁剪图片 */
    public static final String EXTRA_CLPE_IMAGE = "extra_clpe_image";
    public static final String EXTRA_CLPE_IMAGE_SYSTEM = "extra_clpe_image_system";

    /** 单选 */
    public static final int MODE_SINGLE = 0;
    /** 多选 */
    public static final int MODE_MULTI = 1;
    /** 是否裁剪图片 */
    public boolean clipImage = false;
    //调用系统裁剪工具
    public boolean clipImageSystem = false;

    private ArrayList<String> resultList = new ArrayList<String>();
   
    private int mDefaultCount;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);

        Intent intent = getIntent();
        mDefaultCount = intent.getIntExtra(EXTRA_SELECT_COUNT, 9);
        int mode = intent.getIntExtra(EXTRA_SELECT_MODE, MODE_MULTI);
        boolean isShow = intent.getBooleanExtra(EXTRA_SHOW_CAMERA, true);
        if(mode == MODE_MULTI && intent.hasExtra(EXTRA_DEFAULT_SELECTED_LIST)) {
            resultList = intent.getStringArrayListExtra(EXTRA_DEFAULT_SELECTED_LIST);
        }
        clipImage = intent.getBooleanExtra(EXTRA_CLPE_IMAGE, false);
        clipImageSystem = intent.getBooleanExtra(EXTRA_CLPE_IMAGE_SYSTEM, false);
        
        
        Bundle bundle = new Bundle();
        bundle.putInt(MultiImageSelectorFragment.EXTRA_SELECT_COUNT, mDefaultCount);
        bundle.putInt(MultiImageSelectorFragment.EXTRA_SELECT_MODE, mode);
        bundle.putBoolean(MultiImageSelectorFragment.EXTRA_SHOW_CAMERA, isShow);
        bundle.putStringArrayList(MultiImageSelectorFragment.EXTRA_DEFAULT_SELECTED_LIST, resultList);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.image_grid, Fragment.instantiate(this, MultiImageSelectorFragment.class.getName(), bundle))
                .commit();

        // 返回按钮
        findViewById(R.id.ll_image_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        
        findViewById(R.id.ll_image_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(resultList != null && resultList.size() >0){
                    // 返回已选择的图片数据
                    Intent data = new Intent();
                    data.putStringArrayListExtra(EXTRA_RESULT, resultList);
                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        });
    }

    @Override
    public void onSingleImageSelected(String path) {
    	//单选
    	Intent data = new Intent();
    	if (clipImage) {
		 
		}else if (clipImageSystem) {
			File temp = new File(path);
			startPhotoZoom(Uri.fromFile(temp), 20);
		}else {
			 resultList.add(path);
		     data.putStringArrayListExtra(EXTRA_RESULT, resultList);
		     setResult(RESULT_OK, data);
		     finish();
		}
    }

    @Override
    public void onImageSelected(String path) {
        if(!resultList.contains(path)) {
            resultList.add(path);
        }
    }

    @Override
    public void onImageUnselected(String path) {
        if(resultList.contains(path)){
            resultList.remove(path);
            
        } 
    }

    @Override
    public void onCameraShot(File imageFile) {
		if (imageFile != null) {//拍照之后的图片
			if (clipImageSystem || clipImage) {
				System.out.println("-------clipImageSystem-------->"+imageFile.getAbsolutePath());
				startPhotoZoom(Uri.fromFile(imageFile), 20);
			} else {
				Intent data = new Intent();
				resultList.add(imageFile.getAbsolutePath());
				data.putStringArrayListExtra(EXTRA_RESULT, resultList);
				setResult(RESULT_OK, data);
				finish();
			}
		}
    }
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Intent intent = new Intent();
		System.out.println("iamge selector ....------------------------------>");
		if (requestCode==10 && resultCode==RESULT_OK) {
			byte[] b = data.getByteArrayExtra("bitmap");
			Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
			if (bitmap != null){
//				System.out.println("-------clip---------"+bitmap);
				intent.putExtra("bitmap",bitmap);
				setResult(RESULT_OK, intent);
			}else {
				setResult(RESULT_CANCELED, intent);
			}
			finish();
            
		}else if (requestCode==20 && resultCode==RESULT_OK){
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap bitmap = extras.getParcelable("data");
				intent.putExtra("bitmap",bitmap);
//				System.out.println("-------clip---------"+bitmap);
				setResult(RESULT_OK, intent);
			}else {
				setResult(RESULT_CANCELED, intent);
			}
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
		
        
	}
    
    public void startPhotoZoom(Uri uri,int requestCode) {
    	int width = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", width);
		intent.putExtra("outputY", width);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
		intent.putExtra("return-data", true);
		startActivityForResult(intent, requestCode);
	}

	@Override
	public void clear() {
		resultList.clear();
	}
    
    //http://blog.csdn.net/floodingfire/article/details/8144617
 
}
