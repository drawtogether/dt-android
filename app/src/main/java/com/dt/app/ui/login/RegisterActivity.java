package com.dt.app.ui.login;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTVerfiyCode;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.InputMethodUtil;
import com.dt.app.utils.RegisterHelper;
import com.dt.app.utils.ToastUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.HashMap;

/**
 * Created by caiyuhao on 15-9-6.
 */
public class RegisterActivity extends BaseActivity {
    @ViewInject(R.id.register_et)
    public EditText register_et;
    @ViewInject(R.id.register_sendverfycode)
    public Button register_sendverfycode;
    @ViewInject(R.id.linear_mobile_email)
    public LinearLayout linear_mobile_email;
    @ViewInject(R.id.guid_login_img)
    public ImageView guid_login_img;
    @ViewInject(R.id.register_input_code_et)
    public EditText register_input_code_et;
    @ViewInject(R.id.register_input_pwd)
    public EditText register_input_pwd;
    @ViewInject(R.id.register_input_username)
    public EditText register_input_username;
    @ViewInject(R.id.linear_register_outter)
    private LinearLayout linear_register_outter;
    @ViewInject(R.id.login_register_parent)
    private FrameLayout login_register_parent;
    private Activity mActivity;
    public static Integer Mobile = 1;
    public static Integer Email = 2;
    public Integer input_result = -1;

    public String str_sendverfycode;
    private RegisterHelper mRegisterHelper;
    private int heightDifference = 0;
    private boolean ishasKeybord = false;
    private Handler handler = new Handler();
    public SafeCountTimer safeCountTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_register);
        ViewUtils.inject(this);
        mActivity = this;
        mRegisterHelper = new RegisterHelper(mActivity);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mRegisterHelper.initAnimation();

            }
        }, 100);
        mRegisterHelper.initListener();
        mRegisterHelper.getfocusWidget();
        mRegisterHelper.registerVoid();
        //监听键盘的高度
        linear_register_outter.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            //当键盘弹出隐藏的时候会 调用此方法。
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //获取当前界面可视部分
                mActivity.getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                //获取屏幕的高度
                int screenHeight = mActivity.getWindow().getDecorView().getRootView().getHeight();
                //此处就是用来获取键盘的高度的， 在键盘没有弹出的时候 此高度为0 键盘弹出的时候为一个正数
                heightDifference = screenHeight - r.bottom;
                if (heightDifference > 0 && !ishasKeybord) {
                    LogUtils.e("----------1--- " + screenHeight + ",bottom = " + r.bottom);
                    if (linear_mobile_email.getVisibility() == View.VISIBLE ||
                            register_input_username.getVisibility() == View.VISIBLE ||
                            register_input_pwd.getVisibility() == View.VISIBLE) {
                        LogUtils.e("------2----screenHeight---- " + screenHeight + ",bottom = " + r.bottom);
                        ishasKeybord = true;
                        FrameLayout.LayoutParams btnLp = (FrameLayout.LayoutParams) linear_register_outter.getLayoutParams();
                        btnLp.setMargins(0, 0, 0, 200);
                        linear_register_outter.requestLayout();
                    }
                } else {
                    ishasKeybord = false;
                }
            }
        });
        login_register_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (heightDifference > 0) {
                    InputMethodUtil.hideSoftInput(mActivity);

                }
            }
        });
    }

    @OnClick(R.id.register_sendverfycode)
    public void sendVerfyCode(View view) {
        String emailOrmobile = register_et.getText().toString().trim();
        if (TextUtils.isEmpty(emailOrmobile)) {
            ToastUtils.showTextToast(mActivity, "不能为空");
            return;
        }
        if (input_result != Email && input_result != Mobile) {
            ToastUtils.showTextToast(mActivity, "输入无效");
            return;
        }
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("checkUser", true);
        data.put("loginname", emailOrmobile);
        data.put("usage", "reg");
        safeCountTimer = new SafeCountTimer(60 * 1000, 1000);
        safeCountTimer.start();


        try {
            RequestApi.commonRequest(this, Constant.URL.DTSendVerfyCode, data, new ResultLinstener<DTVerfiyCode>() {

                @Override
                public void onSuccess(DTVerfiyCode obj) {
                    if (obj.getCode() == 1) {
                        //successful
                        str_sendverfycode = obj.getData();
                        register_input_code_et.setVisibility(View.VISIBLE);
                        register_input_code_et.setFocusable(true);
//                        register_input_code_et.setText(str_sendverfycode + "");

                        final Animation animation_verfycode = AnimationUtils.loadAnimation(mActivity, R.anim.register_pwd_in_from_right);
                        register_input_code_et.startAnimation(animation_verfycode);
                        animation_verfycode.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
//                                register_input_code_et.setText(str_sendverfycode + "");
//                                register_input_code_et.setSelection(str_sendverfycode.length());
                                register_input_code_et.setFocusable(true);
                                register_input_code_et.requestFocus();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }

                }

                @Override
                public void onFailure(String obj) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onException(String exception) {
                    // TODO Auto-generated method stub

                }
            }, new DTVerfiyCode());
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    @OnClick(R.id.guid_login_img)
    public void loginClick(View view) {
        this.finish();
        overridePendingTransition(0, R.anim.alpha_out);
    }


    /**
     * 定时器
     */
    public class SafeCountTimer extends CountDownTimer {

        public SafeCountTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            register_sendverfycode.setText("" + l / 1000 + "秒后重发");
            register_sendverfycode.setTextColor(Color.WHITE);
            register_sendverfycode.setBackgroundResource(R.color.background);
            register_sendverfycode.setEnabled(false);
        }

        @Override
        public void onFinish() {
            register_sendverfycode.setEnabled(true);
            register_sendverfycode
                    .setBackgroundResource(R.color.white);
            register_sendverfycode.setTextColor(getResources().getColor(
                    R.color.background));
            register_sendverfycode.setText("发送验证码");
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            this.finish();
            overridePendingTransition(0, R.anim.alpha_out);
        }
        return super.onKeyDown(keyCode, event);
    }
}
