package com.dt.app.ui.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.dt.app.R;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.fragment.ArtGalleryFragment;
import com.dt.app.view.ActionBarView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;

/**
 * Created by caiyuhao on 15-9-13.
 */
public class SearchResultListActivity extends BaseFragmentActivity {
    private ActionBarView mActionBarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_menu_search_result);
        ViewUtils.inject(this);
        initView();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        String title = null;
        int tagId = 0;
        if (bundle != null) {
            title = bundle.getString("title");
            tagId = bundle.getInt("tagId", 0);
        }
        mActionBarView = new ActionBarView(this);
        mActionBarView.setBackground(R.color.black);
        mActionBarView.setCenterTitle(R.mipmap.dt_back_w, R.color.white,
                title, ActionBarView.FINISH_TYPE_4);
        Bundle tempbundle = new Bundle();
        tempbundle.putString("key", title);
        tempbundle.putInt("tagId", tagId);
        LogUtils.e("-----------searchkey = "+title+",tagid = "+tagId);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fl_search_content, Fragment.instantiate(this, ArtGalleryFragment.class.getName(), tempbundle))
                .commit();

    }

}
