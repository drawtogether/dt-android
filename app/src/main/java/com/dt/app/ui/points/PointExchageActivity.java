package com.dt.app.ui.points;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.CommonAdapter;
import com.dt.app.adapter.ViewHolder;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.Address;
import com.dt.app.bean.JifenDetail;
import com.dt.app.bean.JifenDetail.JifenGoods;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.common.CommonApis;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.AnimationListenerImpl;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.utils.ActivityUtils;
import com.dt.app.utils.AnimUtils;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.Utils;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ArrawLinearLayout;
import com.dt.app.view.ArrawLinearLayout.ArrawLayoutListener;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 进入积分兑换界面
 *
 * @author libin
 */
public class PointExchageActivity extends BaseActivity implements
        OnClickListener {
    @ViewInject(R.id.tv_gift_remainder_point)
    private TextView tv_gift_remainder_point;
    @ViewInject(R.id.iv_point_gift_left)
    private ImageView iv_point_gift_left;
    @ViewInject(R.id.tv_point_gift_title)
    private TextView tv_point_gift_title;
    @ViewInject(R.id.tv_point_gift_point)
    private TextView tv_point_gift_point;
    @ViewInject(R.id.tv_exchange_count)
    private ArrawLinearLayout tv_exchange_count;
    @ViewInject(R.id.tv_address_operation)
    private ArrawLinearLayout tv_address_operation;
    @ViewInject(R.id.tv_address_operation_details)
    private TextView tv_address_operation_details;
    @ViewInject(R.id.iv_exchange)
    private ImageView iv_exchange;
    @ViewInject(R.id.ll_exchange_bg)
    private LinearLayout ll_exchange_bg;
    
//    @ViewInject(R.id.dt_point_main_down)
//    private LinearLayout dt_point_main_down;
    @ViewInject(R.id.dt_point_main_up)
    private LinearLayout dt_point_main_up;
    @ViewInject(R.id.ll_top_layer)
    private LinearLayout ll_top_layer;
    
    //兑换成功
    @ViewInject(R.id.rl_exchange_success)
    private RelativeLayout rl_exchange_success;
    @ViewInject(R.id.tv_exchange_look)
	private TextView tv_exchange_look;
	@ViewInject(R.id.iv_success_close)
	private ImageView iv_success_close;
	

    @ViewInject(R.id.ll_point_gift_pic)
    private LinearLayout ll_point_gift_pic;

    private BitmapUtils mBitmapUtils;

    private ActionBarView mActionBarView;

    private boolean addAddress = false;

    public static final String POINT_EXCHAGE = "pointexchage";
    private ListView lv_pop;
    private List<Integer> mDataList;
    private int selectNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_point_gift_page_main);
        ViewUtils.inject(this);

        initView();

        load();
    }

    private void initView() {
        try {

            mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
            mActionBarView = new ActionBarView(this);
            mActionBarView.setBackground(R.color.black);
            mActionBarView.setCenterTitle(R.mipmap.dt_back_w, R.color.white,
                    "积分换礼", ActionBarView.FINISH_TYPE_4);

            tv_address_operation.setOnClickListener(this);
//            tv_exchange_count.setOnClickListener(this);
            ll_exchange_bg.setOnClickListener(this);
            iv_success_close.setOnClickListener(this);
            tv_exchange_look.setOnClickListener(this);
 
            
            tv_address_operation_details.setOnClickListener(this);

            int itemWidth = ZTDeviceInfo.getInstance().getWidthDevice();
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(itemWidth, itemWidth / 2);
            ll_point_gift_pic.setLayoutParams(params);

//            Drawable drawable = getResources().getDrawable(R.mipmap.right_arraw);
//            int dwidth = DensityUtil.dip2px(this, 15);
//            int dheight = DensityUtil.dip2px(this, 20);
//            drawable.setBounds(0, 0, dwidth, dheight);//第一0是距左边距离，第二0是距上边距离，40分别是长宽
//            tv_address_operation.setCompoundDrawables(null, null, drawable, null);//
            tv_address_operation.setText("收货");
            tv_exchange_count.setText("数量  1");
            
            tv_exchange_count.setArrawLayoutListener(new ArrawLayoutListener() {
				@Override
				public void open(boolean isClick) {
					isListViewGone = false;
                	lv_pop.setVisibility(View.VISIBLE);
				}
				
				@Override
				public void close() {
					lv_pop.setVisibility(View.GONE);
				}
			});

            lv_pop = (ListView) findViewById(R.id.lv_pop);
            
            mDataList = new ArrayList<Integer>();
			for (int i = 1; i < 11; i++) {
				mDataList.add(i);
			}
			lv_pop.setAdapter(new CommonAdapter<Integer>(this,mDataList,R.layout.simple_list_item) {
				@Override
				public void convert(ViewHolder helper, Integer item) {
					helper.setText(R.id.tv_number, ""+item);
				}
			});
			lv_pop.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					selectNumber = mDataList.get(position);
                    tv_exchange_count.setText("数量 " + selectNumber);
					lv_pop.setVisibility(View.GONE);
					tv_exchange_count.close();
					
					exchangeQ(selectNumber);
				}
			});
			Utils.setListViewHeightBasedOnChildren(lv_pop);

            ll_exchange_bg.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					System.out.println("isEn "+isEnoughJifen+"  "+addAddress);
					if (isEnoughJifen && addAddress) {
						if (event.getAction()==MotionEvent.ACTION_DOWN) {
							ll_exchange_bg.setBackgroundResource(R.drawable.point_exchange_btn);
						}else if (event.getAction()==MotionEvent.ACTION_UP) {
							ll_exchange_bg.setBackgroundResource(R.drawable.oval_red);
						}
					}
					return false;
				}
			});
//            dt_point_main_up.getViewTreeObserver().addOnGlobalLayoutListener(
//				    new OnGlobalLayoutListener() {
//				        @Override
//				        public void onGlobalLayout() {
//				        	maxHegiht = (int) (ZTDeviceInfo.getInstance().getHeightDevice() - dt_point_main_up.getMeasuredHeight()
//									-DensityUtil.dip2px(PointExchageActivity.this, 38));
//							System.out.println("0000000  "+maxHegiht);
//							LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ZTDeviceInfo.getInstance().getWidthDevice(), maxHegiht);
////							dt_point_main_down.setPadding(0, dt_point_main_up.getMeasuredHeight(), 0, 0);
//							dt_point_main_down.setLayoutParams(layoutParams);  
//							dt_point_main_up.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//				        }
//				});
			dt_point_main_up.addOnLayoutChangeListener(new OnLayoutChangeListener() {
				@Override
				public void onLayoutChange(View v, int left, int top, int right,
						int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
					if (!isListViewGone) {
						return;
					}
					maxHegiht = (int) (ZTDeviceInfo.getInstance().getHeightDevice() - dt_point_main_up.getMeasuredHeight()
							-DensityUtil.dip2px(PointExchageActivity.this, 38));
					int itemW = DensityUtil.dip2px(PointExchageActivity.this, 80);
					int topMargin = (maxHegiht -itemW)/2;
					int padding = DensityUtil.dip2px(PointExchageActivity.this, 10);
//					System.out.println("0000000  "+maxHegiht+" "+dt_point_main_up.getMeasuredHeight());
					LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(itemW, itemW);
					layoutParams.setMargins(0, topMargin, 0, topMargin);
					ll_exchange_bg.setLayoutParams(layoutParams); 
					ll_exchange_bg.setPadding(padding, padding, padding, padding);
				}
			});
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private boolean isListViewGone = true;
    private int maxHegiht ;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_address_operation:
            case R.id.tv_address_operation_details:
                if (addAddress) {
                    Intent mIntent = new Intent(PointExchageActivity.this, AddAddressActivity.class);
                    mIntent.putExtra(POINT_EXCHAGE, true);
                    startActivity(mIntent);
                } else {
                    Intent mIntent = new Intent(PointExchageActivity.this, AddressManageActivity.class);
                    startActivity(mIntent);
                }
                break;
 
            case R.id.ll_exchange_bg://兑换
                submit();
                break;
            case R.id.iv_success_close://兑换
            	rl_exchange_success.startAnimation(AnimUtils.aphlaAnimation(false, 500,new AnimationListenerImpl() {
					@Override
					public void onAnimationEnd(Animation animation) {
                        ll_exchange_bg.startAnimation(AnimUtils.scaleAnimation(true, 20.0f, 1.0f, 20.0f, 1.0f, 1, new AnimationListenerImpl(){
                        	@Override
                        	public void onAnimationEnd(
                        			Animation animation) {
//                        		  ll_exchange_bg.setBackgroundResource(R.drawable.oval_gray);
//                        		  ll_exchange_bg.setBackgroundResource(R.drawable.point_exchange_btn);
                        		
                        		setResult(RESULT_OK);
                        		finish();
                        	}
                        },true));
                        rl_exchange_success.setVisibility(View.GONE);
					}
				}));
            	break;
            	
            case R.id.tv_exchange_look:
            	ActivityUtils.startNewIntent(this);
            	break;
        }
    }

    private void load() {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("goodsId", getIntent().getLongExtra("goodsId", 0));
            RequestApi.postCommon(this, Constant.URL.DTOrderConfirm, data,
                    new ResultLinstener<JifenDetail>() {
                        @Override
                        public void onSuccess(JifenDetail obj) {
                            if (obj != null) {
                                List<Address> addresses = obj.getAddresses();
                                if (addresses != null && addresses.size() > 0) {
                                    AddressDatas.fillAddress(addresses);
                                }

                                onResumeShowAddress();

                                Member member = obj.getMember();
                                JifenGoods goods = obj.getGoods();
                                if (goods != null) {
                                    List<Picture> pictures = goods.getPictures();
                                    mBitmapUtils.display(iv_point_gift_left, pictures.get(0).getThumbUrl());
//									mBitmapUtils.display(iv_point_gift_left, getIntent().getStringExtra("url"));
                                }
                                userUseJifen = member.getJifenAvailable();
                                CommonApis.setUserJiFen(PointExchageActivity.this,userUseJifen, false);
                                tv_gift_remainder_point.setText(""+CommonApis.getUserJifen(PointExchageActivity.this));
                                tv_point_gift_title.setText(goods.getName());
                                needUnitJifen = goods.getJifen();
                                tv_point_gift_point.setText("" + needUnitJifen);
                                
                                ll_top_layer.setVisibility(View.GONE);
                                exchangeQ(1);
                                
                            }

                        }

                        @Override
                        public void onFailure(String obj) {

                        }

                        @Override
                        public void onException(String exception) {

                        }
                    }, new JifenDetail());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int userUseJifen;
    private int needUnitJifen;
    private boolean isDownBtn = false;
    private void submit() {
        try {
        	isDownBtn = true;
            final int sumJifen = needUnitJifen * selectNumber;
			if (userUseJifen<sumJifen) {
				ToastUtils.showTextToast(PointExchageActivity.this, "积分不足，不能兑换");
				return;
			}
			if (addAddress) {
				ToastUtils.showTextToast(PointExchageActivity.this, "还没有添加收货地址");
				return;
			}
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("goodsId", getIntent().getLongExtra("goodsId", 0));
            data.put("amount", selectNumber);
            data.put("addressId", AddressDatas.getDefaultAddress().getId());
            RequestApi.postCommon(this, Constant.URL.DTOrderSubmit, data,
                    new ResultLinstener<String>() {
                        @Override
                        public void onSuccess(String obj) {
                            CommonApis.setUserJiFen(PointExchageActivity.this, -sumJifen, true);
                            tv_gift_remainder_point.setText(""+CommonApis.getUserJifen(PointExchageActivity.this));
							ScaleAnimation scaleAnimation = new ScaleAnimation(
									1.0f, 20.0f, 1.0f, 20.f,
									Animation.RELATIVE_TO_SELF, 0.5f,
									Animation.RELATIVE_TO_SELF, 0.5f);
                            scaleAnimation.setDuration(1000);
                            scaleAnimation.setFillAfter(true);
                            scaleAnimation.setAnimationListener(new AnimationListenerImpl() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                    ll_exchange_bg.setBackgroundResource(R.drawable.oval_point_exchange_red);
                                }
                                @Override
                                public void onAnimationEnd(Animation animation) {
                                	rl_exchange_success.setVisibility(View.VISIBLE);
                            		rl_exchange_success.startAnimation(AnimUtils.aphlaAnimation(true, 250));
                                }
                            });
                            ll_exchange_bg.startAnimation(scaleAnimation);
                        }

                        @Override
                        public void onFailure(String obj) {
//                        	ll_exchange_bg.setBackgroundResource(R.drawable.point_exchange_btn);
                        }

                        @Override
                        public void onException(String exception) {
//                        	ll_exchange_bg.setBackgroundResource(R.drawable.point_exchange_btn);
                        }
                    }, new String());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        try {
            tv_gift_remainder_point.setText(""+CommonApis.getUserJifen(this));
            
            onResumeShowAddress();
            exchangeQ(selectNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    private void onResumeShowAddress() {
        if (AddressDatas.getAddress().size() > 0) {
            Address address = AddressDatas.getDefaultAddress();
            if (address == null) {
                address = AddressDatas.getAddress().get(0);
            }
            fillAddress(address);
        } else {
            addAddress = true;
            tv_address_operation.setText("添加收货地址");
            tv_address_operation_details.setVisibility(View.GONE);
            iv_exchange.setImageResource(R.mipmap.right_bold_white);
        }
    }

    private void fillAddress(Address address) {
        tv_address_operation.setText("收货人： " + address.getConsignee() + "   " + address.getMobilephone());
        tv_address_operation_details.setText("收货地址：" + address.getAddressAll());
        iv_exchange.setImageResource(R.mipmap.right_bold_yellow);
        tv_address_operation_details.setVisibility(View.VISIBLE);
        addAddress = false;
    }

    //是否有足够积分兑换礼品
    private boolean isEnoughJifen= false;
    private void exchangeQ(int number){
    	final int sumJifen = needUnitJifen * number;
		if (userUseJifen<sumJifen) {
			isEnoughJifen = false;
			iv_exchange.setImageResource(R.mipmap.right_bold_white);
		}else {
			isEnoughJifen = true;
			iv_exchange.setImageResource(R.mipmap.right_bold_yellow);
		}
    }
//	@Override
//	protected void onStop() {
//		if (mNumberPopwindow!=null) {
//			mNumberPopwindow.dismiss();
//		}
//		super.onStop();
//	}
}
