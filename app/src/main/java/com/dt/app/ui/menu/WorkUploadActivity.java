package com.dt.app.ui.menu;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.*;
import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.QiNiuToken;
import com.dt.app.bean.Tag;
import com.dt.app.common.CommonApis;
import com.dt.app.common.DTFactoryApi;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.listener.HandleCallbackSimple;
import com.dt.app.ui.art.ArtistSpaceActivity;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ActionBarView.ActionBarLinstener;
import com.dt.app.widget.PictureLayout;
import com.dt.app.widget.TagGroup;
import com.dt.app.widget.TagGroup.OnTagChangeListener;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.qiniu.android.storage.UploadManager;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Created by caiyuhao on 15-9-13.
 */
public class WorkUploadActivity extends BaseActivity
        implements View.OnClickListener {

    @ViewInject(R.id.fl_work_upload_img)
    private FrameLayout fl_work_upload_img;
    //	@ViewInject(R.id.iv_work_upload_circle_img)
//	private CircleImageView iv_work_upload_circle_img;
//	@ViewInject(R.id.iv_work_upload_square_img)
//	private ImageView iv_work_upload_square_img;
    @ViewInject(R.id.et_work_upload_title)
    private EditText et_work_upload_title;
    @ViewInject(R.id.et_work_upload_content)
    private EditText et_work_upload_content;
    @ViewInject(R.id.tv_work_upload_point)
    private TextView tv_work_upload_point;
    @ViewInject(R.id.tag_group)
    private TagGroup tagGroup;
    @ViewInject(R.id.pictureLayout)
    private PictureLayout pictureLayout;

    @ViewInject(R.id.tag_group_custom)
    private TagGroup tag_group_custom;

    @ViewInject(R.id.tv_work_upload_writetag_info)
    private TextView tv_work_upload_writetag_info;
    
    //上传
    @ViewInject(R.id.ll_work_upload_progress)
    private LinearLayout ll_work_upload_progress;
    @ViewInject(R.id.pb_work_upload_progress)
    private ProgressBar pb_work_upload_progress;
    @ViewInject(R.id.scroll_view)
    private ScrollView scroll_view;
    

    private int uploadType = 0;
    private ArrayList<String> uploadImages;
    private int width;
    private BitmapUtils mBitmapUtils;
    private ActionBarView mActionBarView;

    private HashMap<String, Integer> serverTagMap = new HashMap<String, Integer>();
    private ArrayList<Tag> serverTags = new ArrayList<Tag>();
    private ArrayList<String> selectTags = new ArrayList<String>();
    private ArrayList<String> customTags = new ArrayList<String>();

    private String type;//1.单幅方图，2.单幅圆图，3.系列作品
    private ImageView rightImageView;
    private boolean isSubmitWork = false;

    private UploadManager mUploadManager;
    private Activity  activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.work_upload_main);
        ViewUtils.inject(this);
        activity = this;
        initView();
        initEvent();
        loadTags();
        initQiNiuToken();
        
        scroll_view.scrollTo(0, 0);
    }

    private void initView() {
        mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
        mActionBarView = new ActionBarView(this);
        mActionBarView.setBackground(R.color.white);
        mActionBarView.setBackHome(R.mipmap.dt_back_b, "", ActionBarView.FINISH_TYPE_4);
        rightImageView = (ImageView) mActionBarView.addImageAction(R.id.title_bar_right_img1, R.mipmap.right_gray);
        mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.title_bar_right_img1:
                        if (isSubmitWork) {
                        	isSubmitWork = false;
//                        	pictureLayout.setFocusable(true);
//                            pictureLayout.setFocusableInTouchMode(true);
//                            pictureLayout.requestFocus();
                            uploadWork();
                        }
                        break;
                }
            }
        });
        
        pictureLayout.setFocusable(true);
        pictureLayout.setFocusableInTouchMode(true);
        pictureLayout.requestFocus();
        
       
    }

    private void initEvent() {
        tagGroup.setClickable(false);

        tag_group_custom.setOnTagChangeListener(new OnTagChangeListener() {
            @Override
            public void onDelete(TagGroup tagGroup, String tag) {
            }

            @Override
            public void onAppend(TagGroup tagGroup, String tag) {
                System.out.println("------onAppend-------> " + tag);
                if (customTags.contains(tag)) {
                    tag_group_custom.removeViewAt(customTags.size());
                } else {
                    customTags.add(tag);
                }

            }

            @Override
            public void onCheck(boolean check, String tag) {
                System.out.println("------onAppend-------> " + check + "  " + tag);
                try {
                    if (check) {
                        customTags.add(tag);
                    } else {
                        int index = customTags.indexOf(tag);
                        customTags.remove(tag);
                        tag_group_custom.removeViewAt(index);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        tagGroup.setOnTagChangeListener(new OnTagChangeListener() {
            @Override
            public void onDelete(TagGroup tagGroup, String tag) {
                selectTags.remove(tag);
            }

            @Override
            public void onAppend(TagGroup tagGroup, String tag) {
            }

            @Override
            public void onCheck(boolean check, String tag) {
//                System.out.println("----------onCheck--------->" + tag + " " + check);
                if (check) {
                    selectTags.add(tag);
                } else {
                    selectTags.remove(tag);
                }
            }
        });


        width = ZTDeviceInfo.getInstance().getWidthDevice();
        uploadType = getIntent().getIntExtra("type", 0);
        uploadImages = getIntent().getStringArrayListExtra("paths");
        if (uploadType != UploadActivity.UPLOAD_MULTI) {
        	int itemw = width - 80;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width,itemw);
            params.topMargin = DensityUtil.dip2px(this, 8);
            params.bottomMargin=DensityUtil.dip2px(this, 8);
            fl_work_upload_img.setLayoutParams(params);
        }

        switch (uploadType) {
            case UploadActivity.UPLOAD_CIRCLE:
                type = "2";
                pictureLayout.initData(true, uploadImages.get(0), mBitmapUtils, 0, 0);
                LogUtils.e("--------uploadImage--2---->" + uploadImages.get(0));
                break;
            case UploadActivity.UPLOAD_SQUARE:
                type = "1";
                Bitmap bitmap = BitmapFactory.decodeFile(uploadImages.get(0));
                pictureLayout.initData(false, uploadImages.get(0), mBitmapUtils, bitmap.getWidth(), bitmap.getHeight());
                LogUtils.e("--------uploadImage---1---width= "+bitmap.getWidth()+",,,height= "+bitmap.getHeight());

                break;
            case UploadActivity.UPLOAD_MULTI:
                type = "3";
                pictureLayout.initData(false, uploadImages, mBitmapUtils, 0, 0);
                break;
        }
       
//        pictureLayout.scrollTo(0,0);
        
        et_work_upload_content.setOnClickListener(this);
        tv_work_upload_writetag_info.setOnClickListener(this);

        et_work_upload_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.length() == 0) {
                    isSubmitWork = false;
                    rightImageView.setImageResource(R.mipmap.right_gray);
                } else {
                    if (!TextUtils.isEmpty(et_work_upload_content.getText().toString())) {
						rightImageView.setImageResource(R.mipmap.right_black);
						isSubmitWork = true;
					}else{
						 isSubmitWork = false;
						rightImageView.setImageResource(R.mipmap.right_gray);
					}
                }
            }
        });
        et_work_upload_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.length() == 0) {
                    tv_work_upload_point.setVisibility(View.VISIBLE);
                    isSubmitWork = false;
					rightImageView.setImageResource(R.mipmap.right_gray);
                } else {
                    tv_work_upload_point.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(et_work_upload_title.getText().toString())) {
						rightImageView.setImageResource(R.mipmap.right_black);
						isSubmitWork = true;
					}else{
						 isSubmitWork = false;
						rightImageView.setImageResource(R.mipmap.right_gray);
					}
						
                }
                
            }
        });

        mUploadManager = new UploadManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_work_upload_content:

                break;

            case R.id.tv_work_upload_writetag_info:
                tv_work_upload_writetag_info.setVisibility(View.GONE);
                break;
        }
    }


    /**
     * 获取标签
     */
    private void loadTags() {
        Map<String, Object> data = new HashMap<String, Object>();
        CommonApis.getTags(this, data, new HandleCallbackSimple<Tag>() {
            @Override
            public void onSuccess(List<Tag> t) {
                if (t != null && t.size() > 0) {
                    serverTags.clear();
                    serverTags.addAll(t);
                    String[] tags = new String[t.size()];
                    for (int i = 0; i < t.size(); i++) {
                        tags[i] = t.get(i).getName();
                        serverTagMap.put(t.get(i).getName(), t.get(i).getId());
                    }
                    tagGroup.setTags(tags);
                }
            }
        });
    }
    private void uploadWork() {
        try {
//        	if (mQiNiuToken==null) {
//				return;
//			}
            if (selectTags.size() == 0) {
                ToastUtils.showTextToast(this, "请选择标签");
                return;
            }

            String title = et_work_upload_title.getText().toString().trim();
            if (TextUtils.isEmpty(title)) {
                ToastUtils.showTextToast(this, "标题不能为空");
                return;
            }
            String content = et_work_upload_content.getText().toString().trim();
            if (TextUtils.isEmpty(content)) {
                ToastUtils.showTextToast(this, "内容不能为空");
                return;
            }
            
            String tagIds = "";
            for (int i = 0; i < selectTags.size(); i++) {
                String tag = selectTags.get(i);
                if (serverTagMap.containsKey(tag)) {
                    tagIds += serverTagMap.get(selectTags.get(i)) + ",";
                }
            }
            if (tagIds.length() > 0) {
                tagIds = tagIds.substring(0, tagIds.length() - 1);
            }

            String tagx = "";
            for (String tag : customTags) {
                tagx += tag + ",";
            }
            if (tagx.length() > 0) {
                tagx = tagx.substring(0, tagx.length() - 1);
            }
            final Map<String, Object> data = new HashMap<String, Object>();
            data.put("title", title);
            data.put("content", content);
            data.put("tagx", tagx);
            data.put("type", type);


            data.put("tagIds[]", tagIds);
            
        	String[] paths = new String[uploadImages.size()];
        	final String[] photoNames = new String[uploadImages.size()];

        	String[] tokens = new String[uploadImages.size()];
            Long userid = PreferencesUtils.getLong(activity, Constant.PrefrencesPt.DTid);
            //"logos/"+userid+"/"+ UUID.randomUUID().toString()+".jpg";
            final int count = uploadImages.size();
            for (int i = 0; i < count; i++) {
                 paths[i] = uploadImages.get(i);
                 photoNames[i] =  "works/"+userid+"/"+ UUID.randomUUID().toString()+".jpg";;
                 tokens[i] = mQiNiuToken.getToken();
            }
            
            ll_work_upload_progress.setVisibility(View.VISIBLE);
            pb_work_upload_progress.setMax(uploadImages.size()*100);
            uploadProgressMap.clear();
            RequestApi.updatePhotoes(paths, photoNames, tokens, new ResultLinstener<JSONObject>() {
				@Override
				public void onSuccess(JSONObject obj) {
					if (obj!=null) {
						//data.put("imageUrls[]", photoNames[0]);

                        if (type.equals("3")) {
                            int count = uploadImages.size();
                            String urlPaths = "";
                            for (int i = 0; i < count; i++) {
                                urlPaths += photoNames[i]+",";

                            }
                            urlPaths = urlPaths.substring(0, urlPaths.length() - 1);
                            data.put("imageUrls[]", urlPaths);
                        } else {
                            data.put("imageUrls[]", photoNames[0]);
                        }

						RequestApi.postCommon(WorkUploadActivity.this, Constant.URL.DTWorksUpload, data, new ResultLinstener<String>() {
                            @Override
                            public void onSuccess(String obj) {
                            	pb_work_upload_progress.setProgress(100);
                                LogUtils.e("------postCommon------>"+obj);
                                RequestApi.k = 0;
                                ToastUtils.showTextToast(WorkUploadActivity.this, "添加作品成功");
                                Bundle bundle = new Bundle();
                                bundle.putLong("memberId", PreferencesUtils.getLong(activity, Constant.PrefrencesPt.DTid));
                                startActivityByCalss(activity, ArtistSpaceActivity.class, bundle);
                                finish();
                            }

                            @Override
                            public void onFailure(String obj) {
                            }

                            @Override
                            public void onException(String exception) {
                            }

                        }, new String());
					}
				}
				@Override
				public void onFailure(String obj) {
					// 进度  0.9 1.0
					try {
						String[] arr = obj.split(",");
						uploadProgressMap.put(arr[0], Double.valueOf(obj.split(",")[1]));
						int progress = 0;
						for (String key : uploadProgressMap.keySet()) {
							progress+=uploadProgressMap.get(key)*100;
						}
						if (progress<=98) {
							pb_work_upload_progress.setProgress(progress);
						}
						
					} catch (Exception e) {
						
					}
					
				}

				@Override
				public void onException(String exception) {
					// 进度
//					System.out.println(exception);
				}
			});

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private Map<String, Double> uploadProgressMap = new HashMap<String, Double>();
    private QiNiuToken mQiNiuToken;
    private void initQiNiuToken(){
    	try {
    		Map<String, Object> data = new HashMap<String, Object>();
    		RequestApi.postCommon(WorkUploadActivity.this, Constant.URL.DTUploadToken, data, new ResultLinstener<QiNiuToken>() {
              @Override
              public void onSuccess(QiNiuToken obj) {
                   if (obj!=null) {
                	   mQiNiuToken = obj;
				   }
              }

              @Override
              public void onFailure(String obj) {
              }

              @Override
              public void onException(String exception) {
              }

          }, new QiNiuToken());
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
 
}
