package com.dt.app.ui.set;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTUser;
import com.dt.app.bean.DTVerfiyCode;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.art.ArtistEditActivity;
import com.dt.app.ui.login.ForgetPwdActivity.SafeCountTimer;
import com.dt.app.utils.*;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * mobile
 */
public class ChangePhoneActivity extends BaseActivity
        implements OnClickListener {
    private Context context;

    @ViewInject(R.id.iv_back)
    private ImageView iv_back;
    @ViewInject(R.id.et_now_phone)
    public EditText et_now_phone;
    @ViewInject(R.id.et_new_phone)
    public EditText et_new_phone;
    @ViewInject(R.id.et_new_phone_code)
    public EditText et_new_phone_code;
    @ViewInject(R.id.modfy_code_phone)
    public Button modfy_code_phone;
    private SafeCountTimer safeCountTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.view_ac_changetelphone);
        context = this;
        ViewUtils.inject(this);
        initView();
        ChangPhoneHelper changPhoneHelper = new ChangPhoneHelper(this);
        changPhoneHelper.editorListener();
    }

    private void initView() {
        String phone = PreferencesUtils.getString(context, Constant.PrefrencesPt.DTmobilephone);
        if (!TextUtils.isEmpty(phone)){
            et_now_phone.setText(phone+"");
        }
        iv_back.setOnClickListener(this);
        modfy_code_phone.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.modfy_code_phone:

                sendVerfyCode();
                break;
            default:
                break;
        }
    }

    /**
     * 定时器
     */
    public class SafeCountTimer extends CountDownTimer {

        public SafeCountTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            modfy_code_phone.setText("" + l / 1000 + "秒后重发");
            modfy_code_phone.setTextColor(Color.WHITE);
            modfy_code_phone.setBackgroundResource(R.color.background);
            modfy_code_phone.setEnabled(false);
        }

        @Override
        public void onFinish() {
            modfy_code_phone.setEnabled(true);
            modfy_code_phone
                    .setBackgroundResource(R.color.white);
            modfy_code_phone.setTextColor(getResources().getColor(
                    R.color.background));
            modfy_code_phone.setText("发送验证码");
        }
    }

    public void sendVerfyCode() {
        if (TextUtils.isEmpty(et_new_phone.getText())){
            Utils.showWrongDialog( et_new_phone, context, "新手机号不能为空");
            return;
        }
        else if (!Utils.isMobile(et_now_phone.getText().toString())) {
            Utils.showWrongDialog(et_new_phone, context, "请输入正确手机号");
            return;
        }
        safeCountTimer = new SafeCountTimer(60 * 1000, 1000);
        safeCountTimer.start();
        String emailOrmobile = et_new_phone.getText().toString().trim();
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("loginname", emailOrmobile);
        try {
            RequestApi.commonRequest(this, Constant.URL.DTSendVerfyCode, data, new ResultLinstener<DTVerfiyCode>() {
                @Override
                public void onSuccess(DTVerfiyCode obj) {
                    if (obj.getCode() == 1) {
                    	et_new_phone_code.setText(""+obj.getData());
                    } else {
                    	
                    }
                }

                @Override
                public void onFailure(String obj) {

                }

                @Override
                public void onException(String exception) {
                    // TODO Auto-generated method stub

                }
            }, new DTVerfiyCode());
        } catch (Exception e) {
            // TODO: handle exception
        }

    }
}
