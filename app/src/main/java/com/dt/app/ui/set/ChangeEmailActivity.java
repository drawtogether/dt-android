package com.dt.app.ui.set;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTUser;
import com.dt.app.bean.DTVerfiyCode;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.Utils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ChangeEmailActivity extends BaseActivity implements View.OnClickListener {
    private Activity context;
    @ViewInject(R.id.tv_back)
    private ImageView tv_back;
    @ViewInject(R.id.et_now_email)
    public EditText et_now_email;
    @ViewInject(R.id.et_new_email)
    public EditText et_new_email;
    @ViewInject(R.id.et_new_email_code)
    public EditText et_new_email_code;
    @ViewInject(R.id.modfy_code_email)
    private Button modfy_code_email;

    private SafeCountTimer safeCountTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.view_ac_changeemail);
        context = this;
        ViewUtils.inject(this);
        initView();
        ChangEmailHelper changEmailHelper = new ChangEmailHelper(context);
        changEmailHelper.editorListener();
    }

    private void initView() {
        String email = PreferencesUtils.getString(context, Constant.PrefrencesPt.DTemail);
        if (!TextUtils.isEmpty(email)) {
            et_now_email.setText(email + "");
        }
        tv_back.setOnClickListener(this);
        modfy_code_email.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_back:
                finish();
                break;
            case R.id.modfy_code_email:

                sendVerfyCode();
                break;
            default:
                break;
        }
    }

    /**
     * 定时器
     */
    public class SafeCountTimer extends CountDownTimer {

        public SafeCountTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            modfy_code_email.setText("" + l / 1000 + "秒后重发");
            modfy_code_email.setTextColor(Color.WHITE);
            modfy_code_email.setBackgroundResource(R.color.background);
            modfy_code_email.setEnabled(false);
        }

        @Override
        public void onFinish() {
            modfy_code_email.setEnabled(true);
            modfy_code_email
                    .setBackgroundResource(R.color.white);
            modfy_code_email.setTextColor(getResources().getColor(
                    R.color.background));
            modfy_code_email.setText("发送验证码");
        }
    }
    public void sendVerfyCode() {
        if (TextUtils.isEmpty(Utils.getString(et_new_email))) {
            Utils.showWrongDialog(et_new_email, context, "新邮箱不能为空");
            return;
        } else if (!Utils.isEmail(et_new_email.getText().toString())) {
            Utils.showWrongDialog(et_new_email, context, "请输入正确邮箱");
            return;
        }
        String emailOrmobile = et_new_email.getText().toString().trim();
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("loginname", emailOrmobile);
        safeCountTimer = new SafeCountTimer(60 * 1000, 1000);
        safeCountTimer.start();
        try {
            RequestApi.commonRequest(this, Constant.URL.DTSendVerfyCode, data, new ResultLinstener<DTVerfiyCode>() {
                @Override
                public void onSuccess(DTVerfiyCode obj) {
                    if (obj.getCode() == 1) {
                    	et_new_email_code.setText(""+obj.getData());
                    } else {
                    	
                    }
                }

                @Override
                public void onFailure(String obj) {

                }

                @Override
                public void onException(String exception) {
                    // TODO Auto-generated method stub

                }
            }, new DTVerfiyCode());
        } catch (Exception e) {
            // TODO: handle exception
        }

    }
}
