package com.dt.app.ui.menu;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.dt.app.listener.HandleCallbackSimple;
import com.dt.app.utils.ImageUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * Created by caiyuhao on 15-9-13.
 */
public class UploadActivity extends BaseActivity implements
        View.OnClickListener {
    @ViewInject(R.id.upload_square_img)
    private ImageView upload_square_img;
    @ViewInject(R.id.upload_circle_img)
    private ImageView upload_circle_img;
    @ViewInject(R.id.upload_multi_img)
    private ImageView upload_multi_img;
    @ViewInject(R.id.iv_back_home_icon)
    private ImageView iv_back_home_icon;

    public static final int UPLOAD_SQUARE = 10;
    public static final int UPLOAD_CIRCLE = 11;
    public static final int UPLOAD_MULTI = 12;
    private ImageUtils mImageUtils;
    int type = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_menu_upload);
        ViewUtils.inject(this);

        upload_square_img.setOnClickListener(this);
        upload_circle_img.setOnClickListener(this);
        upload_multi_img.setOnClickListener(this);
        iv_back_home_icon.setOnClickListener(this);

        mImageUtils = new ImageUtils(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_back_home_icon) {
            this.finish();
            overridePendingTransition(0, R.anim.setting_out_from_top);
            return;

        }
        mImageUtils.getmSelectPath().clear();
        switch (v.getId()) {
            case R.id.upload_square_img:
                type = UPLOAD_SQUARE;
                mImageUtils.startActivityMulti(false);
                break;
            case R.id.upload_circle_img:
                mImageUtils.startActivityMulti(false);
                type = UPLOAD_CIRCLE;
                break;
            case R.id.upload_multi_img:
                type = UPLOAD_MULTI;
                mImageUtils.startActivityMulti(true);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ImageUtils.REQUEST_IMAGE && resultCode == RESULT_OK) {
            mImageUtils.onActivityResult(requestCode, resultCode, data, this, new HandleCallbackSimple<ArrayList<String>>() {
                @Override
                public void onSuccess(ArrayList<String> paths) {
                    if (paths != null && paths.size() > 0) {
                        for (String path : paths) {
                            LogUtils.i("--------------->>>>>>>>>>" + path);
                        }
                        Intent mIntent = new Intent(UploadActivity.this, WorkUploadActivity.class);
                        mIntent.putExtra("paths", paths);
                        mIntent.putExtra("type", type);
                        startActivity(mIntent);
                    }

                }
            });
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
