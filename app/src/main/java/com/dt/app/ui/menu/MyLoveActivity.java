package com.dt.app.ui.menu;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.FragmentAdapter;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.fragment.artist.ArtistAttentionFragment;
import com.dt.app.fragment.artist.ArtistLikesFragment;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ViewPagerCompat;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 我喜爱的作品
 *
 * @author libin
 */
public class MyLoveActivity extends BaseFragmentActivity implements OnClickListener {

    private ViewPagerCompat mPageVp;

    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private FragmentAdapter mFragmentAdapter;

    private ActionBarView mActionBarView;

    @ViewInject(R.id.ll_point_statistics)
    private LinearLayout ll_point_statistics;
    //作品
    @ViewInject(R.id.tv_point_statistics_title)
    private TextView tv_point_statistics_title;
    @ViewInject(R.id.tv_point_statistics)
    private TextView tv_point_statistics;
    @ViewInject(R.id.ll_point_history)
    private LinearLayout ll_point_history;
    //艺术家
    @ViewInject(R.id.tv_point_history)
    private TextView tv_point_history;
    @ViewInject(R.id.tv_point_history_title)
    private TextView tv_point_history_title;

    private boolean isPointStatistics = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_point_history_main);
        ViewUtils.inject(this);

        mActionBarView = new ActionBarView(this);
        mActionBarView.setBackground(R.color.black);
        mActionBarView.setCenterImageTitle(R.mipmap.dt_back_w, R.mipmap.like_h_red, ActionBarView.FINISH_TYPE_4);


        findById();
        init();

        changeTextColor();
    }

    private void findById() {
        mPageVp = (ViewPagerCompat) this.findViewById(R.id.id_page_vp);
        mPageVp.setViewTouchMode(true);

        tv_point_statistics_title.setText("作品");
        tv_point_history_title.setText("艺术家");

        ll_point_history.setOnClickListener(this);
        ll_point_statistics.setOnClickListener(this);
    }

    private void init() {
        long memberId = PreferencesUtils.getLong(this, Constant.PrefrencesPt.DTid);
        mFragmentList.add(new ArtistLikesFragment(memberId));
        mFragmentList.add(ArtistAttentionFragment.newInstance(memberId,true));

        mFragmentAdapter = new FragmentAdapter(
                this.getSupportFragmentManager(), mFragmentList);
        mPageVp.setAdapter(mFragmentAdapter);
        mPageVp.setCurrentItem(0);
        changeTextColor();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_point_statistics:
                isPointStatistics = true;
                changeTextColor();
                mPageVp.setCurrentItem(0);
                break;
            case R.id.ll_point_history:
                isPointStatistics = false;
                changeTextColor();
                mPageVp.setCurrentItem(1);
                break;
        }
    }

    private void changeTextColor() {
        if (isPointStatistics) {
            tv_point_statistics.setTextColor(getResources().getColor(R.color.red_light));
            tv_point_history.setTextColor(getResources().getColor(R.color.gray));

        } else {
            tv_point_statistics.setTextColor(getResources().getColor(R.color.gray));
            tv_point_history.setTextColor(getResources().getColor(R.color.red_light));
        }
    }

    public void changeWorkText(int size) {
        tv_point_statistics.setText("" + size);
    }

    public void changeArtistText(int size) {
        tv_point_history.setText("" + size);
    }
}
