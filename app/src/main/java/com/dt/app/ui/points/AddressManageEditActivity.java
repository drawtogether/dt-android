package com.dt.app.ui.points;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.AddressManageAdapter;
import com.dt.app.adapter.AddressManageAdapter.ViewHolder;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.Address;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.ActionBarView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 地址管理-删除-编辑
 * 
 * @author Administrator
 * 
 */
public class AddressManageEditActivity extends BaseActivity implements OnClickListener{

	private ActionBarView mActionBarView;

	@ViewInject(R.id.pull_refresh_list)
	private ListView pull_refresh_list;
	@ViewInject(R.id.ll_address_footer)
	private LinearLayout ll_address_footer;
	@ViewInject(R.id.ll_address_edit)
	private LinearLayout ll_address_edit;
	@ViewInject(R.id.ll_address_delete)
	private LinearLayout ll_address_delete;
	private AddressManageAdapter manageAdapter;
	private List<Address> mAddresses;
	
	@ViewInject(R.id.ll_delete_info_layer)
	private LinearLayout ll_delete_info_layer;
	@ViewInject(R.id.tv_delete)
	private TextView tv_delete;
	@ViewInject(R.id.tv_cancle)
	private TextView tv_cancle;
	
	
	private int selectPos = -1;
	
	public static final int  EDIT_ADDRESS= 100;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dt_address_manage_edit_main);
		ViewUtils.inject(this);
		initView();

	}

	private void initView() {
		try {
			mActionBarView = new ActionBarView(this);
			mActionBarView.setBackground(R.color.black);
			mActionBarView.setCenterTitle(R.mipmap.dt_back_w, R.color.white,
					"更改地址", ActionBarView.FINISH_TYPE_4);
//			mActionBarView.addImageAction(R.id.title_bar_right_img1,
//					R.mipmap.right_white);
//			mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
//				@Override
//				public void onClick(View view) {
//					switch (view.getId()) {
//					case R.id.title_bar_right_img1:
//
//						break;
//					}
//				}
//			});
			mAddresses = new ArrayList<Address>();
 
			manageAdapter = new AddressManageAdapter(this, mAddresses, true);
			pull_refresh_list.setAdapter(manageAdapter);
			
			Animation animation = AnimationUtils.loadAnimation(this, R.anim.list_bottom_to_top);
			ll_address_footer.startAnimation(animation);
			
			ll_address_edit.setOnClickListener(this);
			ll_address_delete.setOnClickListener(this);
			tv_cancle.setOnClickListener(this);
			tv_delete.setOnClickListener(this);
			
			pull_refresh_list.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if(selectPos == position){
						return;
					}
					if (selectPos>=0) {
						updateItemView(selectPos);
					}
					updateItemView(position);
					selectPos=position;
					
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onResume() {
		try {
			mAddresses.clear();
			mAddresses.addAll(AddressDatas.getAddress());
			manageAdapter.notifyDataSetChanged();
			selectPos=-1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onResume();
	}
	
	private void updateItemView(int itemIndex){     
	    int visiblePosition = pull_refresh_list.getFirstVisiblePosition();     
	    View v = pull_refresh_list.getChildAt(itemIndex - visiblePosition);     
	    ViewHolder viewHolder =(ViewHolder)v.getTag();     
	    if(viewHolder!= null){     
	    	Address gift = mAddresses.get(itemIndex);
//	    	if (gift.isSelect()) {
//	    		mAddresses.get(itemIndex).setSelect(false);
//				viewHolder.iv_address_flag.setImageResource(R.drawable.address_un_select);
//			}else {
//				mAddresses.get(itemIndex).setSelect(true);
//				viewHolder.iv_address_flag.setImageResource(R.drawable.address_select);
//			}
	    }        
	}   

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ll_address_edit:
			if (selectPos >= 0) {
				edit();
			} else {
				ToastUtils.showTextToast(this, "请选择要修改的项");
			}

			break;
		case R.id.ll_address_delete:
			if (selectPos >= 0) {
				ll_delete_info_layer.setVisibility(View.VISIBLE);
			} else {
				ToastUtils.showTextToast(this, "请选择要删除的项");
			}
			break;
		case R.id.tv_delete:
			delete();
			break;
		case R.id.tv_cancle:
			ll_delete_info_layer.setVisibility(View.GONE);
			break;
		}
	}

	private void edit(){
		try {
			Address address = mAddresses.get(selectPos);
			Intent mIntent = new Intent(this,AddAddressActivity.class);
			mIntent.putExtra("editAddress", address);
			startActivityForResult(mIntent, 10);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void delete() {
		try {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("id", mAddresses.get(selectPos).getId());
			RequestApi.postCommon_List(this, Constant.URL.DTAddressDelete, data,
					new ResultLinstener<List<Address>>() {
						@Override
						public void onSuccess(List<Address> obj) {
							if (obj!=null) {
								AddressDatas.fillAddress(obj);
								mAddresses.clear();
								mAddresses.addAll(AddressDatas.getAddress());
								manageAdapter.notifyDataSetChanged();
								selectPos=-1;
								ll_delete_info_layer.setVisibility(View.GONE);
//								ToastUtils.showTextToast(AddressManageEditActivity.this, "删除成功");
							}
						}
						@Override
						public void onFailure(String obj) {

						}

						@Override
						public void onException(String exception) {

						}
					}, new Address());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		if (requestCode==10) {
//			if (resultCode == EDIT_ADDRESS) {//编辑成功
//				Address address = data.getParcelableExtra("editAddress");
//				int count = mAddresses.size();
//				for (int i = 0; i < count; i++) {
//					Address tempAddress = mAddresses.get(i);
//					if (address.getId() == tempAddress.getId()) {
//						mAddresses.set(i, address);
//						break;
//					}
//				}
//				manageAdapter.notifyDataSetChanged();
//				selectPos=-1;
//			}
//		}
//		super.onActivityResult(requestCode, resultCode, data);
//	}
}
