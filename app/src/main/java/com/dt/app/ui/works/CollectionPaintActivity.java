package com.dt.app.ui.works;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dt.app.R;
import com.dt.app.adapter.FragmentAdapter;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.fragment.works.PaintMutilFragment;
import com.dt.app.fragment.works.PaintOneFragment;
import com.dt.app.view.ActionBarView;
import com.dt.app.view.ActionBarView.ActionBarLinstener;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 收藏画
 *
 * @author libin
 */
public class CollectionPaintActivity extends BaseFragmentActivity {

    private ViewPager mPageVp;

    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private FragmentAdapter mFragmentAdapter;

    @ViewInject(R.id.ll_top_layer)
    private LinearLayout ll_top_layer;
    /**
     * Fragment
     */
    private PaintMutilFragment paintMutilFragment;
    private PaintOneFragment paintOneFragment;
    /**
     * ViewPager的当前选中页
     */
    private int currentIndex;
    /**
     * 屏幕的宽度
     */
    private int screenWidth;

    private ActionBarView mActionBarView;

    private int year;
    private int month;

    private ImageView rightImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_collection_paint_main);
        ViewUtils.inject(this);
        year = getIntent().getIntExtra("year", 2015);
        month = getIntent().getIntExtra("month", 1);

        mActionBarView = new ActionBarView(this);
        mActionBarView.setBackground(R.color.black);
        mActionBarView.setCenterTitle(R.mipmap.dt_back_w, R.color.white, year + "", ActionBarView.FINISH_TYPE_4);
        rightImageView = (ImageView) mActionBarView.addImageAction(R.id.title_bar_right_img1, R.mipmap.coulmn_mutil);
        mActionBarView.setActionBarLinstener(new ActionBarLinstener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.title_bar_right_img1:
                        if (currentIndex == 0) {
                            rightImageView.setImageResource(R.mipmap.coulmn_one);
                            currentIndex = 1;
                        } else {
                            rightImageView.setImageResource(R.mipmap.coulmn_mutil);
                            currentIndex = 0;
                        }
                        mPageVp.setCurrentItem(currentIndex);
                        if (currentIndex == 1) {
                            List<UserWork> tempList = paintMutilFragment.getmBeans();
                            if (tempList != null && tempList.size() > 0) {
                                paintOneFragment.setUserWorks(tempList);
                            }
                        }
                        break;
                }
            }
        });

        findById();
        init();
    }

    private void findById() {
        mPageVp = (ViewPager) this.findViewById(R.id.id_page_vp);
        // setViewMarginTop(mPageVp);


    }

    private void init() {

        paintMutilFragment = new PaintMutilFragment(year, month);
        paintOneFragment = new PaintOneFragment();

        mFragmentList.add(paintMutilFragment);
        mFragmentList.add(paintOneFragment);

        mFragmentAdapter = new FragmentAdapter(
                this.getSupportFragmentManager(), mFragmentList);
        mPageVp.setAdapter(mFragmentAdapter);
        currentIndex = 0;
        mPageVp.setCurrentItem(currentIndex);
        mPageVp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            /**
             * state滑动中的状态 有三种状态（0，1，2） 1：正在滑动 2：滑动完毕 0：什么都没做。
             */
            @Override
            public void onPageScrollStateChanged(int state) {

            }

            /**
             * position :当前页面，及你点击滑动的页面 offset:当前页面偏移的百分比
             * offsetPixels:当前页面偏移的像素位置
             */
            @Override
            public void onPageScrolled(int position, float offset,
                                       int offsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentIndex = position;
            }
        });

    }
    
    public void hideLayer(){
    	if (ll_top_layer.getVisibility()==View.VISIBLE) {
    		ll_top_layer.setVisibility(View.GONE);
		}
    }

}
