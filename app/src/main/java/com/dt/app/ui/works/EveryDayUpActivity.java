package com.dt.app.ui.works;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LayoutAnimationController;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.dt.app.MainActivity;
import com.dt.app.R;
import com.dt.app.adapter.EveryDailyAdapter;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.bean.Page;
import com.dt.app.bean.Theme;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.fragment.ArtGalleryFragment;
import com.dt.app.fragment.PointsExchangeFragment;
import com.dt.app.fragment.PointsExchangeFragment2;
import com.dt.app.listener.AnimationListenerImpl;
import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.listener.GestureDetectorListener;
import com.dt.app.listener.ViewScrollListener;
import com.dt.app.receiver.CommentReceiver.CommentReceverIntf;
import com.dt.app.receiver.CommonReceiverUtil;
import com.dt.app.utils.AnimUtils;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PullToRefreshListViewUtils;
import com.dt.app.utils.PullToRefreshListViewUtils.PullOnRefreshListener;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.CustomDrawerLayout;
import com.dt.app.widget.CommonMainTop;
import com.dt.app.widget.CommonSlide;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.util.LogUtils;

import java.util.*;

/**
 * 每日作品
 * 主题2
 *
 * @author Administrator
 */
public class EveryDayUpActivity extends BaseFragmentActivity {


    private EveryDailyAdapter mAdapter;
    private List<UserWork> userWorks = new ArrayList<UserWork>();
    private PullToRefreshListView mPullToRefreshListView;

    public LinearLayout ll_top_layer;
    private PullToRefreshListViewUtils<ListView> mListViewUtils;
    private ProgressBar progress_dt;
    private Page page;

    /* 主题 start */
    private Theme theme;
    View themeHeaderView;
    private ImageView iv_theme2_icon;
    private TextView tv_theme2_content;
    private TextView tv_theme2_title;
    private BitmapUtils mBitmapUtils;
    /* 主题  end  */

    private GestureDetectorListener mDetectorListener;
    private ImageView iv_go_back;

    private CommonReceiverUtil mCommonReceiverUtil;
    public CustomDrawerLayout mCustomDrawerlayout;
    private Handler mhandlerDayup  ;
    private int countArtGallery = 0;
    private int countPointsExchange = 0;
    private ArtGalleryFragment artgalleryFragment;
    private PointsExchangeFragment2 pointsExchangeFragment2;

    int index = 0;
    int indexUp = 0;
    private Handler  mhandler;
    private Timer mTimer;
    private Timer mTimerUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View chatView = LayoutInflater.from(this).inflate(R.layout.dt_worksup_listview, null);
        theme = getIntent().getParcelableExtra("theme");
        mPullToRefreshListView = (PullToRefreshListView) chatView.findViewById(R.id.pull_refresh_list);
        View rl_title_bar = chatView.findViewById(R.id.rl_main_top_common);
        mCustomDrawerlayout = (CustomDrawerLayout) chatView.findViewById(R.id.drawerlayout_work);
        //每日作品
        iv_go_back = (ImageView) chatView.findViewById(R.id.iv_go_back);
        iv_go_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.setting_out_from_top);
            }
        });
        artgalleryFragment = (ArtGalleryFragment) getSupportFragmentManager().getFragments().get(0);
        pointsExchangeFragment2 = (PointsExchangeFragment2) getSupportFragmentManager().getFragments().get(1);
        ll_top_layer = (LinearLayout) chatView.findViewById(R.id.ll_top_layer);
        if (theme != null) {
            //主题标题
            themeHeaderView = LayoutInflater.from(this).inflate(R.layout.dt_theme2_header_view, null);
            iv_theme2_icon = (ImageView) themeHeaderView.findViewById(R.id.iv_theme2_icon);
            tv_theme2_content = (TextView) themeHeaderView.findViewById(R.id.tv_theme2_content);
            tv_theme2_title = (TextView) themeHeaderView.findViewById(R.id.tv_theme2_title);
            int width = ZTDeviceInfo.getInstance().getWidthDevice();
            LinearLayout.LayoutParams imageLayoutParams = new LinearLayout.LayoutParams(width, (int) (width * 0.7));
            iv_theme2_icon.setLayoutParams(imageLayoutParams);
            Theme theme = getIntent().getParcelableExtra("theme");
            if (theme != null) {
                tv_theme2_title.setText(theme.getName());
                tv_theme2_content.setText(theme.getBrief());
                mBitmapUtils = BitmapUtilsConfig.getBitmapUtils();
                mBitmapUtils.display(iv_theme2_icon, theme.getPicture());
            }
            rl_title_bar.setVisibility(View.GONE);
            mPullToRefreshListView.getRefreshableView().addHeaderView(themeHeaderView);
            iv_go_back.setVisibility(View.VISIBLE);
            mDetectorListener = new GestureDetectorListener(this, mPullToRefreshListView.getRefreshableView(), null);

            mCustomDrawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            ll_top_layer.setVisibility(View.GONE);
        } else {
        	LayoutAnimationController lac = AnimationUtils.loadLayoutAnimation(this, R.anim.list_anim_alpha_layout);
            lac.setOrder(LayoutAnimationController.ORDER_NORMAL);
            lac.setDelay(0.2f);
        	mPullToRefreshListView.getRefreshableView().setLayoutAnimation(lac);
        	
            //mCustomDrawerlayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            mDetectorListener = new GestureDetectorListener(this, mPullToRefreshListView.getRefreshableView(), rl_title_bar);
            progress_dt = (ProgressBar) chatView.findViewById(R.id.progress_dt);
            ll_top_layer.setVisibility(View.VISIBLE);
            mPullToRefreshListView.setVisibility(View.INVISIBLE);
            CommonMainTop mainTop = new CommonMainTop(this, chatView);
//			mainTop.init(R.mipmap.daily_s_w, "Daily Arts", " / 每日作品", R.color.black);
            mainTop.init(R.mipmap.daily_artist, R.color.black);
            mainTop.rl_main_top_common.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPullToRefreshListView.getRefreshableView().smoothScrollToPosition(0);
                }
            });
            mainTop.layout(this);


            mCustomDrawerlayout.setDrawerListener(new DrawerLayout.DrawerListener() {
                @Override
                public void onDrawerSlide(View view, float v) {

                }

                @Override
                public void onDrawerOpened(final View view) {

                    mTimer = new Timer();
                    mTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            index++;
                            Message message = new Message();
                            message.what = index;
                            mhandler.sendMessage(message);
                        }
                    }, 100, 30);

                    mhandler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (mCustomDrawerlayout.isDrawerOpen(Gravity.RIGHT)) {
                                pointsExchangeFragment2.progress_dt.setProgress(msg.what);
                            } else {
                                artgalleryFragment.progress_dt.setProgress(msg.what);
                            }
                            if (index >= 100) {
                                if (mCustomDrawerlayout.isDrawerOpen(Gravity.RIGHT)) {
                                    if (countPointsExchange <= 1) {
                                        pointsExchangeFragment2.ll_top_layer.setVisibility(View.GONE);
                                        countPointsExchange = 3;
                                    }
                                } else {
                                    if (countArtGallery <= 1) {
                                        artgalleryFragment.ll_top_layer.setVisibility(View.GONE);

                                        countArtGallery = 3;
                                    }
                                }
                                mTimer.cancel();
                                index = 0;
                            }else {
                                if (mCustomDrawerlayout.isDrawerOpen(Gravity.RIGHT)){
                                    if (countPointsExchange > 1) {
                                        mTimer.cancel();
                                        index = 0;
                                        mhandler = null;
                                    }
                                }else {
                                    if (countArtGallery > 1) {
                                        mTimer.cancel();
                                        index = 0;
                                        mhandler = null;
                                    }
                                }
                            }
                        }
                    };


                }

                @Override
                public void onDrawerClosed(View view) {

                }

                @Override
                public void onDrawerStateChanged(int i) {

                }
            });
        }

        mDetectorListener.setContinueMove(false);
        mDetectorListener.setViewScrollListener(new ViewScrollListener() {
            @Override
            public void scroll(boolean isUp, int... arr) {
                if (isUp) {
                    iv_go_back.startAnimation(AnimUtils.aphlaAnimation(false, GestureDetectorListener.GESTURE_DETECTOR_TIME, new AnimationListenerImpl() {
                        public void onAnimationEnd(Animation animation) {
                            iv_go_back.setVisibility(View.GONE);
                        }

                        ;
                    }));
                } else {
                    iv_go_back.startAnimation(AnimUtils.aphlaAnimation(true, GestureDetectorListener.GESTURE_DETECTOR_TIME, new AnimationListenerImpl() {
                        public void onAnimationEnd(Animation animation) {
                            iv_go_back.setVisibility(View.VISIBLE);
                        }

                        ;
                    }));
                }
            }

        });
        setContentView(chatView);

        initView();


    }

    @Override
    protected void onResume() {
        mCommonReceiverUtil.onResume(mAdapter);
        super.onResume();
    }

    private void initView() {
        mCommonReceiverUtil = new CommonReceiverUtil();
        mCommonReceiverUtil.register(this, new CommentReceverIntf() {
            @Override
            public void success(long workid, String type) {
                mCommonReceiverUtil.success(userWorks, workid, type);
            }
        });

        mListViewUtils = new PullToRefreshListViewUtils<ListView>(mPullToRefreshListView);
        mListViewUtils.init();
        mListViewUtils.setPullOnRefreshListener(new PullOnRefreshListener() {
            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView,
                                          int currentPage) {
                load(currentPage);
            }

            @Override
            public void onPullDownToRefresh(PullToRefreshBase refreshView,
                                            int currentPage) {
                userWorks.clear();
                load(currentPage);
            }
        });
        if (theme != null) {
        	mAdapter = new EveryDailyAdapter(this, userWorks);
        }else {
        	mAdapter = new EveryDailyAdapter(this, userWorks,true);
		}
        mPullToRefreshListView.setAdapter(mAdapter);
        mPullToRefreshListView.getRefreshableView().setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

            }
        });
        if (theme != null) {

            mAdapter.setThemeId(theme.getId());
        } else {

            mTimerUp = new Timer();
            mTimerUp.schedule(new TimerTask() {
                @Override
                public void run() {
                    indexUp++;
                    Message message = new Message();
                    message.what = indexUp;
                    mhandlerDayup.sendMessage(message);
                }
            }, 100, 30);

            mhandlerDayup = new Handler() {
                @Override
                public void handleMessage(Message msg){
                    progress_dt.setProgress(msg.what);
                    LogUtils.e("-------->"+msg.what);
                    if (indexUp >= 100) {
                        ll_top_layer.setVisibility(View.GONE);
                        iv_go_back.setVisibility(View.VISIBLE);

                        mTimerUp.cancel();
                        indexUp = 0;
                        mhandlerDayup = null;

                    }
                }
            };
        }
        load(1);
    }


    private void load(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("currentPage", currentPage);
            data.put("pageSize", 10);
            String url = null;
            if (theme != null) {
                url = Constant.URL.DTThemeWorksDetail;
                data.put("themeId", theme.getId());
            } else {
                url = Constant.URL.DTCurrentWorksDaily;
            }
            RequestApi.postCommon(this, url, data,
                    new ResultLinstener<UserWorks>() {
                        @Override
                        public void onSuccess(UserWorks obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    userWorks.clear();
                                    if (ll_top_layer.getVisibility() == View.VISIBLE && theme == null) {
                                        //ll_top_layer.setVisibility(View.GONE);
                                        //ll_top_layer.clearAnimation();
                                        mPullToRefreshListView.setVisibility(View.VISIBLE);

                                    }
                                }
                                userWorks.addAll(obj.getWorkses());
                                page = obj.getPager();
                                mListViewUtils.setPage(page);
                                mAdapter.notifyDataSetChanged();
                            }
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }
                    }, new UserWorks());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        mCommonReceiverUtil.unregister(this);
        super.onDestroy();
    }

}
