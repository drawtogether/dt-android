package com.dt.app.ui.works;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.Common2Adapter;
import com.dt.app.bean.EveryAdResponse;

import java.util.ArrayList;

/**
 * Created by Besttone on 2016/1/28.
 */
public class EveryAdAdapter extends Common2Adapter<EveryAdResponse.Theme2.OutCell> {

    private Context context;

    private ArrayList<EveryAdResponse.Theme2.OutCell> outCells;

    private int mWidth;

    public EveryAdAdapter(Context context, ArrayList<EveryAdResponse.Theme2.OutCell> outCells) {
        super(context, outCells);
        this.context = context;
        this.outCells = outCells;

        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        mWidth = wm.getDefaultDisplay().getWidth();
    }

    @Override
    public int getCount() {
        if (outCells != null) {
            return outCells.size();
        }
        return 0;
    }

    @Override
    public EveryAdResponse.Theme2.OutCell getItem(int i) {
        if (outCells != null) {
            return outCells.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        EveryAdResponse.Theme2.OutCell outCell = (EveryAdResponse.Theme2.OutCell) getItem(i);
        if (outCell.getCellCount() == 1) {
            EveryAdResponse.Theme2.OutCell.Cell cell = outCell.getCells().get(0);
            if (cell.getType().equals(EveryAdResponse.Theme2.OutCell.Cell.TITLE)) {
                TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.textview_title_item_layout, null);
                textView.setText(cell.getText());
                textView.setBackgroundColor(Color.WHITE);
                view = textView;
            } else if (cell.getType().equals(EveryAdResponse.Theme2.OutCell.Cell.CONTENT)) {
                TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.textview_content_item_layout, null);
                textView.setText(cell.getText());
                view = textView;
                if (!TextUtils.isEmpty(cell.getAlign())) {
                    if (cell.getAlign().equals("center")) {
                        textView.setGravity(Gravity.CENTER);
                    } else if (cell.getAlign().equals("left")) {
                        textView.setGravity(Gravity.LEFT);
                    } else if (cell.getAlign().equals("right")) {
                        textView.setGravity(Gravity.RIGHT);
                    }
                }
                if (!TextUtils.isEmpty(cell.getBgcolor())) {
                    textView.setBackgroundColor(Color.parseColor("#" + cell.getBgcolor()));
                }

                if (!TextUtils.isEmpty(cell.getColor())) {
                    if (cell.getColor().equals("black"))
                        textView.setTextColor(Color.BLACK);
                }
            } else if (cell.getType().equals(EveryAdResponse.Theme2.OutCell.Cell.IMAGE)) {
                view = LayoutInflater.from(context).inflate(R.layout.imageview_item_layout, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
                ViewGroup.LayoutParams params = imageView.getLayoutParams();
                params.height = (cell.getH() * mWidth) / cell.getW();
                params.width = mWidth;

                imageView.setLayoutParams(params);
                mBitmapUtils.display(imageView, cell.getImageUrl() + "?imageMogr2/thumbnail/" + width);
//                imageView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(context, MediaPlayerVideoActivity.class);
//                        String path="http://pl.youku.com/playlist/m3u8?vid=211952939&type=mp4&ts=1427963205&keyframe=0&ep=XgSexcr2o9joQkH8E0i8gTK%2BfyNzio2PZpKNt4i0FG6R%2BzU%2B" +
//                                "t08V4zw4gxlj6duo&sid=842795805253312a6cf10&token=2455&ctype=20&ev=1&oip=2032760745&uc_param_str=xk";
////                        String path = "http://cache.m.iqiyi.com/dc/dt/mobile/20141225/33/81/5490094a95d702d2ede0835df37c5bf8.m3u8?qypid=3045892409_22&qd_src=5be6a2fdfe4f4a1" +
////                                "a8c7b08ee46a18887&qd_tm=1427115639000&qd_ip=121.41.119.169&qd_sc=02acb1123af880e7a29314edd4cce074";
//                        intent.putExtra("path", path);
//                        context.startActivity(intent);
//                    }
//                });
            }
//            else if (cell.getType().equals(EveryAdResponse.Theme2.OutCell.Cell.VIDEO)) {
//                VideoView videoView = (VideoView) LayoutInflater.from(context).inflate(R.layout.video_item_layout, null);
//                String path = "http://pl.youku.com/playlist/m3u8?vid=211952939&type=mp4&ts=1427963205&keyframe=0&ep=" +
//                        "XgSexcr2o9joQkH8E0i8gTK%2BfyNzio2PZpKNt4i0FG6R%2BzU%2Bt08V4zw4gxlj6duo&sid=842795805253312a6cf10&token=2" +
//                        "455&ctype=20&ev=1&oip=2032760745&uc_param_str=xk";
//                Uri uri = Uri.parse(path);
//                videoView.setVideoURI(uri);
//                videoView.setMediaController(new MediaController(context));
//                videoView.requestFocus();
//                videoView.setOnInfoListener(this);
//                videoView.setOnBufferingUpdateListener(this);
//                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                    @Override
//                    public void onPrepared(MediaPlayer mediaPlayer) {
//                        // optional need Vitamio 4.0
//                        mediaPlayer.setPlaybackSpeed(1.0f);
//                    }
//                });
//                view = videoView;
//            }
        }
        return view;
    }
}
