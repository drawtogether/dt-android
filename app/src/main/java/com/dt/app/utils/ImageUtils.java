package com.dt.app.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.TypedValue;

import com.dt.app.listener.HandleCallback;
import com.dt.app.ui.image.MultiImageSelectorActivity;
import com.lidroid.xutils.util.LogUtils;

/**
 * 1、调用startActivity方法，启动图片选择界面
 * 2、在activity调用onActivityResult进行回调
 * 3、调用getBase64Pic得到图片的字符串编码
 * @author libin
 *
 */
public class ImageUtils {
    public static final int REQUEST_IMAGE = 2;
    private ArrayList<String> mSelectPath = new ArrayList<String>();
    
    
    private File mTmpFile;

    public static final int REQUEST_CAMERA = 1000;
	public static final int TAKE_CAMERA_RESULT = 1001;
	
	public ImageUtils(Activity activity){
		this.activity = activity;
	}
	
    public ArrayList<String> getmSelectPath() {
		return mSelectPath;
	}

    private Activity activity;

	private boolean isClipe = false;
     
    public void startActivityMulti(boolean multi) {
        startActivity(multi, false);
    }
    public void startActivityOne(boolean clipe) {
        isClipe = clipe;
        startActivity(false, clipe);
    }
    private void startActivity(boolean multi,boolean clipe) {
        Intent intent = new Intent(activity, MultiImageSelectorActivity.class);
        // 是否显示拍摄图片
        intent.putExtra(MultiImageSelectorActivity.EXTRA_SHOW_CAMERA, false);
        // 选择模式
        if (multi) {
            intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE,
                    MultiImageSelectorActivity.MODE_MULTI);
            // 最大可选择图片数量
            intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 6);
        }else {
            intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_MODE,
                    MultiImageSelectorActivity.MODE_SINGLE);
            // 最大可选择图片数量
            intent.putExtra(MultiImageSelectorActivity.EXTRA_SELECT_COUNT, 1);
        }
        
        if (clipe) {
//			intent.putExtra(MultiImageSelectorActivity.EXTRA_CLPE_IMAGE,true);
            intent.putExtra(MultiImageSelectorActivity.EXTRA_CLPE_IMAGE_SYSTEM,true);
        }
        
        // 默认选择
        if (mSelectPath != null && mSelectPath.size() > 0) {
            intent.putExtra(
                    MultiImageSelectorActivity.EXTRA_DEFAULT_SELECTED_LIST,
                    mSelectPath);
        }
        activity.startActivityForResult(intent, REQUEST_IMAGE);
    }
    
    /**
     * 调用相机
     */
    public void take(){
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		mTmpFile = FileUtils.createTmpFile(activity);
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mTmpFile));
		activity.startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }
    
    public int calculateInSampleSize(BitmapFactory.Options options,
            int maxWidth, int maxHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (width > maxWidth || height > maxHeight) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) maxHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) maxWidth);
            }
            final float totalPixels = width * height;
            final float maxTotalPixels = maxWidth * maxHeight * 2;
            while (totalPixels / (inSampleSize * inSampleSize) > maxTotalPixels) {
                inSampleSize++;
            }
        }
        System.out.println("--------------------------" + inSampleSize);
        return inSampleSize;
    }
    
    public void onActivityResult(int requestCode, int resultCode, Intent data,Activity activity,
            HandleCallback<ArrayList<String>> callback) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == activity.RESULT_OK) {
            	if (isClipe) {
            		Bitmap head = data.getParcelableExtra("bitmap");
            		 if (head!=null) {
            			String pathStr = compressImageToPath(head);
            			mSelectPath.clear();
            			mSelectPath.add(pathStr);
            			callback.onSuccess(mSelectPath);
                     }
                }else {
                    mSelectPath = data
                        	.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                            callback.onSuccess(mSelectPath);
                }
            }
        }
        
     // 相机拍照完成后，返回图片路径
		if (requestCode == REQUEST_CAMERA) {
			if (resultCode == Activity.RESULT_OK) {
				if (mTmpFile != null) {
					startPhotoZoom(Uri.fromFile(mTmpFile), TAKE_CAMERA_RESULT);
				}
			} else {
				if (mTmpFile != null && mTmpFile.exists()) {
					mTmpFile.delete();
				}
			}
		}
		if(requestCode==TAKE_CAMERA_RESULT && resultCode==Activity.RESULT_OK){
			Bundle extras = data.getExtras();
			Bitmap bitmap = null;
			if (extras != null) {
				bitmap = extras.getParcelable("data");
				if (bitmap != null) {
					String pathStr = compressImageToPath(bitmap);
					mSelectPath.clear();
					mSelectPath.add(pathStr);
					callback.onSuccess(mSelectPath);
				}else {
					
				}
			}
		}
    }
 
    public  Bitmap calcuteBitmap(String path, int width, int height) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = calculateInSampleSize(options, 480, 800);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }
    
    public  String getBase64Pic(String path){
    	Bitmap mBitmap = calcuteBitmap(path, 480, 800);
    	return Base64.encode(Bitmap2Bytes(mBitmap));
    }
    public  String getBase64Pic(String path,int width,int height){
    	Bitmap mBitmap = calcuteBitmap(path, width, height);
    	return Base64.encode(Bitmap2Bytes(mBitmap));
    }
    public  byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        return baos.toByteArray();
    }
    public InputStream bitmapToStream(Bitmap bm) {
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	bm.compress(Bitmap.CompressFormat.JPEG, 40, baos);
    	InputStream is = new ByteArrayInputStream(baos.toByteArray());  
    	return is;
    }
    
    
    public static final String IMAGE_TEMP_DIR="/Yue/Pics/";
    
    public String[] compressImage(Context context,List<String> paths){
    	try {
    		String filepath = null;
    		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
    			filepath = Environment.getExternalStorageDirectory().toString()+IMAGE_TEMP_DIR;
    			LogUtils.i("-----sd dir------->"+filepath);
            }else {
            	LogUtils.i("-----data dir------->"+filepath);
                filepath = Environment.getDataDirectory().toString()+IMAGE_TEMP_DIR;
            }
    		if (paths!=null) {
    			int size = paths.size();
    			String[] tempPath = new String[size];
    			for (int i=0;i<size;i++) {
    				String tempFileStr = paths.get(i);
    				String filename = tempFileStr;
    				if (tempFileStr.contains("/")) {
    					int index = tempFileStr.lastIndexOf("/");
    					filename = tempFileStr.substring(index, tempFileStr.length()-1);
                    }
    				Bitmap temp = calcuteBitmap(tempFileStr, 480, 800);
    				
    				FileUtils.writeFile(filepath+filename, bitmapToStream(temp));
    				tempPath[i]=filepath+filename;
                }
    			return tempPath;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    	return null;
    }
    /**
     * 将bitmap写入到文件
     * @param context
     * @param bitmap
     * @return
     */
    public String compressImageToPath(Bitmap bitmap){
    	try {
    		String filepath = null;
    		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
    			filepath = Environment.getExternalStorageDirectory().toString()+IMAGE_TEMP_DIR;
    			LogUtils.i("-----sd dir------->"+filepath);
    		}else {
    			LogUtils.i("-----data dir------->"+filepath);
    			filepath = Environment.getDataDirectory().toString()+IMAGE_TEMP_DIR;
    		}
    		String filename = "/temp_"+System.currentTimeMillis()+".jpg";
    		if (bitmap!=null) {
    			FileUtils.writeFile(filepath+filename, bitmapToStream(bitmap));
    		}
    		return filepath+filename;
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return null;
    }
    
    
    private void startPhotoZoom(Uri uri, int requestCode) {
		int width = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 120, ((Context)activity).getResources()
						.getDisplayMetrics());
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", width);
		intent.putExtra("outputY", width);
		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
		intent.putExtra("return-data", true);
		activity.startActivityForResult(intent, requestCode);
	}
    
}