package com.dt.app.utils;

import android.app.Activity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.DTCheckUser;
import com.dt.app.common.DTFactoryApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.login.RegisterActivity;
import com.lidroid.xutils.util.LogUtils;

/**
 * Created by caiyuhao on 15-9-17.
 */
public class RegisterHelper {
    private RegisterActivity mRegisterAct;
    private String input_str;

    public RegisterHelper(Activity mActivity) {
        if (mActivity instanceof RegisterActivity) {

            mRegisterAct = (RegisterActivity) mActivity;
        }
    }

    /**
     * initAnimation
     */
    public void initAnimation() {
        mRegisterAct.register_et.setVisibility(View.VISIBLE);
        final Animation animation_register = AnimationUtils.loadAnimation(mRegisterAct,
                R.anim.login_in_from_right);
        final Animation animation_login_img = AnimationUtils.loadAnimation(mRegisterAct,
                R.anim.register_img_in_from_bottom);
        Window window = mRegisterAct.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = 1.0f;
        window.setAttributes(lp);
        mRegisterAct.linear_mobile_email.startAnimation(animation_register);
        animation_register.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                mRegisterAct.guid_login_img.setVisibility(View.VISIBLE);
                mRegisterAct.guid_login_img.startAnimation(animation_login_img);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * registerVoid
     */
    public void registerVoid() {
        //username void
        mRegisterAct.register_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String text = mRegisterAct.register_et.getHint().toString();
                    if (text.contains("已注册")) {
                        mRegisterAct.finish();
                        mRegisterAct.overridePendingTransition(0, R.anim.alpha_out);
                        return true;
                    }
                    if (mRegisterAct.input_result == -1) {
                        mRegisterAct.register_et.setText("");
                        mRegisterAct.register_et.setHint("请输入正确的手机号或邮箱");
                        mRegisterAct.register_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                        mRegisterAct.register_et.setFocusable(false);

                    } else {
                        mRegisterAct.register_et.setFocusable(true);
                        if (mRegisterAct.input_result.equals(mRegisterAct.Mobile)) {
                            int visibility1 = mRegisterAct.register_sendverfycode.getVisibility();
                            int visibility2 = mRegisterAct.register_input_code_et.getVisibility();
                            if (visibility1 == View.VISIBLE && visibility2 == View.GONE) {
                                ToastUtils.showTextToast(mRegisterAct, "请先发送验证码!");
                                return true;
                            }
                        }
                        if ((mRegisterAct.input_result.equals(mRegisterAct.Mobile)) ||
                                (mRegisterAct.input_result.equals(mRegisterAct.Email))) {
                            DTFactoryApi.checkUserData(mRegisterAct, input_str, new ResultLinstener() {
                                @Override
                                public void onSuccess(Object obj) {
                                    DTCheckUser checkUser = (DTCheckUser) obj;
                                    Boolean isOK = checkUser.isData();
                                    LogUtils.e("-------onSuccess----->" + isOK);
                                    if (!isOK) {
                                        if (mRegisterAct.input_result == mRegisterAct.Mobile && mRegisterAct.register_sendverfycode.getVisibility() == View.GONE) {
                                            mRegisterAct.input_result = mRegisterAct.Mobile;
                                            mRegisterAct.register_sendverfycode.setVisibility(View.VISIBLE);
                                            final Animation animation_verfycode = AnimationUtils.loadAnimation(mRegisterAct, R.anim.register_verfycode_in_from_right);
                                            mRegisterAct.register_sendverfycode.startAnimation(animation_verfycode);
                                        } else {
                                            mRegisterAct.input_result = mRegisterAct.Email;
                                            mRegisterAct.register_input_pwd.setVisibility(View.VISIBLE);
                                            final Animation animation_pwd = AnimationUtils.loadAnimation(mRegisterAct, R.anim.register_pwd_in_from_right);
                                            mRegisterAct.register_input_pwd.startAnimation(animation_pwd);
                                            mRegisterAct.register_input_pwd.setFocusable(true);

                                            mRegisterAct.register_input_pwd.setFocusableInTouchMode(true);
                                            mRegisterAct.register_input_pwd.requestFocus();
                                            mRegisterAct.register_input_pwd.requestFocusFromTouch();
                                        }
                                    } else {
                                        mRegisterAct.register_et.setText("");
                                        mRegisterAct.register_et.setHint(checkUser.getMessage() + "");
                                        mRegisterAct.register_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                                        mRegisterAct.register_et.setFocusable(false);

                                    }
                                }

                                @Override
                                public void onFailure(String obj) {

                                }

                                @Override
                                public void onException(String exception) {

                                }
                            });
                        }
                    }
                }
                return true;
            }
        });

        mRegisterAct.register_input_code_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    String input_code_et = mRegisterAct.register_input_code_et.getText().toString();
                    if (TextUtils.isEmpty(input_code_et)) {
                        mRegisterAct.register_input_code_et.setText("");
                        mRegisterAct.register_input_code_et.setHint("验证码不能为空");
                        mRegisterAct.register_input_code_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                        mRegisterAct.register_input_code_et.setFocusable(false);

                    } else if (!TextUtils.isEmpty(input_code_et) && input_code_et.length() < 6) {
                        mRegisterAct.register_input_code_et.setText("");
                        mRegisterAct.register_input_code_et.setHint("验证码不能少于六位");
                        mRegisterAct.register_input_code_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                        mRegisterAct.register_input_code_et.setFocusable(false);
                    } else if (!TextUtils.isEmpty(input_code_et) && !input_code_et.equals(mRegisterAct.str_sendverfycode)) {
                        mRegisterAct.register_input_code_et.setText("");
                        mRegisterAct.register_input_code_et.setHint("输入正确的验证码");
                        mRegisterAct.register_input_code_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                        mRegisterAct.register_input_code_et.setFocusable(false);
                    } else {
                        mRegisterAct.register_input_pwd.setVisibility(View.VISIBLE);
                        final Animation animation_pwd = AnimationUtils.loadAnimation(mRegisterAct, R.anim.register_pwd_in_from_right);
                        mRegisterAct.register_input_pwd.startAnimation(animation_pwd);
                        mRegisterAct.register_input_pwd.setFocusable(false);
                    }
                }
                return false;
            }
        });
        mRegisterAct.register_input_pwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String register_input_pwd = mRegisterAct.register_input_pwd.getText().toString().trim();
                    if (TextUtils.isEmpty(register_input_pwd)) {
                        mRegisterAct.register_input_pwd.setText("");
                        mRegisterAct.register_input_pwd.setHint("密码不能为空");
                        mRegisterAct.register_input_pwd.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                        mRegisterAct.register_input_pwd.setFocusable(false);

                    } else if (!TextUtils.isEmpty(register_input_pwd) && register_input_pwd.length() < 6) {
                        mRegisterAct.register_input_pwd.setText("");
                        mRegisterAct.register_input_pwd.setHint("密码不能少于六位");
                        mRegisterAct.register_input_pwd.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                        mRegisterAct.register_input_pwd.setFocusable(false);

                    } else {
                        //mRegisterAct.register_input_pwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        mRegisterAct.register_input_username.setVisibility(View.VISIBLE);
                        final Animation animation_username = AnimationUtils.loadAnimation(mRegisterAct, R.anim.register_pwd_in_from_right);
                        mRegisterAct.register_input_username.startAnimation(animation_username);
                    }

                }
                return false;
            }
        });
        mRegisterAct.register_input_username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String nickname = mRegisterAct.register_input_username.getText().toString().trim();
                    String code = null;
                    if (TextUtils.isEmpty(nickname)) {

                        mRegisterAct.register_input_username.setText("");
                        mRegisterAct.register_input_username.setHint("用户名不能为空");
                        mRegisterAct.register_input_username.setHintTextColor(mRegisterAct.getResources().getColor(R.color.red));
                        mRegisterAct.register_input_username.setFocusable(false);

                        return false;
                    }
                    //server
                    String emailOrmobile = mRegisterAct.register_et.getText().toString().trim();
                    if (mRegisterAct.input_result == mRegisterAct.Mobile) {
                        code = mRegisterAct.register_input_code_et.getText().toString().trim();
                    } else {
                        code = null;
                    }

                    String pwd = mRegisterAct.register_input_pwd.getText().toString().trim();

                    DTFactoryApi.mobileRegData(mRegisterAct, emailOrmobile, pwd, code, nickname, null);
                }
                return false;
            }
        });
    }


    /**
     * init Listener
     */
    public void initListener() {
        //监听输入变化
        mRegisterAct.register_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                input_str = editable.toString();
                if (!TextUtils.isEmpty(input_str)) {
                    if (Utils.isEmail(input_str)) {
                        mRegisterAct.input_result = mRegisterAct.Email;
                    } else if (input_str != null && input_str.length() == 11 && Utils.isMobile(input_str)) {
                        //mobile
                        mRegisterAct.input_result = mRegisterAct.Mobile;
                    } else {
                        mRegisterAct.input_result = -1;

                    }

                } else {

                    mRegisterAct.register_sendverfycode.setVisibility(View.GONE);
                    mRegisterAct.register_input_code_et.setVisibility(View.GONE);
                    mRegisterAct.register_input_pwd.setVisibility(View.GONE);
                    mRegisterAct.register_input_username.setVisibility(View.GONE);

                    //reset
                    mRegisterAct.register_input_code_et.setText("");
                    mRegisterAct.register_input_pwd.setText("");
                    mRegisterAct.register_input_username.setText("");

                    mRegisterAct.register_input_code_et.setHint(mRegisterAct.getString(R.string.forget_write_code));
                    mRegisterAct.register_input_code_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.title_gray_color));
                    mRegisterAct.register_input_pwd.setHint(mRegisterAct.getString(R.string.register_user_pwd));
                    mRegisterAct.register_input_pwd.setHintTextColor(mRegisterAct.getResources().getColor(R.color.title_gray_color));
                    mRegisterAct.register_input_username.setHint(mRegisterAct.getString(R.string.register_user));
                    mRegisterAct.register_input_username.setHintTextColor(mRegisterAct.getResources().getColor(R.color.title_gray_color));

                    if (mRegisterAct.safeCountTimer != null) {
                        mRegisterAct.safeCountTimer.cancel();
                    }
                    mRegisterAct.register_sendverfycode.setEnabled(true);
                    mRegisterAct.register_sendverfycode
                            .setBackgroundResource(R.color.white);
                    mRegisterAct.register_sendverfycode.setTextColor(mRegisterAct.getResources().getColor(
                            R.color.background));
                    mRegisterAct.register_sendverfycode.setText("发送验证码");
                }
            }
        });
    }

    /**
     * 获取焦点
     */
    public void getfocusWidget() {
        //mobile or email
        mRegisterAct.register_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRegisterAct.register_et.setFocusable(true);
                mRegisterAct.register_et.setCursorVisible(true);
                mRegisterAct.register_et.setFocusableInTouchMode(true);
                mRegisterAct.register_et.requestFocus();
                mRegisterAct.register_et.requestFocusFromTouch();
            }
        });
        mRegisterAct.register_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    mRegisterAct.register_et.setHint(mRegisterAct.getString(R.string.guid_username_hit));
                    mRegisterAct.register_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.title_gray_color));
                }
            }
        });

        //验证码
        mRegisterAct.register_input_code_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRegisterAct.register_input_code_et.setFocusable(true);
                mRegisterAct.register_input_code_et.setFocusableInTouchMode(true);
                mRegisterAct.register_input_code_et.requestFocus();
                mRegisterAct.register_input_code_et.requestFocusFromTouch();
            }
        });
        mRegisterAct.register_input_code_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    mRegisterAct.register_input_code_et.setHint(mRegisterAct.getString(R.string.forget_write_code));
                    mRegisterAct.register_input_code_et.setHintTextColor(mRegisterAct.getResources().getColor(R.color.title_gray_color));
                }
            }
        });
        //输入密码
        mRegisterAct.register_input_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRegisterAct.register_input_pwd.setFocusable(true);
                mRegisterAct.register_input_pwd.setFocusableInTouchMode(true);
                mRegisterAct.register_input_pwd.requestFocus();
                mRegisterAct.register_input_pwd.requestFocusFromTouch();
            }
        });
        mRegisterAct.register_input_pwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    mRegisterAct.register_input_pwd.setHint(mRegisterAct.getString(R.string.register_user_pwd));
                    mRegisterAct.register_input_pwd.setHintTextColor(mRegisterAct.getResources().getColor(R.color.title_gray_color));
                }
            }
        });
        //用户名 mRegisterAct.
        mRegisterAct.register_input_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRegisterAct.register_input_username.setFocusable(true);
                mRegisterAct.register_input_username.setFocusableInTouchMode(true);
                mRegisterAct.register_input_username.requestFocus();
                mRegisterAct.register_input_username.requestFocusFromTouch();
            }
        });
        mRegisterAct.register_input_username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    mRegisterAct.register_input_username.setHint(mRegisterAct.getString(R.string.register_user));
                    mRegisterAct.register_input_username.setHintTextColor(mRegisterAct.getResources().getColor(R.color.title_gray_color));
                }
            }
        });
    }
}
