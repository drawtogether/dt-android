package com.dt.app.utils;

import android.app.Activity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.DTCheckUser;
import com.dt.app.common.DTFactoryApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.login.ForgetPwdActivity;
import com.lidroid.xutils.util.LogUtils;

/**
 * Created by caiyuhao on 15-9-18.
 */
public class ForgetPwdHelper {
    private ForgetPwdActivity mForgetPwd;
    private String input_str;
    public ForgetPwdHelper(Activity mActivity) {
        mForgetPwd = (ForgetPwdActivity) mActivity;
    }

    /**
     * init Animation
     */
    public void initAnimation() {
        mForgetPwd.forget_username_et.setVisibility(View.VISIBLE);
        final Animation animation_register = AnimationUtils.loadAnimation(mForgetPwd,
                R.anim.login_in_from_right);

        mForgetPwd.forget_username_et.startAnimation(animation_register);
        Window window = mForgetPwd.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = 1.0f;
        window.setAttributes(lp);
        animation_register.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * initListenter
     */
    public void initListener() {
        //监听输入变化
        mForgetPwd.forget_username_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                input_str = editable.toString();
                if (!TextUtils.isEmpty(input_str)) {
                    if (Utils.isEmail(input_str)) {
                        mForgetPwd.input_result = mForgetPwd.Email;


                    } else if (input_str != null && input_str.length() == 11 && Utils.isMobile(input_str)) {
                        //mobile
                        mForgetPwd.input_result = mForgetPwd.Mobile;

                    } else {
                        mForgetPwd.input_result = -1;
                        //mForgetPwd.forget_send_code.setVisibility(View.GONE);
                    }

                }else {
                    mForgetPwd.forget_send_code.setVisibility(View.GONE);
                    mForgetPwd.forget_input_code_et.setVisibility(View.GONE);
                    mForgetPwd.forget_input_resetpwd.setVisibility(View.GONE);
                    mForgetPwd.forget_input_pwd_confirm.setVisibility(View.GONE);

                    //reset
                    mForgetPwd.forget_input_code_et.setText("");
                    mForgetPwd.forget_input_resetpwd.setText("");
                    mForgetPwd.forget_input_pwd_confirm.setText("");


                    mForgetPwd.forget_input_code_et.setHint(mForgetPwd.getString(R.string.forget_write_code));
                    mForgetPwd.forget_input_code_et.setHintTextColor(mForgetPwd.getResources().getColor(R.color.title_gray_color));
                    mForgetPwd.forget_input_resetpwd.setHint(mForgetPwd.getString(R.string.forget_reset_pwd));
                    mForgetPwd.forget_input_resetpwd.setHintTextColor(mForgetPwd.getResources().getColor(R.color.title_gray_color));
                    mForgetPwd.forget_input_pwd_confirm.setHint(mForgetPwd.getString(R.string.forget_reset_pwd_again));
                    mForgetPwd.forget_input_pwd_confirm.setHintTextColor(mForgetPwd.getResources().getColor(R.color.title_gray_color));

                    if (mForgetPwd.safeCountTimer != null){
                        mForgetPwd.safeCountTimer.cancel();
                    }
                    mForgetPwd.forget_send_code.setEnabled(true);
                    mForgetPwd.forget_send_code
                            .setBackgroundResource(R.color.white);
                    mForgetPwd.forget_send_code.setTextColor(mForgetPwd.getResources().getColor(
                            R.color.background));
                    mForgetPwd.forget_send_code.setText("发送验证码");
                }
            }
        });

    }

    /**
     * forgetpwd
     */
    public void initListenAction() {
        mForgetPwd.forget_username_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (mForgetPwd.input_result == -1) {
                        mForgetPwd.forget_username_et.setText("");
                        mForgetPwd.forget_username_et.setHint("请输入正确的手机号或邮箱");
                        mForgetPwd.forget_username_et.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_username_et.setFocusable(false);
                    }else {
                        mForgetPwd.forget_username_et.setFocusable(true);
                        if ((mForgetPwd.input_result.equals(mForgetPwd.Email)) || (mForgetPwd.input_result.equals(mForgetPwd.Mobile))) {
                            DTFactoryApi.checkUserData(mForgetPwd, input_str, new ResultLinstener() {
                                @Override
                                public void onSuccess(Object obj) {
                                    DTCheckUser  checkUser = (DTCheckUser) obj;
                                    Boolean isOK = checkUser.isData();
                                    LogUtils.e("------isOK------>"+isOK);
                                    if (isOK) {
                                        mForgetPwd.input_result = mForgetPwd.Mobile;
                                        if (mForgetPwd.forget_send_code.getVisibility() == View.GONE){
                                            mForgetPwd.forget_send_code.setVisibility(View.VISIBLE);
                                            final Animation animation_verfycode = AnimationUtils.loadAnimation(mForgetPwd, R.anim.register_verfycode_in_from_right);
                                            mForgetPwd.forget_send_code.startAnimation(animation_verfycode);
                                        }
                                    } else {
                                        mForgetPwd.forget_username_et.setText("");
                                        mForgetPwd.forget_username_et.setHint("请填写注册手机号或邮箱");
                                        mForgetPwd.forget_username_et.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                                        mForgetPwd.forget_username_et.setFocusable(false);
                                    }
                                }
                                @Override
                                public void onFailure(String obj) {

                                }
                                @Override
                                public void onException(String exception) {

                                }
                            });
                        }
                    }

                }
                return true;
            }
        });
        //verfiyCode
        mForgetPwd.forget_input_code_et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String input_code_et = mForgetPwd.forget_input_code_et.getText().toString().trim();
                    if (TextUtils.isEmpty(input_code_et)) {

                        mForgetPwd.forget_input_code_et.setText("");
                        mForgetPwd.forget_input_code_et.setHint("验证码不能为空");
                        mForgetPwd.forget_input_code_et.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_input_code_et.setFocusable(false);

                    } else if ((!TextUtils.isEmpty(input_code_et)) && input_code_et.length() < 6) {

                        mForgetPwd.forget_input_code_et.setText("");
                        mForgetPwd.forget_input_code_et.setHint("验证码不能少于六位");
                        mForgetPwd.forget_input_code_et.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_input_code_et.setFocusable(false);

                    } else {
                        mForgetPwd.forget_input_resetpwd.setVisibility(View.VISIBLE);
                        final Animation animation_username = AnimationUtils.loadAnimation(mForgetPwd, R.anim.forget_pwd_in_from_bottom);
                        mForgetPwd.forget_input_resetpwd.startAnimation(animation_username);

                    }
                }
                return false;
            }
        });
        mForgetPwd.forget_input_resetpwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_NEXT || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String str_pwd_reset = mForgetPwd.forget_input_resetpwd.getText().toString().trim();
                    if (TextUtils.isEmpty(str_pwd_reset)) {

                        mForgetPwd.forget_input_resetpwd.setText("");
                        mForgetPwd.forget_input_resetpwd.setHint("密码不能为空");
                        mForgetPwd.forget_input_resetpwd.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_input_resetpwd.setFocusable(false);

                    } else if ((!TextUtils.isEmpty(str_pwd_reset)) && str_pwd_reset.length() < 6) {

                        mForgetPwd.forget_input_resetpwd.setText("");
                        mForgetPwd.forget_input_resetpwd.setHint("密码不能少于六位");
                        mForgetPwd.forget_input_resetpwd.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_input_resetpwd.setFocusable(false);
                    } else {
                       // mForgetPwd.forget_input_resetpwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        mForgetPwd.forget_input_pwd_confirm.setVisibility(View.VISIBLE);
                        final Animation animation_username = AnimationUtils.loadAnimation(mForgetPwd, R.anim.forget_pwd_in_from_bottom);
                        mForgetPwd.forget_input_pwd_confirm.startAnimation(animation_username);

                    }
                }
                return false;
            }
        });
        //确认密码
        mForgetPwd.forget_input_pwd_confirm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    String str_pwd_confirm = mForgetPwd.forget_input_pwd_confirm.getText().toString().trim();
                    String str_pwd_reset = mForgetPwd.forget_input_resetpwd.getText().toString().trim();
                    if (TextUtils.isEmpty(str_pwd_confirm)) {

                        mForgetPwd.forget_input_pwd_confirm.setText("");
                        mForgetPwd.forget_input_pwd_confirm.setHint("密码不能为空");
                        mForgetPwd.forget_input_pwd_confirm.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_input_pwd_confirm.setFocusable(false);
                    } else if ((!TextUtils.isEmpty(str_pwd_confirm)) && str_pwd_confirm.length() < 6) {
                        mForgetPwd.forget_input_pwd_confirm.setText("");
                        mForgetPwd.forget_input_pwd_confirm.setHint("密码不能少于六位");
                        mForgetPwd.forget_input_pwd_confirm.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_input_pwd_confirm.setFocusable(false);
                    } else if (str_pwd_confirm != null && !str_pwd_confirm.equals(str_pwd_reset)) {

                        mForgetPwd.forget_input_pwd_confirm.setText("");
                        mForgetPwd.forget_input_pwd_confirm.setHint("两次密码不一样");
                        mForgetPwd.forget_input_pwd_confirm.setHintTextColor(mForgetPwd.getResources().getColor(R.color.red));
                        mForgetPwd.forget_input_pwd_confirm.setFocusable(false);
                    } else {
                        //mForgetPwd.forget_input_pwd_confirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        String username = mForgetPwd.forget_username_et.getText().toString().trim();
                        String code = mForgetPwd.forget_input_code_et.getText().toString().trim();
                        String pwd_reset = mForgetPwd.forget_input_pwd_confirm.getText().toString().trim();

                        DTFactoryApi.resetPwdData(mForgetPwd, username, pwd_reset, code,null);
                    }
                }
                return false;
            }
        });
    }



    /**
     * 获取焦点
     */
    public void getfocusWidget(){
        //mobile or email
        mForgetPwd.forget_username_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mForgetPwd.forget_username_et.setFocusable(true);
                mForgetPwd.forget_username_et.setCursorVisible(true);
                mForgetPwd.forget_username_et.setFocusableInTouchMode(true);
                mForgetPwd.forget_username_et.requestFocus();
                mForgetPwd.forget_username_et.requestFocusFromTouch();
            }
        });
        mForgetPwd.forget_username_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    mForgetPwd.forget_username_et.setHint(mForgetPwd.getString(R.string.guid_username_hit));
                    mForgetPwd.forget_username_et.setHintTextColor(mForgetPwd.getResources().getColor(R.color.title_gray_color));
                }
            }
        });

        //验证码
        mForgetPwd.forget_input_code_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mForgetPwd.forget_input_code_et.setFocusable(true);
                mForgetPwd.forget_input_code_et.setFocusableInTouchMode(true);
                mForgetPwd.forget_input_code_et.requestFocus();
                mForgetPwd.forget_input_code_et.requestFocusFromTouch();
            }
        });
        mForgetPwd.forget_input_code_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    mForgetPwd.forget_input_code_et.setHint(mForgetPwd.getString(R.string.forget_write_code));
                    mForgetPwd.forget_input_code_et.setHintTextColor(mForgetPwd.getResources().getColor(R.color.title_gray_color));
                }
            }
        });
        //重置密码
        mForgetPwd.forget_input_resetpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mForgetPwd.forget_input_resetpwd.setFocusable(true);
                mForgetPwd.forget_input_resetpwd.setFocusableInTouchMode(true);
                mForgetPwd.forget_input_resetpwd.requestFocus();
                mForgetPwd.forget_input_resetpwd.requestFocusFromTouch();
            }
        });
        mForgetPwd.forget_input_resetpwd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    mForgetPwd.forget_input_resetpwd.setHint(mForgetPwd.getString(R.string.forget_reset_pwd));
                    mForgetPwd.forget_input_resetpwd.setHintTextColor(mForgetPwd.getResources().getColor(R.color.title_gray_color));
                }
            }
        });
        //确认密码.
        mForgetPwd.forget_input_pwd_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mForgetPwd.forget_input_pwd_confirm.setFocusable(true);
                mForgetPwd.forget_input_pwd_confirm.setFocusableInTouchMode(true);
                mForgetPwd.forget_input_pwd_confirm.requestFocus();
                mForgetPwd.forget_input_pwd_confirm.requestFocusFromTouch();
            }
        });
        mForgetPwd.forget_input_pwd_confirm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){
                    mForgetPwd.forget_input_pwd_confirm.setHint(mForgetPwd.getString(R.string.forget_reset_pwd_again));
                    mForgetPwd.forget_input_pwd_confirm.setHintTextColor(mForgetPwd.getResources().getColor(R.color.title_gray_color));
                }
            }
        });
    }
}
