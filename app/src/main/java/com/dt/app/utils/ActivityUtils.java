package com.dt.app.utils;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import com.dt.app.DTApplication;
import com.dt.app.bean.DTInitData;
import com.dt.app.ui.set.PhotoViewActivity;

public class ActivityUtils {

	/**
	 * 启动图片查看器
	 * @param context
	 * @param url
	 */
	public static void startPhotoViewAct(Context context,String url){
		ArrayList<String> mPhotos  = new ArrayList<String>();
		mPhotos.add(url);
		startPhotoViewAct(context,mPhotos,0);
	}
	/**
	 * 启动图片查看器
	 * @param context
	 * @param mPhotos
	 * @param index
	 */
	public static void startPhotoViewAct(Context context,ArrayList<String> mPhotos,int index){
		Intent mIntent = new Intent(context,PhotoViewActivity.class);
		mIntent.putExtra("photos", mPhotos);
		mIntent.putExtra("index", index);
		context.startActivity(mIntent);
	}

	/**
	 * 查找地址
	 * @param id
	 * @return
	 */
	public static String getItemAddress(Integer id){
		if (id == null)
			return "";
		List<DTInitData.AddressData> addressDataList = DTApplication.getInstance().getAddressDataList();
		for (DTInitData.AddressData addressData : addressDataList) {
			if (addressData.getPid().equals(id)) {
				 return ("" + addressData.getName());
			}
		}
		return "";
	}
	public static void startNewIntent(Activity  context){
		Intent intent = new Intent(context, LoadHtmlActivity.class);
		Bundle bundle_about = new Bundle();
		bundle_about.putString("webview_url", Constant.URL.DTjifeng+"?t=rule");
		bundle_about.putString("webview_title", "积分规则");
		intent.putExtras(bundle_about);
		context.startActivity(intent);
	}
}
