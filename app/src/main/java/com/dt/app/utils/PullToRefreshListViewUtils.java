package com.dt.app.utils;

import android.view.View;
import android.widget.ListView;

import com.dt.app.DTApplication;
import com.dt.app.bean.Page;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

/**
 * PullToRefreshListView 包装
 * 
 * @author libin
 * 
 */
public class PullToRefreshListViewUtils<T extends View> {
	private PullToRefreshListView mPullToRefreshListView;
	 
	private String lastUpdateTime;
	private int currentPage = 1;
	private Page page;

	public void setPage(Page page) {
		this.page = page;
	}

	public PullToRefreshListViewUtils(
			PullToRefreshListView mPullToRefreshListView) {
		this.mPullToRefreshListView = mPullToRefreshListView;
	}

	/**
	 * ：disabled（禁用下拉刷新），pullFromStart（仅支持下拉刷新），pullFromEnd（仅支持上拉刷新），both（二者都支持）
	 * ，manualOnly（只允许手动触发）
	 * 
	 * @param mode
	 */
	
	public void init() {
		init(true, true);
	}

	public void init(boolean load,boolean refresh) {
		lastUpdateTime = Utils.getCurrentDate();
		mPullToRefreshListView.setPullLoadEnabled(load);
		mPullToRefreshListView.setPullRefreshEnabled(refresh);
		mPullToRefreshListView.setScrollLoadEnabled(false);
	 
		//隐藏当第一次加载数据不够填充这个屏幕时，隐藏底部加载框
		mPullToRefreshListView.getFooterLoadingLayout().show(true);
		
		// 隐藏当第一次加载数据不够填充这个屏幕时，隐藏底部加载框
		mPullToRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			/**
	         * 下拉松手后会被调用
	         * 
	         * @param refreshView 刷新的View
	         */
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				setLastUpdateTime();
				currentPage = 1;
//				mPullToRefreshListView.setHasMoreData(true);
				if (listener != null) {
					listener.onPullDownToRefresh(refreshView,currentPage);
				}
			}
			/**
	         * 加载更多时会被调用或上拉时调用
	         * 
	         * @param refreshView 刷新的View
	         */
			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				currentPage++;
				if (page!=null) {
					if (page.getTotalPage()<currentPage) {
						mPullToRefreshListView.onPullUpRefreshComplete();
//						mPullToRefreshListView.setHasMoreData(false);
//						ToastUtils.showTextToast(DTApplication.getInstance().getApplicationContext(), "没有数据了");
					} 
				}
				if (listener != null) {
					listener.onPullUpToRefresh(refreshView, currentPage);
				}
			}
			
		
		});
 
	}
	
	private int delayTime = 1500;
	public void onRefreshOrLoadComplete(final int currentPage){
		mPullToRefreshListView.postDelayed(new Runnable() {
			@Override
			public void run() {
				if (currentPage==1) {
					onRefreshComplete();
				}else {
					onLoadComplete();
				}
			}
		}, delayTime);
		
	}
	
	//刷新完成
	public void onRefreshComplete(){
//		mPullToRefreshListView.setHasMoreData(true);
//		mPullToRefreshListView.postDelayed(new Runnable() {
//			@Override
//			public void run() {
				mPullToRefreshListView.onPullDownRefreshComplete();
//			}
//		}, delayTime);
		
	}
	//加载完成
	public void onLoadComplete(){
//		mPullToRefreshListView.postDelayed(new Runnable() {
//			@Override
//			public void run() {
				mPullToRefreshListView.onPullUpRefreshComplete();
//			}
//		}, delayTime);
		
	}

	private void setLastUpdateTime() {
		
		mPullToRefreshListView.setLastUpdatedLabel(
				lastUpdateTime);
		lastUpdateTime = Utils.getCurrentDate();
	}

 

	private PullOnRefreshListener listener;

	public void setPullOnRefreshListener(PullOnRefreshListener listener) {
		this.listener = listener;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public interface PullOnRefreshListener {
		/**
		 * 下拉松手后会被调用
		 * 
		 * @param refreshView
		 *            刷新的View
		 */
		void onPullDownToRefresh(final PullToRefreshBase refreshView,
				int currentPage);

		/**
		 * 加载更多时会被调用或上拉时调用
		 * 
		 * @param refreshView
		 *            刷新的View
		 */
		void onPullUpToRefresh(final PullToRefreshBase refreshView,
				int currentPage);
	}
}
