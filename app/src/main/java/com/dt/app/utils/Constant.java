package com.dt.app.utils;

/**
 * Created by caiyuhao on 15-9-6.
 */
public class Constant {
    //qq
    public static final String APP_ID="1104852202";
    //wechat
    public static final String wechat_APP_ID="wxe41c35406234a2c2";
    public static final String wechat_APP_secrete="d4624c36b6795d1d99dcf0547af5443d";

    public static class URL {

        public static String Base_Url = "http://www.draw-together.com";
        //sendVerfyCode
        public static String DTSendVerfyCode = Base_Url + "/api/v1/user/sendVerifyCode";
        //checkUser
        public static String DTCheckUser = Base_Url + "/api/v1/user/checkUser";
        //register
        public static String DTRegister = Base_Url + "/api/v1/user/register";
        //login
        public static String DTLogin = Base_Url + "/api/v1/user/login";
        //login 第三方
        public static String DTLogin3p = Base_Url + "/api/v1/user/login3p";
 
        //上传文件
        //获取token
        public static String DTUploadToken = Base_Url+"/api/v1/misc/qiniuToken";
        //请好友注册
        public static String DTUserInvite = Base_Url+"/api/v1/user/invite";
        
        
        /**  USER CENTER */
        //每日作品分页列表
        public static String DTCurrentWorksDaily = Base_Url + "/api/v1/works/daily";
        //保存收货地址
        public static String DTAddressSave = Base_Url + "/api/v1/user/addressSave";
        //收货地址列表
        public static String DTAddressList = Base_Url + "/api/v1/user/addressList";
        //收货地址列表
        public static String DTAddressDelete = Base_Url + "/api/v1/user/addressDelete";
        //设置默认收货地址
        public static String DTAddressSetDefault = Base_Url + "/api/v1/user/addressSetDefault";
        //添加作品
        public static String DTWorksUpload = Base_Url + "/api/v1/member/works/upload";
        //关注作家
        public static String DTSnsFolloweAdd = Base_Url + "/api/v1/member/sns/followeAdd";
        //取消关注作家
        public static String DTSnsFolloweCancel = Base_Url + "/api/v1/member/sns/followCancel";
        //积分统计
        public static String DTJifenStat = Base_Url + "/api/v1/member/jifen/stat";
        //积分换礼列表
        public static String DTJifenOrders = Base_Url + "/api/v1/member/jifen/orders";
        //粉丝列表
        public static String DTSnsFans = Base_Url + "/api/v1/member/sns/fans";
        //分页获取用户关注的人
        public static String DTSnsFollows = Base_Url + "/api/v1/member/sns/follows";
        //修改用户信息
        public static String DTUserProfileUpdate = Base_Url + "/api/v1/user/profileUpdate";
 
        //reset pwd
        public static String DTresetPwd = Base_Url + "/api/v1/user/resetPwd";
        //init Data
        public static String DTinitData = Base_Url + "/api/v1/misc/initData";
        //noticeList
        public static String DTnoticeList = Base_Url + "/api/v1/user/noticeList";
        //删除通知
        public static String DTnoticeDelete = Base_Url + "/api/v1/user/noticeDelete";
        //通知消息明细
        public static String DTnoticeInfo = Base_Url + "/api/v1/user/noticeInfo";
        //修改系统通知已读
        public static String DTnoticeSetRead = Base_Url + "/api/v1/user/noticeSetRead";

        //Setting
        public static String DTSysSetting = Base_Url + "/api/v1/user/setting";
        public static String DTSysSettingUpdate = Base_Url + "/api/v1/user/settingUpdate";
        public static String DTSettingChangePd = Base_Url + "/api/v1/member/profile/updatePwd";

        //appHome
        public static String DTHome = Base_Url + "/api/v1/user/apphome";
        
        
        /**  works */
        //作品详情
        public static String DTWorksView = Base_Url + "/api/v1/works/view";
        //获取搜索标签
        public static String DTTagTopn = Base_Url + "/api/v1/works/tag/topn";
        //打赏明细
        public static String DTDonationView = Base_Url + "/api/v1/works/donation/view";
        //打赏人列表
        public static String DTDonationMembers = Base_Url + "/api/v1/works/donation/members";
        //打赏
        public static String DTDonationSubmit = Base_Url + "/api/v1/works/donation/submit";
        //评论列表
        public static String DTCommentList = Base_Url + "/api/v1/works/comment/list";
        //评论作品
        public static String DTCommentSubmit = Base_Url + "/api/v1/works/comment/submit";
        //喜欢
        public static String DTLikeSubmit = Base_Url + "/api/v1/works/like/submit";
        //取消喜欢
        public static String DTLikeCancel = Base_Url + "/api/v1/works/like/cancel";
        //喜欢作品的用户列表
        public static String DTLikeMembers = Base_Url + "/api/v1/works/like/members";
        //修改作品
        public static String DTWorksUpdate = Base_Url + "/api/v1/member/works/update";
        //删除作品
        public static String DTWorksPurge = Base_Url + "/api/v1/member/works/purge";
        //搜索
        public static String DTWorksSearch = Base_Url + "/api/v1/works/search";
        
        /**  COLLECTION */
        //标签
        public static String DTCollectionList = Base_Url + "/api/v1/collection/list";
        //艺术家
        public static String DTCollectionArtists = Base_Url + "/api/v1/collection/artists";
        //时间线
//        public static String DTTimelineYears = Base_Url + "/api/v1/collection/timeline/sum";
        //时间线
        public static String DTTimelineYears = Base_Url + "/api/v1/collection/timeline/sumx";
        //收藏过的作品按年月分页列表
        public static String DTTimelineList = Base_Url + "/api/v1/collection/timeline/list";

        /**  JIFEN */
        //积分商品列表
        public static String DTGoodsList = Base_Url + "/api/v1/jifen/goods/list";
        //积分商品详情
        public static String DTGoodsView = Base_Url + "/api/v1/jifen/goods/view";
        //积分确认页
        public static String DTOrderConfirm = Base_Url + "/api/v1/jifen/order/confirm";
        //提交积分
        public static String DTOrderSubmit = Base_Url + "/api/v1/jifen/order/submit";
     
        /**  ARTIST */
        //艺术家首页（及个人中心）
        public static String DTArtistHome = Base_Url + "/api/v1/artist/home";
        //分页
        public static String DTWorksList = Base_Url + "/api/v1/artist/works/list";
        //喜欢的作品列表
        public static String DTWorksLikes = Base_Url + "/api/v1/artist/works/likes";
        /**  THEME */
        // 获取主题下作品列表
        public static String DTThemeWorksList = Base_Url + "/api/v1/theme/works/list";
        //主题详情
        public static String DTThemeWorksDetail = Base_Url + "/api/v1/theme/view";
        // 获取主题评论列表
        public static String DTThemeCommentList = Base_Url + "/api/v1/theme/comment/list";
        // 评论主题
        public static String DTThemeCommentSubmit = Base_Url + "/api/v1/theme/comment/submit";
        //绑定第三方
        public static String DTBind3p = Base_Url + "/api/v1/user/bind3p";
        //检查版本
        public static String DTcheckVersion = Base_Url + "/api/v1/misc/checkVersion";
        //积分guanyu
        public static String DTjifeng = Base_Url + "/api/v1/misc/help";
        //未读的消息数
        public static String DTUnreadCount = Base_Url + "/api/v1/user/noticeUnreadCount";
        //静态分享
        public static String DTShare = Base_Url + "/api/v1/works/share";

    }

    public static class Parameter {
        public static  String DTloginname =  "loginname";
        public static  String DTpassword =  "password";
        public static  String DTcode =  "code";
        public static  String DTnickname =  "nickname";
        public static  String DTclientType =  "clientType";

        public static  String DTcomeFrom =  "comeFrom";
        public static  String DTopenid =  "openid";
        public static  String DTgender =  "gender";
        public static  String DTlogo =  "logo";
        public static  String DTlastUpdateTime =  "lastUpdateTime";
    }

    public static class PrefrencesPt{
        public static  String DTnonce =  "nonce";
        public static  String DTmkey =  "mkey";
        public static  String DTid =  "id";

        public static  String DTmobilephone =  "mobilephone";
        public static  String DTnickname =  "nickname";
        public static  String DTlogo =  "logo";
        public static  String DTregisterTime =  "registerTime";
        public static  String DTgender =  "gender";
        public static  String DTisSigned =  "isSigned";
        public static  String DTfollowingCount =  "followingCount";
        public static  String DTfollowersCount =  "followersCount";
        public static  String DTworksCount =  "worksCount";
        public static  String DTlikeCount =  "likeCount";
        public static  String DTdonationCount =  "donationCount";
        public static  String DTcommentCount =  "commentCount";
        public static  String DTlastUpdateTime =  "lastUpdateTime";
        public static  String DTSignature =  "signature";
        public static  String DTLocation =  "location";
        public static  String DTemail =  "email";
        public static  String DTjifenAvailable =  "jifenAvailable";
        public static  String DTpwd =  "pwd";
    }
}
