package com.dt.app.utils;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseActivity;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

/**
 * @Description: 加载HTML文件

 */

public class LoadHtmlActivity extends BaseActivity {
	@ViewInject(R.id.wv_load)
	private WebView wv_load;
	@ViewInject(R.id.tv_title)
	private TextView tv_title;
	@ViewInject(R.id.tv_back)
	private TextView tv_back;
	private String webview_title = "", webview_url = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_ac_load_html);
		ViewUtils.inject(this);
		Intent intent = getIntent();
		webview_title = intent.getStringExtra("webview_title");
		webview_url = intent.getStringExtra("webview_url");
		System.out.println(webview_url);
		LogUtils.e("--------webview_url----->"+webview_url);
		tv_title.setText(webview_title);
		// WebView加载web资源
		wv_load.loadUrl(webview_url);
		// 覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
		wv_load.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				// 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
				view.loadUrl(url);
				return true;
			}
		});

	}
	@OnClick(R.id.tv_back)
	public void back(View view){
		finish();
	}

}
