package com.dt.app.utils;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ViewUtils {

	public static LayoutParams getLayoutParams(View v, int width, int height) {
		ViewParent viewParent = v.getParent();
		if (viewParent != null) {
			if (viewParent instanceof LinearLayout) {
				LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) v
						.getLayoutParams();
				if (layoutParams == null) {
					layoutParams = new LinearLayout.LayoutParams(width, height);
				}else {
					layoutParams.width = width;
					layoutParams.height = height;
				}
				return layoutParams;
			} else if (viewParent instanceof RelativeLayout) {
				RelativeLayout.LayoutParams layoutParams = (android.widget.RelativeLayout.LayoutParams) v
						.getLayoutParams();
				if (layoutParams == null) {
					layoutParams = new RelativeLayout.LayoutParams(width,
							height);
				}else {
					layoutParams.width = width;
					layoutParams.height = height;
				}
				return layoutParams;
			} else if (viewParent instanceof FrameLayout) {
				FrameLayout.LayoutParams layoutParams = (android.widget.FrameLayout.LayoutParams) v
						.getLayoutParams();
				if (layoutParams == null) {
					layoutParams = new FrameLayout.LayoutParams(width, height);
				}else {
					layoutParams.width = width;
					layoutParams.height = height;
				}
				return layoutParams;
			}
		}
		return null;
	}

	public static LayoutParams getAbsLayoutParams(View v, int width, int height) {
		AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) v
				.getLayoutParams();
		if (layoutParams == null) {
			layoutParams = new AbsListView.LayoutParams(width, height);
		}
		return layoutParams;
	}
}
