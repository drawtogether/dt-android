package com.dt.app.utils;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class AnimUtils {
	public static float fromAlpha;
	public static float toAlpha;

	public static Animation aphlaAnimation(boolean show, long durationMillis) {
		return aphlaAnimation(show, durationMillis, null, false);
	}

	public static Animation aphlaAnimation(boolean show, long durationMillis,
			AnimationListener listener) {
		return aphlaAnimation(show, durationMillis, listener, false);
	}

	public static Animation aphlaAnimation(boolean show, long durationMillis,
			AnimationListener listener, boolean after) {
		if (show) {
			fromAlpha = 0.0f;
			toAlpha = 1.0f;
		} else {
			fromAlpha = 1.0f;
			toAlpha = 0.0f;
		}
		AlphaAnimation alphaAnimation = new AlphaAnimation(fromAlpha, toAlpha);
		alphaAnimation.setDuration(durationMillis);
		if (listener != null) {
			alphaAnimation.setAnimationListener(listener);
		}
		if (after) {
			alphaAnimation.setFillAfter(after);
		}
		return alphaAnimation;
	}

	/**
	 * 缩放动画
	 * 
	 * @param relativeToSelf
	 * @param fromX
	 * @param toX
	 * @param fromY
	 * @param toY
	 * @param durationMillis
	 * @return
	 */
	public static Animation scaleAnimation(boolean relativeToSelf, float fromX,
			float toX, float fromY, float toY, long durationMillis) {
		return scaleAnimation(relativeToSelf, fromX, toX, fromY, toY,
				durationMillis, null);
	}

	public static Animation scaleAnimation(boolean relativeToSelf, float fromX,
			float toX, float fromY, float toY, long durationMillis,
			AnimationListener listener) {
		return scaleAnimation(relativeToSelf, fromX, toX, fromY, toY,
				durationMillis, listener, false);
	}

	public static Animation scaleAnimation(boolean relativeToSelf, float fromX,
			float toX, float fromY, float toY, long durationMillis,
			AnimationListener listener, boolean after) {
		ScaleAnimation scaleAnimation = null;
		if (relativeToSelf) {
			scaleAnimation = new ScaleAnimation(fromX, toX, fromY, toY,
					Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
		} else {
			scaleAnimation = new ScaleAnimation(fromX, toX, fromY, toY);
		}
		scaleAnimation.setDuration(durationMillis);
		scaleAnimation.setFillAfter(after);
		if (listener != null) {
			scaleAnimation.setAnimationListener(listener);
		}
		return scaleAnimation;
	}

	/**
	 * 移动动画
	 * 
	 * @param type
	 * @param fromX
	 * @param toX
	 * @param fromY
	 * @param toY
	 * @param durationMillis
	 * @return
	 */
	public static Animation translateAnimation(int type, float fromX,
			float toX, float fromY, float toY, long durationMillis) {
		return translateAnimation(type, fromX, toX, fromY, toY, durationMillis,
				null, false);
	}

	public static Animation translateAnimation(int type, float fromX,
			float toX, float fromY, float toY, long durationMillis,
			AnimationListener listener) {
		return translateAnimation(type, fromX, toX, fromY, toY, durationMillis,
				listener, false);
	}

	public static Animation translateAnimation(int type, float fromX,
			float toX, float fromY, float toY, long durationMillis,
			AnimationListener listener, boolean after) {
		TranslateAnimation translateAnimation = null;
		if (type == Animation.RELATIVE_TO_SELF) {
			translateAnimation = new TranslateAnimation(
					Animation.RELATIVE_TO_SELF, fromX,
					Animation.RELATIVE_TO_SELF, toX,
					Animation.RELATIVE_TO_SELF, fromY,
					Animation.RELATIVE_TO_SELF, toY);

		} else if (type == Animation.RELATIVE_TO_PARENT) {
			translateAnimation = new TranslateAnimation(
					Animation.RELATIVE_TO_PARENT, fromX,
					Animation.RELATIVE_TO_PARENT, toX,
					Animation.RELATIVE_TO_PARENT, fromY,
					Animation.RELATIVE_TO_PARENT, toY);
		} else {
			translateAnimation = new TranslateAnimation(fromX, toX, fromY, toY);
		}
		translateAnimation.setDuration(durationMillis);
		translateAnimation.setFillAfter(after);
		if (listener != null) {
			translateAnimation.setAnimationListener(listener);
		}
		return translateAnimation;
	}
}
