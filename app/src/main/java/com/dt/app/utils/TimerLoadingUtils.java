package com.dt.app.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by caiyuhao on 15-11-16.
 */
public class TimerLoadingUtils extends Timer {
    private Handler mhandler;
    private Activity activity;
    private int index = 0 ;
    public TimerLoadingUtils(Activity activity ,Handler handler){
        mhandler = handler;
    }
    @Override
    public void schedule(TimerTask task, Date when, long period) {
        super.schedule(task, when, period);

    }

    public class TimerLoadTask extends TimerTask {

        @Override
        public void run() {
            index++;
            Message message = new Message();
            message.what = index;
            mhandler.sendMessage(message);
        }
    }
}
