package com.dt.app.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dt.app.R;

/**
 * Created with IntelliJ IDEA.
 * User: nanzhiwen
 * Date: 12-12-19
 * Time: 下午5:10
 * Toast 提示工具类
 */
public class ToastUtils {

    private static Toast mToast;
    private static Dialog mDialog;
    private static Dialog mDataDialog;
    private static Dialog mBindDialog;
    private static ProgressDialog mProgressDialog;

    /**
     * 当快速弹toast时可以快速更新toast显示内容，不会造成toast文本显示滞后的问题.
     *
     * @param context    上下文
     * @param resourceId 文本资源ID
     */
    public static void showTextToast(Context context, int resourceId) {
        showTextToast(context, context.getString(resourceId));
    }

    /**
     * 显示Toast文本
     *
     * @param context 上下文
     * @param toast   要显示的文本内容 （字符串形式）
     */
    public static void showTextToast(Context context, String toast) {
        if (mToast == null) {
            mToast = Toast.makeText(context, toast, Toast.LENGTH_SHORT);
        }

        mToast.setGravity(Gravity.CENTER, 0, 0);

        mToast.setText(toast);
        mToast.show();

    }

    public static void showProgressDialog(Context context, String message, String title) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(message);
            mProgressDialog.setTitle(title);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    
    public static void dismissDialog() {
    	try {
    		if (mDialog!=null) {
        		mDialog.dismiss();
    		}
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public static void showDialog(Context context,String content,OnClickListener listener){
//        if (mDialog == null){
            mDialog = new Dialog(context, R.style.custom_dialog);
            View mDialogContentView= LayoutInflater.from(context).inflate(R.layout.dt_dialog,null);
            TextView tv_dialog_content = (TextView) mDialogContentView.findViewById(R.id.tv_dialog_content);
            LinearLayout ll_image_cancel =   (LinearLayout) mDialogContentView.findViewById(R.id.ll_image_cancel);
            LinearLayout ll_image_confirm =   (LinearLayout) mDialogContentView.findViewById(R.id.ll_image_confirm);
            if (content!=null) {
            	tv_dialog_content.setText(content);
			}
            ll_image_cancel.setOnClickListener(listener);
            ll_image_confirm.setOnClickListener(listener);
            mDialog.setContentView(mDialogContentView);
//        }
        mDialog.show();
    }


    /**
     * showProgressDialog
     *
     * @param context
     */
    public static void showDtDialog(Context context) {
       // if (mDataDialog == null) {
            mDataDialog = new Dialog(context, R.style.loading_dialog);
            View view = LayoutInflater.from(context).inflate(R.layout.dt_progress_small_t, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.progressBar_dt);
            RotateAnimation  refreshingAnimation = (RotateAnimation) AnimationUtils.loadAnimation(context, R.anim.rotating);
            LinearInterpolator lir = new LinearInterpolator();
            refreshingAnimation.setInterpolator(lir);
            imageView.startAnimation(refreshingAnimation);

            mDataDialog.setContentView(view);

            WindowManager.LayoutParams params = mDataDialog.getWindow().getAttributes();
            params.width = 200;
            params.height = 200;

            mDataDialog.getWindow().setAttributes(params);
       // }
        mDataDialog.show();
    }

    /**
     * cancelProgressDialog
     */
    public static void cancelProgressDt() {
        if (mDataDialog != null) {
            mDataDialog.dismiss();
        }
    }

}
