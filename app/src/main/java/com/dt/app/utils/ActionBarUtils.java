package com.dt.app.utils;

import android.app.Activity;
import android.view.View;

import com.dt.app.R;
import com.library.markupartist.android.widget.ActionBar;

/**
 * Created by libin on 2015/8/10.
 */
public class ActionBarUtils {

    public static void initActionBar(Activity context,String title,boolean isHome, final ActionBarClickListener listener) {
        ActionBar actionBar = (ActionBar)context.findViewById(R.id.actionbar_base);
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(isHome);
        actionBar.setHomeAction(new ActionBar.Action() {
            @Override
            public void performAction(View view) {
                if (listener!=null){
                    listener.onClick(view);
                }
            }

            @Override
            public int getDrawable() {
                return  R.drawable.dt_app_icon;
            }

			@Override
			public String getText() {
				// TODO Auto-generated method stub
				return null;
			}
        });

    }


    public  interface ActionBarClickListener{
        public void onClick(View view);
    }




}
