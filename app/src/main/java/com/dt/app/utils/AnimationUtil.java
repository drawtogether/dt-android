package com.dt.app.utils;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;

public class AnimationUtil {

	public Animation fromOutTopBottom(long durationMillis){
		TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, -1.0f, 0); 
		translateAnimation.setDuration(durationMillis);
		return translateAnimation;
	}
	
	public void fromOutTopBottomAlpha(){
		AnimationSet set = new AnimationSet(true);
		TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, -1.0f, 0); 
		translateAnimation.setDuration(500);
		AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
		alphaAnimation.setDuration(1000);
		set.addAnimation(translateAnimation);
		set.addAnimation(alphaAnimation);
		set.start();
		
	}
}
