package com.dt.app.utils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.MainActivity;
import com.dt.app.R;
import com.dt.app.ui.login.DTGuideLogin;

@SuppressLint("NewApi")
public class Utils {
    // 默认图片缓存的目录
    public static final String IMAGE_CACHE_DIR = "picture";
    // 广告换成目录
    public static final String IMAGE_ADS_CACHE_DIR = "/dtpic/ad/";
    public static int totalHeight = 0;

    /**
     * 获取时间搓
     *
     * @return
     */
    public static String getTimeStamp() {
        String time = System.currentTimeMillis() + "";
        return time.substring(0, 10);
    }

    public static String getPhoneType(Activity activity) {
        return "Android";
    }

    public static String getTime(String seconds) {
        try {
            long s = Long.valueOf(seconds) * 1000;
            Date date = new Date(s);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @param time    时间戳
     * @param pattern 格式化模式
     * @return
     */
    public static String getTime(Long time, String pattern) {
        try {
            long s = Long.valueOf(time);
            Date date = new Date(s);
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTime(long mills) {
        return getStringDate(mills + "");
    }

    /**
     * 获取现在时间
     *
     * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
     */
    public static String getStringDate(String time) {
        long tt = Long.parseLong(time);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(tt);
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 获取当前时间
     *
     * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
     */
    public static String getCurrentDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 将文件大小转换成M
     *
     * @param filesize
     * @return
     */
    public static String convertFileSize(long filesize) {
        String strUnit = "B";
        String strAfterComma = "";
        int intDivisor = 1;
        if (filesize >= 1024 * 1024) {
            strUnit = "M";
            intDivisor = 1024 * 1024;
        } else if (filesize >= 1024) {
            strUnit = "K";
            intDivisor = 1024;
        }
        if (intDivisor == 1) {
            return filesize + " " + strUnit;
        }
        strAfterComma = "" + 100 * (filesize % intDivisor) / intDivisor;

        if ("".equals(strAfterComma)) {
            strAfterComma = ".0";
        }

        return filesize / intDivisor + "." + strAfterComma + " " + strUnit;

    }

    public static String convertFileSize(String filesize) {
        if (filesize == null || "".equals(filesize.trim())) {
            return "0";
        }
        long res = 0;
        try {
            res = Long.valueOf(filesize);
            return convertFileSize(res);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String convertStrNull(String str) {
        if ("None".equals(str)) {
            return "";
        }
        return str;
    }

    /**
     * 安装apk
     *
     * @param file
     */
    public static void install(Activity activity, File file) {
        // IntentFilter mFilter = new
        // IntentFilter("android.intent.action.PACKAGE_ADDED");
        // mFilter.setPriority(2147483647);
        // InstallPackageReceiver mReceiver = new InstallPackageReceiver();
        // activity.registerReceiver(mReceiver, mFilter);

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setDataAndType(Uri.parse("file://" + file.toString()),
                "application/vnd.android.package-archive");
        i.putExtra("path", file.toString());
        activity.startActivity(i);
    }

    public static String formatDownloadData(String data) {
        String resStr = "";
        int res = 1;
        try {
            res = Integer.valueOf(data);
            if (res > 10000) {
                res = res / 10000;
                resStr = "万";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return res + resStr;
    }

    /**
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null) {
                return info.isConnected();
            }
        }
        return false;
    }

    public static ProgressDialog show(Context context, String title,
                                      String message) {
        try {
            ProgressDialog pd = ProgressDialog.show(context, title, message,
                    true, false);

            if (title != null)
                pd.setTitle(title);
            pd.setMessage(message);
            pd.setCancelable(true);
            pd.show();
            return pd;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ProgressDialog show(Context context, String message) {
        return show(context, null, message);
    }

    public static ProgressDialog show(Context context) {
        return show(context, null, "加载中");
    }

    /**
     * 计算listview高度
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
    }

    /**
     * @param context
     * @param dp
     * @return
     */
    public static int dp2px(Context context, int dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * 获得状态栏的高度
     *
     * @param context
     * @return
     */
    public static int getStatusHeight(Context context) {
        int statusHeight = -1;
        try {
            Class<?> clazz = Class.forName("com.android.internal.R$dimen");
            Object object = clazz.newInstance();
            int height = Integer.parseInt(clazz.getField("status_bar_height")
                    .get(object).toString());
            statusHeight = context.getResources().getDimensionPixelSize(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusHeight;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier(
                "status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * 日期格式化
     *
     * @param date_time
     * @return
     */
    public static String timeFormat(String date_time) {
        Long time = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // time1 = sdf.format(date_time);
            time = sdf.parse(date_time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Long currentTime = System.currentTimeMillis();
        Long currt = (currentTime - time) / 1000;
        if (currt >= 0) {
            // 1天86400秒
            String html_str = "";
            if (currt / 86400 >= 1) {
                if (currt / 86400 >= 30) {
                    html_str = "<font color='gray'>" + date_time + "</font>";
                } else {
                    html_str = "<font color='gray'>"
                            + Integer.parseInt(String.valueOf(currt / 86400))
                            + "天前</font>";
                }
            }
            if (currt / 86400 < 1 && currt / 3600 >= 1) {
                html_str = "<font color='gray'>"
                        + Integer.parseInt(String.valueOf(currt / 3600))
                        + "小时前</font>";
            }
            if (currt / 3600 < 1) {
                html_str = "<font color='gray'>"
                        + Integer.parseInt(String.valueOf(currt / 60))
                        + "分钟前</font>";
            }
            if (currt / 60 < 1) {
                html_str = "<font color='gray'>" + "刚刚" + "</font>";
            }
            return html_str;
        }
        return null;
    }

    public static void openSoftInput(Context context, EditText editText) {
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public static void hideSoftInput(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    /**
     * 判断是否 邮箱
     *
     * @param strEmail
     * @return
     */
    public static boolean isEmail(String strEmail) {
        String str =  "^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))$";
        Pattern p = Pattern.compile(str,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(strEmail);

        return m.matches();

    }

    /**
     * 判断是否 手机号
     *
     * @param str
     * @return
     */
    public static boolean isMobile(String str) {
       // Pattern p = Pattern.compile("^((13[0-9])|(17[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
        Pattern p = Pattern.compile("[1][3578]\\d{9}");
        Matcher matcher = p.matcher(str);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 隐藏键盘
     */
    public static void hide(View v, Context context) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    /**
     * 显示错误提示对话框
     */
    public static void showWrongDialog(View v, Context context, String msg) {
        LayoutInflater inflaterDl = LayoutInflater.from(context);
        RelativeLayout layout = (RelativeLayout) inflaterDl.inflate(
                R.layout.dialog_changepd_wrong, null);
        // 对话框
        final Dialog dialog = new AlertDialog.Builder(context).create();
        dialog.show();
        dialog.getWindow().setContentView(layout);
        // 关闭按钮
        TextView btnClose = (TextView) layout.findViewById(R.id.tv_close);
        TextView tv_wrong = (TextView) layout.findViewById(R.id.tv_wrong);
        tv_wrong.setText(msg);
        btnClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.8f;
        dialog.getWindow().setAttributes(lp);
        Utils.hide(v, context);
    }

    /**
     * 显示选择对话框
     */
    public static void showChooseDialog(View v, final Activity context, String msg) {
        LayoutInflater inflaterDl = LayoutInflater.from(context);
        RelativeLayout layout = (RelativeLayout) inflaterDl.inflate(
                R.layout.dialog_changepd_choose, null);
        // 对话框
        final Dialog dialog = new AlertDialog.Builder(context).create();
        dialog.show();
        dialog.getWindow().setContentView(layout);
        TextView tv_message = (TextView) layout.findViewById(R.id.tv_message);
        tv_message.setText(msg);
        // 取消按钮
        TextView btnCancel = (TextView) layout.findViewById(R.id.tv_close);
        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        // 确定按钮
        TextView btnOK = (TextView) layout.findViewById(R.id.tv_sure);
        btnOK.setText("确  定");
        btnOK.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferencesUtils.remove(context, Constant.PrefrencesPt.DTnonce);
                PreferencesUtils.remove(context, Constant.PrefrencesPt.DTmkey);
                context.startActivity(new Intent(context, DTGuideLogin.class));
                MainActivity.activity.finish();
                context.finish();
            }
        });
        // 关闭按钮
        TextView btnClose = (TextView) layout.findViewById(R.id.tv_cancle);
        btnClose.setText("取  消");
        btnClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.8f;
        dialog.getWindow().setAttributes(lp);
        Utils.hide(v, context);
    }

    /**
     * 得到toString字符
     */
    public static String getString(EditText text) {
        return text.getText().toString();
    }

    public static String getString(TextView text) {
        return text.getText().toString();
    }

}
