package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * 每日作品列表
 *
 * @author libin
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWorks {
    private Page pager;

    private List<UserWork> workses;


    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }


    public List<UserWork> getWorkses() {
        return workses;
    }

    public void setWorkses(List<UserWork> workses) {
        this.workses = workses;
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class UserWork {
        private Long id;// 作品id
        private Long memberId;// 所属用户id
        private String title;// 标题
        private String content;// 介绍
        private String createTime;// 创建时间

        private Integer status;
        private Integer type;// 类型，1.单幅方图，2.单幅圆图，3.系列作品
        private Integer isSigned;// 是否签约：0 否，1 是
        private Integer dailyOrder;// 被喜欢数
        private Integer likedCount;// 被喜欢数
        private Integer donatedCount;// 被打赏数
        private Integer commentedCount;// 被评论数

        private List<Picture> pictures;
        private Member member;
        private Me me;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getDailyOrder() {
            return dailyOrder;
        }

        public void setDailyOrder(Integer dailyOrder) {
            this.dailyOrder = dailyOrder;
        }

        public Long getMemberId() {
            return memberId;
        }

        public void setMemberId(Long memberId) {
            this.memberId = memberId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public Integer getIsSigned() {
            return isSigned;
        }

        public void setIsSigned(Integer isSigned) {
            this.isSigned = isSigned;
        }

        public Integer getLikedCount() {
            return likedCount;
        }

        public void setLikedCount(Integer likedCount) {
            this.likedCount = likedCount;
        }

        public Integer getDonatedCount() {
            return donatedCount;
        }

        public void setDonatedCount(Integer donatedCount) {
            this.donatedCount = donatedCount;
        }

        public Integer getCommentedCount() {
            return commentedCount;
        }

        public void setCommentedCount(Integer commentedCount) {
            this.commentedCount = commentedCount;
        }

        public List<Picture> getPictures() {
            return pictures;
        }

        public void setPictures(List<Picture> pictures) {
            this.pictures = pictures;
        }

        public Member getMember() {
            return member;
        }

        public void setMember(Member member) {
            this.member = member;
        }

        public Me getMe() {
            return me;
        }

        public void setMe(Me me) {
            this.me = me;
        }


    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Picture {
        private Long id;// 图片id
        private String preColor;// 图片预加载时的占位背景颜色
        private String thumbUrl;// 缩略图地址
        private String imageUrl;// 原始图地址
        private int isSigned;// 是否签约：0 否，1 是
        private Integer h;
        private Integer w;

        public Integer getH() {
            return h;
        }

        public void setH(Integer h) {
            this.h = h;
        }

        public Integer getW() {
            return w;
        }

        public void setW(Integer w) {
            this.w = w;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getPreColor() {
            return preColor;
        }

        public void setPreColor(String preColor) {
            this.preColor = preColor;
        }

        public String getThumbUrl() {
            return thumbUrl;
        }

        public void setThumbUrl(String thumbUrl) {
            this.thumbUrl = thumbUrl;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public int getIsSigned() {
            return isSigned;
        }

        public void setIsSigned(int isSigned) {
            this.isSigned = isSigned;
        }


    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Member {
        //所属用户对象
        private String logo;//头像地址
        private String nickname;//昵称
        private Integer provinceId;//省Id
        private Integer cityId;//城市Id
        private Integer jifen;//城市Id
        private Integer jifenAvailable;//可用积分
        private Integer isSigned;//可用积分
        private Integer gender;//可用积分
        private Long id;

        private int memberId;

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public Integer getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(Integer provinceId) {
            this.provinceId = provinceId;
        }

        public Integer getCityId() {
            return cityId;
        }

        public void setCityId(Integer cityId) {
            this.cityId = cityId;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Integer getJifen() {
            return jifen;
        }

        public void setJifen(Integer jifen) {
            this.jifen = jifen;
        }

        public Integer getJifenAvailable() {
            return jifenAvailable;
        }

        public void setJifenAvailable(Integer jifenAvailable) {
            this.jifenAvailable = jifenAvailable;
        }

        public Integer getIsSigned() {
            return isSigned;
        }

        public void setIsSigned(Integer isSigned) {
            this.isSigned = isSigned;
        }

        public Integer getGender() {
            return gender;
        }

        public void setGender(Integer gender) {
            this.gender = gender;
        }

        public int getMemberId() {
            return memberId;
        }

        public void setMemberId(int memberId) {
            this.memberId = memberId;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Me {
        private Integer isSelf;//是否自己的作品（当前登录用户与该作品的关系）
        private Integer isLiked;//是否已喜欢
        private Integer isDonated;//是否已打赏
        private Integer jifen;//是否已打赏
        private Integer isFollowed;//已关注

        public Integer getIsSelf() {
            return isSelf;
        }

        public void setIsSelf(Integer isSelf) {
            this.isSelf = isSelf;
        }

        public Integer getIsLiked() {
            return isLiked;
        }

        public void setIsLiked(Integer isLiked) {
            this.isLiked = isLiked;
        }

        public Integer getIsDonated() {
            return isDonated;
        }

        public void setIsDonated(Integer isDonated) {
            this.isDonated = isDonated;
        }

        public Integer getJifen() {
            return jifen;
        }

        public void setJifen(Integer jifen) {
            this.jifen = jifen;
        }

        public Integer getIsFollowed() {
            return isFollowed;
        }

        public void setIsFollowed(Integer isFollowed) {
            this.isFollowed = isFollowed;
        }


    }

}
