package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

//积分换礼列表
@JsonIgnoreProperties(ignoreUnknown = true)
public class PointOrder {

	private Page pager;

	private List<Orders> orders;
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Orders {
		private Integer id;
		private Integer orderId;// 订单id
		private Integer goodsId;// 商品id
		private String goodsName;// 商品名称
		private String goodsThumb;// 商品图片
		private Integer jifen;// 购买积分(单价)
		private Integer amount;// 购买数量
		private Integer jifenSum;// 总计积分
		private Integer orderStatus;// 订单状态：0 订单创建，1 积分支付， 2 已发货， 9 取消
		private String createTime;// 创建时间

		public Integer getOrderId() {
			return orderId;
		}

		public void setOrderId(Integer orderId) {
			this.orderId = orderId;
		}

		public Integer getGoodsId() {
			return goodsId;
		}

		public void setGoodsId(Integer goodsId) {
			this.goodsId = goodsId;
		}

		public String getGoodsName() {
			return goodsName;
		}

		public void setGoodsName(String goodsName) {
			this.goodsName = goodsName;
		}

		public String getGoodsThumb() {
			return goodsThumb;
		}

		public void setGoodsThumb(String goodsThumb) {
			this.goodsThumb = goodsThumb;
		}

		public Integer getJifen() {
			return jifen;
		}

		public void setJifen(Integer jifen) {
			this.jifen = jifen;
		}

		public Integer getAmount() {
			return amount;
		}

		public void setAmount(Integer amount) {
			this.amount = amount;
		}

		public Integer getJifenSum() {
			return jifenSum;
		}

		public void setJifenSum(Integer jifenSum) {
			this.jifenSum = jifenSum;
		}

		public Integer getOrderStatus() {
			return orderStatus;
		}

		public void setOrderStatus(Integer orderStatus) {
			this.orderStatus = orderStatus;
		}

		public String getCreateTime() {
			return createTime;
		}

		public void setCreateTime(String createTime) {
			this.createTime = createTime;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

	}

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public List<Orders> getOrders() {
		return orders;
	}

	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}

}
