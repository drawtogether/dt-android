package com.dt.app.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Besttone on 2016/1/28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EveryAdResponse {
    private Page pager;

    private Theme2 theme;

    private int commentsCount;

    private List<UserWorks.UserWork> workses;

    public Theme2 getTheme() {
        return theme;
    }

    public void setTheme(Theme2 theme) {
        this.theme = theme;
    }

    public Page getPager() {
        return pager;
    }

    public void setPager(Page pager) {
        this.pager = pager;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public List<UserWorks.UserWork> getWorkses() {
        return workses;
    }

    public void setWorkses(List<UserWorks.UserWork> workses) {
        this.workses = workses;
    }

    public static class Theme2 {
        private int id;

        private String name;

        private String brief;

        private String picture;

        private ArrayList<OutCell> detailJson;

        private int themeType;

        private int isPublished;

        private int isTop;

        private String tagx;

        private int worksCount;

        private String createTime; //2016-01-13 17:48:12",

        private String updateTime; //2016-01-14 12:34:07",

        private int updateAdminId;

        private String updateAdminName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBrief() {
            return brief;
        }

        public void setBrief(String brief) {
            this.brief = brief;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public ArrayList<OutCell> getDetailJson() {
            return detailJson;
        }

        public void setDetailJson(ArrayList<OutCell> detailJson) {
            this.detailJson = detailJson;
        }

        public int getThemeType() {
            return themeType;
        }

        public void setThemeType(int themeType) {
            this.themeType = themeType;
        }

        public int getIsPublished() {
            return isPublished;
        }

        public void setIsPublished(int isPublished) {
            this.isPublished = isPublished;
        }

        public int getIsTop() {
            return isTop;
        }

        public void setIsTop(int isTop) {
            this.isTop = isTop;
        }

        public String getTagx() {
            return tagx;
        }

        public void setTagx(String tagx) {
            this.tagx = tagx;
        }

        public int getWorksCount() {
            return worksCount;
        }

        public void setWorksCount(int worksCount) {
            this.worksCount = worksCount;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public int getUpdateAdminId() {
            return updateAdminId;
        }

        public void setUpdateAdminId(int updateAdminId) {
            this.updateAdminId = updateAdminId;
        }

        public String getUpdateAdminName() {
            return updateAdminName;
        }

        public void setUpdateAdminName(String updateAdminName) {
            this.updateAdminName = updateAdminName;
        }

        public static class OutCell {
            private int cellCount;

            private ArrayList<Cell> cells;

            public int getCellCount() {
                return cellCount;
            }

            public void setCellCount(int cellCount) {
                this.cellCount = cellCount;
            }

            public ArrayList<Cell> getCells() {
                return cells;
            }

            public void setCells(ArrayList<Cell> cells) {
                this.cells = cells;
            }

            public static class Cell {
                public static final String IMAGE = "image";

                public static final String CONTENT = "content";

                public static final String TITLE = "title";

                public static final String VIDEO = "video";

                private String type;

                private String text;

                private String color;

                private String bgcolor;

                private String align;

                private String imageUrl;

                private int w;

                private int h;

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public String getColor() {
                    return color;
                }

                public void setColor(String color) {
                    this.color = color;
                }

                public String getBgcolor() {
                    return bgcolor;
                }

                public void setBgcolor(String bgcolor) {
                    this.bgcolor = bgcolor;
                }

                public String getAlign() {
                    return align;
                }

                public void setAlign(String align) {
                    this.align = align;
                }

                public int getW() {
                    return w;
                }

                public void setW(int w) {
                    this.w = w;
                }

                public String getImageUrl() {
                    return imageUrl;
                }

                public void setImageUrl(String imageUrl) {
                    this.imageUrl = imageUrl;
                }

                public int getH() {
                    return h;
                }

                public void setH(int h) {
                    this.h = h;
                }
            }
        }

    }
}
