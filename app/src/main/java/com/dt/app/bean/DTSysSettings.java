package com.dt.app.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DTSysSettings {
	private Setting setting;

	public Setting getSetting() {
		return setting;
	}

	public void setSetting(Setting setting) {
		this.setting = setting;
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Setting {
		private Long memberId;
		private Integer isNotify;
		private Integer isLocate;
		public Long getMemberId() {
			return memberId;
		}
		public void setMemberId(Long memberId) {
			this.memberId = memberId;
		}
		public Integer getIsNotify() {
			return isNotify;
		}
		public void setIsNotify(Integer isNotify) {
			this.isNotify = isNotify;
		}
		public Integer getIsLocate() {
			return isLocate;
		}
		public void setIsLocate(Integer isLocate) {
			this.isLocate = isLocate;
		}
		
	}
}
