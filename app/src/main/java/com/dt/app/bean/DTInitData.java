package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by caiyuhao on 15-9-12.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DTInitData {
    private Integer code;//1 有数据更新，0 无数据更新
    private String message;//提示信息
    private InitData  data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public InitData getData() {
        return data;
    }

    public void setData(InitData data) {
        this.data = data;
    }
    
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class InitData {
        //初始数据
        private Long updateTime;//本次更新时间
        private List<AddressData> region;//地区

        private List<LogoData> logo;//趣味头像

        private List<TagData> tag;//预定义标签
        private List<DmData>  dm;
        private SplashData splash;

        public List<DmData> getDm() {
            return dm;
        }

        public void setDm(List<DmData> dm) {
            this.dm = dm;
        }

        public SplashData getSplash() {
            return splash;
        }

        public void setSplash(SplashData splash) {
            this.splash = splash;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }

        public List<AddressData> getRegion() {
            return region;
        }

        public void setRegion(List<AddressData> region) {
            this.region = region;
        }

        public List<LogoData> getLogo() {
            return logo;
        }

        public void setLogo(List<LogoData> logo) {
            this.logo = logo;
        }

        public List<TagData> getTag() {
            return tag;
        }

        public void setTag(List<TagData> tag) {
            this.tag = tag;
        }
    }
    public static class SplashData{
        private Integer id;
        private String imageUrl;
        private Integer w;
        private Integer h;

        public Integer getW() {
            return w;
        }

        public void setW(Integer w) {
            this.w = w;
        }

        public Integer getH() {
            return h;
        }

        public void setH(Integer h) {
            this.h = h;
        }
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }

    public static class DmData{
        private Integer id;
        private String imageUrl;
        private Integer w;
        private Integer h;

        public Integer getW() {
            return w;
        }

        public void setW(Integer w) {
            this.w = w;
        }

        public Integer getH() {
            return h;
        }

        public void setH(Integer h) {
            this.h = h;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }

    public static class AddressData{
        private Integer id;//地区编号
        private Integer pid;//父编号
        private String name;//地区名称

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getPid() {
            return pid;
        }

        public void setPid(Integer pid) {
            this.pid = pid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    public static class LogoData{
        private Integer id;//编号
        private String imageUrl;//图片绝对路径
        private Integer w;
        private Integer h;

        public Integer getW() {
            return w;
        }

        public void setW(Integer w) {
            this.w = w;
        }

        public Integer getH() {
            return h;
        }

        public void setH(Integer h) {
            this.h = h;
        }
        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }
    public static class TagData{
        private Integer id;//编号
        private String name;//标签名

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
