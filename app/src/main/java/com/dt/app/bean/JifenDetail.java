 package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.bean.UserWorks.UserWork;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JifenDetail {

	private Member member;
	private JifenGoods goods;

	private Partener partener;
	
	private List<Address> addresses;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class JifenGoods {
		private Long id;// 商品id
		private String name;// 名称
		private String brief;// 简短描述
		private Integer jifen;// 购买所需积分
		private String serviceInfo;// 服务说明
		private Integer memberId;// 合作艺术家Id
		private Integer partenerId;// 合作机构Id
		private Integer stockAmount;// 合作机构Id
		private Integer status;// 合作机构Id
		private Integer clickCount;// 合作机构Id
		private Integer sortOrder;// 合作机构Id
		private String createTime;// 合作机构Id
		private Integer createAdminId;// 合作机构Id
		private String createAdminName;// 合作机构Id

		private List<Picture> pictures;

		private JifenArtist artist;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getBrief() {
			return brief;
		}

		public void setBrief(String brief) {
			this.brief = brief;
		}

		public Integer getJifen() {
			return jifen;
		}

		public void setJifen(Integer jifen) {
			this.jifen = jifen;
		}

		public String getServiceInfo() {
			return serviceInfo;
		}

		public void setServiceInfo(String serviceInfo) {
			this.serviceInfo = serviceInfo;
		}

		public Integer getMemberId() {
			return memberId;
		}

		public void setMemberId(Integer memberId) {
			this.memberId = memberId;
		}

		public Integer getPartenerId() {
			return partenerId;
		}

		public void setPartenerId(Integer partenerId) {
			this.partenerId = partenerId;
		}

		public List<Picture> getPictures() {
			return pictures;
		}

		public void setPictures(List<Picture> pictures) {
			this.pictures = pictures;
		}

		public Integer getStockAmount() {
			return stockAmount;
		}

		public void setStockAmount(Integer stockAmount) {
			this.stockAmount = stockAmount;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}

		public Integer getClickCount() {
			return clickCount;
		}

		public void setClickCount(Integer clickCount) {
			this.clickCount = clickCount;
		}

		public Integer getSortOrder() {
			return sortOrder;
		}

		public void setSortOrder(Integer sortOrder) {
			this.sortOrder = sortOrder;
		}

		public String getCreateTime() {
			return createTime;
		}

		public void setCreateTime(String createTime) {
			this.createTime = createTime;
		}

		public Integer getCreateAdminId() {
			return createAdminId;
		}

		public void setCreateAdminId(Integer createAdminId) {
			this.createAdminId = createAdminId;
		}

		public String getCreateAdminName() {
			return createAdminName;
		}

		public void setCreateAdminName(String createAdminName) {
			this.createAdminName = createAdminName;
		}

		public JifenArtist getArtist() {
			return artist;
		}

		public void setArtist(JifenArtist artist) {
			this.artist = artist;
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class JifenArtist {
		private Long id;// " : 130,
		private String nickname;// " : "小混蛋123",
		private String gender;// " : 0,
		private String logo;// " : "0",
		private String provinceId;// " : 120000,
		private String cityId;// " : 120101,
		private String jifenAvailable;// " : 935,
		private String isSigned;// " : 1,

		private List<UserWork> workses;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getNickname() {
			return nickname;
		}

		public void setNickname(String nickname) {
			this.nickname = nickname;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getLogo() {
			return logo;
		}

		public void setLogo(String logo) {
			this.logo = logo;
		}

		public String getProvinceId() {
			return provinceId;
		}

		public void setProvinceId(String provinceId) {
			this.provinceId = provinceId;
		}

		public String getCityId() {
			return cityId;
		}

		public void setCityId(String cityId) {
			this.cityId = cityId;
		}

		public String getJifenAvailable() {
			return jifenAvailable;
		}

		public void setJifenAvailable(String jifenAvailable) {
			this.jifenAvailable = jifenAvailable;
		}

		public String getIsSigned() {
			return isSigned;
		}

		public void setIsSigned(String isSigned) {
			this.isSigned = isSigned;
		}

		public List<UserWork> getWorkses() {
			return workses;
		}

		public void setWorkses(List<UserWork> workses) {
			this.workses = workses;
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Partener {

		private Long id;
		private String name;
		private String brief;
		private String logo;
		private String createTime;
		private String createAdminId;
		private String createAdminName;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getBrief() {
			return brief;
		}

		public void setBrief(String brief) {
			this.brief = brief;
		}

		public String getLogo() {
			return logo;
		}

		public void setLogo(String logo) {
			this.logo = logo;
		}

		public String getCreateTime() {
			return createTime;
		}

		public void setCreateTime(String createTime) {
			this.createTime = createTime;
		}

		public String getCreateAdminId() {
			return createAdminId;
		}

		public void setCreateAdminId(String createAdminId) {
			this.createAdminId = createAdminId;
		}

		public String getCreateAdminName() {
			return createAdminName;
		}

		public void setCreateAdminName(String createAdminName) {
			this.createAdminName = createAdminName;
		}

	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public JifenGoods getGoods() {
		return goods;
	}

	public void setGoods(JifenGoods goods) {
		this.goods = goods;
	}

	public Partener getPartener() {
		return partener;
	}

	public void setPartener(Partener partener) {
		this.partener = partener;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

}
