package com.dt.app.bean;
/**
 * 页信息
 * @author libin
 *
 */
public class Page {
	/**
	 * 页面显示条数
	 */
	private int pageSize;

	/**
	 * 当前页数
	 */
	private int currentPage;

	/**
	 * 总数据条数
	 */
	private int totalCount;

	/**
	 * 总页数
	 */
	private int totalPage;
	private int page;
	private int rows;
	private int currentCount;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(int currentCount) {
		this.currentCount = currentCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

}
