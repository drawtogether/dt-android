package com.dt.app.bean;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by caiyuhao on 15-9-21.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DTAppHome implements Serializable {
    private Integer code;//1成功，0失败

    private HomeData data;


    public static class HomeData {
        //数据
        private List<WorksRecommendData> worksRecommends;//作品推荐列表
        private Theme theme;

        public List<WorksRecommendData> getWorksRecommends() {
            return worksRecommends;
        }

        public void setWorksRecommends(List<WorksRecommendData> worksRecommends) {
            this.worksRecommends = worksRecommends;
        }

        public Theme getTheme() {
            return theme;
        }

        public void setTheme(Theme theme) {
            this.theme = theme;
        }
    }

    public static class WorksRecommendData {
        private Long worksId;//作品推荐列表
        private String imageUrl;//作品推荐图

        public WorksRecommendData(){

        }
        public WorksRecommendData(String url, Integer it) {
            imageUrl = url;
        }

        public Long getWorksId() {
            return worksId;
        }

        public void setWorksId(Long worksId) {
            this.worksId = worksId;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }

     

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public HomeData getData() {
        return data;
    }

    public void setData(HomeData data) {
        this.data = data;
    }
}
