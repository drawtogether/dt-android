package com.dt.app.bean;

import java.util.List;

/**
 * 时间线
 * 
 * @author libin
 * 
 */
public class TimeLine {
	public List<TimeLineYear> years;

	public static class TimeLineYear {
		public int collectionQuantity;
		public int year;
		public int worksQuantity;

		public int getCollectionQuantity() {
			return collectionQuantity;
		}

		public void setCollectionQuantity(int collectionQuantity) {
			this.collectionQuantity = collectionQuantity;
		}

		public int getYear() {
			return year;
		}

		public void setYear(int year) {
			this.year = year;
		}

		public int getWorksQuantity() {
			return worksQuantity;
		}

		public void setWorksQuantity(int worksQuantity) {
			this.worksQuantity = worksQuantity;
		}

	}

	public List<TimeLineYear> getYears() {
		return years;
	}

	public void setYears(List<TimeLineYear> years) {
		this.years = years;
	}

}
