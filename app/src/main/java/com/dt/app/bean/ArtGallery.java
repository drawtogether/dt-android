package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.UserWorks.Me;
import com.dt.app.bean.UserWorks.Member;

/**
 * 画廊
 * 
 * @author libin
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtGallery {

	private Page pager;
	private List<ArtCollection> collections;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class ArtCollection {
		private Long id;// 作品id
		private Long memberId;// 所属用户id
		private String title;// 标题
		private String content;// 介绍
		private String createTime;// 创建时间
		private Integer type;// 类型，1.单幅方图，2.单幅圆图，3.系列作品
		private Integer isSigned;// 是否签约：0 否，1 是
		private Integer likedCount;// 被喜欢数
		private Integer donatedCount;// 被打赏数
		private Integer commentedCount;// 被评论数
		private String preColor;// 图片预加载时的占位背景颜色
		private String thumbUrl;// 缩略图地址
		private String imageUrl;// 原始图地址
		private Integer h;
		private Integer w;

		public Integer getH() {
			return h;
		}
		public void setH(Integer h) {
			this.h = h;
		}
		public Integer getW() {
			return w;
		}
		public void setW(Integer w) {
			this.w = w;
		}
		private Member member;
		private Me me;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getMemberId() {
			return memberId;
		}

		public void setMemberId(Long memberId) {
			this.memberId = memberId;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getCreateTime() {
			return createTime;
		}

		public void setCreateTime(String createTime) {
			this.createTime = createTime;
		}

		public Integer getType() {
			return type;
		}

		public void setType(Integer type) {
			this.type = type;
		}

		public Integer getIsSigned() {
			return isSigned;
		}

		public void setIsSigned(Integer isSigned) {
			this.isSigned = isSigned;
		}

		public Integer getLikedCount() {
			return likedCount;
		}

		public void setLikedCount(Integer likedCount) {
			this.likedCount = likedCount;
		}

		public Integer getDonatedCount() {
			return donatedCount;
		}

		public void setDonatedCount(Integer donatedCount) {
			this.donatedCount = donatedCount;
		}

		public Integer getCommentedCount() {
			return commentedCount;
		}

		public void setCommentedCount(Integer commentedCount) {
			this.commentedCount = commentedCount;
		}

		public String getPreColor() {
			return preColor;
		}

		public void setPreColor(String preColor) {
			this.preColor = preColor;
		}

		public String getThumbUrl() {
			return thumbUrl;
		}

		public void setThumbUrl(String thumbUrl) {
			this.thumbUrl = thumbUrl;
		}

		public String getImageUrl() {
			return imageUrl;
		}

		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}

		public Member getMember() {
			return member;
		}

		public void setMember(Member member) {
			this.member = member;
		}

		public Me getMe() {
			return me;
		}

		public void setMe(Me me) {
			this.me = me;
		}

	}

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public List<ArtCollection> getCollections() {
		return collections;
	}

	public void setCollections(List<ArtCollection> collections) {
		this.collections = collections;
	}

}
