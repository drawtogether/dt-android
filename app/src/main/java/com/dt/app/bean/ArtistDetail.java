package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.UserWork;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtistDetail {

	private Page pager;

	private UserWork works;

	private List<Member> likeMembers;
	private List<UserWork> recommendWorkses;

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public UserWork getWorks() {
		return works;
	}

	public void setWorks(UserWork works) {
		this.works = works;
	}

	public List<Member> getLikeMembers() {
		return likeMembers;
	}

	public void setLikeMembers(List<Member> likeMembers) {
		this.likeMembers = likeMembers;
	}

	public List<UserWork> getRecommendWorkses() {
		return recommendWorkses;
	}

	public void setRecommendWorkses(List<UserWork> recommendWorkses) {
		this.recommendWorkses = recommendWorkses;
	}

}
