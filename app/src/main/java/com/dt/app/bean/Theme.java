package com.dt.app.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import android.os.Parcel;
import android.os.Parcelable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Theme implements Parcelable {
	private long id;// 1,
	private String name;// "山不在高",
	private String brief;// "山有多高，心有多大",
	private String picture;// "http://artist7.cn/Data/Uploads/2015-06-03/thumb_556dd3d30b933.jpg"

	public Theme(){
		
	}
	
	public Theme(Parcel source) {
		id = source.readLong();
		name = source.readString();
		brief = source.readString();
		picture = source.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(brief);
		dest.writeString(picture);
	}

	public static final Parcelable.Creator<Theme> CREATOR = new Creator<Theme>() {
		@Override
		public Theme[] newArray(int size) {
			return new Theme[size];
		}

		@Override
		public Theme createFromParcel(Parcel source) {
			return new Theme(source);
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	
}
