package com.dt.app.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by andy on 3/4/15. 上传文件
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZTUploadFIle {

	private Integer code;
	private String  message;
	private CheckData data;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CheckData getData() {
		return data;
	}

	public void setData(CheckData data) {
		this.data = data;
	}
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class CheckData{
	   private Integer hasNewV;
	   private NewAppVersion  newv;

	   public Integer getHasNewV() {
		   return hasNewV;
	   }

	   public void setHasNewV(Integer hasNewV) {
		   this.hasNewV = hasNewV;
	   }

	   public NewAppVersion getNewv() {
		   return newv;
	   }

	   public void setNewv(NewAppVersion newv) {
		   this.newv = newv;
	   }
   }
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static  class NewAppVersion{
		private String  version;
		private String  url;
		private String  releaseTime;
		private String  releaseNote;

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getReleaseTime() {
			return releaseTime;
		}

		public void setReleaseTime(String releaseTime) {
			this.releaseTime = releaseTime;
		}

		public String getReleaseNote() {
			return releaseNote;
		}

		public void setReleaseNote(String releaseNote) {
			this.releaseNote = releaseNote;
		}
	}
}
