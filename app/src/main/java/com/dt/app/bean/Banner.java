package com.dt.app.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 广告
 *
 * @author libin gift_ad
 */
public class Banner implements Serializable {
    private BannerAds search_txt;
    private List<BannerAds> index_slider;
    private List<BannerAds> credits_slider;
    private List<BannerAds> gift_ad;
    private List<BannerAds> task_slider;
    private BannerAds startup_bg;
    private BannerAds huodong_auction;

    public BannerAds getSearch_txt() {
        return search_txt;
    }

    public void setSearch_txt(BannerAds search_txt) {
        this.search_txt = search_txt;
    }

    public BannerAds getStartup_bg() {
        return startup_bg;
    }

    public void setStartup_bg(BannerAds startup_bg) {
        this.startup_bg = startup_bg;
    }

    public List<BannerAds> getIndex_slider() {
        return index_slider;
    }

    public void setIndex_slider(List<BannerAds> index_slider) {
        this.index_slider = index_slider;
    }

    public List<BannerAds> getCredits_slider() {
        return credits_slider;
    }

    public void setCredits_slider(List<BannerAds> credits_slider) {
        this.credits_slider = credits_slider;
    }

    public BannerAds getHuodong_auction() {
		return huodong_auction;
	}

	public void setHuodong_auction(BannerAds huodong_auction) {
		this.huodong_auction = huodong_auction;
	}

	public List<BannerAds> getGift_ad() {
		return gift_ad;
	}

	public void setGift_ad(List<BannerAds> gift_ad) {
		this.gift_ad = gift_ad;
	}

	public List<BannerAds> getTask_slider() {
		return task_slider;
	}

	public void setTask_slider(List<BannerAds> task_slider) {
		this.task_slider = task_slider;
	}

	public static class BannerAds {
        private String type;// "url",
        private String url;// "",
        private String show_text;// "巨人之守护",
        private String img_url;// ""

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getShow_text() {
            return show_text;
        }

        public void setShow_text(String show_text) {
            this.show_text = show_text;
        }

        public String getImg_url() {
            return img_url;
        }

        public void setImg_url(String img_url) {
            this.img_url = img_url;
        }

    }
}
