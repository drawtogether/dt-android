package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.UserWorks.Me;

/**
 * 空间-关注
 * 
 * @author libin
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAttention {

	private Page pager;
	private List<MemberAttention> members;
	private Me me;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class MemberAttention {
		private Long id;// 用户Id
		private String logo;// 头像地址
		private String nickname;// 昵称
		private Integer provinceId;// 省Id
		private int gender;
		private int isSigned;
		private Integer cityId;// 城市Id
		private List<MemberWorks> workses;
		private boolean isAttention=true;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getLogo() {
			return logo;
		}

		public void setLogo(String logo) {
			this.logo = logo;
		}

		public String getNickname() {
			return nickname;
		}

		public void setNickname(String nickname) {
			this.nickname = nickname;
		}

		public Integer getProvinceId() {
			return provinceId;
		}

		public void setProvinceId(Integer provinceId) {
			this.provinceId = provinceId;
		}

		public Integer getCityId() {
			return cityId;
		}

		public void setCityId(Integer cityId) {
			this.cityId = cityId;
		}

		public List<MemberWorks> getWorkses() {
			return workses;
		}

		public void setWorkses(List<MemberWorks> workses) {
			this.workses = workses;
		}

		public int getIsSigned() {
			return isSigned;
		}

		public void setIsSigned(int isSigned) {
			this.isSigned = isSigned;
		}

		public int getGender() {
			return gender;
		}

		public void setGender(int gender) {
			this.gender = gender;
		}

		public boolean isAttention() {
			return isAttention;
		}

		public void setAttention(boolean isAttention) {
			this.isAttention = isAttention;
		}

	}
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class MemberWorks {
		private Integer id;
		private Integer memberId;
		private String thumbUrl;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getThumbUrl() {
			return thumbUrl;
		}

		public void setThumbUrl(String thumbUrl) {
			this.thumbUrl = thumbUrl;
		}

		public Integer getMemberId() {
			return memberId;
		}

		public void setMemberId(Integer memberId) {
			this.memberId = memberId;
		}

	}

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public List<MemberAttention> getMembers() {
		return members;
	}

	public void setMembers(List<MemberAttention> members) {
		this.members = members;
	}

	public Me getMe() {
		return me;
	}

	public void setMe(Me me) {
		this.me = me;
	}

}
