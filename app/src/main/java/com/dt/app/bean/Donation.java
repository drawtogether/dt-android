package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.UserWork;

//打赏
@JsonIgnoreProperties(ignoreUnknown = true)
public class Donation {

	private Page pager;
	private UserWork works;
	private List<Member> donates;

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	 
	public List<Member> getDonates() {
		return donates;
	}

	public void setDonates(List<Member> donates) {
		this.donates = donates;
	}

	public UserWork getWorks() {
		return works;
	}

	public void setWorks(UserWork works) {
		this.works = works;
	}

}
