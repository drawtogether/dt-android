package com.dt.app.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 收货地址
 * 
 * @author Administrator
 * 
 */
public class Address implements Parcelable {
	private Long id;// 地址ID
	private Long memberId;// 地址ID
	private String consignee;// 收货人名称
	private String mobilephone;// 收货人手机号
	private String cityName;// 收货人所在城市
	private String address;// 收货人详细地址
	private String addressAll;// 收货人名完整地址
	private String postcode;// 邮编
	private Integer isDefault;// 是否默认0否，1是
	private String updateTime;// 上次修改时间
//	private boolean isSelect;
	
	public Address() {

	}

	public Address(Parcel source) {
		id = source.readLong();
		consignee = source.readString();
		mobilephone = source.readString();
		cityName = source.readString();
		address = source.readString();
		addressAll = source.readString();
		postcode = source.readString();
		isDefault = source.readInt();
		updateTime = source.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(consignee);
		dest.writeString(mobilephone);
		dest.writeString(cityName);
		dest.writeString(address);
		dest.writeString(addressAll);
		dest.writeString(postcode);
		dest.writeInt(isDefault);
		dest.writeString(updateTime);
	}

	public static final Parcelable.Creator<Address> CREATOR = new Creator<Address>() {
		@Override
		public Address[] newArray(int size) {
			return new Address[size];
		}

		@Override
		public Address createFromParcel(Parcel source) {
			return new Address(source);
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressAll() {
		return addressAll;
	}

	public void setAddressAll(String addressAll) {
		this.addressAll = addressAll;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
//
//	public boolean isSelect() {
//		return isSelect;
//	}
//
//	public void setSelect(boolean isSelect) {
//		this.isSelect = isSelect;
//	}

}
