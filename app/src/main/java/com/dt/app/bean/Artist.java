package com.dt.app.bean;

import java.util.List;

import com.dt.app.bean.UserWorks.Me;
import com.dt.app.bean.UserWorks.Member;

/**
 * 艺术家
 * 
 * @author libin
 * 
 */
public class Artist {

	private Page pager;
	private List<Member> members;
	private Me me;

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	public Me getMe() {
		return me;
	}

	public void setMe(Me me) {
		this.me = me;
	}

}
