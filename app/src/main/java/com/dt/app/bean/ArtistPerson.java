package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.bean.UserWorks.Me;
import com.dt.app.bean.UserWorks.UserWork;

/**
 * 艺术家个人中心
 * 
 * @author libin
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtistPerson {
	private Page pager;

	private List<UserWork> workses;

	private MemberBean member;
	private Me me;

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public List<UserWork> getWorkses() {
		return workses;
	}

	public void setWorkses(List<UserWork> workses) {
		this.workses = workses;
	}

	public MemberBean getMember() {
		return member;
	}

	public void setMember(MemberBean member) {
		this.member = member;
	}

	public Me getMe() {
		return me;
	}

	public void setMe(Me me) {
		this.me = me;
	}

}
