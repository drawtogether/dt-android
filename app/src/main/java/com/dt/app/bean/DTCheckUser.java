package com.dt.app.bean;

import java.io.Serializable;

/**
 * Created by caiyuhao on 15-9-17.
 */
public class DTCheckUser implements Serializable {
    private Integer code;
    private String  message;
    private boolean data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }
}
