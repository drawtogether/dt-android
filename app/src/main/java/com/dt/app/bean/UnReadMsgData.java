package com.dt.app.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by caiyuhao on 15-12-1.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UnReadMsgData {
    private Integer code;
    private String message;
    private UnRead data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UnRead getData() {
        return data;
    }

    public void setData(UnRead data) {
        this.data = data;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public  static class  UnRead{
            private int unreadCount;

        public int getUnreadCount() {
            return unreadCount;
        }

        public void setUnreadCount(int unreadCount) {
            this.unreadCount = unreadCount;
        }
    }
}
