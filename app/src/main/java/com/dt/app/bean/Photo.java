package com.dt.app.bean;

import java.io.Serializable;

public class Photo implements Serializable {
	private String url;
	private String desc;
	private Long id;
	public Photo(String url, Long id) {
		super();
		this.url = url;
		this.id = id;
	}
	public Photo() {
		 
	}
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
