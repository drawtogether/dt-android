package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentList {

	private Page pager;

	private List<Comment> comments;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Comment {
		private Long id;// 评论ID
		private Long worksId;// 作品ID
		private Long themeId;// 作品ID
		private String content;// 评论内容
		private String createTime;// 评论时间
		private Long memberId;// 会员标识
		private String memberLogo;// 会员头像
		private String memberNickname;// 会员昵称
		private Integer memberProvinceId;// 省份ID
		private Integer memberCityId;// 城市ID

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Long getWorksId() {
			return worksId;
		}

		public void setWorksId(Long worksId) {
			this.worksId = worksId;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		public String getCreateTime() {
			return createTime;
		}

		public void setCreateTime(String createTime) {
			this.createTime = createTime;
		}

		public Long getMemberId() {
			return memberId;
		}

		public void setMemberId(Long memberId) {
			this.memberId = memberId;
		}

		public String getMemberLogo() {
			return memberLogo;
		}

		public void setMemberLogo(String memberLogo) {
			this.memberLogo = memberLogo;
		}

		public String getMemberNickname() {
			return memberNickname;
		}

		public void setMemberNickname(String memberNickname) {
			this.memberNickname = memberNickname;
		}

		public Integer getMemberProvinceId() {
			return memberProvinceId;
		}

		public void setMemberProvinceId(Integer memberProvinceId) {
			this.memberProvinceId = memberProvinceId;
		}

		public Integer getMemberCityId() {
			return memberCityId;
		}

		public void setMemberCityId(Integer memberCityId) {
			this.memberCityId = memberCityId;
		}

		public Long getThemeId() {
			return themeId;
		}

		public void setThemeId(Long themeId) {
			this.themeId = themeId;
		}

	}

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}
