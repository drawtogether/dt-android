package com.dt.app.bean;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.UserWorks.Me;

/**
 * Created by administrator on 15-9-6.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DTUser implements Serializable {
    private Integer code;
    private UserData data;
    private String message;
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class UserData {
        private String nonce;
        private String mkey;
        private MemberBean member;

        public String getNonce() {
            return nonce;
        }

        public void setNonce(String nonce) {
            this.nonce = nonce;
        }

        public String getMkey() {
            return mkey;
        }

        public void setMkey(String mkey) {
            this.mkey = mkey;
        }

        public MemberBean getMember() {
            return member;
        }

        public void setMember(MemberBean member) {
            this.member = member;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MemberBean {
        /***
         * "id" : 17,
         * "mobilephone" : "17717031237",
         * "nickname" : "天通苑一样",
         * "gender" : 0,
         * "logo" : "http://img1.imgtn.bdimg.com/it/u=1969984714,3754317325&fm=21&gp=0.jpg",
         * "registerTime" : "2015-09-11 00:32:51",
         * "isSigned" : 0,
         * "followingCount" : 0,
         * "followersCount" : 0,
         * "worksCount" : 0,
         * "likeCount" : 0,
         * "donationCount" : 0,
         * "commentCount" : 0
         */
        private Long id;
        private String mobilephone;
        private String nickname;
        private String logo;
        private String registerTime;
        private Integer gender;
        private Integer isSigned;
        private Integer followingCount=0;
        private Integer followersCount=0;
        private Integer worksCount=0;
        private Integer likeCount=0;
        private Integer donationCount=0;
        private Integer commentCount=0;
        private Integer jifenAvailable=0;
        private String username;
        private String location;
 
        private String email;//email
        private Integer provinceId;//省份id
        private Integer cityId;//城市id
        private String signature;//签名
 

        private Me me;
        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }


        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Integer getProvinceId() {
			return provinceId;
		}

		public void setProvinceId(Integer provinceId) {
			this.provinceId = provinceId;
		}

		public Integer getCityId() {
			return cityId;
		}

		public void setCityId(Integer cityId) {
			this.cityId = cityId;
		}

		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}

		public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getMobilephone() {
            return mobilephone;
        }

        public void setMobilephone(String mobilephone) {
            this.mobilephone = mobilephone;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getRegisterTime() {
            return registerTime;
        }

        public void setRegisterTime(String registerTime) {
            this.registerTime = registerTime;
        }

        public Integer getGender() {
            return gender;
        }

        public void setGender(Integer gender) {
            this.gender = gender;
        }

        public Integer getIsSigned() {
            return isSigned;
        }

        public void setIsSigned(Integer isSigned) {
            this.isSigned = isSigned;
        }

        public Integer getFollowingCount() {
            return followingCount;
        }

        public void setFollowingCount(Integer followingCount) {
            this.followingCount = followingCount;
        }

        public Integer getFollowersCount() {
            return followersCount;
        }

        public void setFollowersCount(Integer followersCount) {
            this.followersCount = followersCount;
        }

        public Integer getWorksCount() {
            return worksCount;
        }

        public void setWorksCount(Integer worksCount) {
            this.worksCount = worksCount;
        }

        public Integer getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(Integer likeCount) {
            this.likeCount = likeCount;
        }

        public Integer getDonationCount() {
            return donationCount;
        }

        public void setDonationCount(Integer donationCount) {
            this.donationCount = donationCount;
        }

        public Integer getCommentCount() {
            return commentCount;
        }

        public void setCommentCount(Integer commentCount) {
            this.commentCount = commentCount;
        }

		public Me getMe() {
			return me;
		}

		public void setMe(Me me) {
			this.me = me;
		}

		public Integer getJifenAvailable() {
			return jifenAvailable;
		}

		public void setJifenAvailable(Integer jifenAvailable) {
			this.jifenAvailable = jifenAvailable;
		}
    }
}
