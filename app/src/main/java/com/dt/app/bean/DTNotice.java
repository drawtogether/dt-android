package com.dt.app.bean;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by caiyuhao on 15-9-15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DTNotice implements Serializable {

    private Integer code;//1成功，0失败
    private String message;//提示信息

    private DataBean  data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public static class DataBean{
        private PagerBean pager;
        private List<NoticeBean> notices;//查询结果集合，查看完整结构
        public PagerBean getPager() {
            return pager;
        }

        public void setPager(PagerBean pager) {
            this.pager = pager;
        }
        public List<NoticeBean> getNotices() {
            return notices;
        }

        public void setNotices(List<NoticeBean> notices) {
            this.notices = notices;
        }
    }
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class NoticeBean {
        private Long id;//自增主键
        private Long fromId;//0 表示系统，>0表示用户
        private String fromName;//发起通知者用户名 or DT
        private String fromLogo;//用户Logo/DT LOGO
        private String location;//地理位置：上海-上海
        private String message;//通知内容
        private String contentType;//业务对象类型：works 表示作品
        private Long contentId;//业务对象Id
        private String contentLogo;//业务对象图
        private String occurTime;//发生时间
        private Integer isRead;//是否已读0否，1是
        private long memberId;

        public long getMemberId() {
            return memberId;
        }

        public void setMemberId(long memberId) {
            this.memberId = memberId;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getFromId() {
            return fromId;
        }

        public void setFromId(Long fromId) {
            this.fromId = fromId;
        }

        public String getFromName() {
            return fromName;
        }

        public void setFromName(String fromName) {
            this.fromName = fromName;
        }

        public String getFromLogo() {
            return fromLogo;
        }

        public void setFromLogo(String fromLogo) {
            this.fromLogo = fromLogo;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getContentType() {
            return contentType;
        }

        public void setContentType(String contentType) {
            this.contentType = contentType;
        }

        public Long getContentId() {
            return contentId;
        }

        public void setContentId(Long contentId) {
            this.contentId = contentId;
        }

        public String getContentLogo() {
            return contentLogo;
        }

        public void setContentLogo(String contentLogo) {
            this.contentLogo = contentLogo;
        }

        public String getOccurTime() {
            return occurTime;
        }

        public void setOccurTime(String occurTime) {
            this.occurTime = occurTime;
        }

        public Integer getIsRead() {
            return isRead;
        }

        public void setIsRead(Integer isRead) {
            this.isRead = isRead;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PagerBean {
        //分页对象
        private Integer pageSize;//页面显示条数
        private Integer currentPage;//当前页数
        private Integer totalCount;//总数据条数
        private Integer totalPage;//总页数
        private Integer rows;
        private Integer currentCount;
        private Integer page;

        public Integer getRows() {
            return rows;
        }

        public void setRows(Integer rows) {
            this.rows = rows;
        }

        public Integer getCurrentCount() {
            return currentCount;
        }

        public void setCurrentCount(Integer currentCount) {
            this.currentCount = currentCount;
        }

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        public Integer getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        public Integer getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(Integer totalPage) {
            this.totalPage = totalPage;
        }
    }
}
