package com.dt.app.bean;

/**
 * Created by caiyuhao on 15-11-5.
 */
public class DTUpload {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
