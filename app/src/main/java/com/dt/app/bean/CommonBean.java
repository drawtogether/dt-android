package com.dt.app.bean;

public class CommonBean {
	private Integer  rsCode;
	public int id;
	public String name;
	public int image;
	public String desc;
	public boolean close;
	public boolean click;
	private Integer versionCode;
	private String versionName;
	private String url_apk;
	private String content;
	private String first;
	public CommonBean(){
		
	}
	public CommonBean(int id, String name,boolean close,boolean click){
		super();
		this.id = id;
		this.name = name;
		this.close = close;
		this.click =click;
	}
	public CommonBean(int id, String name, int image, String desc) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.desc = desc;
	}

	
	public Integer getRsCode() {
		return rsCode;
	}
	public void setRsCode(Integer rsCode) {
		this.rsCode = rsCode;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getImage() {
		return image;
	}
	public void setImage(int image) {
		this.image = image;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public boolean isClose() {
		return close;
	}
	public void setClose(boolean close) {
		this.close = close;
	}
	public boolean isClick() {
		return click;
	}
	public void setClick(boolean click) {
		this.click = click;
	}
	public Integer getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(Integer versionCode) {
		this.versionCode = versionCode;
	}
	public String getVersionName() {
		return versionName;
	}
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	public String getUrl_apk() {
		return url_apk;
	}
	public void setUrl_apk(String url_apk) {
		this.url_apk = url_apk;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getFirst() {
		return first;
	}
	public void setFirst(String first) {
		this.first = first;
	}
	
	
}
