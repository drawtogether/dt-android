package com.dt.app.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.Picture;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JifenGood {

	private Member member;
	private Page pager;
	private List<Goods> goodses;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Goods {
		private Long id;// " : 40,
		private String name;// " : "Magenet数据充电线",
		private String goodsThumb;// " : "http://artist7.cn/Data/Uploads/2015-05-28/5566f632b511c.jpg",
		private Integer jifen;// " : 499
		private List<Picture> pictures;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getGoodsThumb() {
			return goodsThumb;
		}

		public void setGoodsThumb(String goodsThumb) {
			this.goodsThumb = goodsThumb;
		}

		public Integer getJifen() {
			return jifen;
		}

		public void setJifen(Integer jifen) {
			this.jifen = jifen;
		}

		public List<Picture> getPictures() {
			return pictures;
		}

		public void setPictures(List<Picture> pictures) {
			this.pictures = pictures;
		}

	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Page getPager() {
		return pager;
	}

	public void setPager(Page pager) {
		this.pager = pager;
	}

	public List<Goods> getGoodses() {
		return goodses;
	}

	public void setGoodses(List<Goods> goodses) {
		this.goodses = goodses;
	}

}
