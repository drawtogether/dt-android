package com.dt.app;

import android.app.Activity;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.dt.app.adapter.FragmentAdapter;
import com.dt.app.autoupdate.CheckUpdate;
import com.dt.app.base.BaseFragmentActivity;
import com.dt.app.fragment.ArtGalleryFragment;
import com.dt.app.fragment.MainFragment;
import com.dt.app.fragment.PointsExchangeFragment;
import com.dt.app.receiver.CommentReceiver;
import com.dt.app.receiver.CommentReceiver.CommentReceverIntf;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.CustomDrawerLayout;
import com.dt.app.widget.CommonSlide;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends BaseFragmentActivity {
    private ViewPager mPageVp;
    private List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private FragmentAdapter mFragmentAdapter;
    public CustomDrawerLayout drawerlayout;
    /**
     * Fragment
     */
    private MainFragment mainFragment;
    /**
     * ViewPager的当前选中页
     */
     private int currentIndex;
     /**
     * 屏幕的宽度
     */
    private long exitTime = 0;
    private CommentReceiver mCommentReceiver;
    private ArtGalleryFragment artgalleryFragment;
    private PointsExchangeFragment pointsExchangeFragment;

    private int countArtGallery = 0;
    private int countPointsExchange = 0;
    public static Activity activity;
    int index = 0;
    private Handler mhandler;
    private Timer mTimer;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_main);
        ViewUtils.inject(this);
        activity = this;
        findById();
        init();
    }

    private void findById() {

        artgalleryFragment = (ArtGalleryFragment) getSupportFragmentManager().getFragments().get(0);
        pointsExchangeFragment = (PointsExchangeFragment) getSupportFragmentManager().getFragments().get(1);
        mCommentReceiver = new CommentReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(CommentReceiver.SEND_RECEIVER_SUCCES);
        registerReceiver(mCommentReceiver, filter);
        mCommentReceiver.setCommentReceverIntf(new CommentReceverIntf() {
            @Override
            public void success(long workid, String type) {
                artgalleryFragment.success(workid, type);
            }
        });

        mPageVp = (ViewPager) this.findViewById(R.id.id_page_vp);
        drawerlayout = (CustomDrawerLayout) this.findViewById(R.id.drawerlayout);

       /* CommonSlide commonSlide = new CommonSlide(this, 0, null);
        commonSlide.loadListener();*/
        CheckUpdate.getInstance(this).checkUpdate();
    }


    private void init() {
        mainFragment = new MainFragment();
        mFragmentList.add(mainFragment);

        mFragmentAdapter = new FragmentAdapter(
                this.getSupportFragmentManager(), mFragmentList);
        mPageVp.setAdapter(mFragmentAdapter);
        mPageVp.setCurrentItem(0);

        mPageVp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            /**
             * state滑动中的状态 有三种状态（0，1，2） 1：正在滑动 2：滑动完毕 0：什么都没做。
             */
            @Override
            public void onPageScrollStateChanged(int state) {

            }

            /**
             * position :当前页面，及你点击滑动的页面 offset:当前页面偏移的百分比
             * offsetPixels:当前页面偏移的像素位置
             */
            @Override
            public void onPageScrolled(int position, float offset,
                                       int offsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentIndex = position;

            }
        });

        drawerlayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(final View view) {

                mTimer = new Timer();
                mTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        index++;
                        Message message = new Message();
                        message.what = index;
                        mhandler.sendMessage(message);
                    }
                }, 100, 30);

                mhandler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (drawerlayout.isDrawerOpen(Gravity.RIGHT)) {
                            pointsExchangeFragment.progress_dt.setProgress(msg.what);
                        } else {
                            artgalleryFragment.progress_dt.setProgress(msg.what);
                        }
                        //LogUtils.e("------main------>" + msg.what);
                        if (index >= 100) {
                            if (drawerlayout.isDrawerOpen(Gravity.RIGHT)) {
                                if (countPointsExchange <= 1) {
                                    pointsExchangeFragment.ll_top_layer.setVisibility(View.GONE);
                                    countPointsExchange = 3;
                                }
                            } else {
                                if (countArtGallery <= 1) {
                                    artgalleryFragment.ll_top_layer.setVisibility(View.GONE);

                                    countArtGallery = 3;
                                }
                            }
                            mTimer.cancel();
                            index = 0;
                            mhandler = null;
                        } else {
                            if (drawerlayout.isDrawerOpen(Gravity.RIGHT)) {
                                if (countPointsExchange > 1) {
                                    mTimer.cancel();
                                    index = 0;
                                    mhandler = null;
                                }
                            } else {
                                if (countArtGallery > 1) {
                                    mTimer.cancel();
                                    index = 0;
                                    mhandler = null;
                                }
                            }

                        }
                    }
                };
            }

            @Override
            public void onDrawerClosed(View view) {

            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
                exitApp();
            }
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

    /**
     * 退出程序
     */
    private void exitApp() {
        //判断2次点击事件
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            ToastUtils.showTextToast(MainActivity.this, "别按啦  ， 再按就要离开我啦！");
            exitTime = System.currentTimeMillis();
        } else {
            DTApplication.getInstance().appExit(this);
        }
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mCommentReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
