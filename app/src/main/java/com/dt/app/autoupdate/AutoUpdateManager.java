package com.dt.app.autoupdate;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.dt.app.bean.ZTUploadFIle;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ToastUtils;
import com.qiniu.android.utils.StringUtils;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/*
 * 
 * 这个版本更新 是在界面弹出一个对话框 ，并显示更新进度
 * 
 * 
 */
public class AutoUpdateManager {

    private String curVersion;
    private int curCode;
    private String newVersion;
    private Integer hasNewV;
    private int newCode;
    private String updateInfo;
    private UpdateCallback callback;
    private Activity activity;

    private int progress;
    private Boolean hasNewVersion;
    private Boolean canceled;
    private boolean forcedUpdate = false;

    //
    private final String UPDATE_CHECKURL = Constant.URL.DTcheckVersion;
    private String update_apkName;
    private String update_saveName;
    // 存放更新APK文件的路径
    private String update_download_url;

    public final int NETWORK_ERROR = 0;
    public static final int UPDATE_CHECKCOMPLETED = 1;
    public static final int UPDATE_DOWNLOADING = 2;
    public static final int UPDATE_DOWNLOAD_ERROR = 3;
    public static final int UPDATE_DOWNLOAD_COMPLETED = 4;
    public static final int UPDATE_DOWNLOAD_CANCELED = 5;
    public static final int UPDATE_NO_SDCARD = 6;

    // 从服务器上下载apk存放文件夹
    private String savefolder = Environment.getExternalStorageDirectory()
            .getAbsolutePath();// +FileUtils.FILE_LIST;

    public AutoUpdateManager(Activity activity, UpdateCallback updateCallback) {
        this.activity = activity;
        callback = updateCallback;
        canceled = false;
        getCurVersion();
    }

    public String getNewVersionName() {
        return newVersion;
    }

    public String getUpdateInfo() {
        return updateInfo;
    }

    private void getCurVersion() {
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(), 0);
            curVersion = pInfo.versionName;
            curCode = pInfo.versionCode;
        } catch (NameNotFoundException e) {
            Log.e("update", e.getMessage());
            curVersion = "1.1.1000";
            curCode = 1;
        }

    }

    public void checkUpdate() {
        hasNewVersion = false;
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("clientType", "Android");
        map.put("v", curVersion);
        try {
            RequestApi.commonRequest(activity, UPDATE_CHECKURL, map,
                    new ResultLinstener<ZTUploadFIle>() {
                        @Override
                        public void onSuccess(ZTUploadFIle obj) {

                            if (obj == null) {
                                updateHandler.sendEmptyMessage(NETWORK_ERROR);
                                return;
                            }
                            try {

                                hasNewV = obj.getData().getHasNewV();
                                if (hasNewV == 0) {
                                    //ToastUtils.showTextToast(activity,"当前为最新版本");
                                    return;
                                }
                                //newCode = Integer.valueOf(newVersion.replaceAll("\\.", ""));

                                // forcedUpdate = true;//
                                // dataBean.getForce_update();
                                ZTUploadFIle.NewAppVersion newAppversion = obj.getData().getNewv();
                                update_download_url = newAppversion.getUrl();
                                updateInfo = newAppversion.getReleaseNote();
                                newVersion = "最新"+newAppversion.getVersion();
                                update_saveName = getApkName(update_download_url);
                                Log.i("newVerName", newVersion + "");
                                // 根据版本号来比较的
                                if (hasNewV == 1) {
                                    hasNewVersion = true;
                                } else {
                                    hasNewVersion = false;
                                }

                            } catch (Exception e) {
                                newVersion = "";
                                newCode = 0;
                                updateInfo = "";
                                update_download_url = "";
                            }
                            updateHandler
                                    .sendEmptyMessage(UPDATE_CHECKCOMPLETED);
                        }

                        @Override
                        public void onFailure(String obj) {
                            updateHandler.sendEmptyMessage(NETWORK_ERROR);
                        }

                        @Override
                        public void onException(String exception) {
                            updateHandler.sendEmptyMessage(NETWORK_ERROR);
                        }

                    }, new ZTUploadFIle());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void update() {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(
                Uri.fromFile(new File(savefolder, update_saveName)),
                "application/vnd.android.package-archive");
        activity.startActivity(intent);
    }

    private String getApkName(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    public void downloadPackage() {

        new Thread() {
            @Override
            public void run() {
                try {
                    URL url = new URL(update_download_url);

                    HttpURLConnection conn = (HttpURLConnection) url
                            .openConnection();
                    conn.connect();
                    int length = conn.getContentLength();
                    InputStream is = conn.getInputStream();
                    if (Environment.getExternalStorageState().equals(
                            Environment.MEDIA_MOUNTED)) {
                        File destDir = new File(savefolder);
                        if (!destDir.exists()) {
                            destDir.mkdirs();
                        }
                        File ApkFile = new File(savefolder, update_saveName);
                        if (ApkFile.exists()) {
                            ApkFile.delete();
                        }
                        FileOutputStream fos = new FileOutputStream(ApkFile);
                        int count = 0;
                        byte buf[] = new byte[512];
                        do {

                            int numread = is.read(buf);
                            count += numread;
                            progress = (int) (((float) count / length) * 100);

                            updateHandler.sendMessage(updateHandler
                                    .obtainMessage(UPDATE_DOWNLOADING));
                            if (numread <= 0) {
                                updateHandler
                                        .sendEmptyMessage(UPDATE_DOWNLOAD_COMPLETED);
                                break;
                            }
                            fos.write(buf, 0, numread);
                        } while (!canceled);
                        if (canceled) {
                            updateHandler
                                    .sendEmptyMessage(UPDATE_DOWNLOAD_CANCELED);
                        }
                        fos.close();
                        is.close();

                    } else {
                        updateHandler.sendMessage(updateHandler.obtainMessage(
                                UPDATE_NO_SDCARD, "没有找到内存卡！"));
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    updateHandler.sendMessage(updateHandler.obtainMessage(
                            UPDATE_DOWNLOAD_ERROR, e.getMessage()));
                } catch (IOException e) {
                    e.printStackTrace();
                    updateHandler.sendMessage(updateHandler.obtainMessage(
                            UPDATE_DOWNLOAD_ERROR, e.getMessage()));
                }
            }
        }.start();
    }

    public void cancelDownload() {
        canceled = true;
    }

    Handler updateHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case NETWORK_ERROR:
                    callback.networkError();
                    break;
                case UPDATE_CHECKCOMPLETED:
                    callback.checkUpdateCompleted(hasNewVersion, newVersion,
                            updateInfo, update_download_url, forcedUpdate);
                    break;
                case UPDATE_DOWNLOADING:

                    callback.downloadProgressChanged(progress);
                    break;
                case UPDATE_DOWNLOAD_ERROR:

                    callback.downloadCompleted(false, msg.obj.toString(),
                            UPDATE_DOWNLOAD_ERROR);
                    break;
                case UPDATE_DOWNLOAD_COMPLETED:

                    callback.downloadCompleted(true, "", UPDATE_DOWNLOAD_COMPLETED);
                    break;
                case UPDATE_DOWNLOAD_CANCELED:

                    callback.downloadCanceled();
                    break;
                case UPDATE_NO_SDCARD:
                    callback.downloadCompleted(false, msg.obj.toString(),
                            UPDATE_NO_SDCARD);
                    break;
                default:
                    break;
            }
        }
    };

    public interface UpdateCallback {
        public void networkError();

        public void checkUpdateCompleted(Boolean hasUpdate,
                                         CharSequence updateInfo, String detail, String url,
                                         boolean forcedUpdate);

        public void downloadProgressChanged(int progress);

        public void downloadCanceled();

        public void downloadCompleted(Boolean sucess, CharSequence errorMsg,
                                      int errorcode);
    }

}
