package com.dt.app.autoupdate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.text.TextUtils;
import com.dt.app.R;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.CustomAlertDialog;


public class CheckUpdate {
	// ///////////////////////////////自动更新////////////////////////////////
	private AutoUpdateManager updateMan = null;
	private ProgressDialog updateProgressDialog;
	private ProgressDialog pdialog = null;
	private static Activity activity = null;

	private static CheckUpdate checkUpdate = null;

	public static CheckUpdate getInstance(Activity act) {
		activity = act;
		if (checkUpdate == null) {
			checkUpdate = new CheckUpdate();
		}
		return checkUpdate;
	}

	public void checkUpdate() {
		// 表示从设置中点击的更新才弹出检测更新进度条
		/*if (activity instanceof SetActivity) {
			showProgress("正在检查更新...");
		}*/
		// pdialog = showProgressDialog2(activity, "正在检查更新...");
		// 检查是否有更新
		// 如果有更新提示下载
		updateMan = new AutoUpdateManager(activity, appUpdateCb);
		updateMan.checkUpdate();
	}

	/**
	 * 显示加载进度条
	 */
	private void showProgress(String message) {
		if (activity == null) {
			return;
		}
		if (pdialog == null) {
			pdialog = new ProgressDialog(activity);
			pdialog.setMessage(message);
			pdialog.show();
		} else if (pdialog.isShowing()) {
			pdialog.dismiss();
		}

	}

	/**
	 * 停止加载进度
	 */
	private void stopProgress() {
		if (pdialog != null && pdialog.isShowing()) {
			pdialog.dismiss();
		}
	}

	// 自动更新回调函数
	private AutoUpdateManager.UpdateCallback appUpdateCb = new AutoUpdateManager.UpdateCallback() {

		public void downloadProgressChanged(int progress) {
			if (updateProgressDialog != null
					&& updateProgressDialog.isShowing()) {
				updateProgressDialog.setProgress(progress);
			}

		}

		public void downloadCompleted(Boolean sucess, CharSequence errorMsg,
				int errorcode) {
			if (updateProgressDialog != null
					&& updateProgressDialog.isShowing()) {
				updateProgressDialog.dismiss();
			}
			if (sucess) {
				updateMan.update();
			} else {
				if (errorcode == AutoUpdateManager.UPDATE_DOWNLOAD_ERROR) {
					new CustomAlertDialog.Builder(activity)
							.setTitle("提示")
							.setMessage("更新失败，是否重试？")
							.setPositiveButton("重试",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											updateProgressDialog.setProgress(0);
											updateProgressDialog.show();
											updateMan.downloadPackage();
										}
									})
							.setNegativeButton("稍后再说",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {

										}
									}).create().show();
				} else if (errorcode == AutoUpdateManager.UPDATE_NO_SDCARD) {
					new CustomAlertDialog.Builder(activity)
							.setTitle("提示")
							.setMessage("无法下载，没有找到内存卡！")
							.setPositiveButton("确定",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {

										}
									}).create().show();
				}

			}
		}

		public void downloadCanceled() {

		}

		public void checkUpdateCompleted(Boolean hasUpdate,
				CharSequence updateInfo, String detail, final String url,
				boolean forcedUpdate) {
			stopProgress();
			if (hasUpdate) {
				if (TextUtils.isEmpty(detail)) {
					detail = "无更新详情";
				}
				CustomAlertDialog.Builder builder = new CustomAlertDialog.Builder(
						activity);
				builder.setTitle("提示");
				builder.setMessage(activity.getString(R.string.app_name)
						+ updateInfo + "版\n" + detail);
				builder.setPositiveButton("立即更新",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// 前台更新
								updateProgressDialog = new ProgressDialog(
										activity);
								updateProgressDialog.setMessage("正在下载...");
								updateProgressDialog.setIndeterminate(false);
								updateProgressDialog
										.setCanceledOnTouchOutside(false);
								updateProgressDialog.setCancelable(false);
								updateProgressDialog
										.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
								updateProgressDialog.setMax(100);
								updateProgressDialog.setProgress(0);
								updateProgressDialog.show();

								updateMan.downloadPackage();
								// 使用service进行后台更新
								// Intent updateIntent =new
								// Intent(SetActivity.this,
								// AppUpgradeService.class);
								// updateIntent.putExtra("downloadUrl",url);
								// startService(updateIntent);
							}
						});
				if (!forcedUpdate) {// 非强制更新
					builder.setNegativeButton("稍后再说",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

								}
							});
				}

				builder.show();
			}
		}

		@Override
		public void networkError() {
			stopProgress();
		}
	};
}
