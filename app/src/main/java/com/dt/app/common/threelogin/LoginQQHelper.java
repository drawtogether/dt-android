package com.dt.app.common.threelogin;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.lidroid.xutils.util.LogUtils;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners;
import com.umeng.socialize.exception.SocializeException;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import org.json.JSONObject;

import android.app.Activity;
import android.text.TextUtils;

import com.dt.app.common.DTFactoryApi;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ToastUtils;
import com.tencent.connect.UserInfo;
import com.tencent.connect.common.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import java.util.Map;
import java.util.Set;

/**
 * Created by caiyuhao on 15-9-12.
 */
public class LoginQQHelper {
    //qq
    private Activity activity;
    UMSocialService mController;

    public LoginQQHelper(Activity mActivity, UMSocialService controller) {
        activity = mActivity;
        mController = controller;

        UMQQSsoHandler qqSsoHandler = new UMQQSsoHandler(mActivity, "1104852202",
                "2WcHXZkmCxGFYMlb");
        qqSsoHandler.addToSocialSDK();

        UMWXHandler wxHandler = new UMWXHandler(activity, Constant.wechat_APP_ID, Constant.wechat_APP_secrete);
        wxHandler.addToSocialSDK();


    }

    /**
     * update user
     */
    public void updateUserInfo(final SHARE_MEDIA pt) {
        mController.doOauthVerify(activity, pt, new SocializeListeners.UMAuthListener() {
            @Override
            public void onStart(SHARE_MEDIA platform) {
                ToastUtils.showDtDialog(activity);
            }

            @Override
            public void onError(SocializeException e, SHARE_MEDIA platform) {
                //Toast.makeText(activity, "授权错误", Toast.LENGTH_SHORT).show();
                //LogUtils.e("---------SocializeException------->"+e.getMessage());
                ToastUtils.cancelProgressDt();
            }

            @Override
            public void onComplete(Bundle value, SHARE_MEDIA platform) {
                // Toast.makeText(activity, "授权完成", Toast.LENGTH_SHORT).show();
                ToastUtils.cancelProgressDt();
                //LogUtils.e("-------Bundle------>"+value.toString());
                final   String openid = value.getString("openid");
                //LogUtils.e("-------openid------>"+openid);
                //获取相关授权信息
                mController.getPlatformInfo(activity, pt, new SocializeListeners.UMDataListener() {
                    @Override
                    public void onStart() {
                        // Toast.makeText(activity, "获取平台数据开始...", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete(int status, Map<String, Object> info ) {
                        if (status == 200 && info != null) {
                            StringBuilder sb = new StringBuilder();
                            Set<String> keys = info.keySet();
                            for(String key : keys){
                                sb.append(key+"="+info.get(key).toString()+"\r\n");
                            }

                           // LogUtils.e("--------fuck-->"+sb);
                            if (pt.equals(SHARE_MEDIA.QQ)) {
                                Integer gender = info.get("gender").equals("男") ? 1 : 2;
                                DTFactoryApi.qqData(activity, (String) info.get("screen_name"), gender, (String) info.get("profile_image_url"),"qq",openid);

                            } else if (pt.equals(SHARE_MEDIA.WEIXIN)) {
                                Integer gender = info.get("sex").equals("1") ? 1 : 2;
                                DTFactoryApi.qqData(activity, (String) info.get("nickname"), gender, (String) info.get("headimgurl"),"weixin",openid);
                            }
                        }
                    }
                });
            }

            @Override
            public void onCancel(SHARE_MEDIA platform) {
                // Toast.makeText(activity, "授权取消", Toast.LENGTH_SHORT).show();
                ToastUtils.cancelProgressDt();
            }
        });
    }

}
