package com.dt.app.common.threelogin;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.dt.app.utils.Constant;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.umeng.socialize.bean.RequestType;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;

/**
 * Created by caiyuhao on 15-11-4.
 */
public class DTShare {
//    private Activity activity;
//    UMSocialService mController;
//
//    public DTShare(Activity act) {
//        activity = act;
//        mController = UMServiceFactory.getUMSocialService("com.umeng.share");
//        mController.getConfig().removePlatform(SHARE_MEDIA.EMAIL, SHARE_MEDIA.EVERNOTE,
//                SHARE_MEDIA.FACEBOOK, SHARE_MEDIA.FLICKR,
//                SHARE_MEDIA.FOURSQUARE, SHARE_MEDIA.QQ
//                , SHARE_MEDIA.QZONE, SHARE_MEDIA.SINA, SHARE_MEDIA.TENCENT, SHARE_MEDIA.RENREN, SHARE_MEDIA.DOUBAN);
//
//
//        // 添加微信平台
//        UMWXHandler wxHandler = new UMWXHandler(activity, Constant.wechat_APP_ID, Constant.wechat_APP_secrete);
//        wxHandler.addToSocialSDK();
//        // 支持微信朋友圈
//        UMWXHandler wxCircleHandler = new UMWXHandler(activity, Constant.wechat_APP_ID, Constant.wechat_APP_secrete);
//        wxCircleHandler.setToCircle(true);
//        wxCircleHandler.addToSocialSDK();
//    }
//
//    public void openShare(String... args) {
//        mController.openShare(activity, false);
//
//        //设置微信好友分享内容
//        WeiXinShareContent weixinContent = new WeiXinShareContent();
//        //设置title
//        weixinContent.setTitle(args[0]);
//        //设置分享文字
//        weixinContent.setShareContent(args[1]);
//        //设置分享内容跳转URL
//        weixinContent.setTargetUrl(args[2]);
//        //设置分享图片
//        // weixinContent.setShareImage("");
//        mController.setShareMedia(weixinContent);
//
//
//        //设置微信朋友圈分享内容
//      /*  CircleShareContent circleMedia = new CircleShareContent();
//        circleMedia.setShareContent("来自友盟社会化组件（SDK）让移动应用快速整合社交分享功能，朋友圈");
//        //设置朋友圈title
//        circleMedia.setTitle("友盟社会化分享组件-朋友圈");
//        circleMedia.setShareImage(localImage);
//        circleMedia.setTargetUrl("你的URL链接");
//        mController.setShareMedia(circleMedia);*/
//    }
	
 
 
	private Activity context = null;
	private UMSocialService mController = null;
	private Bitmap bitmapCode = null;
	private IWXAPI wxApi;

	private String pTitle = "DT";
	private String pContent = " ";
	private String pUrl = " ";
	private String downUrl = " ";

 

	public DTShare(Activity context) {
		this.context = context;
		// 实例化
		wxApi = WXAPIFactory.createWXAPI(context, Constant.wechat_APP_ID);
		wxApi.registerApp(Constant.wechat_APP_ID);
		// 首先在您的Activity中添加如下成员变量
		mController = UMServiceFactory.getUMSocialService("com.umeng.share",
				RequestType.SOCIAL);
		 mController.getConfig().removePlatform(SHARE_MEDIA.EMAIL, SHARE_MEDIA.EVERNOTE,
               SHARE_MEDIA.FACEBOOK, SHARE_MEDIA.FLICKR,
               SHARE_MEDIA.FOURSQUARE, 
                 SHARE_MEDIA.QZONE, SHARE_MEDIA.SINA, SHARE_MEDIA.TENCENT, SHARE_MEDIA.RENREN, SHARE_MEDIA.DOUBAN);
	}

	public UMSocialService getController() {
		return mController;
	}

	public void showShareUI(String title, String shareContent,
			String targetUrl, String netPic, String type) {

		// 设置分享内容
		if (TextUtils.isEmpty(title)) {
			title = pTitle;
		}
		if (TextUtils.isEmpty(shareContent)) {
			shareContent = pContent;
		}
		if (TextUtils.isEmpty(targetUrl)) {
			targetUrl = pUrl;
		}

		mController.setShareContent(shareContent);
//		try {
//			bitmapCode = IEncodingHandler.gerateLogoCode(context, downUrl, 300);
//			// bitmapCode =
//			// EncodingHandler.createQRCode(Const.URL.URL_SERVER_NEW, 200);
//		} catch (WriterException e) {
//			bitmapCode = null;
//		}

		addWXShare(title, shareContent, targetUrl, netPic, type);
		addFriendCircleShare(title, shareContent, targetUrl,netPic,type);
		addQQContent(title, shareContent, targetUrl,netPic,type);

		// 设置新浪SSO handler
//		mController.getConfig().setSsoHandler(new SinaSsoHandler());
		// 设置腾讯微博SSO handler
//		mController.getConfig().setSsoHandler(new TencentWBSsoHandler());

		UMQQSsoHandler qqSsoHandler = new UMQQSsoHandler(context, "1104852202",
				"2WcHXZkmCxGFYMlb");
		qqSsoHandler.addToSocialSDK();  

		mController.getConfig().removePlatform(SHARE_MEDIA.RENREN,
				SHARE_MEDIA.DOUBAN);
		mController.getConfig()
				.setPlatformOrder(SHARE_MEDIA.TENCENT, SHARE_MEDIA.WEIXIN,
						SHARE_MEDIA.SINA, SHARE_MEDIA.WEIXIN_CIRCLE);
		mController.setShareImage(new UMImage(context, netPic));

		mController.openShare(context, false);

	}

 

	/**
	 * 微信
	 */
	private void addWXShare(String title, String shareContent, String url,
			String netPic, String type) {
		// wx967daebe835fbeac是你在微信开发平台注册应用的AppID, 这里需要替换成你注册的AppID
		// 添加微信平台
		UMWXHandler wxHandler = new UMWXHandler(context, Constant.wechat_APP_ID, Constant.wechat_APP_secrete);
		wxHandler.addToSocialSDK();

		// 设置微信好友分享内容
		WeiXinShareContent weixinContent = new WeiXinShareContent();
		// 设置title
		weixinContent.setTitle(title);
		// 设置分享文字
		weixinContent.setShareContent(shareContent);
		// 设置分享内容跳转URL
		weixinContent.setTargetUrl(url);
		// 设置分享图片
		if (type.equals("1")) {
			weixinContent.setShareImage(new UMImage(context, bitmapCode));
		}
		if (type.equals("0")) {

			weixinContent.setShareImage(new UMImage(context, netPic));
		}
		mController.setShareMedia(weixinContent);
	}

	/**
	 * 微信朋友圈
	 */
	private void addFriendCircleShare(String title, String shareContent,
			String url, String netPic,String type) {
		// String appID = "wx06c10cd07a5ac0e9";
		// 支持微信朋友圈
		UMWXHandler wxCircleHandler = new UMWXHandler(context,Constant.wechat_APP_ID, Constant.wechat_APP_secrete);
		wxCircleHandler.setToCircle(true);
		wxCircleHandler.addToSocialSDK();

		// 设置微信好友分享内容
		CircleShareContent circleMedia = new CircleShareContent();
		// 设置title
		circleMedia.setTitle(title);
		// 设置分享文字
		circleMedia.setShareContent(shareContent);
		// 设置分享内容跳转URL
		circleMedia.setTargetUrl(url);
		if (type.equals("1")) {
			circleMedia.setShareImage(new UMImage(context, bitmapCode));
		}
		if (type.equals("0")) {

			circleMedia.setShareImage(new UMImage(context, netPic));
		}
//		// 设置分享图片
//		circleMedia.setShareImage(new UMImage(context, bitmapCode));
		mController.setShareMedia(circleMedia);
	}
 
    private void addQQContent(String title, String shareContent,
							  String url, String netPic,String type){
		QQShareContent qqShareContent = new QQShareContent();
		qqShareContent.setShareContent(shareContent);
		qqShareContent.setTitle(title);
		qqShareContent.setShareImage(new UMImage(context, netPic));
		qqShareContent.setTargetUrl(url);
		mController.setShareMedia(qqShareContent);

	}
}
