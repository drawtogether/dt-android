package com.dt.app.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;

import com.dt.app.bean.Tag;
import com.dt.app.listener.HandleCallback;
import com.dt.app.listener.HandleCallbackSimple;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;

public class CommonApis {
	
 
	/**
	 * 获取标签
	 * @param activity
	 * @param data
	 * @param callbackSimple
	 */
	public static void getTags(Activity activity,Map<String, Object> data,final HandleCallbackSimple<Tag> callbackSimple){
		try {
			RequestApi.postCommon_List(activity, Constant.URL.DTTagTopn, data, new ResultLinstener<List<Tag>>() {

				@Override
				public void onSuccess(List<Tag> obj) {
					if (callbackSimple!=null) {
						callbackSimple.onSuccess(obj);
					}
				}

				@Override
				public void onFailure(String obj) {
					
				}

				@Override
				public void onException(String exception) {
					
				}
			}, new Tag());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 设置用户积分
	 * @param context
	 * @param jifen 
	 * @param append 是否追加，false:覆盖原来的值，true：累加
	 */
	public static void setUserJiFen(Context context,int jifen,boolean append){
		if(append){
			int cur = PreferencesUtils.getInt(context, Constant.PrefrencesPt.DTjifenAvailable);
			cur = cur + jifen;
			if (cur < 0) {
				PreferencesUtils.putInt(context, Constant.PrefrencesPt.DTjifenAvailable, 0);
			} else {
				PreferencesUtils.putInt(context, Constant.PrefrencesPt.DTjifenAvailable, cur);
			}
			System.out.println("--setUserJiFen---"+cur+" "+jifen);
		}else {
			PreferencesUtils.putInt(context, Constant.PrefrencesPt.DTjifenAvailable, jifen);
		}
		
	}
	
	public static int getUserJifen(Context context){
		return  PreferencesUtils.getInt(context, Constant.PrefrencesPt.DTjifenAvailable);
	}
	
	/**
	 * 喜欢作品和取消作品
	 * @param context
	 * @param like 
	 * @param workid 作品id
	 * @param callback
	 */
	public static void likeSubmit(Context context,final boolean like,long workid,final HandleCallback<String> callback) {
		try {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("worksId", workid);
			String url = null;
			if (like) {
				url =  Constant.URL.DTLikeSubmit;
			}else {
				url =  Constant.URL.DTLikeCancel;
			}
			RequestApi.postCommon((Activity)context, url, data,
					new ResultLinstener<String>() {
						@Override
						public void onSuccess(String obj) {
							callback.onSuccess(obj);
						}

						@Override
						public void onFailure(String obj) {
						}

						@Override
						public void onException(String exception) {
						}
					}, new String());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
