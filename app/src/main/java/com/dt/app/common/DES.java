package com.dt.app.common;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class DES {
	public static String decryptDES(String decryptString, String decryptKey) {
		try {
			byte[] byteMi = DESBase64.decode(decryptString);

			IvParameterSpec zeroIv = new IvParameterSpec(iv);

			SecretKeySpec key = new SecretKeySpec(decryptKey.getBytes(), "DES");

			Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");

			cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);

			byte decryptedData[] = cipher.doFinal(byteMi);

			return new String(decryptedData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	private static byte[] iv = { 1, 2, 3, 4, 5, 6, 7, 8 };

	public static String encryptDES(String encryptString, String encryptKey) {
		try {
			IvParameterSpec zeroIv = new IvParameterSpec(iv);

			SecretKeySpec key = new SecretKeySpec(encryptKey.getBytes(), "DES");

			Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");

			cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);

			byte[] encryptedData = cipher.doFinal(encryptString.getBytes());

			return DESBase64.encode(encryptedData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
}
