package com.dt.app.common;

import java.util.List;

import org.json.JSONException;

/**
 * Created by caiyuhao on 15-7-6.
 *
 /**
 * 返回的结果集合
 *
 * @param <T>>
 */

public interface ResultListLinstener<T>  {
    /**
     * @param obj
     */
    public void onSuccess(List<T> obj) throws JSONException;
    /**
     * @param obj
     */
    public void onFailure(String obj);
    /**
     * @param
     */
    public void onException(String exception);
}
