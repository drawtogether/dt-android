package com.dt.app.common;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;

import com.dt.app.ui.art.ArtGalleryDetailActivity;
import com.dt.app.ui.art.ArtistSpaceActivity;
import com.dt.app.ui.set.PhotoViewActivity;
import com.dt.app.ui.works.RewardActivity;

public class CommonAcitivty {
	/**
	 * 启动艺术家空间/个人空间
	 * @param context
	 * @param memberId
	 */
	public static void startArtistSpaceActivity(Context context,long memberId){
		Intent mIntent = new Intent(context,ArtistSpaceActivity.class);
		mIntent.putExtra("memberId", memberId);
		context.startActivity(mIntent);
	}
	/**
	 * 作品详情
	 * @param context
	 * @param workId
	 */
	public static void startArtGalleryDetailActivity(Context context,long workId,long memberId){
		startArtGalleryDetailActivity(context, workId, memberId, false);
	}
	public static void startArtGalleryDetailActivity(Context context,long workId,long memberId,boolean isLoading){
		Intent intent = new Intent(context,ArtGalleryDetailActivity.class);
		intent.putExtra("workId", workId);
		intent.putExtra("memberId", memberId);
		if (isLoading) {
			intent.putExtra("isLoading", isLoading);
		}
		context.startActivity(intent);
	}
	/**
	 * 图片预览界面
	 * @param context
	 * @param logo 作品对应用户的图像
	 * @param url 作品的url
	 * @param workId 作品id
	 * @param like 是否喜欢
	 */
	public static void startPhotoViewActivity(Context context,String logo,String[] url,long workId,int like){
		Intent intent = new Intent(context,PhotoViewActivity.class);
		ArrayList<String> photos = new ArrayList<String>();
		for (int i = 0; i < url.length; i++) {
			photos.add(url[i]);
		}
		intent.putExtra("photos", photos);
		intent.putExtra("index", 0);
		intent.putExtra("workId", workId);
		intent.putExtra("like", like);
		intent.putExtra("logo", logo);
		context.startActivity(intent);
 
	}
	/**
	 * 打赏界面
	 * @param context
	 * @param workId
	 */
	public static void startRewardActivity(Context context,String url,long workId,String logo,String rewardUrl){
		Intent intent = new Intent(context,RewardActivity.class);
		intent.putExtra("workId", workId);
		intent.putExtra("logo", logo);
		intent.putExtra("rewardUrl", rewardUrl);
		context.startActivity(new Intent(intent));
		
	}
}
