package com.dt.app.common;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import android.util.Log;
import com.dt.app.MainActivity;
import com.dt.app.bean.DTCheckUser;
import com.dt.app.bean.DTUser;
import com.dt.app.ui.login.ForgetPwdActivity;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.lidroid.xutils.util.LogUtils;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;
import org.json.JSONObject;

/**
 * Created by caiyuhao on 15-9-11.
 */
public class DTFactoryApi {
    /**
     * login common
     *
     * @param mActivity
     * @param url
     * @param data
     * @param str_username
     * @param str_pwd
     * @param code
     */
    public static void commonData(final Activity mActivity, String url,
                                  HashMap<String, Object> data,
                                  String str_username,
                                  String str_pwd,
                                  String code,
                                  final ResultLinstener  linstener
                                  ) {
        data.put(Constant.Parameter.DTloginname, str_username);
        data.put(Constant.Parameter.DTpassword, DES.encryptDES(str_pwd, RequestApi.USER_SECRET_default));
        if (!TextUtils.isEmpty(code)) {
            data.put(Constant.Parameter.DTcode, code);
        }
        LogUtils.e("-----url------>" + url);
        ToastUtils.showDtDialog(mActivity);
        try {
            RequestApi.commonRequest(mActivity, url, data, new ResultLinstener<DTUser>() {

                @Override
                public void onSuccess(DTUser obj) {
                    ToastUtils.cancelProgressDt();

                    if (obj.getCode() == 1) {
                        //successful
                        saveData(mActivity, obj);
                        //ToastUtils.showTextToast(mActivity, obj.getMessage() + "");
                        if (!(mActivity instanceof ForgetPwdActivity)) {
                            mActivity.startActivity(new Intent(mActivity, MainActivity.class));
                        }
                        mActivity.finish();
                    }
                    if (linstener != null){
                        linstener.onSuccess(obj);
                    }
                }

                @Override
                public void onFailure(String obj) {
                    if (linstener != null){
                        linstener.onFailure(obj);
                    }
                    ToastUtils.cancelProgressDt();
                }

                @Override
                public void onException(String exception) {
                    if (linstener != null){
                        linstener.onException(exception);
                    }
                    ToastUtils.cancelProgressDt();
                }
            }, new DTUser());
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    /**
     * login
     *
     * @param mActivity
     * @param str_username
     * @param str_pwd
     */
    public static void loginData(final Activity mActivity, String str_username, String str_pwd,
                                 final ResultLinstener  linstener) {

        HashMap<String, Object> data = new HashMap<String, Object>();

        commonData(mActivity, Constant.URL.DTLogin, data, str_username, str_pwd, null, linstener);

    }

    /**
     * register
     *
     * @param mActivity
     * @param str_username
     * @param str_pwd
     * @param code
     * @param nickname
     */
    public static void mobileRegData(final Activity mActivity, String str_username,
                                     String str_pwd, String code, String nickname,
                                     final ResultLinstener  linstener) {


        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put(Constant.Parameter.DTnickname, nickname);
        data.put(Constant.Parameter.DTclientType, "Android");
        commonData(mActivity, Constant.URL.DTRegister, data, str_username, str_pwd, code, linstener);

    }

    /**
     * @param mActivity
     * @param str_username
     * @param str_pwd
     * @param code
     */
    public static void resetPwdData(final Activity mActivity, String str_username,
                                    String str_pwd, String code,
                                    final ResultLinstener  linstener) {


        HashMap<String, Object> data = new HashMap<String, Object>();
        commonData(mActivity, Constant.URL.DTresetPwd, data, str_username, str_pwd, code,linstener);
    }

    /**
     * QQ 登录
     *
     * @param mActivity
     * @param str_username
     * @param gender
     * @param logo
     */
    public static void qqData(final Activity mActivity, String str_username,
                              Integer gender, String logo,String comeFrom,String openid) {
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put(Constant.Parameter.DTnickname, str_username);
        data.put(Constant.Parameter.DTcomeFrom, comeFrom);
        data.put(Constant.Parameter.DTgender, gender);
        data.put(Constant.Parameter.DTlogo, logo);
        data.put(Constant.Parameter.DTopenid, openid);
        data.put(Constant.Parameter.DTclientType, "Android");
        ToastUtils.showDtDialog(mActivity);
        try {
            RequestApi.commonRequest(mActivity, Constant.URL.DTLogin3p, data, new ResultLinstener<DTUser>() {

                @Override
                public void onSuccess(DTUser obj) {
                    ToastUtils.cancelProgressDt();
                    if (obj.getCode() == 1) {
                        //successful
                        saveData(mActivity, obj);
                        //ToastUtils.showTextToast(mActivity, obj.getMessage() + "");

                    }
                }

                @Override
                public void onFailure(String obj) {
                    ToastUtils.cancelProgressDt();
                }

                @Override
                public void onException(String exception) {
                    ToastUtils.cancelProgressDt();
                }
            }, new DTUser());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * check user
     *
     * @param mActivity
     * @param str_username
     * @param linstener
     */
    public static void checkUserData(final Activity mActivity, String str_username, final ResultLinstener linstener) {
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put(Constant.Parameter.DTloginname, str_username);

        // final RegisterActivity  mRegisterActivity   = (RegisterActivity)mActivity;
        try {
            RequestApi.commonRequest(mActivity, Constant.URL.DTCheckUser, data, new ResultLinstener<DTCheckUser>() {

                @Override
                public void onSuccess(DTCheckUser obj) {

                    linstener.onSuccess(obj);
                }

                @Override
                public void onFailure(String obj) {
                    linstener.onFailure(obj);
                }

                @Override
                public void onException(String exception) {
                    linstener.onException(exception);
                }
            }, new DTCheckUser());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void saveData(Activity mActivity, DTUser obj) {
        DTUser.UserData userData = obj.getData();
        DTUser.MemberBean memberBean = userData.getMember();
        String desc_nonce = DES.decryptDES(userData.getNonce(), RequestApi.USER_SECRET_default);
        String save_noce = desc_nonce.substring(0, desc_nonce.lastIndexOf("&"));
        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTnonce, save_noce);
        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTmkey, userData.getMkey());

        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTmobilephone, memberBean.getMobilephone());
        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTemail, memberBean.getEmail());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTdonationCount, memberBean.getDonationCount());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTfollowersCount, memberBean.getFollowersCount());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTfollowingCount, memberBean.getFollowingCount());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTgender, memberBean.getGender());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTisSigned, memberBean.getIsSigned());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTlikeCount, memberBean.getLikeCount());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTjifenAvailable, memberBean.getJifenAvailable());
        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTlogo, memberBean.getLogo());
        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTSignature, memberBean.getSignature());
        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTnickname, memberBean.getNickname());
        PreferencesUtils.putString(mActivity, Constant.PrefrencesPt.DTregisterTime, memberBean.getRegisterTime());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTworksCount, memberBean.getWorksCount());
        PreferencesUtils.putInt(mActivity, Constant.PrefrencesPt.DTcommentCount, memberBean.getCommentCount());
        PreferencesUtils.putLong(mActivity, Constant.PrefrencesPt.DTid, memberBean.getId());
        if (!(mActivity instanceof ForgetPwdActivity)) {
            mActivity.startActivity(new Intent(mActivity, MainActivity.class));
        }
        mActivity.finish();
    }
}
