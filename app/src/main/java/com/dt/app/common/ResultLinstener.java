package com.dt.app.common;


/**
 * Created by caiyuhao on 15-7-6.
 *
 /**
 * 返回的结果集合
 *
 * @param <T>
 */

public interface ResultLinstener<T> {
    /**
     * @param obj
     */
    public void onSuccess(T obj);
    /**
     * @param obj
     */
    public void onFailure(String obj);
    /**
     * @param
     */
    public void onException(String exception);
}
