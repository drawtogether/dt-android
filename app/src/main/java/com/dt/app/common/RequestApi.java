package com.dt.app.common;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;

import com.dt.app.utils.Constant;
import com.dt.app.utils.Md5Util;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.lidroid.xutils.util.LogUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;

/**
 * Created by caiyuhao on 15-7-3.
 */
public class RequestApi {

    public static   String USER_SECRET_default = "dt1234+u";
    public static   String USER_SECRET  ;
    /**
     * 不需要登陆验证的url
     */
    public static final String[] filterUrl = new String[]{
		Constant.URL.DTLogin,
		Constant.URL.DTLogin3p,
		Constant.URL.DTRegister,
		Constant.URL.DTSendVerfyCode,
		Constant.URL.DTCheckUser,
		Constant.URL.DTinitData,
		Constant.URL.DTresetPwd
    };

    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * commonRequest
     * @param activity
     * @param url
     * @param data
     * @param linstener
     * @param clazz
     * @param <T>
     * @throws Exception
     */
    public static <T> void commonRequest(final Activity activity,
                                         final String url, final HashMap<String, Object> data, final ResultLinstener<T> linstener,
                                         final T clazz) throws Exception {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        if (data.containsKey("testonly")){
            requestParams.put("testonly",true);
        }
        client.post(activity, url, (data.containsKey("testonly"))?requestParams:getRequestParams(activity, data, url),
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int i, Header[] headers, byte[] bytes) {
                        LogUtils.e("---------commonRequest--------" + new String(bytes));
                        try {
                            JSONObject data = new JSONObject(new String(bytes));
                            try {
                                T t = (T) objectMapper.readValue(
                                        data.toString(), clazz.getClass());
                                linstener.onSuccess(t);
                            } catch (JsonMappingException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int i, Header[] headers,
                                          byte[] bytes, Throwable throwable) {
                        if (bytes != null && bytes.length > 0) {
                            linstener.onFailure((new String(bytes)));
                        }
                    }
                });
    }


    /**
     * 公共方法-返回列表
     *
     * @param activity
     * @param linstener
     */
    public static <T> void postCommon_List(final Activity activity, final String url, Map<String, Object> data,
                                           final ResultLinstener<List<T>> linstener, final T clazz) {
        postCommon_List(activity, url, data, linstener, clazz, false);
    }

    public static <T> void postCommon_List(final Activity activity, final String url, Map<String, Object> data,
                                           final ResultLinstener<List<T>> linstener, final T clazz, Boolean dialog) {
        postCommon_List(activity, url, data, linstener, clazz, dialog, null);
    }

    public static <T> void postCommon_List(final Activity activity, final String url, Map<String, Object> data,
                                           final ResultLinstener<List<T>> linstener, final T clazz, Boolean dialog, String msg) {
        AsyncHttpClient client = new AsyncHttpClient();
//        final ShapeLoadingDialog mShapeLoadingDialog = new ShapeLoadingDialog(activity);
//        if (dialog) {
//        	if (!TextUtils.isEmpty(msg)) {
//        		 mShapeLoadingDialog.setLoadingText(msg);
//			}else {
//				 mShapeLoadingDialog.setLoadingText("加载中...");
//			}
//            mShapeLoadingDialog.show();
//		}
        client.post(activity, url, getRequestParams(activity, data, url), new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(int arg0, Header[] arg1, byte[] bytes,
                                  Throwable arg3) {
               LogUtils.e("------Throwable------" + arg3.getMessage());
                if (bytes != null && bytes.length > 0) {
                    String res = (new String(bytes));
                    ToastUtils.showTextToast(activity, res);
                    linstener.onFailure(res);
                }
//    			if (mShapeLoadingDialog!=null) {
//           		 	mShapeLoadingDialog.dismiss();
//				}
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] bytes) {
                LogUtils.e("---------heartPostCommon--------" + new String(bytes));
                try {
                    String json = new String(bytes);
                    JSONObject jsonObject = new JSONObject(json);
                    int code = jsonObject.getInt("code");
                    if (code == 1) {
                        List<T> mList = new ArrayList<T>();
                        if (jsonObject.has("data")) {
                            try {
                                JSONArray mArray = jsonObject.optJSONArray("data");
                                if (mArray != null) {
                                    for (int i = 0; i < mArray.length(); i++) {
                                        T app = (T) objectMapper.readValue(mArray.getJSONObject(i).toString(), clazz.getClass());
                                        mList.add(app);
                                    }
                                } else {
                                    JSONObject dataJsonObject = jsonObject.getJSONObject("data");
                                    Iterator<String> iterator = dataJsonObject.keys();
                                    while (iterator.hasNext()) {
                                        String key = iterator.next();
                                        JSONArray mMapArray = (JSONArray) dataJsonObject.get(key);
                                        for (int i = 0; i < mMapArray.length(); i++) {
                                            T app = (T) objectMapper.readValue(mMapArray.getJSONObject(i).toString(), clazz.getClass());
                                            mList.add(app);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        linstener.onSuccess(mList);
                    } else if (code == -10) {

                    } else if (code == -2) {
                        linstener.onSuccess(null);
                    } else {
                        if (jsonObject.has("message")) {
                            ToastUtils.showTextToast(activity, "" + jsonObject.getString("message"));
                        }
                        linstener.onFailure(json);
                    }
                } catch (Exception e) {
                    linstener.onException(e.getMessage());
                    e.printStackTrace();
                }
//    			if (mShapeLoadingDialog!=null) {
//           		 	mShapeLoadingDialog.dismiss();
//				}
            }
        });
    }

    private static RequestParams getRequestParams(Activity activity, Map<String, Object> data, String url) {
        StringBuffer buffer = new StringBuffer("{");
        RequestParams params = new RequestParams();
        Map<String, Object> temp = new HashMap<String, Object>();
        try {
            if (data != null) {


            	 boolean exist = false;
            	 String mkeyStr= PreferencesUtils.getString(activity, Constant.PrefrencesPt.DTmkey);
            	 String nonce= PreferencesUtils.getString(activity, Constant.PrefrencesPt.DTnonce);
                 for (String filter : filterUrl) {
                     if (filter.equals(url)) {
                         exist = true;
                         break;
                     }
                 }
                 if (!exist) {
 					params.put("mkey", mkeyStr);
 					temp.put("mkey", mkeyStr);
 					//params.put("testonly", "true");
                     USER_SECRET = nonce;
// 					temp.put("mkey", PreferencesUtils.getString(activity, Constant.PrefrencesPt.DTmkey));
 					buffer.append("mkey : " +mkeyStr + " ,");
                 }else {
                     USER_SECRET = USER_SECRET_default;
                 }
                 
            	if (data.keySet()!=null) {
            		for (String key : data.keySet()) {
                        buffer.append(key + " : " + data.get(key) + " ,");
                        String val = new String((data.get(key) + "").getBytes(), "utf-8");
                        params.put(key, val);
                        if (key.equals("imageUrls")){
                            LogUtils.e("---------data.keyset()------>"+key+",val = "+data.get(key));

                        }
                        temp.put(key, val);
                    }
				}
                params.put("ts", System.currentTimeMillis() / 1000);
                temp.put("ts", System.currentTimeMillis() / 1000);
                params.put("sign", getSign(temp, url ));
//    			buffer.append("user_id : "+PreferencesUtils.getString(activity, Const.User.USER_ID));
                buffer.append(" : ts=" + (System.currentTimeMillis() / 1000) + " : sign=" + getSign(temp, url));
                //LogUtils.e("------------" + buffer.toString());
                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

    public static <T> void postCommon(final Activity activity,
                                      String url, Map<String, Object> data, final ResultLinstener<T> linstener,
                                      final T clazz) {
        postCommon(activity, url, data, linstener, clazz, false);
    }

    public static <T> void postCommon(final Activity activity,
                                      String url, Map<String, Object> data, final ResultLinstener<T> linstener,
                                      final T clazz, Boolean dialog) {
        postCommon(activity, url, data, linstener, clazz, dialog, null);
    }

    public static <T> void postCommon(final Activity activity,
                                      final String url, Map<String, Object> data, final ResultLinstener<T> linstener,
                                      final T clazz, Boolean dialog, String msg) {
        try {
            AsyncHttpClient client = new AsyncHttpClient();
//    		 final ShapeLoadingDialog mShapeLoadingDialog = new ShapeLoadingDialog(activity);
//    	        if (dialog) {
//    	        	if (!TextUtils.isEmpty(msg)) {
//    	        		 mShapeLoadingDialog.setLoadingText(msg);
//    				}else {
//    					 mShapeLoadingDialog.setLoadingText("加载中...");
//    				}
//    	            mShapeLoadingDialog.show();
//    			}
            client.post(activity, url, getRequestParams(activity, data, url), new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] bytes) {
                    LogUtils.e("---------heartPostCommon--------" + new String(bytes));
                    try {
                        String json = new String(bytes);
                        if (url.equals(Constant.URL.DTUserInvite)) {
                    		linstener.onException(json);
                    		return;
						}
                        JSONObject jsonObject = new JSONObject(json);
                        int code = jsonObject.getInt("code");
                        if (code == 1) {
                            if (clazz instanceof String) {
                            	if (url.equals(Constant.URL.DTJifenStat)) {
                            		linstener.onException(json);
								}
                                linstener.onSuccess(null);
                                return;
                            }
                            if (jsonObject.has("data")) {
                                String data = jsonObject.getString("data");
                                try {
                                    T t = (T) objectMapper.readValue(data, clazz.getClass());
                                    linstener.onSuccess(t);

                                } catch (Exception e) {
                                	e.printStackTrace();
                                    linstener.onSuccess(null);
                                }
                            } else {
                                try {
                                    T t = (T) objectMapper.readValue(json, clazz.getClass());
                                    linstener.onSuccess(t);
                                } catch (Exception e) {
                                	e.printStackTrace();
                                    linstener.onSuccess(null);
                                }
                            }

                        } else if (code == -10) {

                        } else {
                            if (jsonObject.has("message")) {
                                ToastUtils.showTextToast(activity, "" + jsonObject.getString("message"));
                            }
                            linstener.onFailure(json);
                        }
                    } catch (Exception e) {
                        linstener.onException(e.getMessage());
                        e.printStackTrace();
                    }
//	    			if (mShapeLoadingDialog!=null) {
//	           		 	mShapeLoadingDialog.dismiss();
//					}
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, byte[] bytes, Throwable arg3) {
                    System.out.println("------Throwable------" + arg3.getMessage());
                    if (bytes != null && bytes.length > 0) {
                        String res = (new String(bytes));
                        ToastUtils.showTextToast(activity, res);
                        linstener.onFailure(res);
                    }
//	    			if (mShapeLoadingDialog!=null) {
//	           		 	mShapeLoadingDialog.dismiss();
//					}
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param activity
     * @param url
     * @param linstener
     * @param clazz
     * @param <T>
     * @throws Exception
     */
    public static <T> void dtGetCommon(final Context activity,
                                          String url, final ResultLinstener<T> linstener,
                                          final T clazz) {

        AsyncHttpClient client = new AsyncHttpClient();
        LogUtils.e("---------url--------" + url);
        url +="";
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                LogUtils.e("---------heartGetCommon--------" + new String(bytes));
                try {
                    JSONObject data = new JSONObject(new String(bytes));

                    T t = (T) objectMapper.readValue(
                            data.toString(), clazz.getClass());
                    linstener.onSuccess(t);

                } catch (Exception e) {
                    linstener.onException(e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int i, Header[] headers,
                                  byte[] bytes, Throwable throwable) {
                if (bytes != null && bytes.length > 0) {
                    linstener.onFailure((new String(bytes)));
                }
            }
        });
    }

    /**
     * LIST common
     *
     * @param activity
     * @param url
     * @param linstener
     * @param <T>
     */
    public static <T> void heartGetListCommon(final Activity activity,
                                              String url, final ResultListLinstener<T> linstener,
                                              final T classaz) {

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                LogUtils.e("---------heartGetListCommon--------" + new String(bytes));
                try {
                    JSONArray data = new JSONArray(new String(bytes).toString());
                    LogUtils.e("---------length--------" + data.length());
                    List<T> list = new ArrayList<T>(data.length());
                    for (int j = 0; j < data.length(); j++) {
                        T t = (T) objectMapper.readValue(data.getString(j), classaz.getClass());
                        list.add(t);
                    }
                    linstener.onSuccess(list);

                } catch (Exception e) {
                    linstener.onException(e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int i, Header[] headers,
                                  byte[] bytes, Throwable throwable) {
                if (bytes != null && bytes.length > 0) {
                    linstener.onFailure((new String(bytes)));
                }
            }
        });
    }

    public static String getSign(Map<String, Object> data, String url) {
        if (data != null) {
            int size = data.size();
            String[] arr = new String[size];
            int count = 0;
            for (String key : data.keySet()) {
                arr[count] = key;
                count++;
            }
            Arrays.sort(arr);
            String str = "";
            for (int i = 0; i < size; i++) {
                if (i == size - 1) {
                    str += arr[i] + "=" + data.get(arr[i]) + USER_SECRET;
                } else {
                    str += arr[i] + "=" + data.get(arr[i]) + "&";
                }
            }

            String subUrl = "";
            subUrl =url.substring(Constant.URL.Base_Url.length()) + "?";
            LogUtils.e("---------subUrl------->"+subUrl + str);
            return Md5Util.strMD5(subUrl + str);
        }
        return null;
    }
    /**
	 * 上传图片
	 * 
	 * @param path
	 */
	public static void updatePhoto(String path, String photoName,
			String picToken, final ResultLinstener listener) {

		String data = path;
		String key = photoName;
		String token = picToken;
		UploadManager uploadManager = new UploadManager();

		uploadManager.put(data, key, token, new UpCompletionHandler() {
            @Override
            public void complete(String s, ResponseInfo responseInfo,
                                 JSONObject jsonObject) {
                try {
                    if (responseInfo.isOK()) {

                        listener.onSuccess(jsonObject);
                    } else {
                    	listener.onSuccess(null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new UploadOptions(null, null, false,
                new UpProgressHandler() {
                    public void progress(String key, double percent) {
                        try {
                            listener.onFailure(key + "," + percent);
                        } catch (Exception e) {
                            // TODO: handle exception
                        }
                    }
                }, null));
	}

	/**
	 * 批量上传图片
	 * 
	 * @param path
	 */
	public static void updatePhotoes(final String[] path, String[] photoName,
			String[] picToken, final ResultLinstener listener) {

		String[] data = path;
		String[] key = photoName;
		String[] token = picToken;
		UploadManager uploadManager = new UploadManager();

		for (int i = 0; i < path.length; i++) {
			uploadManager.put(data[i], key[i], token[i],
					new UpCompletionHandler() {
						@Override
						public void complete(String s,
								ResponseInfo responseInfo, JSONObject jsonObject) {
							try {
								LogUtils.e("-----updatePhotoes-1---->" + k);
								LogUtils.e("-----jsonObject---->" + jsonObject);
								if (responseInfo.isOK()) {
                                    LogUtils.e("----------k-->"+k+",path.length-1"+(path.length-1));
									if (k == path.length - 1) {
										listener.onSuccess(jsonObject);
										LogUtils.e("-----updatePhotoes---2-->"+ k);
										k = 0;
									}
									k++;
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					},new UploadOptions(null, null, false,
                            new UpProgressHandler(){
                        public void progress(String key, double percent){
                        	 try {
                             	listener.onFailure("progress:  "+key+","+percent);
             				} catch (Exception e) {
             					e.printStackTrace();
             				}
                        }
                    }, null));
		}

	}
  public  static int k = 0;
}
