package com.dt.app;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import cn.jpush.android.api.JPushInterface;
import com.dt.app.autoupdate.CheckUpdate;
import com.dt.app.base.BaseActivity;
import com.dt.app.bean.DTInitData;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.login.DTGuideLogin;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class DTWelcomeActivity extends BaseActivity {
    private Activity mActivity;

    @ViewInject(R.id.img_logo_center)
    private ImageView img_logo_center;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dt_welcome);
        ViewUtils.inject(this);
        mActivity = this;

        Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.welcome_in_from_alpha);
        img_logo_center.startAnimation(animation);
        initData();
        redirectByTime();

    }

    private void redirectByTime() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                String mkey = PreferencesUtils.getString(mActivity, Constant.PrefrencesPt.DTmkey);
                if (TextUtils.isEmpty(mkey)) {
                    startActivity(new Intent(DTWelcomeActivity.this, DTGuideLogin.class));
                } else {
                    startActivity(new Intent(DTWelcomeActivity.this, MainActivity.class));
                }
                finish();
            }
        }, 2000);
    }

    /**
     * initData
     */
    private void initData() {
        try {
            Long lastTime = PreferencesUtils.getLong(mActivity, Constant.Parameter.DTlastUpdateTime);
            if (lastTime == -1) {
                lastTime = System.currentTimeMillis() / 1000;
                PreferencesUtils.putLong(mActivity, Constant.Parameter.DTlastUpdateTime, lastTime);
            }
            //LogUtils.e("------lastTime-------->" + lastTime);
            HashMap<String, Object> data = new HashMap<String, Object>();
            //data.put(Constant.Parameter.DTlastUpdateTime, lastTime);
            RequestApi.commonRequest(this, Constant.URL.DTinitData, data, new ResultLinstener<DTInitData>() {

                @Override
                public void onSuccess(DTInitData obj) {
                    DTInitData.InitData initData = obj.getData();
                    DTApplication.getInstance().setDmDatas(initData.getDm());
                    DTApplication.getInstance().setLogoDataList(initData.getLogo());
                    DTApplication.getInstance().setAddressDataList(initData.getRegion());

                    //LogUtils.e("------getImageUrl()-------->" + initData.getSplash().getImageUrl());
                    String registerID = JPushInterface.getRegistrationID(mActivity);
                    if (!TextUtils.isEmpty(registerID)){
                        LogUtils.e("------registerID---->" + registerID);
                        bind3p(registerID);
                    }
                }

                @Override
                public void onFailure(String obj) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onException(String exception) {
                    // TODO Auto-generated method stub

                }
            }, new DTInitData());
        } catch (Exception e) {
            // TODO: handle exception
        }

    }

    private void bind3p(String openid){
        try {
            HashMap<String, Object> data = new HashMap<String, Object>();
            data.put("comeFrom","jpush");
            data.put("openid",openid);
            data.put("clientType","Android");
            RequestApi.commonRequest(this, Constant.URL.DTBind3p, data, new ResultLinstener<String>() {

                @Override
                public void onSuccess(String obj) {
                    if (obj != null){
                        LogUtils.e("------------>"+obj);
                    }
                }

                @Override
                public void onFailure(String obj) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onException(String exception) {
                    // TODO Auto-generated method stub

                }
            }, new String());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
