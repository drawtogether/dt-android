package com.dt.app.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.jpush.android.api.JPushInterface;
import com.umeng.analytics.MobclickAgent;

public abstract class BaseFragment extends Fragment {
    protected Context context;
    protected LinearLayout ll_loading_dialog;
    protected TextView tv_load_nodata_info;
    private View contentView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        
        contentView = setContentView(inflater);
        if (contentView != null) {
        	initView(contentView);
//            ll_loading_dialog = (LinearLayout) contentView.findViewById(R.id.ll_loading_dialog);
//            tv_load_nodata_info = (TextView) contentView.findViewById(R.id.tv_load_nodata_info);
        }
        return contentView;
    }

    public abstract void initView(View contentView);

    public abstract void initData(Bundle savedInstanceState);

	public View findViewById(int id) {
		return contentView.findViewById(id);
	}
 
    public abstract View setContentView(LayoutInflater inflater);


    /**
     * 关闭ListView的加载层
     */
    protected void dismissLoading() {
        if (ll_loading_dialog != null) {
            ll_loading_dialog.setVisibility(View.GONE);
        }
    }
    /**
     * 关闭没有数据时的显示层
     */
    protected void dismissLoadNoDataInfo() {
    	if (tv_load_nodata_info != null) {
    		tv_load_nodata_info.setVisibility(View.GONE);
    	}
    }
    /**
     * 关闭没有数据时的显示层
     */
    protected void showLoadNoDataInfo(String text) {
    	if (tv_load_nodata_info != null) {
    		tv_load_nodata_info.setText(""+text);
    		tv_load_nodata_info.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("fragment");
        MobclickAgent.onPause(context);

    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("fragment");
        MobclickAgent.onResume(context);

    }
}
