package com.dt.app.base;

import cn.jpush.android.api.JPushInterface;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;

import com.dt.app.DTApplication;
import com.dt.app.R;
import com.umeng.analytics.MobclickAgent;

public abstract class BaseFragmentActivity extends FragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DTApplication.getInstance().addActivity(this);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

	}

	public void onAttachedToWindow() {
		super.onAttachedToWindow();
	}

	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
		JPushInterface.onPause(this);

	}

	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
		JPushInterface.onResume(this);
	}

	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		DTApplication.getInstance().finishActivity(this);
		super.onDestroy();
	}

	private final static int SCANNIN_GREQUEST_CODE = 1;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SCANNIN_GREQUEST_CODE) {

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public ProgressDialog show(Context context, String title, String message) {
		try {
			ProgressDialog pd = ProgressDialog.show(context, title, message,
					true, false);

			if (title != null)
				pd.setTitle(title);
			pd.setMessage(message);
			pd.setCancelable(true);
			pd.show();
			return pd;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ProgressDialog show(Context context, String message) {
		return show(context, null, message);
	}

	public ProgressDialog show(Context context) {
		return show(context, null, getString(R.string.data_loading));
	}

	public ProgressDialog showProgressDialog() {
		return show(this, null, getString(R.string.data_loading));
	}

	public void dismiss(ProgressDialog pd) {
		if (pd == null)
			return;

		if (pd.isShowing() && pd.getWindow() != null) {
			try {
				pd.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void startIntent(Class  classz){
		startActivity(new Intent(this,classz));
		overridePendingTransition(R.anim.alpha_in, R.anim.alpha_out);
    }
	
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}
}

