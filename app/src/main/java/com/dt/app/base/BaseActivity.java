package com.dt.app.base;

import cn.jpush.android.api.JPushInterface;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewParent;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.dt.app.DTApplication;
import com.library.markupartist.android.widget.ActionBar;
import com.umeng.analytics.MobclickAgent;

public abstract class BaseActivity extends Activity {
    public final String mPageName = "dt";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DTApplication.getInstance().addActivity(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        DTApplication.getInstance().addActivity(this);
    }


    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }


    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        DTApplication.getInstance().finishActivity(this);
        super.onDestroy();
    }


    protected void startActivityByCalss(Class clazz) {
        startActivity(new Intent(this, clazz));
    }

    /**
     * Activity��ת
     * @param context
     * @param targetActivity
     * @param bundle
     */
    public void startActivityByCalss(Context context,Class<?> targetActivity,Bundle bundle){
        Intent intent = new Intent(context, targetActivity);
        if(null != bundle){
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    public void setActionBar(ActionBar actionBar){
    	if (Build.VERSION.SDK_INT > 18) {
    		if (actionBar!=null) {
    			try {
    				ViewParent viewParent = actionBar.getParent();
    				if (viewParent instanceof LinearLayout) {
    					LinearLayout.LayoutParams layoutParams = (LayoutParams) actionBar.getLayoutParams();
            			layoutParams.topMargin = com.dt.app.utils.Utils.getStatusBarHeight(this);
            			actionBar.setLayoutParams(layoutParams);
					}else if (viewParent instanceof RelativeLayout) {
						RelativeLayout.LayoutParams layoutParams =  (android.widget.RelativeLayout.LayoutParams) actionBar.getLayoutParams();
            			layoutParams.topMargin = com.dt.app.utils.Utils.getStatusBarHeight(this);
            			actionBar.setLayoutParams(layoutParams);
					} else if (viewParent instanceof FrameLayout) {
						FrameLayout.LayoutParams layoutParams =  (android.widget.FrameLayout.LayoutParams) actionBar.getLayoutParams();
            			layoutParams.topMargin = com.dt.app.utils.Utils.getStatusBarHeight(this);
            			actionBar.setLayoutParams(layoutParams);
					} 
    				
				} catch (Exception e) {
					e.printStackTrace();
				}
    		}
    	}
    }
    @Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(mPageName);
        MobclickAgent.onResume(this);
        JPushInterface.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(mPageName);
        MobclickAgent.onPause(this);
        JPushInterface.onPause(this);
    }
}
