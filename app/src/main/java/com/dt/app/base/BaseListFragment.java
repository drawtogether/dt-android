package com.dt.app.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.dt.app.R;
import com.dt.app.bean.Page;
import com.dt.app.listener.LayoutCallback;
import com.dt.app.utils.PullToRefreshListViewUtils;
import com.dt.app.utils.PullToRefreshListViewUtils.PullOnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public abstract class BaseListFragment extends  Fragment{
	protected PullToRefreshListView mPullToRefreshListView;
	protected PullToRefreshListViewUtils<ListView> mListViewUtils;
	 
	protected Context mContext;
	protected int mainLayoutId = R.layout.dt_listview;
	protected FrameLayout fl_listview_bg;
	protected RelativeLayout rl_title_bar;
 
	protected boolean pullLoadEnabled=false;
	protected boolean scrollLoadEnabled=false;
	protected Page page;
	protected LayoutInflater mLayoutInflater;
	protected LayoutCallback mCallback;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		mLayoutInflater = inflater;
		initParameters();
		View view = inflater.inflate(mainLayoutId, null);
		mPullToRefreshListView = (PullToRefreshListView) view.findViewById(R.id.pull_refresh_list);
		
		fl_listview_bg =(FrameLayout) view .findViewById(R.id.fl_listview_bg);
		rl_title_bar = (RelativeLayout) view.findViewById(R.id.rl_title_bar);
		if (rl_title_bar!=null) {
			rl_title_bar.setVisibility(View.GONE);
		}
		initView();
		onCreateView(view);
		return view;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.mContext = getActivity();
		if (getActivity() instanceof LayoutCallback) {
			mCallback = (LayoutCallback) getActivity();
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initLoadData();
	}

	private void initView() {
		mListViewUtils = new PullToRefreshListViewUtils<ListView>(mPullToRefreshListView);
		mListViewUtils.init();
		mPullToRefreshListView.getRefreshableView().setAdapter(setAdapter());
	 
		mListViewUtils.setPullOnRefreshListener(new PullOnRefreshListener() {
			@Override
			public void onPullDownToRefresh(
					PullToRefreshBase refreshView, int currentPage) {
				onRefrsh(currentPage);
			}

			//下拉松手后会被调用
			@Override
			public void onPullUpToRefresh(
					PullToRefreshBase refreshView, int currentPage) {
				try {
//					if (page==null) {
//						return;
//					}
//					if (page.getTotalPage()<currentPage) {
//						mPullToRefreshListView.onRefreshComplete();
//						ToastUtils.showTextToast(getActivity(), "没有更多作品了");
//						return;
//					}
//					
					onLoad(currentPage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
		if (mCallback!=null) {
			mPullToRefreshListView.getRefreshableView().setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
						switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN://没有执行
							downY = (int) event.getRawY();
							break;
						case MotionEvent.ACTION_MOVE:
//							if (paddingTop!=0 || paddingTop!=-mCallback.callGetHeaderHeight()) {
//								if (downY==0) {
//									downY = (int) event.getRawY();
//								}else{
//									int diffY = (int) (event.getRawY()-downY);
//									System.out.println("--------------->>> "+diffY+" downY:"+downY+" top: "+paddingTop);
//									paddingTop+=diffY;
//									if (diffY<0) {//up
//										if (mCallback.callGetHeaderHeight()-Math.abs(paddingTop)<10) {
//											paddingTop=-mCallback.callGetHeaderHeight();
//										}
//									}else {//down
//										if (Math.abs(paddingTop)<10) {
//											paddingTop=0;
//										}
//									}
//									mCallback.callPadding(paddingTop);
//									downY = (int) event.getRawY();
//								}
//							}
							break;
						case MotionEvent.ACTION_UP:
							downY=0;
							break;
						}
					return false;
				}
			});
		}
	}
	
	private int downY;
	private int paddingTop=0;
	
	/**
	 * 下拉刷新
	 * @param currentPage
	 */
	public abstract void onRefrsh(int currentPage);
	/**
	 * 加载更多
	 * @param currentPage
	 */
	public abstract void onLoad(int currentPage);
	/**
	 * 初始化加载数据
	 */
	public abstract void initLoadData();
	/**
	 * 处理其他组件
	 * @param view
	 */
	public abstract void onCreateView(View view);
	/**
	 * 初始化参数
	 * mainLayoutId = R.layout.dt_listview_main;
	 * boolean pullLoadEnabled=false;
	 * boolean scrollLoadEnabled=false;
	 * @return
	 */
	public abstract void initParameters();
	/**
	 * 设置适配器
	 * @return
	 */
	public abstract ListAdapter setAdapter();
 
}
