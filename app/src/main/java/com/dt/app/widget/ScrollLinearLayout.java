package com.dt.app.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.Scroller;

public class ScrollLinearLayout extends LinearLayout {

	private Scroller mScroller;

	public ScrollLinearLayout(Context context) {
		super(context);
		init();
	}

	public ScrollLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		mScroller = new Scroller(getContext());

	}

	public void scroll(int startY, int dy) {
		//System.out.println("------------getScrollY---->>> "+getScrollY());
		mScroller.startScroll(0, startY, 0, dy,500);
		postInvalidate();
	}

	@Override
	public void computeScroll() {
		super.computeScroll();
		if (mScroller.computeScrollOffset()) {
			scrollTo(0, mScroller.getCurrY());
			postInvalidate();
		}
	}
}
