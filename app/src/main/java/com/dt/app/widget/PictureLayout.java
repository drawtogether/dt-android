package com.dt.app.widget;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dt.app.R;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.ui.art.ArtGalleryDetailActivity;
import com.dt.app.ui.set.PhotoViewActivity;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ZTDeviceInfo;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.BitmapUtils;

/**
 * 显示图片
 *
 * @author libin
 */
public class PictureLayout extends LinearLayout {
    private String[] imageUrls;

    private BitmapUtils mBitmapUtils;
    private Context context;
    private int height;
    private int width;
    private int itemWidth;
    private int spaceSpan;
    private int clickType = -1;
    private long workId;
    private long memberId;
    private String logo;

    private Integer picwidth;
    private Integer picheight;


    private int collectionState = 0;

    public int getCollectionState() {
        return collectionState;
    }

    public void setCollectionState(int collectionState) {
        this.collectionState = collectionState;
    }


    private boolean isCircle;


    public PictureLayout(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public PictureLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public PictureLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        init();
    }

    private void init() {
        height = ZTDeviceInfo.getInstance().getHeightDevice();
        spaceSpan = DensityUtil.dip2px(context, 2);
        width = ZTDeviceInfo.getInstance().getWidthDevice();
        itemWidth = (width - spaceSpan) / 2;


    }

    public void setOnClick(int clickType, long workId, long memberId, String logo) {
        this.clickType = clickType;
        this.workId = workId;
        this.memberId = memberId;
        this.logo = logo;
    }

    public void initData(boolean isCircle, List<String> imageUrl, BitmapUtils mBitmapUtils, Integer w, Integer h) {
        initData(isCircle, imageUrl.toArray(new String[imageUrl.size()]), mBitmapUtils, w, h);
    }

    public void initData(boolean isCircle, String imageUrl, BitmapUtils mBitmapUtils, Integer w, Integer h) {
        initData(isCircle, new String[]{imageUrl}, mBitmapUtils, w, h);
    }

    public void initData(boolean isCircle, String[] imageUrls, BitmapUtils mBitmapUtils, Integer w, Integer h) {
        this.isCircle = isCircle;
        this.imageUrls = imageUrls;
        this.mBitmapUtils = mBitmapUtils;

        setGravity(Gravity.CENTER_HORIZONTAL);
        setOrientation(VERTICAL);
        if (getParent() instanceof FrameLayout) {
            setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT));
        } else if (getParent() instanceof LinearLayout) {
            setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        } else if (getParent() instanceof RelativeLayout) {
            setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        }

//		setPadding(0, spaceSpan, 0, spaceSpan);
        if (isCircle) {
            setBackgroundColor(Color.WHITE);
            addView(createLayoutImage(0, 0, 0, 0, imageUrls[0]));
        } else {
            if (imageUrls.length == 1) {
                //setPadding(0, spaceSpan*5, 0, spaceSpan*5);
                addView(createLayoutImage(width, ((0 != w) && (0 != h)) ? ((width * h) / w) : width, 0, 0, imageUrls[0]));
                this.setBackgroundResource(0);
            } else if (imageUrls.length == 2) {
                setOrientation(HORIZONTAL);
                addView(createLayoutImage(itemWidth, itemWidth, 0, 0, imageUrls[0]));
                addView(createLayoutImage(itemWidth, itemWidth, 0, spaceSpan, imageUrls[1]));
                this.setBackgroundResource(0);
            } else if (imageUrls.length == 3) {
                addView(createLayout(0, imageUrls[0], imageUrls[1]));
                addView(createLayoutImage(width, (int) (width * 1.2), spaceSpan, 0, imageUrls[2]));
                this.setBackgroundResource(R.color.white);
            } else if (imageUrls.length == 4) {
                addView(createLayout(0, imageUrls[0], imageUrls[1]));
                addView(createLayout(spaceSpan, imageUrls[2], imageUrls[3]));
                this.setBackgroundResource(R.color.white);
            } else if (imageUrls.length == 5) {
                addView(createLayout(0, imageUrls[0], imageUrls[1]));
                addView(createLayout(spaceSpan, imageUrls[2], imageUrls[3]));
                addView(createLayoutImage(width, (int) (width * 1.2), spaceSpan, 0, imageUrls[4]));
                this.setBackgroundResource(R.color.white);
            } else if (imageUrls.length == 6) {
                addView(createLayout(0, imageUrls[0], imageUrls[1]));
                addView(createLayout(spaceSpan, imageUrls[2], imageUrls[3]));
                addView(createLayout(spaceSpan, imageUrls[4], imageUrls[5]));
                this.setBackgroundResource(R.color.white);
            }
        }

    }

    /**
     * @return
     */
    private LinearLayout createLayout(int top, String url1, String url2) {
        LinearLayout layout = new LinearLayout(context);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.topMargin = top;
        layout.setLayoutParams(params);
        //layout.setBackgroundResource(R.color.white);
        layout.setGravity(Gravity.CENTER_HORIZONTAL);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.addView(createLayoutImage(itemWidth, itemWidth, 0, 0, url1));
        layout.addView(createLayoutImage(itemWidth, itemWidth, 0, spaceSpan, url2));
        return layout;
    }


    private LinearLayout.LayoutParams setLayoutParamsView(int width, int height, int top, int left) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, height);
        params.topMargin = top;
        params.leftMargin = left;
        return params;
    }

    /**
     * 创建圆形布局
     *
     * @param url
     * @return
     */
    private View createLayoutImage(int width, int height, int top, int left, String url) {
        if (collectionState == 1) {
            RelativeLayout relativeLayout = new RelativeLayout(context);
            relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(width, height));
            //relativeLayout.setBackgroundResource(R.color.white);
            ImageView imageView = null;
            if (isCircle) {
                imageView = createCircle(url);
            } else {
                imageView = createImage(width, height, top, left, url);
            }
            relativeLayout.addView(imageView);
            relativeLayout.addView(createCollectionImage());
            return relativeLayout;
        } else {
            if (isCircle) {
                return createCircle(url);
            }
            return createImage(width, height, top, left, url);
        }
    }

    /**
     * 创建圆形图片
     *
     * @param url
     * @return
     */
    private CircleImageView createCircle(String url) {
        CircleImageView imageView = new CircleImageView(context);
        int itemw = width - 80;
        imageView.setLayoutParams(setLayoutParamsView(itemw, itemw, 0, 0));
        imageView.setScaleType(ScaleType.CENTER_CROP);
        if (clickType > 0) {
            onClick(imageView, url);
        }
        mBitmapUtils.display(imageView, url);

        return imageView;
    }

    private ImageView createImage(int width, int height, int top, int left, String url) {
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(setLayoutParamsView(width, height, top, left));
        //imageView.setBackgroundResource(R.color.white);
        imageView.setScaleType(ScaleType.CENTER_CROP);
        if (clickType > 0) {
            onClick(imageView, url);
        }
        mBitmapUtils.display(imageView, url);

        return imageView;
    }

    /**
     * 创建收藏图片
     */
    private ImageView createCollectionImage() {
        ImageView imageView = new ImageView(context);
        int w = DensityUtil.dip2px(context, 25);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(w, w);
        params.bottomMargin = spaceSpan / 2;
        params.rightMargin = spaceSpan / 2;
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        imageView.setLayoutParams(params);
        imageView.setBackgroundResource(R.mipmap.reward_collection);
        imageView.setScaleType(ScaleType.CENTER_CROP);
        return imageView;
    }


    public void onClick(View view, final String url) {
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickType == DAILY) {
                    Intent intent = new Intent(context, PhotoViewActivity.class);
                    ArrayList<String> photos = new ArrayList<String>();
                    int index = 0;
                    if (imageUrls != null && imageUrls.length > 1) {
                        for (int i = 0; i < imageUrls.length; i++) {
                            photos.add(imageUrls[i]);
                            if (url.equals(imageUrls[i])) {
                                index = i;
                            }
                        }
                    } else {
                        index = 0;
                        photos.add(url);
                    }
                    int[] locations = new int[2];
                    v.getLocationInWindow(locations);
                    intent.putExtra("photos", photos);
                    intent.putExtra("index", index);
                    intent.putExtra("workId", workId);
                    intent.putExtra("logo", logo);
                    intent.putExtra("x", locations[0]);
                    intent.putExtra("y", locations[1]);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.scale_center_in, R.anim.setting_apla_to_apla);
                } else if (clickType == ART) {
                    CommonAcitivty.startArtGalleryDetailActivity(context, workId, memberId, true);
                }
            }
        });
    }

    public static final int ART = 1;
    public static final int DAILY = 2;
}
