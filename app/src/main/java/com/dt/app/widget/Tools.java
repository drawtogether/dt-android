package com.dt.app.widget;

import java.util.ArrayList;

/**
 * 工具类
 * 
 * @author LiChaofei 2013-4-16 上午9:56:25
 */
public class Tools {
	/**
	 * 小圆半径
	 */
	private int differRadius;

	private int margin;

	private int radius;

	public Tools(int radius, int differRadius, int margin) {
		this.radius = radius;
		this.differRadius = differRadius;
		this.margin = margin;

	}

	/**
	 * 存放的坐标默认是顺时针
	 * 
	 * @param totalCount
	 * @return
	 */
	public ArrayList<double[]> getCoordianteSeries(int totalCount) {
		ArrayList<double[]> result = new ArrayList<double[]>();
		double unitRandis = 90.0 / (totalCount - 1);
		double x = 0;
		double y = 0;
		double randiss = 0;
		for (int i = 0; i < totalCount; i++) {
			randiss = i * unitRandis;
			x = Math.cos(Math.toRadians(randiss)) * radius + differRadius
					+ margin;
			y = Math.sin(Math.toRadians(randiss)) * radius + differRadius
					+ margin;
			result.add(new double[] { x, y });
		}
		return result;
	}
}
