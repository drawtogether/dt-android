package com.dt.app.widget;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dt.app.R;
import com.dt.app.animation.EnlargeAnimation;
import com.dt.app.animation.ShrinkAnimation;
import com.dt.app.utils.DensityUtil;

/**
 * Path菜单
 * 
 * @author LiChaofei 2013-4-16 下午1:46:09
 */
public class PathMenuLayout extends RelativeLayout {

	private Context context;
	private int margin;
	private int differRandius;
	private int radius;
	private int count;
	protected boolean expanded;
	private View homeMenu;
	
	/**
	 * homeMenu的位置，共4种情况
	 * @see ViewPosition
	 */
	private int position;
	
	public int getPosition() {
		return position;
	}

	public synchronized void setPosition(int position) {
		this.position = position;
	}

	private OnIconClickListener mOnIcomClick;
	private boolean hasListener;

	public PathMenuLayout(Context context) {
		super(context);
		this.context = context;
	}

	public PathMenuLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;

		initLayout(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.layout_path_menu, this,
				true);
	}

	public PathMenuLayout(final Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		initLayout(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.layout_path_menu, this,
				true);

	}


	/**
	 * 读取自定义属性的值
	 * @author LiChaofei
	 * <br/>2013-4-18 上午11:24:19
	 * @param context
	 * @param attrs
	 */
	private void initLayout(final Context context, AttributeSet attrs) {
		homeMenu = findViewById(R.id.menu_home);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.PathMenuLayout);
		differRandius = a.getInt(R.styleable.PathMenuLayout_differRadius, 0);
		margin = a.getInt(R.styleable.PathMenuLayout_margin, 0);
		radius = a.getInt(R.styleable.PathMenuLayout_radius, 200);	
		count = a.getInt(R.styleable.PathMenuLayout_radius, count);	
		
		differRandius = DensityUtil.dip2px(context, differRandius);
		margin = DensityUtil.dip2px(context, margin);
		radius = DensityUtil.dip2px(context, radius);
		
		setPosition(a.getInt(R.styleable.PathMenuLayout_position, 0));
		a.recycle();		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
 
	private ArrayList<ImageView> getImageChildren() {
		ArrayList<ImageView> imageViews = new ArrayList<ImageView>();
		int childCount = getChildCount();
		for (int i = 0; i < childCount; i++) {
			if (getChildAt(i) instanceof ImageView) {
				imageViews.add((ImageView) getChildAt(i));
			}
		}
		return imageViews;
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		homeMenu = findViewById(R.id.menu_home);
		changeHomeMenuPosition();
		ArrayList<ImageView> imageViews = getImageChildren();
		int count = imageViews.size();
		Tools tool=new Tools(radius, differRandius,margin);
		ArrayList<double[]> coorSeries = tool.getCoordianteSeries(count);
		for (int i = 0; i < count ; i++) {
			ImageView imageView = imageViews.get(i);
			changeImageViewParams(imageView, coorSeries, i);
			imageView.setTag(position);//tag作为设置动画起始位置的依据
			imageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Animation scale = AnimationUtils.loadAnimation(context,R.anim.scale_disapear);
					v.startAnimation(scale);
					mOnIcomClick.onIconClick(v);
				}
			});
		}
		if(!hasListener){
			homeMenu.setOnClickListener(homeMenuClickListener);
			hasListener=true;
		}
		super.onLayout(changed, l, t, r, b);
	}
	
	/**
	 * homeMenu的点击事件
	 */
	final View.OnClickListener homeMenuClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			ArrayList<ImageView> imageViews = getImageChildren();
			if (expanded) {
				for (ImageView view : imageViews) {
					if (view.equals(v))
						continue;
					Animation animation = new ShrinkAnimation(1000, view);

					view.startAnimation(animation);
				}
				Animation rotateClockwiseAnimation=new RotateAnimation(225,0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				rotateClockwiseAnimation.setDuration(200);
				rotateClockwiseAnimation.setFillAfter(true);
				v.startAnimation(rotateClockwiseAnimation);
				expanded = false;
			} else {
				for (ImageView view : imageViews) {
					if (view.equals(v))
						continue;
					Animation animation = new EnlargeAnimation(1000, view);
					view.startAnimation(animation);
				}
				Animation rotateAntiClockwiseAnimation=new RotateAnimation(0,225, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				rotateAntiClockwiseAnimation.setDuration(200);
				rotateAntiClockwiseAnimation.setFillAfter(true);
				v.startAnimation(rotateAntiClockwiseAnimation);
				expanded = true;
			}
		}
	};

	/**
	 * 根据position修改ImageView布局
	 * LiChaofei
	 * 2013-4-18 上午9:01:43
	 * @param imageView
	 * @param coorSeries
	 * @param index
	 */
	private void changeImageViewParams(ImageView imageView,ArrayList<double[]> coorSeries, int index) {
		LayoutParams params = (LayoutParams) imageView.getLayoutParams();
		switch (position) {
		case ViewPosition.POSITION_LEFT_BOTTOM://View在左下角
			params.leftMargin = (int) coorSeries.get(index)[0];
			params.bottomMargin = (int) coorSeries.get(index)[1];
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			break;
		case ViewPosition.POSITION_LEFT_TOP://View在左上角
			params.topMargin = (int)coorSeries.get(index)[0];
			params.leftMargin = (int)coorSeries.get(index)[1];
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			break;
		case ViewPosition.POSITION_RIGHT_TOP://View在右上角
			params.rightMargin = (int)coorSeries.get(index)[0];
			params.topMargin = (int)coorSeries.get(index)[1];
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			break;
		case ViewPosition.POSITION_RIGHT_BOTTOM://View在右下角
			params.bottomMargin = (int)coorSeries.get(index)[0];
			params.rightMargin = (int)coorSeries.get(index)[1];
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			break;		
		}
		
		imageView.setLayoutParams(params);
		
	}

	/**
	 * 根据position改变View HomeMenu的位置
	 * LiChaofei
	 * 2013-4-17 下午5:28:03
	 */
	private void changeHomeMenuPosition() {
		RelativeLayout.LayoutParams params=(LayoutParams) homeMenu.getLayoutParams();
		switch (position) {
		case ViewPosition.POSITION_LEFT_BOTTOM://View在左下角		
//			params.leftMargin =0;
//			params.bottomMargin = 0;
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			break;
		case ViewPosition.POSITION_LEFT_TOP://View在左上角
//			params.topMargin = 0;
//			params.leftMargin = 0;
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			break;
		case ViewPosition.POSITION_RIGHT_TOP://View在右上角
//			params.rightMargin = 0;
//			params.topMargin = 0;
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
			break;
		case ViewPosition.POSITION_RIGHT_BOTTOM://View在右下角
//			params.bottomMargin = 0;
//			params.rightMargin = 0;
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			break;		
		}
		homeMenu.setLayoutParams(params);		
	}
	
	public interface OnIconClickListener{
		public void onIconClick(View v);
	}

	/**
	 * 设置每个图标的点击事件，需要根据标识判断
	 * LiChaofei
	 * 2013-4-18 上午10:33:19
	 * @param onIcomClick
	 */
	public void setOnIcomClick(OnIconClickListener onIcomClick) {
		this.mOnIcomClick = onIcomClick;
	}
	
}
