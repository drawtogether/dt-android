package com.dt.app.widget;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.dt.app.R;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ViewUtils;

public class CommonMainTop {
	public RelativeLayout rl_main_top_common;
	private ImageView dt_main_ad_header;
	// private TextView tv_font_main;
	// private TextView tv_font_title;
	private Activity activity;

	public CommonMainTop(Activity activity, View view) {
		this.activity = activity;
		rl_main_top_common = (RelativeLayout) view
				.findViewById(R.id.rl_main_top_common);
		dt_main_ad_header = (ImageView) view
				.findViewById(R.id.dt_main_ad_header);
		// tv_font_main = (TextView) view.findViewById(R.id.tv_font_main);
		// tv_font_title = (TextView) view.findViewById(R.id.tv_font_title);
	}

	public void init(int image) {
		init(image, 0);
	}

	public void init(int image, int backgroundColor) {
		if (backgroundColor > 0) {
			rl_main_top_common.setBackgroundColor(activity.getResources()
					.getColor(backgroundColor));
		}
		dt_main_ad_header.setBackgroundResource(image);
		// tv_font_main.setText(fontText);
		// tv_font_title.setText(title);
	}

	public void finish() {
		rl_main_top_common.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.finish();
			}
		});
	}

	public  void layout(Context context) {
		int i_w = DensityUtil.dip2px(context, 210);
		int i_h = DensityUtil.dip2px(context, 30);
		RelativeLayout.LayoutParams layoutParams = (LayoutParams) ViewUtils
				.getLayoutParams(dt_main_ad_header, i_w, i_h);
		dt_main_ad_header.setLayoutParams(layoutParams);
		dt_main_ad_header.setBackgroundResource(R.mipmap.daily_artist);
	}
}
