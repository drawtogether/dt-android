package com.dt.app.widget;

/**
 * View的位置，共4种情况
 * <p><table>
 * <tr><td>name</td><td>value</td><td>description</td></tr>
 * <tr><td>POSITION_LEFT_BOTTOM </td><td>0</td><td>左下角</td></tr>
 * <tr><td>POSITION_LEFT_TOP </td><td>1</td><td>左上角</td></tr>
 * <tr><td>POSITION_RIGHT_TOP </td><td>2</td><td>右上角</td></tr>
 * <tr><td>POSITION_RIGHT_BOTTOM </td><td>3</td><td>右下角</td></tr>
 * </table></p>
 * @author LiChaofei <br/>
 * 2013-4-18 上午11:05:29
 */
public final class ViewPosition {

	public static final int POSITION_LEFT_BOTTOM=0;
	public static final int POSITION_LEFT_TOP=1;
	public static final int POSITION_RIGHT_TOP=2;
	public static final int POSITION_RIGHT_BOTTOM=3;
}
