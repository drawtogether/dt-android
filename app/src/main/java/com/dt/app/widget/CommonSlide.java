package com.dt.app.widget;

import android.app.Activity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import com.dt.app.MainActivity;
import com.dt.app.R;
import com.dt.app.ui.works.EveryDayUpActivity;

/**
 * Created by caiyuhao on 15-10-26.
 */
public class CommonSlide {
    public Activity mactivity;
    private View view_left;
    private View view_right;

    private float DownX = 0;
    private float DownY = 0;

    public CommonSlide(Activity activity,int isVisable,View  view) {
        this.mactivity = activity;
        if (mactivity instanceof MainActivity){
            view_left = mactivity.findViewById(R.id.view_left);
            view_right = mactivity.findViewById(R.id.view_right);

        }
    }

    /**
     *
     */
    public void loadListener() {

        view_left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        DownX = event.getX();//float DownX
                        DownY = event.getY();//float DownY

                        break;
                    case MotionEvent.ACTION_MOVE:
                        float moveX = event.getX() - DownX;//X轴距离
                        float moveY = event.getY() - DownY;//y轴距离
                        if (moveX > 5) {
                            if (mactivity instanceof MainActivity) {
                                MainActivity act = (MainActivity) mactivity;
                                (act).drawerlayout.openDrawer(Gravity.LEFT);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return true;
            }
        });
        view_right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        DownX = event.getX();//float DownX
                        DownY = event.getY();//float DownY

                        break;
                    case MotionEvent.ACTION_MOVE:
                        float moveX = event.getX() - DownX;//X轴距离
                        float moveY = event.getY() - DownY;//y轴距离
                        if (moveX < -5) {
                            if (mactivity instanceof MainActivity) {
                                MainActivity act = (MainActivity) mactivity;
                                act.drawerlayout.openDrawer(Gravity.RIGHT);
                            }
                        }

                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return true;
            }
        });
    }

}
