package com.dt.app.widget;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.map.introspect.BasicClassIntrospector.GetterMethodFilter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.dt.app.DTApplication;
import com.dt.app.R;
import com.dt.app.utils.DensityUtil;

/**
 * 富文本
 *
 * @author libin
 */
public class RichTextView extends TextView {

    public RichTextView(Context context) {
        super(context);
    }

    public RichTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RichTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (!TextUtils.isEmpty(text)) {
            super.setText(replace(text), type);
        } else {
            super.setText(text, type);
        }
    }

    private Pattern buildPattern() {
        StringBuilder patternString = new StringBuilder();
        patternString.append('(');
        for (int i = 0; i < DTApplication.getInstance().getmRichText().size(); i++) {
            String s = DTApplication.getInstance().getmRichText().get(i);
            patternString.append(s);
            patternString.append('|');
        }
        patternString.replace(patternString.length() - 1,
                patternString.length(), ")");
        return Pattern.compile(patternString.toString());
    }

    private CharSequence replace(CharSequence text) {
        try {
            SpannableStringBuilder builder = new SpannableStringBuilder(text);
            Pattern pattern = buildPattern();
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                String key = matcher.group().substring(0, 6);
                if (DTApplication.getInstance().getmRichTextPic().containsKey(key)) {
                    int id = DTApplication.getInstance().getmRichTextPic().get(key);
                    if (id == R.mipmap.like_h_red_small) {
//                        BitmapFactory.Options options = new BitmapFactory.Options();
//                        options.inSampleSize = 2;
//                        Bitmap bitmap = BitmapFactory.decodeResource(
//                                getResources(), id, options);
//                        if (bitmap != null) {
//                            ImageSpan span = new ImageSpan(getContext(),bitmap);
//                        }
                        
                        int size = DensityUtil.dip2px(getContext(), 15);
                    	EmojiconSpan  span = new EmojiconSpan(getContext(), R.mipmap.like_h_red_small, size, DynamicDrawableSpan.ALIGN_BASELINE, (int)getTextSize());
                        builder.setSpan(span, matcher.start(), matcher.end(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        
                        if ((matcher.start()-1)>0) {
                        	 builder.insert(matcher.start(), " ");
						}
                        builder.insert(matcher.end()+1, " ");
                    } else {
                        String regx = matcher.group(0);
                        String scoreStr = regx.substring(regx.indexOf(":") + 1, regx.length() - 1);
                        ImageSpan span = new ImageSpan(getContext(), drawCircle(scoreStr));
                        builder.setSpan(span, matcher.start(), matcher.end(),
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        if ((matcher.start()-1)>0) {
                       	 	builder.insert(matcher.start(), " ");
						}
                       builder.insert(matcher.end()+1, " ");
                    }
                }
            }
            return builder;
        } catch (Exception e) {
            return text;
        }
    }

    private Bitmap drawCircle(String number) {
    	int width = DensityUtil.dip2px(getContext(), 25);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor("#f96363"));// 设置颜色
        Bitmap bitmap = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawCircle(width / 2, width / 2, width / 2, paint);
        
       //画文字
        Rect targetRect = new Rect(0, 0, width, width);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.WHITE);
        paint.setTextSize(getResources().getDimension(R.dimen.font_body_12));
        String text = "" + number;
        FontMetricsInt fontMetrics = paint.getFontMetricsInt();
        int baseline = targetRect.top + (targetRect.bottom - targetRect.top - fontMetrics.bottom + fontMetrics.top) / 2 - fontMetrics.top;
        canvas.drawText(text, targetRect.centerX(), baseline, paint);
        return bitmap;
    }
    /**
     * RGB_565：没有alpha透明度，所以背景有黑色边，每个像素都存储在2字节。
     * ARGB_8888：有alpha透明度，每个像素都存储在4字节。
     * @param number
     * @return
     */
    private Bitmap drawableToBitmap(String number) {
        int width = DensityUtil.dip2px(getContext(), 25);
        Bitmap bitmap = Bitmap
                .createBitmap(width, width, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);

        Rect targetRect = new Rect(0, 0, width, width);
        //将图片画在bitmap的画布上
        Paint mPaint = new Paint();
        mPaint.setColor(Color.parseColor("#f96363"));
        mPaint.setAntiAlias(true);
        mPaint.setFilterBitmap(true);

        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(width / 2, width / 2, width / 2, mPaint);
        mPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));

        canvas.drawBitmap(bitmap, targetRect, targetRect, mPaint);
        //画文字
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setColor(Color.WHITE);
        mPaint.setTextSize(getResources().getDimension(R.dimen.font_body_12));
        String text = "" + number;
        FontMetricsInt fontMetrics = mPaint.getFontMetricsInt();
        int baseline = targetRect.top + (targetRect.bottom - targetRect.top - fontMetrics.bottom + fontMetrics.top) / 2 - fontMetrics.top;
        canvas.drawText(text, targetRect.centerX(), baseline, mPaint);
        return bitmap;
    }

    public Bitmap createRoundedCornerBitmap() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap src = BitmapFactory.decodeResource(
                getResources(), R.mipmap.like_h_red_small, options);
        
        final int w = DensityUtil.dip2px(getContext(), 20);
        // 高质量32位图
        Bitmap bitmap = Bitmap.createBitmap(w, w, Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(bitmap);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(0xff424242);
        // 防止边缘的锯齿
        paint.setAntiAlias(true);
        // 用来对位图进行滤波处理
        paint.setFilterBitmap(true);
        Rect rect = new Rect(0, 0, w, w);
//        RectF rectf = new RectF(rect);
//        // 绘制带圆角的矩形
//        canvas.drawRoundRect(rectf, radius, radius, paint);

        // 取两层绘制交集。显示上层
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        // 绘制图像
        canvas.drawBitmap(src, rect, rect, paint);
        return bitmap;
    }
}
