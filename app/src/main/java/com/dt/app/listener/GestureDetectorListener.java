package com.dt.app.listener;

import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.utils.AnimUtils;
/**
 * 	public boolean dispatchTouchEvent(MotionEvent ev) {
		mDetector.onTouchEvent(ev);
		return super.dispatchTouchEvent(ev);
	}
 * @author Administrator
 *
 */
public class GestureDetectorListener implements OnGestureListener{
	private ListView mListView;
	private GestureDetector mDetector;
	public GestureDetector getmDetector() {
		return mDetector;
	}

	public static final int GESTURE_DETECTOR_TIME = 260;

	private Context context;
	
	private Animation inAnimation;
	private Animation outAnimation;
	private int heightHeader;
	
	private float lastDistanceY=0;
	
	private View title;
	/**
	 * distanceY是前一次e2与现在e2值相减，第一次distanceY值不能确定其滑动方向
	 */
	private boolean isMove = true;
	
	private boolean isUpMove = false;
	private boolean isDownMove = false;
	/**
	 * 是否持续移动
	 */
	private boolean isContinueMove = true;

	public void setContinueMove(boolean isContinueMove) {
		this.isContinueMove = isContinueMove;
	}

	public GestureDetectorListener(Context context,ListView mListView,View title) {
		this.context = context;
		this.mListView = mListView;
		mDetector = new GestureDetector(context,this);
		this.title = title;
		
		if (this.title!=null) {
			this.title.setBackgroundColor(context.getResources().getColor(R.color.black));
			heightHeader =  (int) context.getResources().getDimension(R.dimen.actionbar_height2);
//			inAnimation = AnimationUtils.loadAnimation(context, R.anim.title_in_from_top);
//			outAnimation = AnimationUtils.loadAnimation(context, R.anim.title_out_to_top);
			
			inAnimation = AnimUtils.aphlaAnimation(true, GESTURE_DETECTOR_TIME);
			outAnimation = AnimUtils.aphlaAnimation(false, GESTURE_DETECTOR_TIME);

			TextView header = new  TextView(context);
			AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,heightHeader);
			header.setLayoutParams(params);
			header.setBackgroundColor(context.getResources().getColor(R.color.white));
			this.mListView.addHeaderView(header);
		}
		
		//只接受MOVE和UP事件
		this.mListView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction()==MotionEvent.ACTION_UP) {
					isMove = true;
				}
				mDetector.onTouchEvent(event);
				
				return false;
			}
		});
	}

	@Override
	public boolean onDown(MotionEvent arg0) {
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		if (!isMove) {
			if (Math.abs(distanceX) > Math.abs(distanceY)) {
				return false;
			}
			if (Math.abs(distanceY)>0.1) {
				if (distanceY>0) {
					if (listener!=null) {
						if (isContinueMove) {
							listener.scroll(true);
						}else {
							if (!isUpMove) {
								isUpMove = true;
								isDownMove = false;
								listener.scroll(true);
							}
						}
					}
					
					if (!outAnimationFlag && this.title!=null) {
						out();
					}
				}else {
					if (listener!=null) {
						
						if (isContinueMove) {
							listener.scroll(false);
						}else {
							if (!isDownMove) {
								isDownMove = true;
								isUpMove = false;
								listener.scroll(false);
							}
						}
					}
					if (!inAnimationFlag  && this.title!=null) {
						in();
					}
				}
			}
		}else {
			isMove = false;
		}
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		isMove = true;
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}
 
	
	/**
	 * 向上
	 */
	private boolean outAnimationFlag = false;
	/**
	 * 向下
	 */
	private boolean inAnimationFlag = false;
	private void out(){
		outAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				outAnimationFlag = true;
				inAnimationFlag = false;
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				title.setVisibility(View.GONE);
			}
		});
		title.startAnimation(outAnimation);
	}
	private void in(){
//		title.setBackgroundColor(context.getResources().getColor(R.color.transparent));
		inAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				inAnimationFlag = true;
				outAnimationFlag = false;
				title.setVisibility(View.VISIBLE);


			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
				
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
			}
		});
		title.startAnimation(inAnimation);
	}
	
	private ViewScrollListener listener;
	public void setViewScrollListener(ViewScrollListener l){
		this.listener = l;
	}

	/**
	 *
	 * @param view
	 * @param p1
	 * @param p2
     */
	public static void slideview(final View view ,final float p1, final float p2) {
		TranslateAnimation animation = new TranslateAnimation(p1, p2, 0, 0);
		animation.setInterpolator(new OvershootInterpolator());
		animation.setDuration(100);
		animation.setStartOffset(100);
		animation.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				int left = view.getLeft();
				int top = view.getTop()+(int)(p2-p1);
				int width = view.getWidth();
				int height = view.getHeight();
				view.clearAnimation();
				//left top ,right,bottom
				view.layout(0, top,0, 0);
			}
		});
		view.startAnimation(animation);
	}
}
