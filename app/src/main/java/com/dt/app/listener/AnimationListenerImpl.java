package com.dt.app.listener;

import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class AnimationListenerImpl implements AnimationListener{

	@Override
	public void onAnimationStart(Animation animation) {
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		
	}

}
