package com.dt.app.listener;

import java.util.List;

import android.graphics.Bitmap;

public interface HandleCallback<T> {

	public  void onSuccess(T t);
	public  void onSuccess(List<T> t);
	public  void onTakePictrue(Bitmap t);
}
