package com.dt.app.listener;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditChangedListener implements TextWatcher {
	private CharSequence temp;// 监听前的文本
	private int editStart;// 光标开始位置
	private int editEnd;// 光标结束位置
	private final int charMaxNum = 10;
	
	EditText editText;
	TextView textView;
	Context context;
	public EditChangedListener(Context context,EditText editText,TextView textView){
		this.editText = editText;
		this.textView = textView;
		this.context = context;
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		temp = s;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (textView!=null) {
			textView.setText("还能输入" + (charMaxNum - s.length()) + "字符");
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		/** 得到光标开始和结束位置 ,超过最大数后记录刚超出的数字索引进行控制 */
		editStart = editText.getSelectionStart();
		editEnd = editText.getSelectionEnd();
		if (temp.length() > charMaxNum) {
			Toast.makeText(context, "你输入的字数已经超过了限制！", Toast.LENGTH_LONG).show();
			s.delete(editStart - 1, editEnd);
			int tempSelection = editStart;
			editText.setText(s);
			editText.setSelection(tempSelection);
		}

	}
};