package com.dt.app.listener;

import android.graphics.Bitmap;
import android.view.animation.AlphaAnimation;

import com.dt.app.DTApplication;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.bitmap.BitmapCommonUtils;
import com.lidroid.xutils.bitmap.BitmapDisplayConfig;

public class BitmapUtilsConfig {

	private static BitmapDisplayConfig displayConfig = null;
	private static BitmapUtils mBitmapUtils =null;
 
	static {
		
		mBitmapUtils = new BitmapUtils(DTApplication.getInstance().getApplicationContext());
		
		displayConfig = new BitmapDisplayConfig();
		// 显示原始图片,不压缩, 尽量不要使用, 图片太大时容易OOM。
//		displayConfig.setShowOrigional(true);
		displayConfig.setBitmapConfig(Bitmap.Config.RGB_565);
		// 设置图片的最大尺寸, 不设置时更具控件属性自适应
		displayConfig.setBitmapMaxSize(BitmapCommonUtils
				.getScreenSize(DTApplication.getInstance().getApplicationContext()));
		// 实现一个渐变动画。
		AlphaAnimation animation = new AlphaAnimation(0.5f, 1.0f);
		animation.setDuration(500);
		displayConfig.setAnimation(animation);
		
		mBitmapUtils.configDefaultDisplayConfig(displayConfig);
	}

	public static BitmapDisplayConfig getBitmapDisplayConfig() {
		return displayConfig;
	}
	
	
	public static BitmapUtils getBitmapUtils(){
		return mBitmapUtils;
	}
}
