package com.dt.app.listener;

public interface LayoutCallback<T> {
	public void callback(T t);
	
	public void callPadding(int val);
	
	public int callGetHeaderHeight();
}
