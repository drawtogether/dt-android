package com.dt.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.dt.app.R;
import com.dt.app.adapter.PointExchangeAdapter;
import com.dt.app.bean.JifenGood;
import com.dt.app.bean.JifenGood.Goods;
import com.dt.app.bean.Page;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.CommonApis;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.AnimationListenerImpl;
import com.dt.app.listener.GestureDetectorListener;
import com.dt.app.listener.ViewScrollListener;
import com.dt.app.utils.*;
import com.dt.app.utils.PullToRefreshListViewUtils.PullOnRefreshListener;
import com.dt.app.view.CircleImageView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 积分换礼
 *
 * @author libin
 */
public class PointsExchangeFragment2 extends Fragment {

    private PointExchangeAdapter mAdapter;
    private List<Goods> mBeans = new ArrayList<Goods>();


    private PullToRefreshListView mPullToRefreshListView;
    private ListView mListView;
    private Activity context;


    public LinearLayout ll_top_layer;

    PullToRefreshListViewUtils<ListView> mPullToRefreshListViewUtils;

    private View pointHeaderView;
    private CircleImageView iv_user_icon;
    private TextView tv_user_point;
    private Page page;
    private Member member;
    private BitmapUtils mBitmapUtils;
    private Button footer_button ;
    private GestureDetectorListener mDetectorListener;
    private LinearLayout ll_listview_footer;
    private static View chatView;
    public ProgressBar progress_dt;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (chatView != null) {
            ViewGroup parent = (ViewGroup) chatView.getParent();
            LogUtils.e("--------chatview not null----");
            if (parent != null)
                parent.removeView(chatView);
        }
        chatView = inflater.inflate(R.layout.dt_listview_point_exchange, null);
        View rl_main_top_common = chatView.findViewById(R.id.rl_main_top_common);
        ImageView imageView = (ImageView) chatView.findViewById(R.id.dt_main_ad_header);
        imageView.setBackgroundResource(R.mipmap.gift_exchange);
       /* int h_l = DensityUtil.dip2px(context, 30);
        int w_l = DensityUtil.dip2px(context, 171);
        imageView.setLayoutParams(ViewUtils.getLayoutParams(imageView, w_l, h_l));*/
        
        ll_listview_footer = (LinearLayout) chatView.findViewById(R.id.ll_listview_footer);
        mPullToRefreshListView = (PullToRefreshListView) chatView.findViewById(R.id.pull_refresh_list);
        mDetectorListener = new GestureDetectorListener(getActivity(), mPullToRefreshListView.getRefreshableView(), rl_main_top_common);
        ll_top_layer = (LinearLayout) chatView.findViewById(R.id.ll_top_layer);
        progress_dt = (ProgressBar) chatView.findViewById(R.id.progress_dt);

        footer_button = (Button) chatView.findViewById(R.id.dt_pointexchange_rule);
        //ll_top_layer.setBackgroundResource(R.mipmap.point_exchange_logo);
        mPullToRefreshListView.setVisibility(View.GONE);
        pointHeaderView = inflater.inflate(R.layout.dt_point_exchange_header, null);
        iv_user_icon = (CircleImageView) pointHeaderView.findViewById(R.id.iv_user_icon);
        iv_user_icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonAcitivty.startArtistSpaceActivity(context, member.getId());
            }
        });
        tv_user_point = (TextView) pointHeaderView.findViewById(R.id.tv_user_point);

        footer_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityUtils.startNewIntent(context);
            }
        });
        initView();
        return chatView;

    }
    public PointsExchangeFragment2(){

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        mBitmapUtils = new BitmapUtils(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData(1);
    }

    @Override
    public void onResume() {
        if (tv_user_point != null) {
            tv_user_point.setText("" + CommonApis.getUserJifen(context));
        }
        super.onResume();
    }

    private void initView() {
        ll_top_layer.setVisibility(View.VISIBLE);


        mPullToRefreshListViewUtils = new PullToRefreshListViewUtils<ListView>(mPullToRefreshListView);
        mPullToRefreshListViewUtils.init();
        mPullToRefreshListViewUtils.setPullOnRefreshListener(new PullOnRefreshListener() {
            @Override
            public void onPullUpToRefresh(PullToRefreshBase refreshView,
                                          int currentPage) {
                loadData(currentPage);
            }

            @Override
            public void onPullDownToRefresh(PullToRefreshBase refreshView,
                                            int currentPage) {
                loadData(currentPage);
            }
        });
        mAdapter = new PointExchangeAdapter(context, mBeans);
        mListView = mPullToRefreshListView.getRefreshableView();
        mListView.setAdapter(mAdapter);
        mListView.addHeaderView(pointHeaderView);
        mListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

            }
        });

        //pageAnimation();
    }


    private void loadData(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("currentPage", currentPage);
            data.put("pageSize", 10);
            RequestApi.postCommon(getActivity(), Constant.URL.DTGoodsList, data,
                    new ResultLinstener<JifenGood>() {
                        @Override
                        public void onSuccess(JifenGood obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    mBeans.clear();
                                    member = obj.getMember();
                                    mBitmapUtils.display(iv_user_icon, member.getLogo());
                                    String str_jifen = "<b>" + member.getJifenAvailable() + "</b>";
                                    tv_user_point.setText(Html.fromHtml(str_jifen));
                                }
                                mBeans.addAll(obj.getGoodses());
                                page = obj.getPager();
                                mPullToRefreshListViewUtils.setPage(page);
                                mAdapter.notifyDataSetChanged();
                            }
                            mPullToRefreshListViewUtils.onRefreshOrLoadComplete(currentPage);
                            mPullToRefreshListView.setVisibility(View.VISIBLE);
                            ll_listview_footer.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onFailure(String obj) {
                            mPullToRefreshListViewUtils.onRefreshOrLoadComplete(currentPage);

                        }

                        @Override
                        public void onException(String exception) {
                            mPullToRefreshListViewUtils.onRefreshOrLoadComplete(currentPage);

                        }
                    }, new JifenGood());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pageAnimation() {
        Animation translateAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_in_from_top);
        translateAnimation.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                ll_top_layer.startAnimation(AnimUtils.aphlaAnimation(false, 1000, new AnimationListenerImpl() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                        alphaAnimation.setDuration(1000);

                        ll_top_layer.setVisibility(View.GONE);
                        ll_top_layer.clearAnimation();
                    }
                }));
            }
        });
        ll_top_layer.startAnimation(translateAnimation);
    }
}
