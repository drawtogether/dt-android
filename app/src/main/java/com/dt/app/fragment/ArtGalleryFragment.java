package com.dt.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.dt.app.R;
import com.dt.app.adapter.ArtGalleryAdapter;
import com.dt.app.bean.Page;
import com.dt.app.bean.UserWorks;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.GestureDetectorListener;
import com.dt.app.receiver.CommentReceiver;
import com.dt.app.ui.art.ArtistActivity;
import com.dt.app.ui.works.TimeLineActivity;
import com.dt.app.utils.Constant;
import com.library.pullableview.PullableListView;
import com.library.pullableview.listener.PullToRefreshLayout3;
import com.lidroid.xutils.util.LogUtils;
import uk.co.senab.photoview.gestures.GestureDetector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 艺术画廊
 *
 * @author libin
 */
public class ArtGalleryFragment extends Fragment implements PullToRefreshLayout3.OnRefreshListener{

    private ArtGalleryAdapter mAdapter;
    private List<UserWorks.UserWork> mBeans = new ArrayList<UserWorks.UserWork>();
    //private PullToRefreshListView mPullToRefreshListView;
    private PullableListView  pull_refresh_list;
    private PullToRefreshLayout3  prl;
    private Activity context;
    private int currentPage = 1;

    public LinearLayout ll_top_layer;
    //private PullToRefreshListViewUtils<ListView> mListViewUtils;
    private Page page;

    /* 搜索 start */
    private String searchKey;
    private boolean isSearch = false;
    private int tagId;
    /* 搜索   end  */

    private ImageView iv_image_right1;
    private ImageView iv_image_right2;
    public ProgressBar progress_dt;
    private View chatView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (chatView != null) {
            ViewGroup parent = (ViewGroup) chatView.getParent();
            LogUtils.e("--------ArtGalleryFragment not null----");
            if (parent != null)
                parent.removeView(chatView);
        }
        chatView = inflater.inflate(R.layout.dt_art_listview_main, null);

        View rl_main_top_common = chatView.findViewById(R.id.rl_main_top_common);
        prl = (PullToRefreshLayout3) chatView.findViewById(R.id.refresh_view);
        prl.setOnRefreshListener(this);

        pull_refresh_list = (PullableListView) chatView.findViewById(R.id.pull_refresh_list);
        ll_top_layer = (LinearLayout) chatView.findViewById(R.id.ll_top_layer);
        if (!isSearch) {
            ImageView imageView = (ImageView) chatView.findViewById(R.id.dt_main_ad_header);
            imageView.setBackgroundResource(R.mipmap.artgallery);
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    pull_refresh_list.smoothScrollToPosition(0);
                }
            });
            progress_dt = (ProgressBar) chatView.findViewById(R.id.progress_dt);
           // GestureDetectorListener.slideview(pull_refresh_list,0,32);
            //pull_refresh_list.layout(0,300,0,0);
            iv_image_right1 = (ImageView) chatView.findViewById(R.id.iv_image_right1);
            iv_image_right2 = (ImageView) chatView.findViewById(R.id.iv_image_right2);
            iv_image_right1.setVisibility(View.VISIBLE);
            iv_image_right2.setVisibility(View.VISIBLE);
            new GestureDetectorListener(context,pull_refresh_list, rl_main_top_common);
            ll_top_layer.setVisibility(View.VISIBLE);
            pull_refresh_list.setVisibility(View.GONE);


        } else {
            pull_refresh_list.setVisibility(View.VISIBLE);
            rl_main_top_common.setVisibility(View.GONE);
            ll_top_layer.setVisibility(View.GONE);

        }
        initView();

        return chatView;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        if (getArguments() != null) {
            searchKey = getArguments().getString("key");
            tagId = getArguments().getInt("tagId", 0);
            isSearch = true;
            //LogUtils.e("-----------searchkey = "+searchKey+",tagid = "+tagId);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData(1);
    }

    private void initView() {
        if (!isSearch) {
            ll_top_layer.setVisibility(View.VISIBLE);
            iv_image_right1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, ArtistActivity.class));
                }
            });
            iv_image_right2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, TimeLineActivity.class));
                }
            });

        }

        mAdapter = new ArtGalleryAdapter(context, mBeans);
        pull_refresh_list.setAdapter(mAdapter);

    }

    public void loadData(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("currentPage", currentPage);
            data.put("pageSize", 10);
            String urlStr = null;
            if (!isSearch) {
                urlStr = Constant.URL.DTCollectionList;
            } else {
                urlStr = Constant.URL.DTWorksSearch;
                data.put("tagId", tagId);
                data.put("keyword", searchKey);
            }
            LogUtils.e("-------artGalleryFrament------>" + data.toString());
            RequestApi.postCommon(getActivity(), urlStr, data,
                    new ResultLinstener<UserWorks>() {
                        @Override
                        public void onSuccess(UserWorks obj) {
                            if (obj != null) {
                                if (currentPage == 1)
                                    mBeans.clear();
                                mBeans.addAll(obj.getWorkses());
                                page = obj.getPager();
                                //mListViewUtils.setPage(page);
                                mAdapter.notifyDataSetChanged();
                            }
                            pull_refresh_list.setVisibility(View.VISIBLE);

                            onRefreshOrLoadComplete(currentPage);


                        }

                        @Override
                        public void onFailure(String obj) {
                            onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onException(String exception) {
                            onRefreshOrLoadComplete(currentPage);
                        }
                    }, new UserWorks());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void success(long workid, String type) {
        if (CommentReceiver.TYPE_ARTGALLERY.equals(type)) {
            int count = mBeans.size();
            for (int i = 0; i < count; i++) {
                UserWorks.UserWork work = mBeans.get(i);
                if (work.getId() == workid) {
                    isRefreshAdapter = true;
                    mBeans.get(i).setCommentedCount(work.getCommentedCount() + 1);
                    break;
                }
            }
        }
    }

    private boolean isRefreshAdapter = false;

    @Override
    public void onResume() {
        if (isRefreshAdapter) {
            mAdapter.notifyDataSetChanged();
        }
        super.onResume();
    }

    @Override
    public void onRefresh(PullToRefreshLayout3 pullToRefreshLayout) {

        loadData(currentPage);
    }

    @Override
    public void onLoadMore(PullToRefreshLayout3 pullToRefreshLayout) {
        currentPage++;
        if (page!=null) {
            if (page.getTotalPage()<currentPage) {
                onRefreshOrLoadComplete(currentPage);
            }else {
                loadData(currentPage);
            }
        }
    }

    /**
     *  通知
     * @param currentPage
     */
    public void onRefreshOrLoadComplete(final int currentPage){
        new Handler()
        {
            @Override
            public void handleMessage(Message msg)
            {
                // 千万别忘了告诉控件刷新完毕了哦！
                if (currentPage==1) {
                    prl.refreshFinish(PullToRefreshLayout3.SUCCEED);

                }else {
                    prl.loadmoreFinish(PullToRefreshLayout3.SUCCEED);
                }
            }
        }.sendEmptyMessageDelayed(0, 1000);
    }
}
