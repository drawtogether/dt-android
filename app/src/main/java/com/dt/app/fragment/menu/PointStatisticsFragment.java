package com.dt.app.fragment.menu;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.dt.app.utils.ActivityUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.base.BaseFragment;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ToastUtils;

/**
 * 积分统计
 *
 * @author libin
 */
public class PointStatisticsFragment extends BaseFragment {
    private LinearLayout ll_point_history_1;
    private TextView tv_point_history_look;
    private LayoutInflater mInflater;

    @Override
    public void initView(View contentView) {
        ll_point_history_1 = (LinearLayout) findViewById(R.id.ll_point_history_1);
        tv_point_history_look = (TextView) findViewById(R.id.tv_point_history_look);
        tv_point_history_look.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ///ToastUtils.showTextToast(getActivity(), "look");
                ActivityUtils.startNewIntent(getActivity());
            }
        });

    }


    /**
     * 创建线,黑色线高度5dip，灰色线高度0.5dip
     */
    private void createLine(boolean black) {
        float unit = 0.5f;
        int color = R.color.gray;
        if (black) {
            unit = 5;
            color = R.color.black;
        }
        int height = DensityUtil.dip2px(getActivity(), unit);
        TextView textView = new TextView(getActivity());
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height));
        textView.setBackgroundColor(getActivity().getResources().getColor(color));
        ll_point_history_1.addView(textView);
    }

    /**
     * 左边标题，中间隐藏，右边积分
     */
    private void createRightPoint(String title, int point) {
        View view = mInflater.inflate(R.layout.dt_point_statistics_frag_item, null);
        TextView tv_point_history_left = (TextView) view.findViewById(R.id.tv_point_history_left);
        TextView tv_point_history_center = (TextView) view.findViewById(R.id.tv_point_history_center);
        TextView tv_point_history_right = (TextView) view.findViewById(R.id.tv_point_history_right);
        tv_point_history_left.setText(title);
        tv_point_history_right.setText("" + point);
        tv_point_history_center.setVisibility(View.GONE);
        ll_point_history_1.addView(view);
    }

    /**
     * 左边标题，中间数字，右边积分-数字
     */
    private void createRightPoint(String title, int centerPoint, int point) {
        createRightPoint(title, centerPoint, point, false);
    }

    private void createRightPoint(String title, int centerPoint, int point, boolean heart) {
        View view = mInflater.inflate(R.layout.dt_point_statistics_frag_item, null);
        TextView tv_point_history_left = (TextView) view.findViewById(R.id.tv_point_history_left);
        TextView tv_point_history_center = (TextView) view.findViewById(R.id.tv_point_history_center);
        TextView tv_point_history_right = (TextView) view.findViewById(R.id.tv_point_history_right);
        tv_point_history_left.setText(title);
        tv_point_history_center.setText("" + centerPoint);
        tv_point_history_right.setText("积分 " + point);
        if (heart) {
            Drawable drawable = getActivity().getResources().getDrawable(R.mipmap.like_h_red);
            int width = DensityUtil.dip2px(getActivity(), 18);
            drawable.setBounds(0, 0, width, width);//第一0是距左边距离，第二0是距上边距离，40分别是长宽
            tv_point_history_left.setCompoundDrawables(null, null, drawable, null);//
        }
        ll_point_history_1.addView(view);
    }


    public PointStatisticsFragment() {

    }

    @Override
    public void initData(Bundle savedInstanceState) {
        loadData();
    }

    @Override
    public View setContentView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.dt_point_statistics_frag_main, null);
        mInflater = LayoutInflater.from(getActivity());
        return view;
    }

    public void loadData() {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            RequestApi.postCommon(getActivity(), Constant.URL.DTJifenStat, data,
                    new ResultLinstener<String>() {
                        @Override
                        public void onSuccess(String obj) {
                        }

                        @Override
                        public void onFailure(String obj) {
                        }

                        @Override
                        public void onException(String exception) {
                            try {
                                JSONObject jsonObject = new JSONObject(exception);
                                if (jsonObject.has("data")) {
                                    JSONObject dataJsonObject = jsonObject.getJSONObject("data");
                                    int jifenAvailable = dataJsonObject.getInt("jifenAvailable");
                                    int jifenUsed = dataJsonObject.getInt("jifenUsed");
                                    int jifenTotal = dataJsonObject.getInt("jifenTotal");
                                    //头部
                                    createRightPoint("目前可用积分", jifenAvailable);

                                    JSONArray mArray = dataJsonObject.getJSONArray("jifenUsedDetail");
                                    if (mArray != null && mArray.length() > 0) {
                                        for (int i = 0; i < mArray.length(); i++) {
                                            createCenterLayout(mArray.getJSONObject(i));
                                        }
                                    }
//                                    Iterator<String> iterator = dataJsonObject.keys();
//                                    while (iterator.hasNext()) {
//                                        String keyString = iterator.next();
//                                        if ("jifenAvailable".equals(keyString) || "jifenUsed".equals(keyString) || "jifenTotal".equals(keyString)) {
//                                            continue;
//                                        }
//                                        createCenterLayout(dataJsonObject, keyString);
//                                    }

                                    //尾部
                                    createLine(true);
                                    createRightPoint("总的积分", jifenTotal);
                                    createLine(false);
                                    createRightPoint("已用积分", jifenUsed);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new String());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createCenterLayout(JSONObject jsonObject) {
        try {
            createLine(false);
//            JSONObject uploadJsonObject = jsonObject.getJSONObject(key);
            createRightPoint(jsonObject.getString("ruleName"), jsonObject.getInt("totalCount"), jsonObject.getInt("jifen"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
