package com.dt.app.fragment.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.view.View;
import android.widget.ListAdapter;

import com.dt.app.R;
import com.dt.app.adapter.PointHistoryAdapter;
import com.dt.app.base.BaseListFragment;
import com.dt.app.bean.PointOrder;
import com.dt.app.bean.PointOrder.Orders;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;

/**
 * 换礼记录
 *
 * @author libin
 */
public class PointHistoryFragment extends BaseListFragment {
    private PointHistoryAdapter mAdapter;
    private List<Orders> mBeans = new ArrayList<Orders>();

    @Override
    public void initLoadData() {
        loadData(1, 0);
    }

    @Override
    public void onCreateView(View view) {
        fl_listview_bg.setBackgroundColor(getResources().getColor(R.color.black));
        mPullToRefreshListView.getRefreshableView().setDivider(mContext.getResources().getDrawable(R.drawable.drawable_gray));
        mPullToRefreshListView.getRefreshableView().setDividerHeight(DensityUtil.dip2px(mContext, 1) / 2);
        mPullToRefreshListView.setPullLoadEnabled(true);
        mPullToRefreshListView.setPullRefreshEnabled(false);
    }


    @Override
    public ListAdapter setAdapter() {
        mAdapter = new PointHistoryAdapter(mContext, mBeans);
        return mAdapter;
    }


    @Override
    public void initParameters() {
        mainLayoutId = R.layout.dt_listview_main2;
    }

    @Override
    public void onRefrsh(int currentPage) {
        loadData(currentPage, 0);
    }

    @Override
    public void onLoad(int currentPage) {
        loadData(currentPage, 0);
    }

    public void loadData(final int currentPage, int orderStatus) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("orderStatus", orderStatus);
            data.put("currentPage", currentPage);
            RequestApi.postCommon(getActivity(), Constant.URL.DTJifenOrders, data,
                    new ResultLinstener<PointOrder>() {
                        @Override
                        public void onSuccess(PointOrder obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    mBeans.clear();
                                    if (page == null) {
                                        page = obj.getPager();
                                    }
                                    mListViewUtils.setPage(page);
                                }
                                mBeans.addAll(obj.getOrders());
                                mAdapter.notifyDataSetChanged();
                            }
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }
                    }, new PointOrder());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
