package com.dt.app.fragment.artist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.R.integer;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;

import com.dt.app.adapter.ArtistWorkAdapter;
import com.dt.app.base.BaseListFragment;
import com.dt.app.bean.ArtistPerson;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;

public class ArtistWorkFragment extends BaseListFragment {

    private ArtistWorkAdapter mAdapter;
    private List<UserWork> mBeans = new ArrayList<UserWork>();
    private long memberId;
    private boolean isSelf=false;//是否是自己
    public ArtistWorkFragment() {

    }

    public ArtistWorkFragment(long memberId) {
        this.memberId = memberId;
    }


    @Override
    public void onRefrsh(int currentPage) {
        loadFirstData();
    }

    @Override
    public void onLoad(int currentPage) {

        loadData(currentPage);
    }

    @Override
    public void initLoadData() {
        loadFirstData();
    }

    @Override
    public void onCreateView(View view) {
        mListViewUtils.init(true, false);
    }

    @Override
    public void initParameters() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public ListAdapter setAdapter() {
        if (memberId == PreferencesUtils.getLong(getActivity(), Constant.PrefrencesPt.DTid)){
            //个人空间
            mAdapter = new ArtistWorkAdapter(getActivity(), mBeans,true);
        }else {
            mAdapter = new ArtistWorkAdapter(getActivity(), mBeans,false);
        }
        return mAdapter;
    }

    public void loadFirstData() {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("memberId", memberId);
            RequestApi.postCommon(getActivity(), Constant.URL.DTArtistHome, data,
                    new ResultLinstener<ArtistPerson>() {
                        @Override
                        public void onSuccess(ArtistPerson obj) {
                            if (obj != null) {
                                mBeans.clear();
                                mAdapter.setMemberBean(obj.getMember());
                                if (mCallback != null) {
                                    mCallback.callback(obj);
                                }
                                mBeans.addAll(obj.getWorkses());
                                page = obj.getPager();
                                mAdapter.notifyDataSetChanged();
                            }
                            mListViewUtils.onLoadComplete();
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onLoadComplete();
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onLoadComplete();
                        }
                    }, new ArtistPerson());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadData(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("memberId", memberId);
            data.put("currentPage", currentPage);
            ToastUtils.showDtDialog(getActivity());
            RequestApi.postCommon(getActivity(), Constant.URL.DTWorksList, data,
                    new ResultLinstener<ArtistPerson>() {
                        @Override
                        public void onSuccess(ArtistPerson obj) {
                            if (obj != null) {
                                mBeans.addAll(obj.getWorkses());
                                page = obj.getPager();
                                mListViewUtils.setPage(page);
                                mAdapter.notifyDataSetChanged();
                            }
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                            ToastUtils.cancelProgressDt();
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                            ToastUtils.cancelProgressDt();
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                            ToastUtils.cancelProgressDt();
                        }
                    }, new ArtistPerson());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void success(long workid, String type) {
        int count = mBeans.size();
        for (int i = 0; i < count; i++) {
            UserWorks.UserWork work = mBeans.get(i);
            if (work.getId() == workid) {
                isRefreshAdapter = true;
                mBeans.get(i).setCommentedCount(work.getCommentedCount() + 1);
                break;
            }
        }
    }
    public void editSuccess(long workid, String title,String content,int postion) {
    	try {
			if (postion>0 && postion<mBeans.size()) {
				UserWorks.UserWork work = mBeans.get(postion);
				work.setTitle(title);
				work.setContent(content);
				mBeans.set(postion, work);
				isRefreshAdapter = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
    }

    private boolean isRefreshAdapter = false;

    @Override
    public void onResume() {
        if (isRefreshAdapter) {
            mAdapter.notifyDataSetChanged();
        }
        super.onResume();
    }
}
