package com.dt.app.fragment.works;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.dt.app.R;
import com.dt.app.adapter.PaintMutilAdapter;
import com.dt.app.base.BaseFragment;
import com.dt.app.bean.Page;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.works.CollectionPaintActivity;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ToastUtils;
import com.dt.app.utils.Utils;
import com.lidroid.xutils.util.LogUtils;
import com.waterfall.widget.XListView;
import com.waterfall.widget.XListView.IXListViewListener;

/**
 * 收藏画-两列
 *
 * @author libin
 */
public class PaintMutilFragment extends BaseFragment implements
        IXListViewListener {
    private XListView water_list;
    private PaintMutilAdapter mutilAdapter;

    private List<UserWork> mBeans = new ArrayList<UserWork>();

    public List<UserWork> getmBeans() {
        return mBeans;
    }

    private int downX;
    private int downY;
    private int mTouchSlop;
    private String beforeUpdateTime;

    private int year;
    private int month;

    private Page page;
    private int currentPage = 1;

    public PaintMutilFragment() {

    }

    public PaintMutilFragment(int year, int month) {
        this.year = year;
        this.month = month;
    }

    @Override
    public void initView(View contentView) {
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop();
        water_list = (XListView) findViewById(R.id.water_list);
        water_list.setPullLoadEnable(true);
        water_list.setPullRefreshEnable(true);

        water_list.setXListViewListener(this);

        beforeUpdateTime = Utils.getCurrentDate();
        water_list.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = (int) event.getX();
                        downY = (int) event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        int x = (int) (event.getX() - downX);
                        int y = (int) (event.getY() - downY);
                        if (Math.abs(y) > mTouchSlop && Math.abs(y) > Math.abs(x)) {
                            return false;
                        } else {
                            return true;
                        }
                }
                return false;
            }
        });
        mutilAdapter = new PaintMutilAdapter(getActivity(), mBeans, true);
        water_list.setAdapter(mutilAdapter);
 
        LayoutAnimationController lac = AnimationUtils.loadLayoutAnimation(context, R.anim.list_anim_alpha_layout);
        lac.setOrder(LayoutAnimationController.ORDER_NORMAL);
        water_list.setLayoutAnimation(lac);

    }

    @Override
    public void initData(Bundle savedInstanceState) {
        loadData();
    }

    @Override
    public View setContentView(final LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.dt_collection_paint_mutil, null);
        return view;
    }

    /**
     * load data.....
     */
    public void loadData() {
        water_list.setRefreshTime(beforeUpdateTime);
        beforeUpdateTime = Utils.getCurrentDate();
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("currentPage", currentPage);
            data.put("pageSize", 10);
            data.put("year", year);
            String urlStr = Constant.URL.DTTimelineList;
            RequestApi.postCommon(getActivity(), urlStr, data,
                    new ResultLinstener<UserWorks>() {
                        @Override
                        public void onSuccess(UserWorks obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    mBeans.clear();
                                    water_list.stopRefresh();
                                }
                                mBeans.addAll(obj.getWorkses());
                                page = obj.getPager();
                                mutilAdapter.notifyDataSetChanged();
                                
                                water_list.stopLoadMore();
                                 
                            }else {
                            	water_list.stopRefresh();
								water_list.stopLoadMore();
							}
                            callCollection();
                        }
                        	

                        @Override
                        public void onFailure(String obj) {
                        	callCollection();
                        }

                        @Override
                        public void onException(String exception) {
                        	callCollection();
                        }
                    }, new UserWorks());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void callCollection(){
    	try {
    		((CollectionPaintActivity)getActivity()).hideLayer();
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    @Override
    public void onRefresh() {
        currentPage = 1;
        loadData();
    }

    @Override
    public void onLoadMore() {

        currentPage += 1;
        LogUtils.e("-----onLoadMore----mCurrentPage--->>> "+currentPage);
        LogUtils.e("-----onLoadMore----page.getTotalPage()--->>> "+page.getTotalPage());
        if (currentPage > page.getTotalPage()) {
//            ToastUtils.showTextToast(getActivity(), "没有数据");
            water_list.stopLoadMore();
        } else {
            loadData();
        }
    }

}
