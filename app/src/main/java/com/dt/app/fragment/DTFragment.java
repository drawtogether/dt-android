package com.dt.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dt.app.R;
import com.lidroid.xutils.BitmapUtils;

public final class DTFragment extends Fragment {
    private static final String KEY_CONTENT = "DTFragment";

    private String mContent = "";
    private ImageView mImage;
    private BitmapUtils bitmapUtils;
    private String mUrl;

    public static DTFragment newInstance(String content) {
        DTFragment fragment = new DTFragment();
        fragment.mUrl = content;
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dt_guidelogin_img, null);
        mImage = (ImageView) view.findViewById(R.id.welcome_guid_img);
        bitmapUtils = getInstance();
        bitmapUtils.display(mImage, mUrl);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }

    /**
     * BitmapUtils
     *
     * @return
     */
    public BitmapUtils getInstance() {
        if (bitmapUtils == null) {
            bitmapUtils = new BitmapUtils(getActivity());
        }
        return bitmapUtils;
    }
}
