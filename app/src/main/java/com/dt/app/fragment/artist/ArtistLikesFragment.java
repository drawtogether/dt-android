package com.dt.app.fragment.artist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.view.View;
import android.widget.ListAdapter;

import com.dt.app.adapter.EveryDailyAdapter;
import com.dt.app.base.BaseListFragment;
import com.dt.app.bean.Page;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.menu.MyLoveActivity;
import com.dt.app.utils.Constant;

/**
 * 喜爱作品
 *
 * @author libin
 */
public class ArtistLikesFragment extends BaseListFragment {

    private EveryDailyAdapter mAdapter;
    private List<UserWork> mBeans = new ArrayList<UserWork>();
    private long memberId;

    public ArtistLikesFragment() {

    }

    public ArtistLikesFragment(long memberId) {
        this.memberId = memberId;
    }

    private Page page;


    @Override
    public void onRefrsh(int currentPage) {
        loadData(currentPage);
    }

    @Override
    public void onLoad(int currentPage) {
        loadData(currentPage);
    }

    @Override
    public void initLoadData() {
        loadData(1);
    }

    @Override
    public void onCreateView(View view) {
        mListViewUtils.init(true, false);
    }

    @Override
    public void initParameters() {
    }


    @Override
    public ListAdapter setAdapter() {
        mAdapter = new EveryDailyAdapter(getActivity(), mBeans);
        return mAdapter;
    }


    public void loadData(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("memberId", memberId);
            data.put("isLoadMember", 1);
            data.put("currentPage", currentPage);
            RequestApi.postCommon(getActivity(), Constant.URL.DTWorksLikes, data,
                    new ResultLinstener<UserWorks>() {
                        @Override
                        public void onSuccess(UserWorks obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    mBeans.clear();
                                    if (page == null) {
                                        page = obj.getPager();
                                    }
                                    mListViewUtils.setPage(page);
                                    if (getActivity() instanceof MyLoveActivity) {
                                        ((MyLoveActivity) getActivity()).changeWorkText(page.getTotalCount());
                                    }
                                }
                                mBeans.addAll(obj.getWorkses());
                                page = obj.getPager();
                                mAdapter.notifyDataSetChanged();
                            }
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }
                    }, new UserWorks());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void success(long workid, String type) {
        int count = mBeans.size();
        for (int i = 0; i < count; i++) {
            UserWorks.UserWork work = mBeans.get(i);
            if (work.getId() == workid) {
                isRefreshAdapter = true;
                mBeans.get(i).setCommentedCount(work.getCommentedCount() + 1);
                break;
            }
        }
    }

    private boolean isRefreshAdapter = false;

    @Override
    public void onResume() {
        if (isRefreshAdapter) {
            mAdapter.notifyDataSetChanged();
        }
        super.onResume();
    }
}
