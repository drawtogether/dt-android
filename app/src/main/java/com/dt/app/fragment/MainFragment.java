package com.dt.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.*;
import android.widget.RelativeLayout.LayoutParams;

import com.dt.app.R;
import com.dt.app.adapter.CommonAdapter;
import com.dt.app.adapter.ViewHolder;
import com.dt.app.bean.DTAppHome;
import com.dt.app.bean.Theme;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.listener.AnimationListenerImpl;
import com.dt.app.ui.menu.MainMenuActivity;
import com.dt.app.ui.menu.SearchActivity;
import com.dt.app.ui.menu.UploadActivity;
import com.dt.app.ui.works.EveryAdDayActivity;
import com.dt.app.ui.works.EveryDayUpActivity;
import com.dt.app.utils.*;
import com.dt.app.view.BannerView;
import com.dt.app.view.ImageCycleView;
import com.library.pullableview.PullableListView;
import com.library.pullableview.listener.PullToRefreshLayout2;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.*;

/**
 * mainFragment
 */
public class MainFragment extends Fragment implements OnClickListener, PullToRefreshLayout2.OnRefreshListener {
    //private PullToRefreshListView mPullToRefreshListView;
    private Activity context;

    private String lastUpdateTime;

    private View viewHeader;
    private BannerView mBannerView;
    private LinearLayout ll_banner;
    private int mHeight;
    private int headerHeight;

    //底部
    private LinearLayout ll_main_footer_layout;
    private ImageView iv_main_1_img;
    private ImageView iv_main_2_img;
    private ImageView iv_main_3_img;
    private List<Theme> mDatas = new ArrayList<Theme>();
    private CommonAdapter commonAdapter;
    //private PullToRefreshListViewUtils<ListView> mListViewUtils;
    private PullableListView pullableListView;
    private PullToRefreshLayout2 ptrl;
    private BitmapUtils bimMap;

    @ViewInject(R.id.ll_top_layer)
    private LinearLayout ll_top_layer;
    @ViewInject(R.id.dt_loading_data_draw)
    private ImageView dt_loading_data_draw;
    @ViewInject(R.id.progress_dt)
    private ProgressBar progress_dt;
    private AlphaAnimation alphaOneToZeroAnimation;
    private Timer mTimer;
    private int index = 0;
    private Handler mHandlerTimer;

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 8:

                    if (alphaOneToZeroAnimation != null) {
                        ll_main_footer_layout.startAnimation(alphaOneToZeroAnimation);
                    }
                    break;
                default:
                    break;
            }
        }

        ;
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View chatView = inflater.inflate(R.layout.dt_main_fragment, container, false);
        com.lidroid.xutils.ViewUtils.inject(this, chatView);
        mHeight = ZTDeviceInfo.getInstance().getHeightDevice();
        headerHeight = DensityUtil.dip2px(context, 275);

        ptrl = (PullToRefreshLayout2) chatView.findViewById(R.id.refresh_view);
        pullableListView = (PullableListView) chatView.findViewById(R.id.pull_refresh_list);
        ptrl.setOnRefreshListener(this);


        ll_main_footer_layout = (LinearLayout) chatView.findViewById(R.id.ll_main_footer_layout);


        iv_main_1_img = (ImageView) chatView.findViewById(R.id.iv_main_1_img);
        iv_main_2_img = (ImageView) chatView.findViewById(R.id.iv_main_2_img);
        iv_main_3_img = (ImageView) chatView.findViewById(R.id.iv_main_3_img);
        initView();
        loadData();
        dt_loading_data_draw.setImageResource(R.mipmap.dt_loading_main_small);


        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                index++;
                Message message = new Message();
                message.what = index;
                mHandlerTimer.sendMessage(message);
            }
        }, 100, 30);

        mHandlerTimer = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                progress_dt.setProgress(msg.what);
                if (index >= 100) {
                    ll_top_layer.setVisibility(View.GONE);
                    index = 0;
                    mTimer.cancel();
                    mHandlerTimer = null;
                }
                super.handleMessage(msg);
            }
        };
        return chatView;

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    /**
     * initView
     */
    private void initView() {
        iv_main_1_img.setOnClickListener(this);
        iv_main_2_img.setOnClickListener(this);
        iv_main_3_img.setOnClickListener(this);
        bimMap = new BitmapUtils(context);

        viewHeader = context.getLayoutInflater().inflate(R.layout.dt_main_header, null);

        viewHeader.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, headerHeight));
        ImageView imageView = (ImageView) viewHeader.findViewById(R.id.dt_main_ad_header);
        int i_h = DensityUtil.dip2px(context, 30);
        int i_w = DensityUtil.dip2px(context, 230);
        RelativeLayout.LayoutParams layoutParams = (LayoutParams) ViewUtils.getLayoutParams(imageView, i_w, i_h);
        imageView.setLayoutParams(layoutParams);
        imageView.setBackgroundResource(R.mipmap.daily_artist);

        ll_banner = (LinearLayout) viewHeader.findViewById(R.id.ll_banner);

        // mListViewUtils = new PullToRefreshListViewUtils<ListView>(mPullToRefreshListView);
        lastUpdateTime = Utils.getCurrentDate();

//        mHeight = mHeight - DensityUtil.dip2px(context, 25);
        pullableListView.addHeaderView(viewHeader);
        commonAdapter = new CommonAdapter<Theme>(context, mDatas, R.layout.dt_main_fragment_item) {
            @Override
            public void convert(ViewHolder helper, final Theme item) {
                if (helper.getConvertView() != null) {
                    helper.getConvertView().setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,
                            mHeight - headerHeight + 2));

                    bimMap.display(helper.getView(R.id.img_theme), item.getPicture());
                    helper.getView(R.id.img_theme).setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent mIntent = new Intent(mContext, EveryAdDayActivity.class);
                            mIntent.putExtra("theme", item);
                            context.startActivity(mIntent);
                            context.overridePendingTransition(R.anim.setting_in_from_bottom, R.anim.setting_apla_to_apla);
                        }
                    });
                }
            }

        };
        pullableListView.setAdapter(commonAdapter);


        alphaOneToZeroAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaOneToZeroAnimation.setDuration(500);
        alphaOneToZeroAnimation.setAnimationListener(new AnimationListenerImpl() {
            @Override
            public void onAnimationEnd(Animation animation) {
                showNoAnimationFooter = true;
                System.out.println("-------》》》 end execute anim ... ");
                ll_main_footer_layout.setVisibility(View.INVISIBLE);
            }
        });
    }


    /**
     * 是否是loadData第一次加载动画
     */
    private boolean isFirstLoadAnim = true;
    /**
     * 前一次动画是否执行完，没有则不能再次执行动画
     */
    private boolean showNoAnimationFooter = false;

    /**
     * loadData
     */
    private void loadData() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        try {
            RequestApi.commonRequest(context, Constant.URL.DTHome, data, new ResultLinstener<DTAppHome>() {
                @Override
                public void onSuccess(DTAppHome obj) {
                    if (obj.getCode() == 1) {
                        //successful
                        new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                // 千万别忘了告诉控件刷新完毕了哦！
                                ptrl.refreshFinish(PullToRefreshLayout2.SUCCEED);
                            }
                        }.sendEmptyMessageDelayed(0, 1000);

                        LogUtils.e("-------refreshFinish---------->");
                        DTAppHome.HomeData homeData = obj.getData();
                        initAds(homeData.getWorksRecommends());
                        if (mDatas != null && mDatas.size() > 0) {
                            if (mDatas.get(0).getId() == homeData.getTheme().getId()
                                    && mDatas.get(0).getPicture().equals(homeData.getTheme().getPicture())) {
                                if (isFirstLoadAnim || showNoAnimationFooter) {
                                    isFirstLoadAnim = false;
                                    System.out.println("----------onfresh---->>>>>>>>>> ");
                                    showFooter();
                                }
                                return;
                            }
                            mDatas.clear();
                        }
                        mDatas.add(homeData.getTheme());
                        commonAdapter.notifyDataSetChanged();

                        if (isFirstLoadAnim || showNoAnimationFooter) {
                            isFirstLoadAnim = false;
                            System.out.println("----------onfresh---->>>>>>>>>> ");
                            showFooter();
                        }

                    }
                }

                @Override
                public void onFailure(String obj) {
                    ptrl.refreshFinish(PullToRefreshLayout2.SUCCEED);
                }

                @Override
                public void onException(String exception) {
                    ptrl.refreshFinish(PullToRefreshLayout2.SUCCEED);
                }
            }, new DTAppHome());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * initAds
     *
     * @param worksRecommends
     */
    private void initAds(List<DTAppHome.WorksRecommendData> worksRecommends) {

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< vinky add start>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        ImageCycleView imageCycleViewPager = new ImageCycleView(context);
        ArrayList<ImageCycleView.ImageInfo> mBannerList = new ArrayList<ImageCycleView.ImageInfo>();
        for (DTAppHome.WorksRecommendData worksRecommend : worksRecommends) {
            mBannerList.add(new ImageCycleView.ImageInfo(worksRecommend.getImageUrl(), "", ""));
        }
        ll_banner.addView(imageCycleViewPager);
        imageCycleViewPager.loadData(mBannerList, new ImageCycleView.LoadImageCallBack() {
            @Override
            public ImageView loadAndDisplay(ImageCycleView.ImageInfo imageInfo) {
                BitmapUtils bitmapUtils = new BitmapUtils(context);
                ImageView imageView = new ImageView(context);
                bitmapUtils.display(imageView, imageInfo.image.toString());
                return imageView;
            }
        });
        imageCycleViewPager.setOnPageClickListener(new ImageCycleView.OnPageClickListener() {
            @Override
            public void onClick(View imageView, ImageCycleView.ImageInfo imageInfo) {
                Intent mIntent = new Intent(getActivity(), EveryDayUpActivity.class);
                context.startActivity(mIntent);
                context.overridePendingTransition(R.anim.setting_in_from_bottom, R.anim.setting_apla_to_apla);
                LogUtils.e("--------setOnPageClickListener----------");
            }
        });

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<< vinky add end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<< vinky comment start >>>>>>>>>>>>>>>>>>>>>>>>>>>>
//        List<Photo> taskAds = new ArrayList<Photo>();
//        for (DTAppHome.WorksRecommendData worksRecommend : worksRecommends) {
//            taskAds.add(new Photo(worksRecommend.getImageUrl(), worksRecommend.getWorksId()));
//        }
//        mBannerView = new BannerView(context, taskAds, BannerView.FIRST_ADS);
//        ll_banner.addView(mBannerView.initView());
//        mBannerView.getAdapter().setOnPageClickListener(new OnPageClickListener() {
//            @Override
//            public void onClick(Photo banner, int position) {
//                Intent mIntent = new Intent(getActivity(), EveryDayUpActivity.class);
//                context.startActivity(mIntent);
//                context.overridePendingTransition(R.anim.setting_in_from_bottom, R.anim.setting_apla_to_apla);
//            }
//        });
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< vinky comment end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    }

    /**
     * showFooter
     */
    private void showFooter() {
        System.out.println("-------》》》 start execute anim ... " + showNoAnimationFooter + " " + isFirstLoadAnim);
        showNoAnimationFooter = false;
        ll_main_footer_layout.setVisibility(View.VISIBLE);
        mHandler.sendEmptyMessageDelayed(8, 5500);
        LayoutAnimationController lac = AnimationUtils.loadLayoutAnimation(context, R.anim.list_anim_layout);
        lac.setOrder(LayoutAnimationController.ORDER_NORMAL);
        //lac.setDelay(0.5f);
        ll_main_footer_layout.setLayoutAnimation(lac);
    }

    @Override
    public void onDestroy() {
        try {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        } catch (Exception e) {
        }
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_main_1_img:
                context.startActivity(new Intent(getActivity(), MainMenuActivity.class));
                context.overridePendingTransition(R.anim.setting_in_from_bottom, R.anim.setting_apla_to_apla);
                break;
            case R.id.iv_main_2_img:
                Intent mIntent = new Intent(getActivity(), SearchActivity.class);
                mIntent.putExtra("cancelhidden", true);
                context.startActivity(mIntent);
                context.overridePendingTransition(R.anim.setting_in_from_bottom, R.anim.setting_apla_to_apla);
                break;
            case R.id.iv_main_3_img:
                context.startActivity(new Intent(getActivity(), UploadActivity.class));
                context.overridePendingTransition(R.anim.setting_in_from_bottom, R.anim.setting_apla_to_apla);
                break;
            default:
                break;
        }
    }

    public void showNoAnimationFooter() {
        mHandler.removeCallbacksAndMessages(null);
        ll_main_footer_layout.setVisibility(View.VISIBLE);
        mHandler.sendEmptyMessageDelayed(8, 5500);
    }


    @Override
    public void onResume() {
        try {
//            mBannerView.onResume();
            if (showNoAnimationFooter) {
                showNoAnimationFooter();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onRefresh(PullToRefreshLayout2 pullToRefreshLayout) {
//        if (isFirstLoadAnim || showNoAnimationFooter) {
//            isFirstLoadAnim = false;
//            System.out.println("----------onfresh---->>>>>>>>>> ");
//            showFooter();
//        }
        loadData();
    }

    @Override
    public void onLoadMore(PullToRefreshLayout2 pullToRefreshLayout) {
        ptrl.loadmoreFinish(PullToRefreshLayout2.SUCCEED);
    }

    @Override
    public void onShowMenu(PullToRefreshLayout2 pullToRefreshLayout) {
        if (isFirstLoadAnim || showNoAnimationFooter) {
            isFirstLoadAnim = false;
            System.out.println("----------onfresh---->>>>>>>>>> ");
            showFooter();
        }
    }

}
