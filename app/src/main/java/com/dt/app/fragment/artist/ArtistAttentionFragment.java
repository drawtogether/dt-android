package com.dt.app.fragment.artist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListAdapter;

import com.dt.app.R;
import com.dt.app.adapter.ArtistAttentionAdapter;
import com.dt.app.base.BaseListFragment;
import com.dt.app.bean.UserAttention;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.menu.MyLoveActivity;
import com.dt.app.utils.Constant;

/**
 * 关注
 *
 * @author libin
 */
public class ArtistAttentionFragment extends BaseListFragment {
    private long memberId;
    private int picCnt = 10;
    private boolean love = false;
    private ArtistAttentionAdapter mAdapter;
    private List<UserAttention.MemberAttention> mBeans = new ArrayList<UserAttention.MemberAttention>();

    public ArtistAttentionFragment() {
    }
    public static ArtistAttentionFragment newInstance(long memberId) {
    	return newInstance(memberId, false);
    }
    public static ArtistAttentionFragment newInstance(long memberId,boolean love) {
        ArtistAttentionFragment array = new ArtistAttentionFragment();
        Bundle args = new Bundle();
        args.putLong("memberId", memberId);
        args.putBoolean("love", love);
        array.setArguments(args);
        return array;
    }

    @Override
    public void onRefrsh(int currentPage) {
        loadData(currentPage);
    }

    @Override
    public void onLoad(int currentPage) {
        loadData(currentPage);
    }

    @Override
    public void initLoadData() {
        loadData(1);
    }

    @Override
    public void onCreateView(View view) {
        memberId = getArguments() != null ? getArguments().getLong("memberId")
                : -1;
        love =  getArguments() != null ? getArguments().getBoolean("love")
                : false;
        mListViewUtils.init(true, false);
        if(love){
        	mPullToRefreshListView.setBackgroundColor(Color.WHITE);
        }else {
         
//        		View view2 = LayoutInflater.from(mContext).inflate(R.layout.dt_attention_listview, null);
//    			mPullToRefreshListView.getRefreshableView().addHeaderView(view2);
//            mAdapter = new ArtistAttentionAdapter(getActivity(), mBeans);
		}
    }

    @Override
    public void initParameters() {
    }

    @Override
    public ListAdapter setAdapter() {
    	//add headerView
    	mAdapter = new ArtistAttentionAdapter(getActivity(), mBeans);
        return mAdapter;
    }

    public void loadData(final int currentPage) {
        try {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("memberId", memberId);
            data.put("currentPage", currentPage);
            data.put("picCnt", picCnt);
            RequestApi.postCommon(getActivity(), Constant.URL.DTSnsFollows,
                    data, new ResultLinstener<UserAttention>() {
                        @Override
                        public void onSuccess(UserAttention obj) {
                            if (obj != null) {
                                if (currentPage == 1) {
                                    mBeans.clear();
                                    page = obj.getPager();
                                    mListViewUtils.setPage(page);
                                    if (getActivity() instanceof MyLoveActivity) {
                                        ((MyLoveActivity) getActivity()).changeArtistText(page.getTotalCount());
                                    }
                                    if (obj.getMembers()==null || obj.getMembers().size()==0) {
                                    	mPullToRefreshListView.getFooterLoadingLayout().show(false);
    								}
                                }
                                
                                mAdapter.setMe(obj.getMe());
                                mBeans.addAll(obj.getMembers());
                                mAdapter.notifyDataSetChanged();
                            }
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onFailure(String obj) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }

                        @Override
                        public void onException(String exception) {
                            mListViewUtils.onRefreshOrLoadComplete(currentPage);
                        }
                    }, new UserAttention());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
