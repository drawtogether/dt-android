package com.dt.app.fragment.artist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.dt.app.R;
import com.dt.app.adapter.CommonAdapter;
import com.dt.app.adapter.ViewHolder;
import com.dt.app.base.BaseFragment;
import com.dt.app.bean.Artist;
import com.dt.app.bean.Page;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.ui.art.ArtistActivity;
import com.dt.app.ui.works.WorkLovesActivity;
import com.dt.app.utils.Constant;
import com.dt.app.utils.ToastUtils;
import com.dt.app.view.PullToRefreshView;
import com.dt.app.view.PullToRefreshView.OnFooterRefreshListener;
import com.dt.app.view.PullToRefreshView.OnHeaderRefreshListener;

public class AllArtistFragment extends BaseFragment {
    //粉丝
    public static final int SNS_FANS = 10;
    public long memberId;

    private GridView gv_artist_all;
    private int type;

    private int pageSize = 24;
    private int currentPage = 1;
    private String keyword;
    private String category;
    private String[] cates = new String[]{"all", "new", "followed", "random",""};

//	category  String分类：all - 所有，new - 最新加入，followed - 我关注的(需用户标识mkey参数)，random - 随机看看
//	keyword	 String 搜索关键字

    private List<Member> members;
    private CommonAdapter<Member> mAdapter;
    public Page page;
    private PullToRefreshView main_pull_refresh_view;

    @Override
    public void initView(View contentView) {
        gv_artist_all = (GridView) findViewById(R.id.pull_refresh_list);
        main_pull_refresh_view = (PullToRefreshView) findViewById(R.id.main_pull_refresh_view);

        type = getArguments() != null ? getArguments().getInt("type") : -1;
        if (type != SNS_FANS) {
            category = cates[type];
        } else {
            memberId = getArguments() != null ? getArguments().getLong("memberId") : -1;
        }

        gv_artist_all.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {
                    Member member = members.get(position);
//					 if (type == SNS_FANS) {
                    CommonAcitivty.startArtistSpaceActivity(getActivity(), member.getId());
//		             }

                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        });

        main_pull_refresh_view.setOnHeaderRefreshListener(new OnHeaderRefreshListener() {
            @Override
            public void onHeaderRefresh(PullToRefreshView view) {
                currentPage = 1;
                members.clear();
                loadData();
            }
        });

        main_pull_refresh_view.setOnFooterRefreshListener(new OnFooterRefreshListener() {
            @Override
            public void onFooterRefresh(PullToRefreshView view) {
                if (page == null) {
                    return;
                }
                currentPage++;
                if (page.getTotalPage() < currentPage) {
                    main_pull_refresh_view.onFooterRefreshComplete();
//                    ToastUtils.showTextToast(context, "没有数据了");
                } else {
                    loadData();
                }
            }
        });
    }

    public AllArtistFragment() {

    }

    public static AllArtistFragment newInstance(int type) {
        return newInstance(type, 0);
    }

    public static AllArtistFragment newInstance(int type, long memberId) {
        AllArtistFragment array = new AllArtistFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        args.putLong("memberId", memberId);
        array.setArguments(args);
        return array;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        members = new ArrayList<UserWorks.Member>();
        mAdapter = new CommonAdapter<Member>(context, members, R.layout.artist_all_fragment_item, false) {
            @Override
            public void convert(ViewHolder helper, final Member item) {
                helper.getView(R.id.tv_artist_name).setVisibility(View.VISIBLE);
                helper.setText(R.id.tv_artist_name, item.getNickname());
                if (type == SNS_FANS) {
                	((TextView)helper.getView(R.id.tv_artist_name)).setTextColor(getResources().getColor(R.color.white));
                }
                mBitmapUtils.display(helper.getView(R.id.iv_artist_all), item.getLogo());
            }
        };
        gv_artist_all.setAdapter(mAdapter);
        if (type!=4) {
        	loadData();
		}
    }

    @Override
    public View setContentView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.artist_all_fragment_main, null);
        return view;
    }

    private void loadData() {
        try {

            Map<String, Object> data = new HashMap<String, Object>();
            String urlStr = null;
            if (type != SNS_FANS) {
                if (!TextUtils.isEmpty(keyword)) {
                    data.put("keyword", keyword);
                } else {
                    data.put("category", category);
                }
                urlStr = Constant.URL.DTCollectionArtists;
            } else {
                data.put("memberId", memberId);
                urlStr = Constant.URL.DTSnsFans;
                main_pull_refresh_view.setBackgroundColor(getResources().getColor(R.color.black));
                gv_artist_all.setSelector(getResources().getDrawable(R.drawable.dt_black_selector));
            }

            data.put("currentPage", currentPage);
            data.put("pageSize", pageSize);

            RequestApi.postCommon(getActivity(), urlStr, data, new ResultLinstener<Artist>() {
                @Override
                public void onSuccess(Artist obj) {
                    if (obj != null) {
                        if (currentPage == 1) {
                            members.clear();
                            page = obj.getPager();
                            try {
                                if (getActivity() instanceof ArtistActivity) {
                                    ArtistActivity artistActivity = ((ArtistActivity) getActivity());
                                    if (artistActivity.isFirstArtistCount) {
                                        artistActivity.setArtistCount(page.getTotalCount());
                                        artistActivity.isFirstArtistCount = false;
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        members.addAll(obj.getMembers());
                        mAdapter.notifyDataSetChanged();
                        if (currentPage == 1) {
                            main_pull_refresh_view.onHeaderRefreshComplete();
                        } else {
                            main_pull_refresh_view.onFooterRefreshComplete();
                        }
                    }
                }

                @Override
                public void onFailure(String obj) {
                    if (currentPage == 1) {
                        main_pull_refresh_view.onHeaderRefreshComplete();
                    } else {
                        main_pull_refresh_view.onFooterRefreshComplete();
                    }
                }

                @Override
                public void onException(String exception) {
                    if (currentPage == 1) {
                        main_pull_refresh_view.onHeaderRefreshComplete();
                    } else {
                        main_pull_refresh_view.onFooterRefreshComplete();
                    }
                }
            }, new Artist());
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

//    public void clearKeyword() {
//        keyword = null;
//        currentPage = 1;
//        loadData();
//    }

    public void searchKeyword(String keyword) {
        this.keyword = keyword;
        currentPage = 1;
        loadData();
    }

}
