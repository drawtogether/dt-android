package com.dt.app.fragment.works;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;

import com.dt.app.R;
import com.dt.app.adapter.PaintMutilAdapter;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.utils.PullToRefreshListViewUtils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

/**
 * 收藏画-单列
 *
 * @author libin
 */
public class PaintOneFragment extends Fragment {

    private PaintMutilAdapter mAdapter;
    private List<UserWork> mBeans = new ArrayList<UserWork>();
    private PullToRefreshListView mPullToRefreshListView;
    private PullToRefreshListViewUtils<ListView> mListViewUtils;
    private Activity context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View chatView = inflater.inflate(R.layout.dt_listview_main, null);
        mPullToRefreshListView = (PullToRefreshListView) chatView
                .findViewById(R.id.pull_refresh_list);
        chatView.findViewById(R.id.rl_title_bar).setVisibility(View.GONE);
        chatView.findViewById(R.id.fl_listview_bg).setBackgroundColor(getResources().getColor(R.color.black));
        initView();
        return chatView;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }


    private void initView() {
        mListViewUtils = new PullToRefreshListViewUtils<ListView>(mPullToRefreshListView);
        mListViewUtils.init(false, false);
        mAdapter = new PaintMutilAdapter(getActivity(), mBeans, false);
        mPullToRefreshListView.setAdapter(mAdapter);
//        mListViewUtils.setPullOnRefreshListener(new PullToRefreshListViewUtils.PullOnRefreshListener() {
//
//            @Override
//            public void onPullDownToRefresh(PullToRefreshBase refreshView, int currentPage) {
//
//            }
//
//            @Override
//            public void onPullUpToRefresh(PullToRefreshBase refreshView, int currentPage) {
//
//            }
//        });
        
        LayoutAnimationController lac = AnimationUtils.loadLayoutAnimation(context, R.anim.list_anim_alpha_layout);
        lac.setOrder(LayoutAnimationController.ORDER_NORMAL);
        mPullToRefreshListView.getRefreshableView().setLayoutAnimation(lac);

    }


    public void setUserWorks(List<UserWork> userWorks) {
        mBeans.clear();
        mBeans.addAll(userWorks);
        mAdapter.notifyDataSetChanged();
    }

}
