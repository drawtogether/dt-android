package com.dt.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import cn.jpush.android.api.JPushInterface;
import com.lidroid.xutils.util.LogUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;

import com.dt.app.bean.DTInitData;
import com.dt.app.utils.ZTDeviceUtil;
import com.qiniu.android.storage.UploadManager;

/**
 * Created by andy on 6/3/14.
 */
public class DTApplication extends Application {
    private static final String TAG = "GameCoffeeApp";
    /**
     * 用于Activity管理和应用程序退出
     */
    private Stack<Activity> activityStack;
    private String versionName;
    private List<DTInitData.DmData> dmDatas;
    private List<DTInitData.LogoData> logoDataList;
    private List<DTInitData.AddressData> addressDataList;
    private List<DTInitData.TagData> tagsDataList;
    /**
     * Singleton pattern
     */
    private static DTApplication instance = null;

    public static DTApplication getInstance() {
        return instance;
    }

    private List<String> mRichText = new ArrayList<String>();
    private Map<String, Integer> mRichTextPic = new HashMap<String, Integer>();
    private UploadManager uploadManager;

//	public UploadManager getUploadManager() {
//		if (uploadManager==null) {
//			uploadManager = new UploadManager(initQiNiuYunSDK());
//		}
//		return uploadManager;
//	}

    @Override
    public void onCreate() {
        /*
         * CrashHandler ch = CrashHandler.getInstance(); ch.init(this);
		 */
        Log.d(TAG, "onCreate");
        super.onCreate();
        instance = this;
        LogUtils.allowE = false;

        // 获取应用的版本号
        try {
            setAppVersionName();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ZTDeviceUtil.initDeviceInfo(this);
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        initFonts();
        initRich();
    }

    private void initRich() {
        mRichText.add("\\x5Bheart\\x5D");
        mRichText.add("\\x5Bjifen:\\d{1,10}\\x5D");

        mRichTextPic.put("[heart", R.mipmap.like_h_red_small);
        mRichTextPic.put("[jifen", R.mipmap.lingxing);
    }

    private void initFonts() {
        try {
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/fzqh.ttf")
                    .setFontAttrId(R.attr.fontPath).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<Activity>();
        }
        activityStack.add(activity);
    }

    public Stack<Activity> getActivity() {
        return activityStack;
    }

    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        Activity activity = activityStack.lastElement();
        return activity;
    }

    /**
     * 结束当前Activity（堆栈中最后一个压入的）
     */
    public void finishActivity() {
        Activity activity = activityStack.lastElement();
        finishActivity(activity);
    }

    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }

    /**
     * 结束指定类名的Activity
     */
    public void finishActivity(Class<?> cls) {
        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                finishActivity(activity);
            }
        }
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = activityStack.size(); i < size; i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    /*
     * 获取当前程序的版本号
     */
    private void setAppVersionName() throws Exception {
        // 获取packagemanager的实例
        PackageManager packageManager = getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = packageManager.getPackageInfo(getPackageName(),
                0);
        versionName = packInfo.versionName;
    }

    public List<DTInitData.LogoData> getLogoDataList() {
        return logoDataList;
    }

    public void setLogoDataList(List<DTInitData.LogoData> logoDataList) {
        this.logoDataList = logoDataList;
    }

    public String getAppVersionName() {
        return versionName;
    }

    public List<DTInitData.AddressData> getAddressDataList() {
        return addressDataList;
    }

    public void setAddressDataList(List<DTInitData.AddressData> addressDataList) {
        this.addressDataList = addressDataList;
    }

    public List<DTInitData.DmData> getDmDatas() {
        return dmDatas;
    }

    public void setDmDatas(List<DTInitData.DmData> dmDatas) {
        this.dmDatas = dmDatas;
    }

    public List<DTInitData.TagData> getTagsDataList() {
        return tagsDataList;
    }

    public void setTagsDataList(List<DTInitData.TagData> tagsDataList) {
        this.tagsDataList = tagsDataList;
    }

    private static int getMemoryCacheSize(Context context) {
        int memoryCacheSize;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            int memClass = ((ActivityManager) context
                    .getSystemService(Context.ACTIVITY_SERVICE))
                    .getMemoryClass();
            memoryCacheSize = (memClass / 8) * 1024 * 1024; // 1/8 of app memory
        } else {
            memoryCacheSize = 2 * 1024 * 1024;
        }
        return memoryCacheSize;
    }

    /**
     * 退出应用程序
     */
    public void appExit(Context context) {
        try {
            finishAllActivity();
            System.exit(0);
        } catch (Exception e) {
        }
    }

    public List<String> getmRichText() {
        return mRichText;
    }

    public Map<String, Integer> getmRichTextPic() {
        return mRichTextPic;
    }


//	private Configuration initQiNiuYunSDK(){
//		Configuration config = new Configuration.Builder()
//        .chunkSize(256 * 1024)  //分片上传时，每片的大小。 默认 256K
//        .putThreshhold(512 * 1024)  // 启用分片上传阀值。默认 512K
//        .connectTimeout(10) // 链接超时。默认 10秒
//        .responseTimeout(60) // 服务器响应超时。默认 60秒
////        .recorder(NULL)  // recorder 分片上传时，已上传片记录器。默认 null
////        .recorder(recorder, keyGen)  // keyGen 分片上传时，生成标识符，用于片记录器区分是那个文件的上传记录
////        .zone(Zone.zone0) // 设置区域，指定不同区域的上传域名、备用域名、备用IP。默认 Zone.zone0
//        .build();
//	//重用 uploadManager。一般地，只需要创建一个 uploadManager 对象
//		return config;
//	
//	}

}
