package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.dt.app.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PhotoAdapter extends Common2Adapter<String> {
	private int gridViewWidth;
	public PhotoAdapter(Context context, List<String> mDatas,int gridViewWidth) {
		super(context, mDatas);
		this.mDatas = mDatas;
		this.gridViewWidth = gridViewWidth/3;
//		if (mDatas.size()==1) {
//			this.gridViewWidth = gridViewWidth*2/3;
//		}
//		if (mDatas.size()==2) {
//			this.gridViewWidth = gridViewWidth/2;
//		}
//		if (mDatas.size()>=3) {
//			this.gridViewWidth = gridViewWidth/3;
//		}
		
	}

	private List<String> mDatas;

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		try {
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.found_release_dynamic_item, null);
				convertView.setLayoutParams(new AbsListView.LayoutParams(gridViewWidth,gridViewWidth));
				ViewUtils.inject(holder, convertView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			holder.iv_close.setVisibility(View.GONE);
			String ulrStr = mDatas.get(position);
			 

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.iv_image)
		private ImageView iv_image;
		@ViewInject(R.id.iv_close)
		private ImageView iv_close;

	}
}
