package com.dt.app.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.JifenGood.Goods;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.ui.points.PointGiftActivity;
import com.dt.app.utils.DensityUtil;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PointExchangeAdapter extends Common2Adapter<Goods> {
    private List<Goods> mBeans;
    private int space;
    public PointExchangeAdapter(Context context, List<Goods> mDatas) {
        super(context, mDatas);
        this.mBeans = mDatas;
        space = DensityUtil.dip2px(mContext, 3);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.dt_point_exchange_item,
                    null);
            ViewUtils.inject(holder, convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            final Goods goods = mBeans.get(position);
            final ArrayList<Picture> pictures = (ArrayList<Picture>) goods.getPictures();
            if(pictures!=null && pictures.size()>0){
            	Picture picture = pictures.get(0);
            	int w = picture.getW()==null?0:picture.getW();
            	int h = picture.getH()==null?0:picture.getH();
            	RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width+space,430);
                holder.iv_artgallery_icon.setLayoutParams(params);

                mBitmapUtils.display(holder.iv_artgallery_icon, picture.getThumbUrl()+"?imageMogr2/thumbnail/"+width);
            }else {
            	 RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width+space, width);
                 holder.iv_artgallery_icon.setLayoutParams(params);

            	mBitmapUtils.display(holder.iv_artgallery_icon, goods.getGoodsThumb()+"?imageMogr2/thumbnail/"+width);
			}
            
            holder.tv_exchange_title.setText(goods.getName());
            holder.tv_exchange_point.setText("" + goods.getJifen());
           
            holder.iv_artgallery_icon.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                	ArrayList<String> urlsArrayList = new ArrayList<String>();
                	if (pictures!=null) {
						for (Picture picture : pictures) {
							urlsArrayList.add(picture.getThumbUrl());
						}
					}
                    Intent mIntent = new Intent(mContext, PointGiftActivity.class);
                    mIntent.putExtra("goodsId", goods.getId());
                    mIntent.putExtra("url", goods.getGoodsThumb());
                    mIntent.putExtra("pictures", urlsArrayList);
                    mContext.startActivity(mIntent);
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }

    public final class ViewHolder {
        @ViewInject(R.id.iv_artgallery_icon)
        private ImageView iv_artgallery_icon;
        @ViewInject(R.id.tv_exchange_title)
        private TextView tv_exchange_title;
        @ViewInject(R.id.tv_exchange_point)
        private TextView tv_exchange_point;
    }
}
