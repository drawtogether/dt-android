package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.dt.app.listener.BitmapUtilsConfig;
import com.dt.app.utils.ZTDeviceInfo;
import com.lidroid.xutils.BitmapUtils;

public class Common2Adapter<T> extends BaseAdapter {
	protected LayoutInflater mInflater;
	protected Context mContext;
	protected List<T> mDatas;
	 
	protected Integer width;
	protected Integer height;
	protected BitmapUtils mBitmapUtils;
	public Common2Adapter(Context context, List<T> mDatas) {
		this(context, mDatas, false);
	}
	public Common2Adapter(Context context, List<T> mDatas,boolean alpha) {
		this.mContext = context;
		this.mInflater = LayoutInflater.from(mContext);
		this.mDatas = mDatas;
		width = ZTDeviceInfo.getInstance().getWidthDevice();
		height = ZTDeviceInfo.getInstance().getHeightDevice();
		mBitmapUtils = new BitmapUtils(mContext);
		if (alpha) {
			mBitmapUtils.configDefaultDisplayConfig(BitmapUtilsConfig.getBitmapDisplayConfig());
		}
	}
	public Common2Adapter(Context context, List<T> mDatas,int drawable) {
		this.mContext = context;
		this.mInflater = LayoutInflater.from(mContext);
		this.mDatas = mDatas;
		width = ZTDeviceInfo.getInstance().getWidthDevice();
		height = ZTDeviceInfo.getInstance().getHeightDevice();
	}
	@Override
	public int getCount() {
		return mDatas.size();
	}

	@Override
	public T getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		return null;

	}

}