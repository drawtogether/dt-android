package com.dt.app.adapter;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dt.app.DTApplication;
import com.dt.app.bean.DTInitData;
import com.dt.app.fragment.DTFragment;

public class DTFragmentAdapter extends FragmentPagerAdapter {
    private List<DTInitData.DmData> logoDatas = DTApplication.getInstance().getDmDatas();


    public DTFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        //LogUtils.e("------logoDatas------->"+logoDatas.size());

        DTInitData.DmData  mlogo = logoDatas.get(position);

        return DTFragment.newInstance(mlogo.getImageUrl());
    }
    @Override
    public int getCount() {
        if(logoDatas != null && logoDatas.size()>0){
            return logoDatas.size();
        }else {
            return 0;
        }
    }

   /* @Override
    public CharSequence getPageTitle(int position) {
        return DTFragmentAdapter.CONTENT[position % CONTENT.length];
    }*/
    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            notifyDataSetChanged();
        }
    }
}