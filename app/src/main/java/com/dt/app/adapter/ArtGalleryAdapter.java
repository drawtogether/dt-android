package com.dt.app.adapter;

import java.util.List;

import com.lidroid.xutils.util.LogUtils;
import u.aly.co;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.threelogin.DTShare;
import com.dt.app.commonlayout.CommonFooterFour;
import com.dt.app.utils.ActivityUtils;
import com.dt.app.widget.PictureLayout;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ArtGalleryAdapter extends Common2Adapter<UserWorks.UserWork> {
    private List<UserWorks.UserWork> mBeans;
    private DTShare dtshare;

    public ArtGalleryAdapter(Context context, List<UserWorks.UserWork> mDatas) {
        super(context, mDatas);
        this.mBeans = mDatas;
        dtshare = new DTShare((Activity)context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.art_gallery_fragment_item,
                    null);
            holder = new ViewHolder(convertView);
            ViewUtils.inject(holder, convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        try {
            final UserWorks.UserWork collection = mBeans.get(position);
            final Member member = collection.getMember();
            if (member != null) {
                if (!TextUtils.isEmpty(member.getLogo())) {
                    mBitmapUtils.display(holder.iv_artgallery_user_icon,
                            member.getLogo());
                }
                holder.tv_artgallery_user_name.setText(member.getNickname());

//                holder.tv_artgallery_position.setText(ActivityUtils.getItemAddress(member.getProvinceId()));
                holder.iv_artgallery_user_icon.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonAcitivty.startArtistSpaceActivity(mContext, member.getId());
                    }
                });
            }
            if (!TextUtils.isEmpty(collection.getTitle())){
                holder.tv_artgallery_title.setText(collection.getTitle());
                holder.tv_artgallery_title.setVisibility(View.VISIBLE);
            }else {
                holder.tv_artgallery_title.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(collection.getContent())){
                holder.tv_artgallery_desc.setText(collection.getContent());
                holder.tv_artgallery_desc.setVisibility(View.VISIBLE);
            }else {
                holder.tv_artgallery_desc.setVisibility(View.GONE);
            }
            if (collection.getMe() != null) {
                holder.mCommonFooterFour.setMe(collection.getMe());
            }
            holder.mCommonFooterFour.setObject(collection,dtshare);
            holder.mCommonFooterFour.setWorksId(collection.getId(), member.getLogo(), new String[]{collection.getPictures().get(0).getImageUrl()},position);
            holder.mCommonFooterFour.tv_love_num.setText(collection.getLikedCount() + "人喜欢");
            int commentCount = collection.getCommentedCount();
            if (commentCount > 0) {
                holder.mCommonFooterFour.tv_artgallery_comment_count.setVisibility(View.VISIBLE);
                holder.mCommonFooterFour.tv_artgallery_comment_count.setText(commentCount + "");
            } else {
                holder.mCommonFooterFour.tv_artgallery_comment_count.setVisibility(View.GONE);
            }

            int type = collection.getType();//1.单幅方图，2.单幅圆图，3.系列作品

//			if (pictures!=null && pictures.size()>0) {
            holder.pictureLayout.removeAllViews();
            holder.pictureLayout.setOnClick(PictureLayout.ART, collection.getId(),collection.getMemberId(), member.getLogo());
         
            UserWorks.Picture  picture = collection.getPictures().get(0);
            String url = picture.getThumbUrl();
            String colorStr = picture.getPreColor();

            if (!TextUtils.isEmpty(colorStr)&&colorStr.contains("#")){
                colorStr = colorStr.substring(1,colorStr.length());
            }
            colorStr = colorStr == null ? "#FFFFFF" : "#"+colorStr;
            //imageMogr2/thumbnail/1200
            LogUtils.e("---------------->url = "+url);
            if (!TextUtils.isEmpty(url) && url.contains("?")){
                url += "&&imageMogr2/thumbnail/"+width;
            }else {
                url += "?imageMogr2/thumbnail/"+width;
            }
            if (type == 1) {
                holder.pictureLayout.initData(false, url, mBitmapUtils, picture.getW(), picture.getH());
                holder.pictureLayout_bg.setBackgroundColor(Color.parseColor(colorStr));
//                holder.pictureLayout.setBackgroundColor(Color.RED);
            } else if (type == 2) {
                holder.pictureLayout.initData(true, url, mBitmapUtils, picture.getW(), picture.getH());
                holder.pictureLayout_bg.setBackgroundColor(Color.parseColor(colorStr));
            } else if (type == 3) {
//					List<String> urlsList = new ArrayList<String>();
//					for (Picture picture : pictures) {
//						urlsList.add(picture.getImageUrl());
//					}
//					holder.pictureLayout.initData(false,urlsList, mBitmapUtils);
//					holder.pictureLayout.setBackgroundColor(Color.parseColor(colorStr));
            }
//			}

            //
            if (collection.getIsSigned()==1) {
            	holder.ll_artist_sign.setVisibility(View.VISIBLE);
			}else {
				holder.ll_artist_sign.setVisibility(View.GONE);
			}
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public final class ViewHolder {
        @ViewInject(R.id.iv_artgallery_user_icon)
        private ImageView iv_artgallery_user_icon;
        @ViewInject(R.id.tv_artgallery_user_name)
        private TextView tv_artgallery_user_name;
//        @ViewInject(R.id.tv_artgallery_position)
//        private TextView tv_artgallery_position;
        @ViewInject(R.id.tv_artgallery_title)
        private TextView tv_artgallery_title;
        @ViewInject(R.id.tv_artgallery_desc)
        private TextView tv_artgallery_desc;
        @ViewInject(R.id.ll_artist_sign)
        private LinearLayout ll_artist_sign;
        
        @ViewInject(R.id.pictureLayout_bg)
        private LinearLayout pictureLayout_bg;
        @ViewInject(R.id.pictureLayout)
        private PictureLayout pictureLayout;

        public ViewHolder(View view) {
            mCommonFooterFour = new CommonFooterFour(mContext, view);

        }

        private CommonFooterFour mCommonFooterFour;


    }
}
