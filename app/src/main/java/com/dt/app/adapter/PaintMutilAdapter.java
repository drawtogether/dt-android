package com.dt.app.adapter;

import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dt.app.R;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.utils.DensityUtil;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PaintMutilAdapter extends Common2Adapter<UserWork> {
	private Activity mContext;
	private HashMap<Integer, View> lmap = new HashMap<Integer, View>();
	private int space;
	private boolean isMutilLayout=true;
	public PaintMutilAdapter(Activity context, List<UserWork> wishBeans,boolean isMutilLayout) {
		super(context, wishBeans,false);
		mContext = context;
		this.isMutilLayout = isMutilLayout;
		space = DensityUtil.dip2px(mContext, 5);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (lmap.get(position) == null) {
			viewHolder = new ViewHolder();
			convertView = View.inflate(mContext, R.layout.image, null);
			ViewUtils.inject(viewHolder, convertView);
			convertView.setTag(viewHolder);
			lmap.put(position, convertView);
		} else {
			convertView = lmap.get(position);
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final UserWork mWishBean = mDatas.get(position);

		List<Picture> pictures = mWishBean.getPictures();
		if (pictures != null && pictures.size() > 0) {
			LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) viewHolder.iv_img_icon
					.getLayoutParams();
			if (isMutilLayout) {
				layoutParams.width = (width-space*2) / 2;
			}else {
				layoutParams.width = width;
			}
			layoutParams.bottomMargin = space;
			layoutParams.height = (layoutParams.width * pictures.get(0).getH())
					/ pictures.get(0).getW();
			if (layoutParams.height < layoutParams.width) {
				if (layoutParams.height < layoutParams.width / 2) {
					layoutParams.height = layoutParams.width / 2;
				} else {
					layoutParams.height = layoutParams.width;
				}
			}
//			if (isMutilLayout){
//				viewHolder.iv_img_icon.setPadding(space, 0, space, 0);
//			}else {
//				viewHolder.iv_img_icon.setPadding(0, space, 0, space);
//			}

			viewHolder.iv_img_icon.setLayoutParams(layoutParams);
			Picture  picture = pictures.get(0);
			String colorStr = picture.getPreColor();
			colorStr = colorStr == null ? "#FFFFFF" : "#"+colorStr;
			colorStr = colorStr.toUpperCase();
			viewHolder.iv_img_icon.setBackgroundColor(Color.parseColor(colorStr));
			mBitmapUtils.display(viewHolder.iv_img_icon, pictures.get(0).getThumbUrl());
			
			viewHolder.iv_img_icon.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					CommonAcitivty.startArtGalleryDetailActivity(mContext, mWishBean.getId(),mWishBean.getMemberId(),true);
				}
			});
		}
		return convertView;

	}

	static class ViewHolder {
		@ViewInject(R.id.iv_img_icon)
		public ImageView iv_img_icon;
	}

}