package com.dt.app.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.UserAttention;
import com.dt.app.bean.UserWorks.Me;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.RequestApi;
import com.dt.app.common.ResultLinstener;
import com.dt.app.utils.Constant;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ToastUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ArtistAttentionAdapter extends Common2Adapter<UserAttention.MemberAttention> {
	private int itemSize;
	private int leftMargin;
	private Me me;

	public void setMe(Me me) {
		this.me = me;
	}

	public ArtistAttentionAdapter(Context context, List<UserAttention.MemberAttention> mDatas) {
		super(context, mDatas);
		itemSize = width/3;
		leftMargin = DensityUtil.dip2px(mContext, 10);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.dt_artist_attention_item,null);
			holder = new ViewHolder();
			ViewUtils.inject(holder, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		try {
			final UserAttention.MemberAttention attention = mDatas.get(position);
			mBitmapUtils.display(holder.iv_artgallery_user_icon, attention.getLogo());
			holder.tv_artgallery_user_name.setText(attention.getNickname());
//			holder.tv_artgallery_position.setText(attention.getProvinceId()+" ");
			List<UserAttention.MemberWorks> works = attention.getWorkses();
			if (works != null && works.size() > 0) {
				int count = works.size();
				if (count > 10) {
					count = 10;
				}
				int tempCount = count;
				if (holder.tv_artgallery_user_name.getTag()==null || !holder.tv_artgallery_user_name.getTag().equals(works.get(0).getThumbUrl())) {
					holder.tv_artgallery_user_name.setTag(works.get(0).getThumbUrl());
					holder.ll_attention.removeAllViews();
					for (int i = 0; i < tempCount; i++) {
						createImageView(works.get(i),holder.ll_attention);
					}
				}
				
			}
			holder.iv_artgallery_user_icon.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					CommonAcitivty.startArtistSpaceActivity(mContext, Long.valueOf(attention.getId())); 
				}
			});
			 
			if (me!=null && me.getIsSelf()==1) {
				holder.iv_artgallery_user_attention.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							Activity activity = (Activity) mContext;
							while (activity.getParent()!=null) {
								activity = activity.getParent();
							}
							String message = null;
							if (attention.isAttention()) {
								message = "是否取消关注艺术家？";
							}else {
								message = "是否关注艺术家？";
							}
							ToastUtils.showDialog(activity, message, new OnClickListener() {
								@Override
								public void onClick(View v) {
									switch (v.getId()) {
									case R.id.ll_image_confirm:
										ToastUtils.dismissDialog();
										if (attention.isAttention()) {
											//执行取消关注
											loadData(attention.getId(), false, holder,position);
										}else {
											loadData(attention.getId(), true, holder,position);
										}
										break;

									case R.id.ll_image_cancel:
										ToastUtils.dismissDialog();
										break;
									}
								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}else {
				holder.iv_artgallery_user_attention.setVisibility(View.GONE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.iv_artgallery_user_icon)
		private ImageView iv_artgallery_user_icon;
		@ViewInject(R.id.iv_artgallery_user_attention)
		private ImageView iv_artgallery_user_attention;
		@ViewInject(R.id.tv_artgallery_user_name)
		private TextView tv_artgallery_user_name;
//		@ViewInject(R.id.tv_artgallery_position)
//		private TextView tv_artgallery_position;
		@ViewInject(R.id.ll_attention)
		private LinearLayout ll_attention;
 
	}
	
	private void createImageView(final UserAttention.MemberWorks works,LinearLayout ll_attention){
		ImageView imageView = new ImageView(mContext);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(itemSize,itemSize);
		layoutParams.leftMargin = leftMargin;
		imageView.setLayoutParams(layoutParams);
		imageView.setScaleType(ScaleType.CENTER_CROP);
		mBitmapUtils.display(imageView, works.getThumbUrl()+"?imageMogr2/thumbnail/"+width);
		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CommonAcitivty.startArtGalleryDetailActivity(mContext, works.getId(),works.getMemberId()); 
			}
		});
		ll_attention.addView(imageView);
	}
	
	/**
	 * 
	 * @param memberId
	 * @param isFollow true 关注，false 取消关注
	 */
	public void loadData(long memberId,final boolean isFollow,final ViewHolder holder,final int position){
		try {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("memberId", memberId);
			String urlStr = null;
			if (isFollow) {
				urlStr = Constant.URL.DTSnsFolloweAdd;
			}else {
				urlStr = Constant.URL.DTSnsFolloweCancel;
			}
			RequestApi.postCommon((Activity)mContext, urlStr, data,
					new ResultLinstener<String>() {
				@Override
				public void onSuccess(String obj) {
					if (isFollow) {
						holder.iv_artgallery_user_attention.setImageResource(R.mipmap.like_h_red);
						mDatas.get(position).setAttention(true);
						ToastUtils.showTextToast(mContext, "关注成功");
					}else {
//						holder.iv_artgallery_user_attention.setImageResource(R.mipmap.like_m_gray);
						mDatas.remove(position);
						notifyDataSetChanged();
						ToastUtils.showTextToast(mContext, "取消关注成功");
					}
				}
				
				@Override
				public void onFailure(String obj) {
				}
				
				@Override
				public void onException(String exception) {
				}
			}, new String());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
