package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.dt.app.listener.BitmapUtilsConfig;
import com.lidroid.xutils.BitmapUtils;

public abstract class CommonAdapter<T> extends BaseAdapter {
	protected LayoutInflater mInflater;
	protected Context mContext;
	protected List<T> mDatas;
	protected final int mItemLayoutId;
	protected BitmapUtils mBitmapUtils;

	public CommonAdapter(Context context, List<T> mDatas, int itemLayoutId) {
		this(context,mDatas,itemLayoutId,true);
	}
 
	public CommonAdapter(Context context, List<T> mDatas, int itemLayoutId,boolean alpha) {
		this.mContext = context;
		this.mInflater = LayoutInflater.from(mContext);
		this.mDatas = mDatas;
		this.mItemLayoutId = itemLayoutId;
		mBitmapUtils = new BitmapUtils(mContext);
		if (alpha) {
			mBitmapUtils.configDefaultDisplayConfig(BitmapUtilsConfig.getBitmapDisplayConfig());
		}
	}
	

	@Override
	public int getCount() {
		return mDatas.size();
	}

	@Override
	public T getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder = getViewHolder(position, convertView,
				parent);
		convert(viewHolder, getItem(position));
		convert(viewHolder, getItem(position),position);
		return viewHolder.getConvertView();

	}

	/**
	 * public void convert(ViewHolder helper, String item) {
	 * helper.setText(R.id.id_tv_title,item); }
	 * 
	 * @param helper
	 * @param item
	 */
	public abstract void convert(ViewHolder helper, T item);
	protected void convert(ViewHolder helper, T item,int pos){
		
	}

	private ViewHolder getViewHolder(int position, View convertView,
			ViewGroup parent) {
		return ViewHolder.get(mContext, convertView, parent, mItemLayoutId,
				position);
	}
	 
}