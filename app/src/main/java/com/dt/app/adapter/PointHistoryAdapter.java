package com.dt.app.adapter;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.PointOrder.Orders;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
/**
 * 换礼记录
 * @author libin
 *
 */
public class PointHistoryAdapter extends Common2Adapter<Orders> {

	private HashMap<Integer, View> lmap = new HashMap<Integer, View>();

	public PointHistoryAdapter(Context context, List<Orders> wishBeans) {
		super(context, wishBeans);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (lmap.get(position) == null) {
			viewHolder = new ViewHolder();
			convertView = View.inflate(mContext,R.layout.dt_point_history_frag_item, null);
			ViewUtils.inject(viewHolder, convertView);
			convertView.setTag(viewHolder);
			lmap.put(position, convertView);
		} else {
			convertView = lmap.get(position);
			viewHolder = (ViewHolder) convertView.getTag();
		}
		try {
			Orders order = mDatas.get(position);
			mBitmapUtils.display(viewHolder.iv_history_icon, order.getGoodsThumb());
			viewHolder.tv_point_history_title.setText(order.getGoodsName());
			viewHolder.tv_point_history_count.setText("兑换数量："+order.getAmount());
			viewHolder.tv_point_history_usepoint.setText("使用积分："+order.getJifenSum());
			String date = order.getCreateTime().split(" ")[0];
			viewHolder.tv_point_history_date.setText("兑换日期："+date);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;

	}

	static class ViewHolder {
		@ViewInject(R.id.iv_history_icon)
		private ImageView iv_history_icon;
		@ViewInject(R.id.tv_point_history_title)
		private TextView tv_point_history_title;
		@ViewInject(R.id.tv_point_history_count)
		private TextView tv_point_history_count;
		@ViewInject(R.id.tv_point_history_usepoint)
		private TextView tv_point_history_usepoint;
		@ViewInject(R.id.tv_point_history_date)
		private TextView tv_point_history_date;
	}

}