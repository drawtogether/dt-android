package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.dt.app.R;
import com.dt.app.bean.CommonBean;
import com.dt.app.utils.DensityUtil;
import com.dt.app.utils.ZTDeviceInfo;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
/**
 * 选择图片和视频
 * @author libin
 *
 */
public class ReleaseDynamicAdapter extends Common2Adapter<CommonBean> {
	 
	private int itemWidth;
	private BitmapUtils mBitmapUtils;
	public ReleaseDynamicAdapter(Context context, List<CommonBean> mDatas) {
		super(context, mDatas);
		itemWidth = (ZTDeviceInfo.getInstance().getWidthDevice()-DensityUtil.dip2px(mContext, 35))/4;
		mBitmapUtils = new BitmapUtils(mContext);
		  
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.found_release_dynamic_item, null);
			ViewUtils.inject(holder, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		final CommonBean bean = mDatas.get(position);
		
		if (convertView!=null) {
			convertView.setLayoutParams(new AbsListView.LayoutParams(itemWidth,itemWidth));
		}
		
		if (bean.close) {
			holder.iv_close.setVisibility(View.VISIBLE);
			holder.iv_close.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int seek=-1;
					 for (int i = 0; i < mDatas.size(); i++) {
						if (mDatas.get(i).name.equals(bean.name)) {
							seek = i;
							break;
						}                  
					}
					if(seek>=0  && seek < mDatas.size()){
						mDatas.remove(seek);
						mDatas.add(new CommonBean(-1, "drawable://" + R.drawable.dt_app_icon, false, true));
						notifyDataSetChanged();
					}
				}
			});
		}else {
			holder.iv_close.setVisibility(View.GONE);
		}
 
		 
		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.iv_image)
		public ImageView iv_image;
		@ViewInject(R.id.iv_close)
		public ImageView iv_close;
		
	}
	
}
