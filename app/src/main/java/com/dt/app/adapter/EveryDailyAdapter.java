package com.dt.app.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.DTUser.MemberBean;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.common.threelogin.DTShare;
import com.dt.app.commonlayout.CommonFooterFour;
import com.dt.app.widget.PictureLayout;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class EveryDailyAdapter extends Common2Adapter<UserWork> {
    private List<UserWork> mBeans;
    private MemberBean memberBean;

    private long themeId = -1;
    /**
     * 是否是每日作品
     */
    boolean isDaily=false;
	private DTShare dtshare;
    public void setMemberBean(MemberBean memberBean) {
        this.memberBean = memberBean;
    }

    public EveryDailyAdapter(Context context, List<UserWork> mDatas) {
        super(context, mDatas);
        this.mBeans = mDatas;
        dtshare = new DTShare((Activity)context);
    }

    public EveryDailyAdapter(Context context, List<UserWork> mDatas, boolean isDaily) {
        super(context, mDatas);
        this.mBeans = mDatas;
        this.isDaily = isDaily;
        dtshare = new DTShare((Activity)context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.art_gallery_fragment_item,
                    null);
            holder = new ViewHolder(convertView);
            ViewUtils.inject(holder, convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            final UserWork userWork = mBeans.get(position);
            final Member member = userWork.getMember();
            List<Picture> pictures = userWork.getPictures();
            int count = pictures.size();
            String[] urlSt = new String[count];
            if (pictures != null && count > 0) {
                for (int i = 0; i < count; i++) {
                    urlSt[i] = pictures.get(i).getImageUrl();
                }
            }
            String logoStr = null;
            if (member != null) {
                holder.rl_artgallery_user_layout.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(member.getLogo())) {
                    mBitmapUtils.display(holder.iv_artgallery_user_icon, member.getLogo());
                }
                holder.iv_artgallery_user_icon.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonAcitivty.startArtistSpaceActivity(mContext, member.getId());
                    }
                });
                holder.tv_artgallery_user_name.setText(member.getNickname());
//                holder.tv_artgallery_position.setText("" + member.getProvinceId() + " " + member.getCityId());
                logoStr = member.getLogo();
            } else {
                if (memberBean != null) {
                    logoStr = memberBean.getLogo();
                }
                holder.rl_artgallery_user_layout.setVisibility(View.GONE);

            }
            if (!TextUtils.isEmpty(userWork.getTitle())){
                holder.tv_artgallery_title.setText(userWork.getTitle());
                holder.tv_artgallery_title.setVisibility(View.VISIBLE);
            }else {
                holder.tv_artgallery_title.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(userWork.getContent())){
                holder.tv_artgallery_desc.setText(userWork.getContent());
                holder.tv_artgallery_desc.setVisibility(View.VISIBLE);
            }else {
                holder.tv_artgallery_desc.setVisibility(View.GONE);
            }
            if (userWork.getMe() != null) {
                holder.mCommonFooterFour.setMe(userWork.getMe());
            }
            if (themeId > 0) {
                holder.mCommonFooterFour.setThemeId(themeId);
            }
            holder.mCommonFooterFour.setObject(userWork,dtshare);
            holder.mCommonFooterFour.setWorksId(userWork.getId(), logoStr, urlSt,position);
            holder.mCommonFooterFour.tv_love_num.setText(userWork.getLikedCount() + "人喜欢");
            int commentCount = userWork.getCommentedCount();
            if (commentCount > 0) {
                holder.mCommonFooterFour.tv_artgallery_comment_count.setVisibility(View.VISIBLE);
                holder.mCommonFooterFour.tv_artgallery_comment_count.setText(commentCount + "");
            } else {
                holder.mCommonFooterFour.tv_artgallery_comment_count.setVisibility(View.GONE);
            }

            int type = userWork.getType();//1.单幅方图，2.单幅圆图，3.系列作品

            if (pictures != null && pictures.size() > 0) {
                holder.pictureLayout.removeAllViews();
                int clickType = PictureLayout.DAILY;
//                if (isDaily) {
//                	clickType = PictureLayout.ART;
//				}
                holder.pictureLayout.setOnClick(clickType, userWork.getId(), userWork.getMemberId(),logoStr);
                Picture picture_item = pictures.get(0);
                String colorStr = picture_item.getPreColor();
                colorStr = colorStr == null ? "#FFFFFF" : "#"+colorStr;
                if (isDaily) {
                	holder.pictureLayout.setCollectionState(picture_item.getIsSigned());
				}
                if (type == 1) {
                    String url = pictures.get(0).getThumbUrl()+"?imageMogr2/thumbnail/"+width;

                    holder.pictureLayout.initData(false, url, mBitmapUtils,picture_item.getW(),picture_item.getH());
                    holder.pictureLayout_bg.setBackgroundColor(Color.parseColor(colorStr));
                } else if (type == 2) {
                    String url = pictures.get(0).getThumbUrl()+"?imageMogr2/thumbnail/"+width;
                    holder.pictureLayout.initData(true, url, mBitmapUtils,picture_item.getW(),picture_item.getH());
                    holder.pictureLayout_bg.setBackgroundColor(Color.parseColor("#FFFFFF"));
                } else if (type == 3) {
                    List<String> urlsList = new ArrayList<String>();
                    for (Picture picture : pictures) {
                        urlsList.add(picture.getThumbUrl()+"?imageMogr2/thumbnail/"+width);
                    }
                    holder.pictureLayout.initData(false, urlsList, mBitmapUtils,picture_item.getW(),picture_item.getH());
                    holder.pictureLayout_bg.setBackgroundColor(Color.parseColor(colorStr));
                }
            }
            if (isDaily) {
            	if (userWork.getIsSigned()==1) {
                	holder.ll_artist_sign.setVisibility(View.VISIBLE);
    			}else {
    				holder.ll_artist_sign.setVisibility(View.GONE);
    			}
			}
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public long getThemeId() {
        return themeId;
    }

    public void setThemeId(long themeId) {
        this.themeId = themeId;
    }

    public final class ViewHolder {
        @ViewInject(R.id.iv_artgallery_user_icon)
        private ImageView iv_artgallery_user_icon;
        @ViewInject(R.id.tv_artgallery_user_name)
        private TextView tv_artgallery_user_name;
//        @ViewInject(R.id.tv_artgallery_position)
//        private TextView tv_artgallery_position;
        @ViewInject(R.id.tv_artgallery_title)
        private TextView tv_artgallery_title;
        @ViewInject(R.id.tv_artgallery_desc)
        private TextView tv_artgallery_desc;
        @ViewInject(R.id.rl_artgallery_user_layout)
        private RelativeLayout rl_artgallery_user_layout;
        @ViewInject(R.id.ll_artist_sign)
        private LinearLayout ll_artist_sign;
        
        @ViewInject(R.id.pictureLayout_bg)
        private LinearLayout pictureLayout_bg;
        @ViewInject(R.id.pictureLayout)
        private PictureLayout pictureLayout;

        public ViewHolder(View view) {
            mCommonFooterFour = new CommonFooterFour(mContext, view);
            //ViewUtils.inject(mCommonFooterFour, view);
        }

        private CommonFooterFour mCommonFooterFour;


    }
}
