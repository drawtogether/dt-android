package com.dt.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.Tag;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.List;

public class TextAdapter extends BaseAdapter {
	private List<Tag> mBeans;
    private Context  mctx;
	public TextAdapter(Context context, List<Tag> mDatas) {
		this.mBeans = mDatas;
		mctx = context;
	}

	@Override
	public int getCount() {
		return mBeans.size();
	}

	@Override
	public Object getItem(int i) {
		return mBeans.get(i);
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(mctx).inflate(R.layout.dt_text_item,null);
			ViewUtils.inject(holder, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.search_item_tv.setText(mBeans.get(position).getName()+"");

		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.search_item_tv)
		public TextView search_item_tv;

	}
}
