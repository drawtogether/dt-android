package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.DTNotice;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class NoticeListAdapter extends Common2Adapter<DTNotice.NoticeBean> {
	private List<DTNotice.NoticeBean> mBeans;
	private LayoutInflater  inflate;
	private Context  mcontext;
	public NoticeListAdapter(Context context, List<DTNotice.NoticeBean> mDatas) {
		super(context, mDatas);
		this.mBeans = mDatas;
		inflate = LayoutInflater.from(context);
		mcontext = context;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflate.inflate(R.layout.dt_menu_msglist_item, null);
			holder = new ViewHolder();
			ViewUtils.inject(holder,convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final DTNotice.NoticeBean noticeBean = mBeans.get(position);
		holder.dt_notice_name.setText(noticeBean.getFromName() + "");

		mBitmapUtils.display(holder.dt_notice_user_photo, noticeBean.getFromLogo());
		if (noticeBean.getContentType() != null && noticeBean.getContentType().equals("works")){
			mBitmapUtils.display(holder.dt_notice_img,noticeBean.getContentLogo());
			holder.dt_notice_img.setVisibility(View.VISIBLE);
		}else {
			holder.dt_notice_img.setVisibility(View.GONE);
		}

		holder.dt_notice_img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				CommonAcitivty.startArtGalleryDetailActivity(mcontext, noticeBean.getContentId(), noticeBean.getMemberId(),true);
			}
		});
		holder.dt_notice_time.setText(noticeBean.getOccurTime() + "");
		holder.dt_notice_address.setText(noticeBean.getLocation()+"");
		holder.dt_notice_content.setText(""+noticeBean.getMessage());
		holder.dt_notice_user_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				CommonAcitivty.startArtistSpaceActivity(mContext, noticeBean.getMemberId());
			}
		});

		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.dt_notice_user_photo)
		private CircleImageView dt_notice_user_photo;
		@ViewInject(R.id.dt_notice_name)
		private TextView dt_notice_name;
		@ViewInject(R.id.dt_notice_address)
		private TextView dt_notice_address;
		@ViewInject(R.id.dt_notice_time)
		private TextView dt_notice_time;
		@ViewInject(R.id.dt_notice_content)
		private TextView dt_notice_content;
		@ViewInject(R.id.dt_notice_img)
		private ImageView dt_notice_img;

	}
}
