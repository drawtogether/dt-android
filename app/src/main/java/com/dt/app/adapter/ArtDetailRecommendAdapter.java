package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.bean.UserWorks.Picture;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.utils.DensityUtil;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 艺术详情页-每日推荐
 * @author libin
 *
 */
public class ArtDetailRecommendAdapter extends Common2Adapter<UserWork> {
	private List<UserWork> mBeans;
	private int itemWidth;
	public ArtDetailRecommendAdapter(Context context, List<UserWork> mDatas) {
		super(context, mDatas);
		this.mBeans = mDatas;
		itemWidth = (width-DensityUtil.dip2px(mContext, 10))/2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.dt_art_gallery_detail_item,
					null);
			ViewUtils.inject(holder, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		try {
			final UserWork userWork = mBeans.get(position);
			if (userWork!=null) {
				
				holder.tv_artgallery_title.setText(userWork.getTitle());
				holder.tv_artgallery_love.setText(userWork.getLikedCount()+" 喜欢");
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(itemWidth,itemWidth);
				holder.iv_artgallery_icon.setLayoutParams(params);
				final Member member = userWork.getMember();
				if (member!=null) {
					mBitmapUtils.display(holder.iv_artgallery_user_icon,member.getLogo());
					holder.iv_artgallery_user_icon.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							CommonAcitivty.startArtistSpaceActivity(mContext, member.getId());
						}
					});
				}
				holder.iv_artgallery_icon.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						CommonAcitivty.startArtGalleryDetailActivity(mContext, userWork.getId(),userWork.getMemberId(),true);
					}
				});
				List<Picture> pictures = userWork.getPictures();
				if (pictures!=null && pictures.size()>0) {
					mBitmapUtils.display(holder.iv_artgallery_icon, pictures.get(0).getThumbUrl());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.iv_artgallery_icon)
		private ImageView iv_artgallery_icon;
		@ViewInject(R.id.tv_artgallery_title)
		private TextView tv_artgallery_title;
		@ViewInject(R.id.tv_artgallery_love)
		private TextView tv_artgallery_love;
		@ViewInject(R.id.iv_artgallery_user_icon)
		private CircleImageView iv_artgallery_user_icon;



	}
}
