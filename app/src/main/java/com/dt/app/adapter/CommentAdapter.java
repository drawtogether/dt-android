package com.dt.app.adapter;

import java.text.SimpleDateFormat;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.CommentList.Comment;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
/**
 * 评论
 * @author libin
 *
 */
public class CommentAdapter extends Common2Adapter<Comment> {
	private List<Comment> mBeans;
	//am-上午pm-下午
	 
	public CommentAdapter(Context context, List<Comment> mDatas) {
		super(context, mDatas);
		this.mBeans = mDatas;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.dt_comment_item,
					null);
			ViewUtils.inject(holder, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final Comment comment = mBeans.get(position);
		mBitmapUtils.display(holder.iv_comment_user_icon, comment.getMemberLogo());
		holder.tv_comment_user_name.setText(comment.getMemberNickname());
		holder.tv_comment.setText(comment.getContent());
//		Drawable drawable = mContext.getResources().getDrawable(R.mipmap.clock_comment);
//        int dwidth = DensityUtil.dip2px(mContext, 15);
//        int dheight = DensityUtil.dip2px(mContext, 15);
//        drawable.setBounds(0, 0, dwidth, dheight);//第一0是距左边距离，第二0是距上边距离，40分别是长宽
//        holder.tv_comment_time.setCompoundDrawables(drawable, null, null, null);//
//		holder.tv_comment_time.setText(formatTime(comment.getCreateTime()));
		holder.iv_comment_user_icon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				CommonAcitivty.startArtistSpaceActivity(mContext, comment.getMemberId());
			}
		});
		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.iv_comment_user_icon)
		private CircleImageView iv_comment_user_icon;
		@ViewInject(R.id.tv_comment_user_name)
		private TextView tv_comment_user_name;
		@ViewInject(R.id.tv_comment)
		private TextView tv_comment;
		@ViewInject(R.id.tv_comment_time)
		private TextView tv_comment_time;
 
		 

	}
	
//	private String formatTime(String time){
//		try {
//			String stri = sdformat.format(sdf.parse(time.trim()));
//			if (stri.contains("上午")) {
//				stri = stri.replace("上午", "AM");
//			}else if (stri.contains("下午")) {
//				stri = stri.replace("下午", "PM");
//			}
//			return stri.trim();
//		}catch(Exception e){
//			
//		}
//		return time;
//	}
}
