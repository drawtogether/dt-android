package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.UserWorks.Member;
import com.dt.app.common.CommonAcitivty;
import com.dt.app.view.CircleImageView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ImageTextAdapter extends Common2Adapter<Member> {
	private List<Member> mBeans;
	private int drawable;

	public ImageTextAdapter(Context context, List<Member> mDatas,int drawable) {
		super(context, mDatas,false);
		this.mBeans = mDatas;
		this.drawable = drawable;
	}
	public ImageTextAdapter(Context context, List<Member> mDatas) {
		super(context, mDatas,false);
		this.mBeans = mDatas;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.artist_all_fragment_item,
					null);
			if (drawable>0) {
				convertView.setBackgroundDrawable(mContext.getResources().getDrawable(drawable));
			}
			ViewUtils.inject(holder, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		try {
			final Member member = mBeans.get(position);
			mBitmapUtils.display(holder.iv_artist_all, member.getLogo());
			holder.tv_artist_name.setVisibility(View.VISIBLE);
			if (drawable>0) {
				holder.tv_artist_name.setTextColor(Color.WHITE);
			}
			holder.tv_artist_name.setText(formatName(member.getNickname()));
			holder.iv_artist_all.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					CommonAcitivty.startArtistSpaceActivity(mContext, member.getId());
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.iv_artist_all)
		public CircleImageView iv_artist_all;
		@ViewInject(R.id.tv_artist_name)
		public TextView tv_artist_name;

	}
	
	private String formatName(String name){
		String tempName = "";
		if (!TextUtils.isEmpty(name)) {
			if (name.length()<=6) {
				tempName = name;
			}else {
				tempName = name.substring(0, 6);
			}
		}
		return tempName;
	}
}
