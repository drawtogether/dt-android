package com.dt.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.bean.Address;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class AddressManageAdapter extends Common2Adapter<Address> {
	private List<Address> mBeans;
	private boolean isDefault;
	public AddressManageAdapter(Context context, List<Address> mDatas,boolean isDefault) {
		super(context, mDatas);
		this.mBeans = mDatas;
		this.isDefault = isDefault;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			
			convertView = mInflater.inflate(R.layout.dt_address_manage_item,
					null);
			holder = new ViewHolder();
			ViewUtils.inject(holder, convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final Address address = mDatas.get(position);
		holder.tv_address_user.setText(address.getConsignee());
		holder.tv_address_userphone.setText(address.getMobilephone());
		holder.tv_address.setText(address.getAddress());
		
		 
		if (isDefault) {
			if (address.getIsDefault()==1) {//是否默认0否，1是
				holder.iv_address_flag.setImageResource(R.drawable.address_select);
				holder.tv_address.setTextColor(mContext.getResources().getColor(R.color.black));
				holder.tv_address_userphone.setTextColor(mContext.getResources().getColor(R.color.black));
				holder.tv_address_user.setTextColor(mContext.getResources().getColor(R.color.black));
			}else{
				holder.iv_address_flag.setImageResource(R.drawable.address_un_select);
				holder.tv_address.setTextColor(mContext.getResources().getColor(R.color.gray_set_account_text));
				holder.tv_address_userphone.setTextColor(mContext.getResources().getColor(R.color.gray_set_account_text));
				holder.tv_address_user.setTextColor(mContext.getResources().getColor(R.color.gray_set_account_text));
			}
		}else {
			holder.iv_address_flag.setImageResource(R.drawable.address_un_select);
		}
		
		
		
		return convertView;
	}

	public final class ViewHolder {
		@ViewInject(R.id.tv_address_user)
		private TextView tv_address_user;
		@ViewInject(R.id.tv_address_userphone)
		private TextView tv_address_userphone;
		@ViewInject(R.id.tv_address)
		private TextView tv_address;
		@ViewInject(R.id.iv_address_flag)
		public ImageView iv_address_flag;
		
		 

	}
}
