package com.dt.app.commonlayout;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.dt.app.R;
import com.dt.app.adapter.ArtistWorkAdapter;
import com.dt.app.bean.UserWorks;
import com.dt.app.bean.UserWorks.Me;
import com.dt.app.bean.UserWorks.UserWork;
import com.dt.app.common.CommonApis;
import com.dt.app.common.threelogin.DTShare;
import com.dt.app.listener.HandleCallbackSimple;
import com.dt.app.ui.art.ArtistSpaceActivity;
import com.dt.app.ui.art.ArtistWorkEditActivity;
import com.dt.app.ui.works.CommentActivity;
import com.dt.app.ui.works.RewardActivity;
import com.dt.app.ui.works.WorkLovesActivity;
import com.dt.app.utils.Constant;
import com.dt.app.utils.PreferencesUtils;
import com.dt.app.utils.ToastUtils;
import com.lidroid.xutils.util.LogUtils;

public class CommonFooterFour implements OnClickListener {
    public ImageView iv_artgallery_heart;
    public FrameLayout fl_artgallery_comment;
    public FrameLayout fl_artgallery_comment2;
    public ImageView iv_artgallery_comment;
    public ImageView iv_artgallery_reward;
    public ImageView iv_artgallery_share;
    public ImageView iv_artgallery_delete;
    public TextView tv_love_num;
    public TextView tv_artgallery_comment_count;

    private Context context;
    private DTShare dtshare;
    private long themeId = -1;

    private boolean isArtistSpace;
    private BaseAdapter baseAdapter;

    public CommonFooterFour(Context context, View view) {
        this(context, view, false, null);
    }

    public CommonFooterFour(Context context, View view, boolean isArtistSpace, BaseAdapter baseAdapter) {
        this.context = context;
        this.isArtistSpace = isArtistSpace;
        this.baseAdapter = baseAdapter;
        iv_artgallery_heart = (ImageView) view.findViewById(R.id.iv_artgallery_heart);
        fl_artgallery_comment = (FrameLayout) view.findViewById(R.id.fl_artgallery_comment);
        fl_artgallery_comment2 = (FrameLayout) view.findViewById(R.id.fl_artgallery_comment2);
        iv_artgallery_comment = (ImageView) view.findViewById(R.id.iv_artgallery_comment);
        iv_artgallery_reward = (ImageView) view.findViewById(R.id.iv_artgallery_reward);
        iv_artgallery_share = (ImageView) view.findViewById(R.id.iv_artgallery_share);
        iv_artgallery_delete = (ImageView) view.findViewById(R.id.iv_artgallery_delete);
        tv_love_num = (TextView) view.findViewById(R.id.tv_love_num);
        tv_artgallery_comment_count = (TextView) view.findViewById(R.id.tv_artgallery_comment_count);

        if (isArtistSpace) {
            fl_artgallery_comment2.setVisibility(View.GONE);
            iv_artgallery_delete.setVisibility(View.VISIBLE);
            iv_artgallery_heart.setImageResource(R.drawable.work_edit_img);
            LogUtils.i("aritst space ... ");
        }

        dtshare = new DTShare((Activity) context);
        iv_artgallery_comment.setOnClickListener(this);
        iv_artgallery_reward.setOnClickListener(this);
        iv_artgallery_share.setOnClickListener(this);
        iv_artgallery_heart.setOnClickListener(this);
        iv_artgallery_delete.setOnClickListener(this);
        tv_love_num.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.tv_love_num:
                    Intent mIntent = new Intent(context, WorkLovesActivity.class);
                    mIntent.putExtra("worksId", workid);
                    context.startActivity(mIntent);
                    break;
                case R.id.iv_artgallery_comment:
                    Intent commentIntent = new Intent(context, CommentActivity.class);
                    commentIntent.putExtra("worksId", workid);
                    try {
                        if (object instanceof UserWorks.UserWork) {//艺术画廊
                            UserWorks.UserWork new_name = (UserWorks.UserWork) object;
                            commentIntent.putExtra("memberId", new_name.getMemberId());
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    commentIntent.putExtra("workUserIcon", workUserIcon);
                    if (themeId > 0) {
                        commentIntent.putExtra("themeId", themeId);
                    }
                    context.startActivity(commentIntent);
                    break;
                case R.id.iv_artgallery_reward:
//				if (me ==null || me.getIsDonated()==Integer.valueOf(1) ||imageUrl!=null) {
                    if (me == null || imageUrl == null) {
                        return;
                    }
                    //CommonAcitivty.startPhotoViewActivity(context,workUserIcon, imageUrl, workid,1);
//				CommonAcitivty.startPhotoViewActivity(context, imageUrl, workid,me.getIsLiked());

                    Intent intent = new Intent(context, RewardActivity.class);
                    intent.putExtra("workId", workid);
                    intent.putExtra("logo", workUserIcon);
                    intent.putExtra("rewardUrl", imageUrl[0]);
                    context.startActivity(new Intent(intent));
                    break;
                case R.id.iv_artgallery_share:
                    //ToastUtils.showTextToast(context, "分享");
                    try {
                        if (object instanceof UserWorks.UserWork) {//艺术画廊
                            UserWorks.UserWork new_name = (UserWorks.UserWork) object;

                            LogUtils.e("-----target---->" + imageUrl[0]);
                            String targetUrl = Constant.URL.DTShare + "?worksId=" + new_name.getId();
                            dtshare.showShareUI(context.getString(R.string.dt_share_arttitle), new_name.getTitle() + ",来自DT用户"
                                            + PreferencesUtils.getString(context, Constant.PrefrencesPt.DTnickname),
                                    targetUrl, imageUrl[0], "0");
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                    }


                    break;
                case R.id.iv_artgallery_heart:
                    if (isArtistSpace) {
                        Intent editIntent = new Intent(context, ArtistWorkEditActivity.class);
                        UserWork userWork = (UserWork) object;
                        editIntent.putExtra("title", userWork.getTitle());
                        editIntent.putExtra("desc", userWork.getContent());
                        editIntent.putExtra("workid", workid);
                        editIntent.putExtra("position", position);
                        ((Activity) context).startActivityForResult(editIntent, ArtistSpaceActivity.EDIT_WORK_REQ);
                    } else {
                        LogUtils.i("------------> 加心 " + me);
                        if (me == null) {
                            return;
                        }
                        if (me.getIsLiked() == Integer.valueOf(1)) {
                            likeSubmit(false);
                        } else {
                            likeSubmit(true);
                        }
                    }
                    break;
                case R.id.iv_artgallery_delete:
                    if (isArtistSpace) {
                        ToastUtils.showDialog(context, "是否删除本作品?", new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                switch (v.getId()) {
                                    case R.id.ll_image_cancel:
                                        ToastUtils.dismissDialog();
                                        break;
                                    case R.id.ll_image_confirm:
                                        ToastUtils.dismissDialog();
                                        ((ArtistWorkAdapter) baseAdapter).delete(workid, position);
                                        break;
                                }
                            }
                        });

                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Long workid;
    private String workUserIcon;
    private String[] imageUrl;
    private int position;

    public void setWorksId(Long workid, String workUserIcon, String[] url, int position) {
        this.workid = workid;
        this.workUserIcon = workUserIcon;
        imageUrl = url;
        this.position = position;
    }

    private Me me;

    public void setMe(Me me) {
        if (me != null) {
            System.out.println("me: " + me.getIsSelf());
            this.me = me;
//            if (me.getIsDonated() == Integer.valueOf(1)) {
//                iv_artgallery_reward
//                        .setImageResource(R.mipmap.reward_shortcut_black);
//            } else {
//                iv_artgallery_reward
//                        .setImageResource(R.mipmap.reward_shortcut_gray);
//            }
            if (!isArtistSpace) {
                if (me.getIsLiked() == Integer.valueOf(1)) {
                    iv_artgallery_heart.setImageResource(R.mipmap.reward_select);
                } else {
                    iv_artgallery_heart.setImageResource(R.mipmap.reward_normal);
                }
            } else {
                if (isArtistSpace) {
                    if (me.getIsSelf() == null || me.getIsSelf() != Integer.valueOf(1)) {
                        isSelf(false);
                    }
                }
            }
        } else {
            if (isArtistSpace) {
                isSelf(false);
            }
        }

    }


    private void isSelf(boolean issleft) {
        iv_artgallery_delete.setEnabled(issleft);
        iv_artgallery_heart.setEnabled(issleft);
    }

    private void likeSubmit(final boolean like) {
        try {
            CommonApis.likeSubmit(context, like, workid, new HandleCallbackSimple<String>() {
                @Override
                public void onSuccess(String t) {
                    try {
                        String numText = tv_love_num.getText().toString().trim();
                        int num = Integer.valueOf(numText.split("人")[0]);
//                		collection.getLikedCount() + "人喜欢"
                        if (like) {
                            num = num + 1;
                        } else {
                            num = num - 1;
                        }
                        tv_love_num.setText("" + num + "人喜欢");
                        if (object != null) {
                            if (object instanceof UserWorks.UserWork) {//艺术画廊
                                UserWorks.UserWork new_name = (UserWorks.UserWork) object;
                                new_name.setLikedCount(num);
                            }
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                    if (like) {//jia心
                        me.setIsLiked(1);
                        iv_artgallery_heart.setImageResource(R.mipmap.reward_select);
                    } else {
                        me.setIsLiked(0);
                        iv_artgallery_heart.setImageResource(R.mipmap.reward_normal);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public long getThemeId() {
        return themeId;
    }

    public void setThemeId(long themeId) {
        this.themeId = themeId;
    }

    private Object object;

    public void setObject(Object obj, DTShare dtShare) {
        this.object = obj;
        this.dtshare = dtShare;
    }


}
